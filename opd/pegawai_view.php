<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Pegawai</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 text-right">
							<button onclick="location.href = '../opd/pegawai_tambah.php'" class="btn btn-primary">Tambah Data</button>
						</div>
					</div>
				</br>
				<div class="row">
					<div class="col-lg-12">				
						<div id="tabel_pegawai" class="table-responsive">
							<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
								<thead>
									<tr>
										<th width="30px">No</th>
										<th style="width:30px;">NIP</th>
										<th>Nama Pegawai</th>
										<th>Pangkat</th>
										<th width="50px">Golongan</th>
										<th>Jabatan</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama Pegawai</th>
										<th>Pangkat</th>
										<th>Golongan</th>
										<th>Jabatan</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">				
				<h4 class="modal-title">Anda yakin untuk menghapus data ini ?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var KodePegawai;
	var KodeOPD;
	var dataTable1;

	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		LoadData();  
	});

	// function LoadData(){
	// 	var action = "LoadData";
	// 	$.ajax({
	// 		url: "pegawai_aksi.php",
	// 		method: "POST",
	// 		data: {action: action, KodeOPD: KodeOPD},
	// 		dataType: 'json',
	// 		success: function (data) {
	// 			if(data.response == 200){
	// 				$('#tabel_pegawai').html(data.Tabel);
	// 				$('#tabeldata').DataTable();	
	// 			}else{
	// 				alert('request failed');
	// 			}				
	// 		}
	// 	});
	// }

	function LoadData(){		
		var action = "LoadData";
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-right'
			}],
			"ajax":{
				url :"pegawai_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action ,
					d.KodeOPD = KodeOPD;
				},
				error: function(){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="7">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");

				}
			}
		} );						
	}

	$(document).on('click', '#btnDelete', function () {
		KodePegawai = $(this).val();
		//KodeOPD = $(this).attr("data-value");
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btHapus', function () {
		var action = "HapusData";
		$.ajax({
			url: "pegawai_aksi.php",
			method: "POST",
			data: {KodePegawai: KodePegawai, KodeOPD: KodeOPD, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					dataTable1.ajax.reload();
					$('#ModalHapus').modal("toggle");
				}else{
					alert('request failed');
				}
			}
		});
	});

	$(document).on('click', '#btnUpdate', function () {
		var json = $(this).val();
		window.location = "naik_jabatan_view.php?"+json;
	});

	$(document).on('click', '#btnAktivitas', function () {
		var KodePegawai = $(this).val();
		window.location = "aktivitas_pegawai_view.php?KodePegawai="+KodePegawai;
	});

	$(document).on('click', '#btnHistory', function () {
		var KodePegawai = $(this).val();
		window.location = "pegawai_history.php?KodePegawai="+KodePegawai;
	});
</script>
