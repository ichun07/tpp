<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Verifikasi Telat dan Pulang Awal</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div id="sukses"></div>
					<div class="row">
						<div class="col-md-12 text-right">
							<button id="btnRekap" class="btn btn-primary">Rekap Absensi Bulanan</button>
						</div>
					</br></br>
					<div class="col-md-2 text-left">
						Bulan/Tahun
					</div>
					<div class="col-md-5 text-left">
						<?php 
						date_default_timezone_set('Asia/Jakarta');
						$year = date('Y');
						$month = date('m');
						?>
						<select id="cb_bulan" name="cb_bulan" class="form-control">				
							<option value="1"<?php if ((int)$month == 1) echo ' selected="selected"'; ?>>Januari</option>
							<option value="2"<?php if ((int)$month == 2) echo ' selected="selected"'; ?>>Februari</option>
							<option value="3"<?php if ((int)$month == 3) echo ' selected="selected"'; ?>>Maret</option>
							<option value="4"<?php if ((int)$month == 4) echo ' selected="selected"'; ?>>April</option>
							<option value="5"<?php if ((int)$month == 5) echo ' selected="selected"'; ?>>Mei</option>
							<option value="6"<?php if ((int)$month == 6) echo ' selected="selected"'; ?>>Juni</option>
							<option value="7"<?php if ((int)$month == 7) echo ' selected="selected"'; ?>>Juli</option>
							<option value="8"<?php if ((int)$month == 8) echo ' selected="selected"'; ?>>Agustus</option>
							<option value="9"<?php if ((int)$month == 9) echo ' selected="selected"'; ?>>September</option>
							<option value="10"<?php if ((int)$month == 10) echo ' selected="selected"'; ?>>Oktober</option>
							<option value="11"<?php if ((int)$month == 11) echo ' selected="selected"'; ?>>November</option>
							<option value="12"<?php if ((int)$month == 12) echo ' selected="selected"'; ?>>Desember</option>
						</select>
					</div>
					<div class="col-md-5 text-left">
						<select id="cb_tahun" name="cb_tahun" class="form-control">
							<option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
							<?php
							for($i = $year-1; $i > $year-10; $i--){
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
							?>
						</select>
					</div>
				</br></br></br>
				<div class="col-lg-12">
					<div id="message"></div>					
					<div class="table-responsive">
						<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
							<thead>
								<tr>
									<th width="30px" class="text-center">No.</th>
									<th width="80px">Tanggal</th>
									<th>Nama Pegawai</th>
									<th>Jabatan</th>
									<th width="30px" class="text-center">Tidak Apel</th>
									<th width="30px" class="text-center">Tidak Senam</th>
									<th width="30px" class="text-center">Terlambat</th>
									<th width="30px" class="text-center">Pulang Awal</th>
									<th width="50px">Dokumen</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
</div>
<div id="ModalVerifikasi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Dokumen Verifikasi</h4>
			</div>
			<div class="modal-body">
				<div class="form-group" style="padding-bottom: 20px;">
					<form id="uploadimage" action="" method="post" enctype="multipart/form-data" >
						<div width="100%" class="text-center">
							<div id="image_preview">

							</div>
						</div>						
						<hr id="line">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="file" name="file" aria-describedby="file" accept="image/x-png,image/gif,image/jpeg">
							<label class="custom-file-label" id="txtNamaFile" for="file" style="display: inline-block; overflow: hidden">
								Choose file
							</label>
						</div>
					</form>
				</div>
			</div>				
			<div class="modal-footer">
				<button class="btn btn-info" id="btnUpload">
					Simpan
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">
					Batal
				</button>
			</div>
		</div>
	</div>
</div>
</div>

<div id="ModalHKE" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Hari Kerja Efektif</h4>
			</div>
			<div class="modal-body">
				<div class="form-group" style="padding-bottom: 20px;">
					<label id="txtTextJmlHari" name="txtTextJmlHari">Jumlah Hari Efektif</label>
					<input type="number" name="txtJmlHari" id="txtJmlHari" class="form-control" required data-msg="Jumlah hari tidak boleh kosong!"/>
				</div>				
				<div class="modal-footer">
					<button class="btn btn-info" id="btnSimpanRekap">
						Simpan
					</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">
						Batal
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once 'footer.php';
?>
<script>
	var KodeOPD,Bulan,Tahun,Pelanggaran;
	var Tanggal,KodePegawai;
	var dataTable1;
	var action;


	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		Tahun = $("[name='cb_tahun']").val();
		Bulan = $("[name='cb_bulan']").val();
		console.log("Bulan : "+Bulan+" Tahun : "+Tahun+" KodeOPD : "+KodeOPD);
		action = "LoadData";
		LoadData();
	} );

	$('#cb_tahun').change(function () {
		Tahun = $(this).val();
		console.log("Bulan : "+Bulan+" Tahun : "+Tahun+" KodeOPD : "+KodeOPD);
		dataTable1.ajax.reload();
	});

	$('#cb_bulan').change(function () {
		Bulan = $(this).val();
		console.log("Bulan : "+Bulan+" Tahun : "+Tahun+" KodeOPD : "+KodeOPD);
		dataTable1.ajax.reload();
	});

	function LoadData(){
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-center'
			},
			{
				targets: 5,
				className: 'text-center'
			},
			{
				targets: 6,
				className: 'text-center'
			},
			{
				targets: 7,
				className: 'text-center'
			},
			{
				targets: 8,
				className: 'text-center'
			}],
			"ajax":{
				url :"verifikasi_kedatangan_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action ,
					d.KodeOPD = KodeOPD,
					d.Bulan = Bulan,
					d.Tahun = Tahun;
				},
				error: function(){
					$(".lookup-error").html("");
					// $("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");

				}
			}
		} );
	}
	
	$(document).on('click', '#btnDokumen', function () {
		document.getElementById("txtNamaFile").innerHTML = 'Choose file';
		KodePegawai = $(this).val();
		Tanggal = $(this).attr("data-value");
		var DokumenPendukung = $(this).attr("data-value2");
		if(DokumenPendukung > 0){
			var action = "AmbilFoto";
			$.ajax({
				url: "verifikasi_absen_aksi.php",
				method: "POST",
				data: {action: action, KodePegawai: KodePegawai, Tanggal: Tanggal},
				dataType: 'json',
				success: function (data) {
					if(data != 500){
						$('#image_preview').html(data);		
					}else{
						var noimage = '<img id="previewing" src="noimage.png" />';
						$('#image_preview').html(noimage);
					}			
					$('#previewing').attr('width', '250px');
					$('#previewing').attr('height', '230px');				
				}
			});
		}else{
			var noimage = '<img id="previewing" src="noimage.png" />';
			$('#image_preview').html(noimage);
		}

		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');	

		$('#ModalVerifikasi').modal('show');
	});

	$(document).on('click', '#btnUpload', function () {		
		var file_data = $("#file").prop("files")[0];
		var formData = new FormData();
		formData.append("file", file_data);
		formData.append("Tanggal", Tanggal);
		formData.append("KodePegawai", KodePegawai);
		formData.append("action", "UploadDokumen");
		$.ajax({
			url: "verifikasi_absen_aksi.php",
			type: "POST",
			data: formData,
			contentType: false,
			cache: false,
			processData:false,
			dataType: 'json',
			success: function(data){
				var error_message = data.error_response;
				console.log("Error : "+error_message);
				if(data.response == 200){
					swal("Berhasil","Berhasil unggah dokumen","success");
					dataTable1.ajax.reload(null,false);
				}else if(data.response == 404){
					swal("Error",""+error_message,"error");
				}else{
					swal("Error",""+error_message,"error");
				}
				$('#ModalVerifikasi').modal('toggle');
				var noimage = '<img id="previewing" src="noimage.png" />';
				$('#image_preview').html(noimage);
				document.getElementById("file").value = "";
				document.getElementById("txtNamaFile").innerHTML = 'Choose file';
			}
		});
	});

	$(document).on('click', '#btnRekap', function () {
		document.getElementById("txtTextJmlHari").innerHTML = 'Jumlah HKE Bulan ke-'+Bulan+' Tahun '+Tahun;
		$('#ModalHKE').modal('show');	
	});

	$(document).on('click', '#btnSimpanRekap', function () {
		var action = "RekapAbsen";
		Tahun = $("[name='cb_tahun']").val();
		Bulan = $("[name='cb_bulan']").val();
		var JmlHariEfektif = $("[name='txtJmlHari']").val();
		if(JmlHariEfektif == '' || JmlHariEfektif < 1){
			swal("Peringatan","Masukkan jumlah hari efektif!","warning");
		}else{
			$.ajax({
				url: "tpp_bulanan_aksi.php",
				method: "POST",
				data: {action: action, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun, JmlHariEfektif:JmlHariEfektif},
				dataType: 'json',
				success: function (data) {
					if(data.response === 200){
						$("[name='txtJmlHari']").val("");
						$('#ModalHKE').modal('toggle');
						location.href='absensi_pegawai_view.php';
					}else{
						alert("Gagal Rekap Absensi");
					}				
				}
			});
		}			
	});

	//proses gambar

	// Function to preview image after validation
	$(function() {
		$("#file").change(function() {
			var file = this.files[0];
			var imagefile = file.type;
			var match= ["image/jpeg","image/png","image/jpg"];
			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
				$('#previewing').attr('src','noimage.png');
				$("#sukses").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
				return false;
			}
			else
			{
				var reader = new FileReader();
				reader.onload = imageIsLoaded;
				reader.readAsDataURL(this.files[0]);
			}
		});
	});
	function imageIsLoaded(e) {
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
	};

	$('input[type="file"]').change(function(e){
		var fileName = e.target.files[0].name;
		$('.custom-file-label').html(fileName);
	});
</script>

<style type="text/css">
h1
{
	text-align: center;
	background-color: #FEFFED;
	height: 70px;
	color: rgb(95, 89, 89);
	margin: 0 0 -29px 0;
	padding-top: 14px;
	border-radius: 10px 10px 0 0;
	font-size: 35px;
}
.main {
	position: absolute;
	top: 50px;
	left: 20%;
	width: 450px;
	height:530px;
	border: 2px solid gray;
	border-radius: 10px;
}
.main label{
	color: rgba(0, 0, 0, 0.71);
	margin-left: 60px;
}
#image_preview{
	position: absolute;
	font-size: 30px;
	left: 100px;
	top: 30px;
	width: 300px;
	height: 300px;
	text-align: center;
	line-height: 180px;
	font-weight: bold;
	color: #C0C0C0;
	background-color: #FFFFFF;
	overflow: auto;
}
.submit{
	font-size: 16px;
	background: linear-gradient(#ffbc00 5%, #ffdd7f 100%);
	border: 1px solid #e5a900;
	color: #4E4D4B;
	font-weight: bold;
	cursor: pointer;
	width: 300px;
	border-radius: 5px;
	padding: 10px 0;
	outline: none;
	margin-top: 20px;
	margin-left: 15%;
}
.submit:hover{
	background: linear-gradient(#ffdd7f 5%, #ffbc00 100%);
}
#file {
	color: red;
	padding: 5px;
	margin-top: 10px;
}
#message{
	position:absolute;
	top:120px;
	left:815px;
}
#success
{
	color:green;
}
#invalid
{
	color:red;
}
#line
{
	margin-top: 274px;
}
#error
{
	color:red;
}
#error_message
{
	color:blue;
}
#loading
{
	display:none;
	position:absolute;
	top:50px;
	left:850px;
	font-size:25px;
}
</style>