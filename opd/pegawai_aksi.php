<?php 
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadDataKab") {
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetDataPegawai($conn, $KodeOPD);
		echo json_encode($result);
	}	

	if($_POST["action"] == "LoadData"){
		$requestData = $_REQUEST;
		$KodeOPD = $_POST['KodeOPD'];
		$result = DataPegawaiOPD($conn, $requestData, $KodeOPD);
		echo json_encode($result);
	}

	if ($_POST["action"] == "InsertData") {
		$NIP = $_POST['NIP'];
		$Status = $_POST['Status'];
		$NamaPegawai = $_POST['NamaPegawai'];
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$Username = $NIP;
		$PokokTPP = $_POST['PokokTPP'];
		$NoAkun = $_POST['NoAkun'];
		$Password = base64_encode($NIP);
		$result = InsertDataPegawai($conn, $PokokTPP, $Status, $NIP, $NamaPegawai, $Pangkat, $Golongan, $KodeJabatan, $KodeOPD, $Username, $Password, $NoAkun);
		echo json_encode($result);
	}
	if ($_POST["action"] == "AmbilData") {
		$KodePegawai = $_POST["KodePegawai"];
		$KodeOPD = $_POST["KodeOPD"];
		$result = GetOneDataPegawai($conn, $KodePegawai, $KodeOPD);
		echo json_encode($result);
	}
	if ($_POST["action"] == "UpdateData") {
		$KodePegawai = $_POST['KodePegawai'];
		$NIP = $_POST['NIP'];
		$NamaPegawai = $_POST['NamaPegawai'];
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$Username = $_POST['Username'];
		$Password = base64_encode($_POST['Password']);
		$PokokTPP = $_POST['PokokTPP'];
		$UbahJabatan = $_POST['UbahJabatan'];
		$Status = $_POST['Status'];
		$result = UpdateDataPegawai($conn, $KodePegawai, $NIP, $NamaPegawai, $Pangkat, $Golongan, $KodeJabatan, $KodeOPD, $Username, $Password, $PokokTPP, $UbahJabatan, $Status);
		echo json_encode($result);
	}
	if ($_POST["action"] == "HapusData") {
		$KodePegawai = $_POST["KodePegawai"];
		$KodeOPD = $_POST["KodeOPD"];
		$result = DeleteDataPegawai($conn, $KodePegawai, $KodeOPD);
		echo json_encode($result);
	}

	if ($_POST["action"] == "GetJabatan") {
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetDataJabatan($conn, $KodeOPD);
		echo json_encode($result);
	}

	if($_POST['action'] == 'ReplacePegawai'){
		$NIP = $_POST['NIP'];
		$Status = $_POST['Status'];
		$NamaPegawai = $_POST['NamaPegawai'];
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$Username = $NIP;
		$PokokTPP = $_POST['PokokTPP'];
		$NoAkun =$_POST['NoAkun'];
		$Password = base64_encode($NIP);
		$result = ReplaceJabatanPegawai($conn, $PokokTPP, $Status, $NIP, $NamaPegawai, $Pangkat, $Golongan, $KodeJabatan, $KodeOPD, $Username, $Password, $NoAkun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'HistoryJabatan'){
		$KodePegawai = $_POST['KodePegawai'];
		$result = GetHistoryJabatan($conn, $KodePegawai);
		echo json_encode($result);
	}
}

function InsertDataPegawai($conn, $PokokTPP, $Status, $NIP, $NamaPegawai, $Pangkat, $Golongan, $KodeJabatan, $KodeOPD, $Username, $Password, $NoAkun){
	$sql = "SELECT COUNT(KodePegawai) AS Jumlah FROM mstpegawai WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$result = mysqli_fetch_array($res);
		if ($result['Jumlah'] > 0) {
			return array('response' => 500, 'error' => 'Jabatan sudah terisi!');
		} else {
			$KodePegawai = AutoKodePegawai($conn);
			$sql = "INSERT INTO mstpegawai (KodePegawai, NIP, NamaPegawai, Pangkat, Golongan, PokokTPP, KodeJabatan, KodeOPD, Username, Password, Status, NoAkun) VALUES ('$KodePegawai', '$NIP', '$NamaPegawai', '$Pangkat', '$Golongan', '$PokokTPP', '$KodeJabatan', '$KodeOPD', '$Username', '$Password', '$Status', '$NoAkun')";
			$res = $conn->query($sql);
			if($res){
				$KodeHistory = AutoKodeHistory($conn,$KodePegawai);
				date_default_timezone_set('Asia/Jakarta');
				$TanggalPengangkatan = date('Y-m-d');
				$sql = "INSERT INTO historyjabatan (KodePegawai, KodeHistory, TanggalPengangkatan, KodeJabatan, KodeOPD) VALUES ('$KodePegawai', '$KodeHistory', '$TanggalPengangkatan', '$KodeJabatan', '$KodeOPD')";
				$res = $conn->query($sql);
				if($res){
					session_start();
					InsertLog($conn, 'INSERT', 'Menambah Pegawai', $_SESSION['KodeUser']);
					return array('response' => 200);
				}else{
					return array('response' => 500);
				}		
			}else{
				return array('response' => 500);
			}
		}
	}else{
		return array('response' => 500);
	}	
}

function UpdateDataPegawai($conn, $KodePegawai, $NIP, $NamaPegawai, $Pangkat, $Golongan, $KodeJabatan, $KodeOPD, $Username, $Password, $PokokTPP, $UbahJabatan, $Status){
	$sql = '';
	$sql = "UPDATE mstpegawai SET NIP = '$NIP', NamaPegawai = '$NamaPegawai', Pangkat = '$Pangkat', Golongan = '$Golongan', PokokTPP = '$PokokTPP', KodeJabatan = '$KodeJabatan', Username = '$Username', Password = '$Password', Status = '$Status' WHERE KodePegawai = '$KodePegawai' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200 );
	}else{
		return array('response' => 500);
	}
}

function GetDataPegawai($conn, $KodeOPD){
	$arrayOutput = '';
	$output = '';
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password, j.KelasJabatan 
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
	WHERE p.KodeOPD = '$KodeOPD'
	GROUP BY p.KodePegawai
	ORDER BY p.KodePegawai ASC";
	$res = $conn->query($sql);
	if($res){
		$arrayOutput['response'] = 200;
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th width="30px">No</th>
		<th style="width:30px;">NIP</th>
		<th>Nama Pegawai</th>
		<th>Pangkat</th>
		<th width="50px">Golongan</th>
		<th>Jabatan</th>
		</tr>
		</thead>
		<tfoot>
		<tr>
		<th>No</th>
		<th>NIP</th>
		<th>Nama Pegawai</th>
		<th>Pangkat</th>
		<th>Golongan</th>
		<th>Jabatan</th>
		</tr>
		</tfoot>
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$url = "KodePegawai=".$row['KodePegawai']."&KodeOPD=".$row['KodeOPD'];
			$output.= '<tr>
			<td>'.$no++.'</td>
			<td>'.$row['NIP'].'</td>
			<td>'.$row['NamaPegawai'].'</td>
			<td>'.$row['Pangkat'].'</td>
			<td class="text-right">'.$row['Golongan'].'</td>
			<td>'.$row['NamaJabatan'].'</td>
			
			</tr>';
		}
		$output .='</tbody>
		</table>';
		$arrayOutput['Tabel'] = $output;
	}else{
		$arrayOutput['response'] = 500;
	}
	return $arrayOutput;							
}

function GetOneDataPegawai($conn, $KodePegawai, $KodeOPD){
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.NilaiJabatan, p.KodeOPD, p.Username, p.Password, p.Status 
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE p.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$row=mysqli_fetch_array($res);
		$Pass = base64_decode($row['Password']);
		$row['Password'] = $Pass;
		$row['HtmlJabatan'] = GetDataJabatanEdit($conn,$row['KodeJabatan'],$KodeOPD,$row['NamaJabatan'],$row['NilaiJabatan'],$row['HargaJabatan']);
		return array('response' => 200, 'Data' => $row);
	}else{
		return array('response' => 500);
	}
}

function GetDataOPDEdit($conn,$KodeOPD,$NamaOPD){
	$output = '';
	$sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Nama OPD</label>';
		$output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
		$output .= "<option value='".$KodeOPD."'>".$NamaOPD."</option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
		}
		$output .= '</select>';
	}
	return $output;
}

function GetDataJabatanEdit($conn,$KodeJabatan,$KodeOPD,$NamaJabatan,$NilaiJabatan,$HargaJabatan){
	$output = '';
	$sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD
	FROM mstjabatan j
	WHERE j.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Jabatan</label>';
		$output .= "<select class='form-control' id='cbJabatan' name='cbJabatan'>";
		$output .= "<option data-value='" . $NilaiJabatan . "' data-value2='" . $HargaJabatan . "' value='".$KodeJabatan."' selected>".$NamaJabatan."</option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option data-value='" . $row['NilaiJabatan'] . "' data-value2='" . $row['HargaJabatan'] . "' value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
		}
		$output .= '</select>';
	}
	return $output;
}

function DeleteDataPegawai($conn, $KodePegawai, $KodeOPD){
	$sql = "DELETE FROM mstpegawai WHERE KodePegawai = '$KodePegawai' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function GetDataJabatan($conn, $KodeOPD){
	$arrayOutput = '';
	$output = '';
	$output1 = '';
	$sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD, o.NamaOPD 
	FROM mstjabatan j
	LEFT JOIN mstopd o
	ON o.KodeOPD = j.KodeOPD
	WHERE j.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Jabatan</label>';
		$output .= "<select class='form-control' id='cbJabatan' name='cbJabatan'>";
		$output .= "<option data-value='0' data-value2='0' value='0'> - Pilih Jabatan - </option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option data-value='" . $row['NilaiJabatan'] . "' data-value2='" . $row['HargaJabatan'] . "' value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
		}
		$output .= '</select>';
		$arrayOutput['cbJabatan'] = $output;

		$sql = "SELECT * FROM mstpangkat ORDER BY Pangkat";
		$res = $conn->query($sql);
		if($res){			
			$arrayOutput['response'] = 200;
			$output1 .='<label class="form-control-label">Pangkat / Golongan</label>';
			$output1 .= "<select class='form-control' id='cbPangkat' name='cbPangkat'>";
			$output1 .= "<option data-value='0' data-value2='0' value='0'> - Pilih Pangkat - </option>";
			while ($row = $res->fetch_assoc()) {
				$output1 .= "<option data-value='" . $row['Golongan'] . "' value='" . $row['Pangkat'] . "'>" . $row['Pangkat'] . " / " . $row['Golongan'] . "</option>";
			}
			$output1 .= '</select>';
			$arrayOutput['cbPangkat'] = $output1;
			return $arrayOutput;
		}else{
			return array('response' => 500);
		}		
	}else{
		return array('response' => 500);
	}
}

function AutoKodePegawai($conn){
	$sql = "SELECT RIGHT(KodePegawai,16) AS kode FROM mstpegawai ORDER BY KodePegawai DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
	return 'PGW-' . $bikin_kode;
}

function AutoKodeHistory($conn,$KodePegawai){
	$sql = "SELECT RIGHT(KodeHistory,16) AS kode FROM historyjabatan WHERE KodePegawai = '$KodePegawai' ORDER BY KodeHistory DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
	return 'HTR-' . $bikin_kode;
}

function ReplaceJabatanPegawai($conn, $PokokTPP, $Status, $NIP, $NamaPegawai, $Pangkat, $Golongan, $KodeJabatan, $KodeOPD, $Username, $Password, $NoAkun){
	$sql = "UPDATE mstpegawai SET PokokTPP = '0', KodeJabatan = NULL WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$KodePegawai = AutoKodePegawai($conn);
		$sql = "INSERT INTO mstpegawai (KodePegawai, NIP, NamaPegawai, Pangkat, Golongan, PokokTPP, KodeJabatan, KodeOPD, Username, Password, Status, NoAkun) VALUES ('$KodePegawai', '$NIP', '$NamaPegawai', '$Pangkat', '$Golongan', '$PokokTPP', '$KodeJabatan', '$KodeOPD', '$Username', '$Password', '$Status', '$NoAkun')";
		$res = $conn->query($sql);
		if($res){
			$KodeHistory = AutoKodeHistory($conn,$KodePegawai);
			date_default_timezone_set('Asia/Jakarta');
			$TanggalPengangkatan = date('Y-m-d');
			$sql = "INSERT INTO historyjabatan (KodePegawai, KodeHistory, TanggalPengangkatan, KodeJabatan, KodeOPD) VALUES ('$KodePegawai', '$KodeHistory', '$TanggalPengangkatan', '$KodeJabatan', '$KodeOPD')";
			$res = $conn->query($sql);
			if($res){
				return array('response' => 200);
			}else{
				return array('response' => 500);
			}
		}else{
			return array('response' => 500);
		}
	}else{
		return array('response' => 500);
	}
}

function GetHistoryJabatan($conn, $KodePegawai){
	$arrayOutput = '';
	$output = '';
	$sql = "SELECT h.KodePegawai, h.KodeHistory, h.TanggalPengangkatan, h.NoSKPengangkatan, h.TanggalSampai, h.Keterangan, h.KodeJabatan, h.KodeOPD, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala
	FROM historyjabatan h
	LEFT JOIN mstjabatan j ON j.KodeJabatan = h.KodeJabatan AND j.KodeOPD = h.KodeOPD
	WHERE h.KodePegawai = '$KodePegawai'
	ORDER BY h.TanggalPengangkatan ASC";
	$res = $conn->query($sql);
	if($res){
		$arrayOutput['response'] = 200;
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th width="30px">No</th>
		<th>Kode Jabatan</th>
		<th>Jabatan</th>
		<th>Tanggal Pengangkatan</th>
		<th>Tanggal Sampai</th>
		<th>No. SK Pengangkatan</th>
		</tr>
		</thead>
		<tfoot>
		<tr>
		<th>No</th>
		<th>Kode Jabatan</th>
		<th>Jabatan</th>
		<th>Tanggal Pengangkatan</th>
		<th>Tanggal Sampai</th>
		<th>No. SK Pengangkatan</th>
		</tr>
		</tfoot>
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$output.= '<tr>
			<td>'.$no++.'</td>
			<td>'.$row['KodeManual'].'</td>
			<td>'.$row['NamaJabatan'].'</td>
			<td>'.$row['TanggalPengangkatan'].'</td>
			<td>'.$row['TanggalSampai'].'</td>
			<td>'.$row['NoSKPengangkatan'].'</td>
			</tr>';
		}
		$output .='</tbody>
		</table>';
		$arrayOutput['Tabel'] = $output;
	}else{
		$arrayOutput['response'] = 500;
	}
	return $arrayOutput;	
}

function DataPegawaiOPD($conn,$requestData,$KodeOPD){
	$columns = array( 
		0 => 'NamaPegawai',
		1 => 'NIP', 
		2 => 'NamaPegawai',
		3 => 'Pangkat',
		4 => 'Golongan',
		5 => 'NamaJabatan',
	);

	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password, j.KelasJabatan ";
	$sql.="FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
	WHERE p.KodeOPD = '$KodeOPD'
	GROUP BY p.KodePegawai
	ORDER BY p.KodePegawai ASC";
	$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
	$totalFiltered = $totalData;


	if( !empty($requestData['search']['value']) ) {
		$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password, j.KelasJabatan ";
		$sql.="FROM mstpegawai p
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD ";
		$sql.="WHERE p.KodeOPD = '$KodeOPD' AND p.NamaPegawai LIKE '".$requestData['search']['value']."%' ";
		$sql.=" OR p.KodeOPD = '$KodeOPD' AND p.NIP LIKE '".$requestData['search']['value']."%' ";
		$sql.=" OR p.KodeOPD = '$KodeOPD' AND j.NamaJabatan LIKE '".$requestData['search']['value']."%' ";
		$sql.=" OR p.KodeOPD = '$KodeOPD' AND p.Pangkat LIKE '".$requestData['search']['value']."%' ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
		$totalFiltered = mysqli_num_rows($query);
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");	
	} else {
		$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password, j.KelasJabatan ";
		$sql.="FROM mstpegawai p
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD 
		WHERE p.KodeOPD = '$KodeOPD' ";
		$sql.="ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	}

	$data = array();
	$no = 1;
	while( $row=mysqli_fetch_array($query) ) {
		$nestedData=array(); 		
		$url = "KodePegawai=".$row['KodePegawai']."&KodeOPD=".$row['KodeOPD'];
		$Aksi = '<button value = '.$url.' name="update" id="btnUpdate" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
		<button data-value="' . $row['KodeOPD'] . '" value="' . $row['KodePegawai'] . '" name="delete" id="btnDelete" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></button>
		<button value = '.$row['KodePegawai'].' name="aktivitas" id="btnAktivitas" class="btn btn-info"><span class="fa fa-arrow-right" aria-hidden="true"></span></button>
		<button value = '.$row['KodePegawai'].' name="btnHistory" id="btnHistory" class="btn btn-success"><span class="fa fa-line-chart" aria-hidden="true"></span></button>';
		$nestedData[] = $no++;
		$nestedData[] = $row['NIP'];
		$nestedData[] = $row['NamaPegawai'];
		$nestedData[] = $row['Pangkat'];
		$nestedData[] = $row['Golongan'];
		$nestedData[] = $row['NamaJabatan'];		
		$nestedData[] = $Aksi;		

		$data[] = $nestedData;

	}



	$json_data = array(
		"draw"            => intval( $requestData['draw'] ),
		"recordsTotal"    => intval( $totalData ),
		"recordsFiltered" => intval( $totalFiltered ),
		"data"            => $data   		);

	return $json_data;  
}
?>