<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';
ob_start();
$NamaBulan = array (1 =>   'Januari',
	'Februari',
	'Maret',
	'April',
	'Mei',
	'Juni',
	'Juli',
	'Agustus',
	'September',
	'Oktober',
	'November',
	'Desember'
);
$KodeOPD = $_GET['KodeOPD'];
$NamaOPD = base64_decode($_GET['NamaOPD']);
$Bulan = $_GET['Bulan'];
$Tahun = $_GET['Tahun'];
$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, p.PPH, j.KodeManual, j.NilaiJabatan, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IFNULL(SUM(atv.DurasiKerjaACC),0) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, s.PersenSKP, IFNULL(s.NilaiSKP, 0) AS NilaiSKP, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	LEFT JOIN skppegawai s ON s.KodePegawai = tb.KodePegawai AND s.Tahun = (tb.Tahun - 1)
	LEFT JOIN capaiankinerjaopd c ON c.KodeOPD = p.KodeOPD AND c.Tahun = tb.Tahun AND c.Triwulan = IF(3 >= tb.Bulan AND tb.Bulan >= 1, 1, IF(6 >= tb.Bulan AND tb.Bulan >= 4, 2, IF(9 >= tb.Bulan AND tb.Bulan >= 7, 3, IF(12 >= tb.Bulan and tb.Bulan >= 10, 4, TRUE))))
	WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun";
$res = $conn->query($sql);
$no = 1;
$JmlTppPersen = 0;
$JmlTppRp = 0;
$JmlPotongan = 0;
$JmlTppDiterima = 0;
?>
<page style="width:100%; padding: 10px;" backtop="7mm" backbottom="7mm" backleft="7mm" backright="7mm">
	<page_footer> 
	<label style="text-align: right;">[[page_cu]]/[[page_nb]]</label>
	</page_footer>
	<h2 style="width:100%; margin-top: 0px; margin-bottom: 0px; text-align: center;"><?php echo $NamaOPD; ?></h2>
	<h2 style="width:100%; margin-top: 6px; margin-bottom: 0px; text-align: center;">DAFTAR REKAPITULASI PEMBERIAN TPP BERBASIS KINERJA</h2>
	<h3 style="width:100%; margin-top: 6px; margin-bottom: 6px; text-align: center;"><?php echo "BULAN ".strtoupper($NamaBulan[(int)$Bulan])." TAHUN ".$Tahun; ?></h3>
	<table class="tbl" border="1" style="width:100%; margin-top: 10px; border-collapse: collapse; font-size: 9px;">
		<tr>
			<th style="width:3%; text-align: center; padding: 4px;">No</th>
			<th style="width:5%; text-align: center; padding: 4px;">Nama / NIP</th>
			<th style="width:1%; text-align: center; padding: 4px;">Jabatan</th>
			<th style="width:4%; text-align: center; padding: 4px;">Kelas<br>Jabatan</th>
			<th style="width:4%; text-align: center; padding: 4px;">Nilai<br>Jabatan</th>
			<th style="width:3%; text-align: center; padding: 4px;">TPP<br>Pokok</th>
			<th style="width:6%; text-align: center; padding: 4px;">Tingkat<br>Kedisiplinan<br>(%)</th>
			<th style="width:6%; text-align: center; padding: 4px;">TPP<br>Kedisiplinan<br>(30%)</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">Nilai<br>Aktivitas</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">TPP<br>Aktivitas<br>(30%)</th>
			<th style="width:3.5%; text-align: center; padding: 4px;">Nilai<br>SKP</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">TPP<br>SKP<br>(20%)</th>
			<th style="width:3.5%; text-align: center; padding: 4px;">Nilai<br>SAKIP</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">TPP<br>SAKIP<br>(20%)</th>
			<th style="width:3.5%; text-align: center; padding: 4px;">Jumlah<br>TPP<br>Bruto</th>
			<th style="width:3.5%; text-align: center; padding: 4px;">PPH<br>21</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">TPP<br>Diterima</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">TTD</th>
			<th style="width:4.5%; text-align: center; padding: 4px;">No.<br>Rek.</th>
		</tr>
		<tr>
			<th style="text-align: center; padding: 4px;">1</th>
			<th style="text-align: center; padding: 4px;">2</th>
			<th style="text-align: center; padding: 4px;">3</th>
			<th style="text-align: center; padding: 4px;">4</th>
			<th style="text-align: center; padding: 4px;">5</th>
			<th style="text-align: center; padding: 4px;">6</th>
			<th style="text-align: center; padding: 4px;">7</th>
			<th style="text-align: center; padding: 4px;">8</th>
			<th style="text-align: center; padding: 4px;">9</th>
			<th style="text-align: center; padding: 4px;">10</th>
			<th style="text-align: center; padding: 4px;">11</th>
			<th style="text-align: center; padding: 4px;">12</th>
			<th style="text-align: center; padding: 4px;">13</th>
			<th style="text-align: center; padding: 4px;">14</th>
			<th style="text-align: center; padding: 4px;">15</th>
			<th style="text-align: center; padding: 4px;">16</th>
			<th style="text-align: center; padding: 4px;">17</th>
			<th style="text-align: center; padding: 4px;">18</th>
			<th style="text-align: center; padding: 4px;">19</th>
		</tr>
		<?php
		while ($row = $res->fetch_assoc()) {
			# code...
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
			$ProsenKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $KodeOPD, $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);
			
			$NilaiJabatan = $row['NilaiJabatan'];
			$PokokTPP = $row['PokokTPPPegawai'];
			$PersenSKP = $row['PersenSKP'];
			$NilaiSKP = $row['NilaiSKP'];
			$NilaiSAKIP = $row['NilaiKategoriSAKIP'];

			$BebanAktivitas = $row['MenitAktivitas'];

			//jika pegawai mempunyai bawahan maka mendapat 1% dari beban tiap aktivitas bawahan
			$NilaiTambah = HitungNilaiAktivitasBawahan($conn, $KodeOPD, $row['KodeJabatan'], $Bulan, $Tahun);
			//cek apakah beban pegawai melebihi nilai jabatan
			//jika beban melebihi nilai jabatan maka yang dipakai adalah nilai jabatan 
			$JmlPoinAktivitas = ($BebanAktivitas + $NilaiTambah) > $row['NilaiJabatan'] ? $row['NilaiJabatan'] : ($BebanAktivitas + $NilaiTambah);
			//menghitung nilai aktivitas dengan membagi jml poin aktivitas dengan nilai jabatan
			$NilaiAktivitas = $JmlPoinAktivitas == 0 ? 0 : ($JmlPoinAktivitas / $NilaiJabatan);

			$TppDisiplin = Disiplin($ProsenKehadiran, $PokokTPP);
			$TppAktivitas = Aktivitas($JmlPoinAktivitas, $NilaiJabatan, $PokokTPP);
			$TppSKP = SKP($PersenSKP, $PokokTPP);
			$TppSakip = Sakip($NilaiSAKIP, $PokokTPP);

			$BrutoTPP = TPP($TppDisiplin, $TppAktivitas, $TppSKP, $TppSakip);
			$Pph21 = $row['PPH'] == 0 ? 0 : ($BrutoTPP * ($row['PPH'] / 100));
			$TppDiterima = $BrutoTPP - $Pph21;

			?>
			<tr>
				<td style="text-align: center; padding: 4px;"><?php echo $no++; ?></td>
				<td style="width: 8%; text-align: left; padding: 4px;"><?php echo $row['NamaPegawai']. '<br>'. $row['NIP']; ?></td>
				<td style="width: 10%; text-align: left; padding: 4px;"><?php echo ucwords(strtolower($row['NamaJabatan'])); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['KelasJabatan']; ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['NilaiJabatan']; ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($row['PokokTPPPegawai']); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($ProsenKehadiran,2); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($TppDisiplin); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($NilaiAktivitas,2); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($TppAktivitas); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($NilaiSKP,2); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($TppSKP); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $NilaiSAKIP; ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($TppSakip); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($BrutoTPP); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($Pph21); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo number_format($TppDiterima); ?></td>
				<td></td>
				<td></td>
			</tr>
			<?php
		}
		?>
	</table>

	<table style="width: 100%; margin-top: 30px;">
		<tr>
			<td style="width: 50%"></td>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			$tgl=date('d-m-Y');
			$TanggalTxt = date('d');
			$BulanTxt = date('m');
			$TahunTxt = date('Y');
			?>
			<td style="text-align: center; width: 50%">Jombang, <?php echo $TanggalTxt.' '.$NamaBulan[(int)$BulanTxt].' '.$TahunTxt; ?></td>
		</tr>
		<tr style="text-align: center;">
			<td style="text-align: center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Kepala Perangkat Daerah,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							.........................................
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							
						</td>
					</tr>
				</table>			
			</td>
			<td style="text-align: center; center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Bendahara Pengeluaran,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							.........................................
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							
						</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>	
</page>
<style>
.tbl{
	width: 595pt;
	border: 1px solid #000;
}
</style>
<?php
function HitungNilaiAktivitasBawahan($conn, $AtasanKodeOPD, $KodeJabatanAtasan, $Bulan, $Tahun){
	$NilaiTambahan = 0;
	$sql = "SELECT IFNULL(atv.DurasiKerjaACC, 0) AS DurasiKerjaACC
	FROM aktivitaspegawai atv
	LEFT JOIN mstpegawai p ON p.KodePegawai = atv.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE j.AtasanLangsungKodeJab = '$KodeJabatanAtasan' AND j.AtasanLangsungOPD = '$AtasanKodeOPD' AND atv.ACCAtasan = 'DISETUJUI' AND MONTH(atv.Tanggal) = '$Bulan' AND YEAR(atv.Tanggal) = '$Tahun'
	GROUP BY atv.KodeAktivitas";
	$res = $conn->query($sql);
	if ($res) {
		while ($row = $res->fetch_assoc()) {
			$NilaiTambahan += (0.01 * $row['DurasiKerjaACC']);
		}
		return $NilaiTambahan;
	}else{
		return $NilaiTambahan;
	}
}


$filename="tpp_produktivitas_".$Bulan."_".$Tahun.".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF
//==========================================================================================================

require_once('../html2pdf/html2pdf.class.php');
$content = ob_get_clean();
//$content = '<page style="font-family: freeserif">'.nl2br($content).'</page>';
try
{
	$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(5, 5, 5, 5));
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->pdf->SetTitle('TPP Berbasis Kinerja');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }
?>