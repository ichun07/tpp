<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
if (isset($_GET['KodePegawai'])) {
	$KodePegawai = $_GET['KodePegawai'];
}
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Riwayat Jabatan Pegawai</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 text-left">
							<button onclick="location.href = '../opd/pegawai_view.php'" class="btn btn-success">Kembali</button>
						</div>
					</div>
				</br>
				<div class="row">
					<div class="col-lg-12">				
						<div id="tabel_pegawai" class="table-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var KodePegawai;
	var KodeOPD;

	$(document).ready(function() {
		KodePegawai = "<?php echo $KodePegawai; ?>";
		LoadData();  
	});

	function LoadData(){
		var action = "HistoryJabatan";
		$.ajax({
			url: "pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodePegawai: KodePegawai},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#tabel_pegawai').html(data.Tabel);
					$('#tabeldata').DataTable();	
				}else{
					alert('request failed');
				}				
			}
		});
	}
</script>
