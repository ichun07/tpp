<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Forms</h2>
        </div>
    </header>
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item active">Forms</li>
        </ul>
    </div>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../opd/sistem_setting_view.php'"
                    class="btn btn-success">Kembali
                </button>
            </br></br>
            <div class="card">
                <div class="card-close">
                    <div class="dropdown">
                        <button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow">
                        <a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a
                        href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a>
                    </div>
                </div>
            </div>
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Tambah Sistem Setting</h3>
            </div>
            <div class="card-body">
                <form method="post" action="" id="form_setting">
                    <div class="form-group">
                        <label class="form-control-label">Nama Setting</label>
                        <input type="text" placeholder="Nama Setting" class="form-control"
                        name="txtSettingName" required data-msg="Nama Setting tidak boleh kosong!">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Nilai Setting</label>
                        <input type="text" placeholder="Nilai Setting" class="form-control"
                        name="txtSettingValue" required data-msg="Nilai Setting tidak boleh kosong!">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Tipe Setting</label>
                        <input type="text" placeholder="Tipe Setting" class="form-control"
                        name="txtTipeSetting" required data-msg="Tipe Setting tidak boleh kosong!">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
    var KodeOPD;
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
    });

    $("#form_setting").submit(function(e) {
        e.preventDefault();
        var SettingName = $("[name='txtSettingName']").val();
        var SettingValue = $("[name='txtSettingValue']").val();
        var TipeSetting = $("[name='txtTipeSetting']").val();
        var action = "InsertData";
        var formData = new FormData();
        formData.append("KodeOPD", KodeOPD);
        formData.append("settingName", SettingName);
        formData.append("settingValue", SettingValue);
        formData.append("tipeSetting", TipeSetting);
        formData.append("action", action);

        $.ajax({
            url: "sistem_setting_aksi.php",
            method: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    $("[name='txtSettingName']").val("");
                    $("[name='txtSettingValue']").val("");
                    $("[name='txtTipeSetting']").val("");
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/sistem_setting_view.php'>lihat semua data</a>.</div>");
                } else {
                    alert("Insert Data Failed");
                }
            }
        });
    });

    function simpanData() {
        var SettingName = $("[name='txtSettingName']").val();
        var SettingValue = $("[name='txtSettingValue']").val();
        var TipeSetting = $("[name='txtTipeSetting']").val();
        var action = "InsertData";
        $.ajax({
            url: "sistem_setting_aksi.php",
            method: "POST",
            data: {
                KodeOPD: KodeOPD,
                settingName: SettingName,
                settingValue: SettingValue,
                tipeSetting: TipeSetting,
                action: action
            },
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    $("[name='txtSettingName']").val("");
                    $("[name='txtSettingValue']").val("");
                    $("[name='txtTipeSetting']").val("");
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/sistem_setting_view.php'>lihat semua data</a>.</div>");
                } else {
                    alert("Insert Data Failed");
                }
            }
        });
    }
</script>