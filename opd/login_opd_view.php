<?php 
include_once '../config/koneksi.php';
if ($_POST) {
    include_once 'login_opd_aksi.php';
    $Username = $_POST['txtUsername'];
    $Password = base64_encode($_POST['txtPassword']);
    $KodeOPD = $_POST['cbKodeOpd'];
    $result = CheckUserLogin($conn, $Username, $Password, $KodeOPD);
    if($result == 200){
        echo "<script>location.href='index.php'</script>";
    }else if($result == 404){
        echo "<script>swal ( 'Oops' ,  'Data tidak cocok!' ,  'error' );</script>";
    }else{
        echo "<script>swal ( 'Oops' ,  'Gagal Login!' ,  'error' );</script>";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bagian Organisasi Kab.Jombang</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="komponen/img/favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
    <div class="page login-page">
        <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <div class="row">
                    <!-- Logo & Information Panel-->
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center" style="background-color: #007bff;"  >
                            <div class="content">
                                <div class="logo">
                                    <h1>Sistem Informasi TPP</h1>
                                </div>
                                <p>Bagian Organisasi Sekretariat Daerah Kab.Jombang</p>
                            </div>
                        </div>
                    </div>
                    <!-- Form Panel    -->
                    <div class="col-lg-6 bg-white">
                        <div class="form d-flex align-items-center">
                            <div class="content">
                                <form method="post" class="form-validate">
                                    <div class="form-group" id="select_opd">
                                        <?php 
                                        $output = '';
                                        $sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
                                        $res = $conn->query($sql);
                                        if ($res) {
                                            $output .= '<label class="form-control-label">Nama OPD</label>';
                                            $output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
                                            $output .= "<option value='0'> - Pilih OPD - </option>";
                                            while ($row = $res->fetch_assoc()) {
                                                $output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
                                            }
                                            $output .= '</select>';
                                            echo $output;
                                        }
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <input id="txtUsername" type="text" name="txtUsername" required
                                        data-msg="Please enter your username" class="input-material">
                                        <label for="txtUsername" class="label-material">User Name</label>
                                    </div>
                                    <div class="form-group">
                                        <input id="txtPassword" type="password" name="txtPassword" required
                                        data-msg="Please enter your password" class="input-material">
                                        <label for="txtPassword" class="label-material">Password</label>
                                    </div>
                                    <button type="submit" id="btnLogin" class="btn btn-primary" style="background-color: #007bff;">Login</button>
                                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyrights text-center">
                <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>
                    <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </p>
            </div>
        </div>
        <!-- JavaScript files-->
        <script src="../komponen/vendor/jquery/jquery.min.js"></script>
        <script src="../komponen/vendor/popper.js/umd/popper.min.js"></script>
        <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
        <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
        <!-- Main File-->
        <script src="../komponen/js/front.js"></script>
    </body>    
    </html>