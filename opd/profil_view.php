<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
$KodeUser = $_SESSION['KodeUser'];
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Data User</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">                
              <div class="col-lg-12">
                  <div class="card">        
                    <div class="card-body">
                        <form method="post">
                            <div class="client-avatar text-center"><img src="../komponen/img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle" style="display: none;"></div>
                        </br></br>
                        <div class="form-group">
                            <label class="form-control-label">Nama User</label>
                            <input type="text" placeholder="Nama User" class="form-control" name="txtNamaUser"
                            required data-msg="Nama User tidak boleh kosong!"
                            id="txtNamaUser">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Username</label>
                            <input type="text" placeholder="Username" class="form-control" name="txtUsername"
                            required data-msg="Username tidak boleh kosong!" id="txtUsername">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Password</label>
                            <input type="password" placeholder="Password" class="form-control"
                            name="txtPassword" required data-msg="Password tidak boleh kosong!"
                            id="txtPassword">
                        </div>
                        <div class="form-group">
                            <button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    var KodeOPD,KodeUser;
    $(document).ready(function () {
        KodeUser = "<?php echo $KodeUser; ?>";
        KodeOPD = "<?php echo $KodeOPD; ?>";
        LoadDataProfil();
    });

    function LoadDataProfil() {
        var action = "AmbilData";
        $.ajax({
            url: "../kab/daftar_user_aksi.php",
            method: "POST",
            data: {id: KodeUser, action: action, IsKabupaten:0},
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    $('#txtNamaUser').val(data.Nama);
                    $('#txtUsername').val(data.Username);
                    $('#txtPassword').val(data.Password);
                } else {
                    alert('request failed');
                }
            }
        });
    }

    function simpanData() {
        var NamaUser = $("[name='txtNamaUser']").val();
        var Username = $("[name='txtUsername']").val();
        var Password = $("[name='txtPassword']").val();
        var action = "UpdateData";
        $.ajax({
            url: "../kab/daftar_user_aksi.php",
            method: 'POST',
            data: {
                id: KodeUser,
                namaUser: NamaUser,
                username: Username,
                password: Password,
                kodeOPD: KodeOPD,
                action: action,
                IsKabupaten:0
            },
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    LoadDataProfil();                    
                    var urlForward = 'profil_view.php?nama='+NamaUser;
                    window.location.replace(urlForward);
                } else {
                    alert('request failed');
                }
            }
        });
    }
</script>
<?php 
if (isset($_GET['nama'])) {
    $div = "<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Mengubah Profil!</strong></div>";
    echo '<script type="text/javascript">
        $("#sukses").html("'.$div.'");
    </script>';
}
?>