<?php
require "../komponen/excel_reader.php";
include_once '../config/koneksi.php';

if(isset($_POST)){
    $target = basename($_FILES['filepegawaiall']['name']) ;
    move_uploaded_file($_FILES['filepegawaiall']['tmp_name'], $target);
    $data = new Spreadsheet_Excel_Reader($_FILES['filepegawaiall']['name'],false);
    $baris = $data->rowcount($sheet_index = 0);
    $nama = '';
    for ($i=2; $i<=$baris; $i++){

        $barisreal = $baris-1;
        $k = $i-1;

        $percent = intval($k/$barisreal * 100)."%";

        $NoAkun = $data->val($i, 2);
        $Nama = $data->val($i, 4);

        if($Nama != $nama){
            $nama = $Nama;
            $NamaPegawai = ucwords(strtolower($Nama));
            $KodePegawai = AutoKodePegawai($conn);
            $query = "INSERT INTO mstpegawai (KodePegawai, NoAkun, NamaPegawai) VALUES ('$KodePegawai', '$NoAkun', '$NamaPegawai')";
            $hasil = $conn->query($query);
        }

    }


    unlink($_FILES['filepegawaiall']['name']);
    if(!$hasil){
        die(mysqli_error($conn));
        echo json_encode(array('response' => 500));
    }else{
        echo json_encode(array('response' => 200));
    }
    
}

function AutoKodePegawai($conn){
    $sql = "SELECT RIGHT(KodePegawai,16) AS kode FROM mstpegawai ORDER BY KodePegawai DESC LIMIT 1";
    $res = mysqli_query($conn, $sql);
    $result = mysqli_fetch_array($res);
    if ($result['kode'] == null) {
        $kode = 1;
    } else {
        $kode = ++$result['kode'];
    }
    $bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
    return 'PGW-' . $bikin_kode;
}

?>