<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Rekapitulasi TPP Kinerja</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 text-left">
					<?php 
					date_default_timezone_set('Asia/Jakarta');
					$year = date('Y');
					$month = date('m');
					?>
					<div class="form-group">
						<select id="cb_triwulan" name="cb_triwulan" class="form-control">             
							<option value="1" <?php if (3 >= (int)$month and (int)$month >= 1) echo ' selected="selected"'; ?>>Triwulan 1</option>
							<option value="2"<?php if (6 >= (int)$month and (int)$month >= 4) echo ' selected="selected"'; ?>>Triwulan 2</option>
							<option value="3"<?php if (9 >= (int)$month and (int)$month >= 7) echo ' selected="selected"'; ?>>Triwulan 3</option>
							<option value="4"<?php if (12 >= (int)$month and (int)$month >= 10) echo ' selected="selected"'; ?>>Triwulan 4</option>
						</select>
					</div>
				</div>
				<div class="col-md-4 text-left">
					<div class="form-group">
						<select class="form-control" id="cb_tahun" name="cb_tahun">
							<option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
							<?php
							for($i = $year-1; $i > $year-10; $i--){
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="col-md-4 text-right">
					<button id="btnCetak" class="btn btn-primary">Cetak Tpp Kinerja</button>
					<button onclick="location.href = '../opd/capaian_opd_tambah.php'" class="btn btn-primary">Tambah Data</button>
				</div>

				<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center">
						<h3 class="h4">Nilai Kinerja Perangkat Daerah</h3>
					</div>
					<div class="card-body">
						<div id="tabel_capaian" class="table-responsive">
						</div>
					</div>
				</div>		
			</br>
			<div class="card">
				<div class="card-header d-flex align-items-center">
					<h3 class="h4">Rekapitulasi TPP</h3>
				</div>
				<div class="card-body">
					<div id="tabel_pegawai" class="table-responsive">
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var KodeOPD,Tahun,Triwulan;
	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		Tahun = $("[name='cb_tahun']").val();
		Triwulan = $("[name='cb_triwulan']").val();
		LoadData();  
	});

	$('#cb_tahun').change(function () {
		Tahun = $(this).val();
		console.log("Tahun : "+Tahun);
		LoadData();
	});

	$('#cb_triwulan').change(function () {
		Triwulan = $(this).val();
		console.log("Triwulan : "+Triwulan);
		LoadData();
	});
	
	function LoadData(){
		var action = "LoadData";
		$.ajax({
			url: "capaian_opd_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD,Tahun: Tahun, Triwulan: Triwulan},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#tabel_capaian').html(data.Tabel);
					$('#tabel_pegawai').html(data.Tabel2);
					$('#tabeldata').DataTable();	
				}else{
					alert('request failed');
				}				
			}
		});
	}

	$(document).on('click', '#btnCetak', function () {
		console.log("Klik");
		var Url = "KodeOPD="+KodeOPD+"&Triwulan="+Triwulan+"&Tahun="+Tahun;
		window.open("capaian_opd_report.php?"+Url);
	});
</script>