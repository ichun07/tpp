<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
$KodePegawai = '';
$Bulan = '';
$Tahun = '';

if (isset($_GET['KodePegawai'])) {
	$KodePegawai = $_GET['KodePegawai'];
	$Bulan = $_GET['Bulan'];
	$Tahun = $_GET['Tahun'];
}else{
	echo "<script>alert('request failed');</script>";
}

$NamaBulan = array (1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Detail Absensi</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="col-lg-12">
				<div class="recent-activities card">
					<div class="card-header">
						<h3 class="h4">Identitas Pegawai</h3>
					</div>
					<div class="card-body">
						<!-- Draw Html -->
						<div class="col-md-12">
							<div class="row">
								<label class="text-info"> Nama</label>
							</div>
							<div class="row">
								<h5 id="txtNama" style="padding-bottom: 10px; padding-left: 5px;"></h5>
							</div>
							<div class="row">
								<label class="text-info"> Pangkat / Golongan</label>
							</div>
							<div class="row">
								<h5 id="txtPangkat" style="padding-bottom: 10px; padding-left: 5px;"></h5>
							</div>
							<div class="row">
								<label class="text-info"> NIP</label>
							</div>
							<div class="row">
								<h5 id="txtNIP" style="padding-bottom: 10px; padding-left: 5px;"></h5>
							</div>
							<div class="row">
								<label class="text-info"> Jabatan</label>
							</div>
							<div class="row">
								<h5 id="txtJabatan" style="padding-bottom: 10px; padding-left: 5px;"></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="recent-activities card">
					<div class="card-header">
						<h3 class="h4">Absensi Pegawai <?php echo " Bulan ".$NamaBulan[(int)$Bulan]." Tahun ".$Tahun; ?></h3>
					</div>
					<div class="card-body">
						<!-- Draw Html -->
						<div class="table-responsive" id="dataAbsensi">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var KodeOPD, KodePegawai, Bulan, Tahun;
	$(document).ready(function () {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		KodePegawai = "<?php echo $KodePegawai; ?>";
		Bulan = "<?php echo $Bulan; ?>";
		Tahun = "<?php echo $Tahun; ?>";
		LoadData();
	});

	function LoadData() {
		var action = "DetailPegawai";
		$.ajax({
			url: "absensi_pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodePegawai: KodePegawai, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun},
			dataType: 'json',
			success: function (data) {
				if (data.response == 200) {
					var DataPegawai = data.DataPegawai;
					document.getElementById('txtNama').innerHTML = DataPegawai.NamaPegawai;
					document.getElementById('txtPangkat').innerHTML = DataPegawai.Pangkat + " / " + DataPegawai.Golongan;
					document.getElementById('txtNIP').innerHTML = DataPegawai.NIP;
					document.getElementById('txtJabatan').innerHTML = DataPegawai.NamaJabatan;
					$('#dataAbsensi').html(data.DataHtml);
					$('#tabeldata').DataTable();	
				} else {
					alert('request failed');
				}
			}
		});
	}

</script>
