<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
    <div class="loader" style="display: none;"></div>
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Import Absensi Pegawai</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>        
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form name="myForm" id="myForm" onSubmit="return validateForm()" action="" method="post" enctype="multipart/form-data">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupFileAddon01">
                                        Upload
                                    </span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="filepegawaiall" name="filepegawaiall" aria-describedby="filepegawaiall" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                    <label class="custom-file-label" for="filepegawaiall">
                                        Choose file
                                    </label>
                                </div>
                            </div>
                        </br>
                        <input class="btn btn-primary" type="submit" name="submit" value="Simpan">
                    </form>
                </br></br>
                <label>*Hanya file XLS (Excel 2003) yang diijinkan untuk import absensi pegawai.</label>
            </br></br>
            <div class="progress" style="display:none">
                <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="sr-only">0%</span>
                </div>
            </div>
        </div>                            
    </div>
</div>    
</div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });

    var KodeOPD;
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
    });

    function validateForm()
    {
        function hasExtension(inputID, exts) {
            var fileName = document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
        }

        if(!hasExtension('filepegawaiall', ['.xls'])){
            alert("Hanya file XLS (Excel 2003) yang diijinkan.");
            return false;
        }
    }
    
    $("#myForm").submit(function(e) {
        e.preventDefault();
        var file_data = $("#filepegawaiall").prop("files")[0];
        var formData = new FormData();
        formData.append("filepegawaiall", file_data);
        $('.progress').show();
        $('.loader').show();
        console.log(""+formData);
        $.ajax({
            xhr : function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener('progress', function(e){
                    if(e.lengthComputable){
                        console.log('Bytes Loaded : ' + e.loaded);
                        console.log('Total Size : ' + e.total);
                        console.log('Persen : ' + (e.loaded / e.total));
                        
                        var percent = Math.round((e.loaded / e.total) * 100);
                        
                        $('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
                    }
                });
                return xhr;
            },
            url: "import_pegawai_aksi.php",
            type: "POST",
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',      
            success: function(data){
                $('.loader').hide();
                if(data.response === 200){
                  swal("Berhasil","Berhasil unggah dokumen.","success")
                  .then((value) => {
                      if(value){
                        location.href='verifikasi_absen_view.php';
                    }
                });
              }else if(data.response === 404){
                swal("Error",""+error_message,"error");
            }else{
                swal("Error",""+error_message,"error");
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
          console.log("Error : "+errorThrown);
      }
  });
    });

</script>

<style type="text/css">
.loader{
 position: fixed;
 left: 0px;
 top: 0px;
 width: 100%;
 height: 100%;
 z-index: 9999;
 background: url('loading_icon.gif') 50% 50% no-repeat rgb(249,249,249);
}
</style>