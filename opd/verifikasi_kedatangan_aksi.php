<?php
include_once '../config/koneksi.php';

if($_POST["action"] == "LoadData"){
	$requestData= $_REQUEST;
	$Bulan = $_POST['Bulan'];
	$Tahun = $_POST['Tahun'];
	$KodeOPD = $_POST['KodeOPD'];
	$result = GetDataTidakApel($conn,$requestData,$Bulan,$Tahun,$KodeOPD);
	echo json_encode($result);
}

function GetDataTidakApel($conn,$requestData,$Bulan,$Tahun,$KodeOPD){
	$columns = array( 
// datatable column index  => database column name
		0 => 'Tanggal',
		1 => 'Tanggal',
		2 => 'NamaPegawai', 
		3 => 'NamaJabatan', 
		4 => 'IsApel', 
		5 => 'IsSenam',
		6 => 'TL1,TL2,TL3',
		7 => 'PA1,PA2,PA3'
	);

// getting total number records without any search
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, ap.IsJumat,IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD 
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.DokumenPendukung IS NULL AND (TL1 OR TL2 OR TL3 OR PA1 OR PA2 OR PA3) = '1'";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, ap.IsJumat,IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD ";
	$sql.="WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.DokumenPendukung IS NULL AND p.KodeOPD = '$KodeOPD' AND (TL1 OR TL2 OR TL3 OR PA1 OR PA2 OR PA3) = '1'
	AND ap.IsHadir = '1' AND p.NamaPegawai LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.DokumenPendukung IS NULL AND p.KodeOPD = '$KodeOPD' AND (TL1 OR TL2 OR TL3 OR PA1 OR PA2 OR PA3) = '1'
	AND ap.IsHadir = '1' AND p.NIP LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.DokumenPendukung IS NULL AND p.KodeOPD = '$KodeOPD' AND (TL1 OR TL2 OR TL3 OR PA1 OR PA2 OR PA3) = '1'
	AND ap.IsHadir = '1' AND j.NamaJabatan LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.DokumenPendukung IS NULL AND p.KodeOPD = '$KodeOPD' AND (TL1 OR TL2 OR TL3 OR PA1 OR PA2 OR PA3) = '1'
	AND ap.IsHadir = '1' AND ap.Tanggal LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); // again run query with limit
	
} else {	

	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, ap.IsJumat,IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.DokumenPendukung IS NULL AND (TL1 OR TL2 OR TL3 OR PA1 OR PA2 OR PA3) = '1'";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
$no = 1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array();
	if($row['IsJumat'] == '1'){
		if($row['IsSenam'] == '1'){
			$TidakSenam = '<td class="text-center">&#10004;</td>';			
		}else{
			$TidakSenam = '<td class="text-center"></td>';
		}
		$TidakApel = '<td class="text-center"></td>';
	}else{
		if($row['IsApel'] == '1'){
			$TidakApel = '<td class="text-center">&#10004;</td>';
		}else{
			$TidakApel = '<td class="text-center"></td>';
		}
		$TidakSenam = '<td class="text-center"></td>';
	}

	if($row['TL1'] == 1 || $row['TL2'] == 1 || $row['TL3'] == 1){
		$Terlambat ='<td class="text-center">&#10004;</td>';
	}else{
		$Terlambat ='<td class="text-center"></td>';
	}
	if($row['PA1'] == 1 || $row['PA2'] == 1 || $row['PA3'] == 1){
		$PulangAwal ='<td class="text-center">&#10004;</td>';
	}else{
		$PulangAwal ='<td class="text-center"></td>';
	}
	if($row['DokumenPendukung'] < 1){
		$Dokumen ='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnDokumen" id="btnDokumen" class="btn btn-primary"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="0"></td>';
	}else{
		$Dokumen ='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnDokumen" id="btnDokumen" class="btn btn-success"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="1"></td>';
	}	

	$nestedData[] = $no++;
	$nestedData[] = date('d - m - Y', strtotime($row['Tanggal']));
	$nestedData[] = $row['NamaPegawai'];
	$nestedData[] = $row['NamaJabatan'];
	$nestedData[] = $TidakApel;
	$nestedData[] = $TidakSenam;
	$nestedData[] = $Terlambat;
	$nestedData[] = $PulangAwal;
	$nestedData[] = $Dokumen;		
	
	$data[] = $nestedData;

}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
		);

return $json_data;  // send data as json format
}