<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
    if ($_POST["action"] == "LoadData") {
        $KodeOPD = $_POST['KodeOPD'];
        $result = GetDataSetting($conn, $KodeOPD);
        echo json_encode($result);
    }
    if ($_POST["action"] == "InsertData") {
        $SettingName = $_POST["settingName"];
        $SettingValue = $_POST["settingValue"];
        $TipeSetting = $_POST["tipeSetting"];
        $KodeOPD = $_POST['KodeOPD'];
        $result = InsertDataSetting($conn, $KodeOPD, $SettingName, $SettingValue, $TipeSetting);
        echo json_encode($result);
    }
    if ($_POST["action"] == "AmbilData") {
        $SettingName = $_POST["id"];
        $KodeOPD = $_POST['KodeOPD'];
        $output = GetOneDataSetting($conn, $KodeOPD, $SettingName);
        echo json_encode($output);
    }
    if ($_POST["action"] == "UpdateData") {
        $SettingName = $_POST["id"];
        $SettingValue = $_POST["settingValue"];
        $TipeSetting = $_POST["tipeSetting"];
        $KodeOPD = $_POST['KodeOPD'];
        $result = UpdateDataSetting($conn, $KodeOPD, $SettingName, $SettingValue, $TipeSetting);
        echo json_encode($result);
    }
    if ($_POST["action"] == "HapusData") {
        $SettingName = $_POST["id"];
        $KodeOPD = $_POST['KodeOPD'];
        $result = DeleteDataSetting($conn, $KodeOPD, $SettingName);
        echo json_encode($result);
    }
}


function GetDataSetting($conn, $KodeOPD)
{
    $output = '';
    $sql = "SELECT SettingName, SettingValue, KodeOPD, TipeSetting FROM sistemsettingopd WHERE KodeOPD = '$KodeOPD' ORDER BY SettingName ASC";
    $res = $conn->query($sql);
    if ($res) {
        $output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
	<thead>
	<tr>
	<th width="30px">No</th>
	<th>Nama Setting</th>
	<th>Nilai Setting</th>
	<th>Tipe Setting</th>
	<th>Aksi</th>
	</tr>
	</thead>
	<tfoot>
	<tr>
	<th>No</th>
	<th>Nama Setting</th>
	<th>Nilai Setting</th>
	<th>Tipe Setting</th>
	<th>Aksi</th>
	</tr>
	</tfoot>
	<tbody>';
        $no = 1;
        while ($row = $res->fetch_assoc()) {
            $output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . $row['SettingName'] . '</td>
			<td>' . $row['SettingValue'] . '</td>
			<td>' . $row['TipeSetting'] . '</td>
			<td class="text-center">
			<button value="' . $row['SettingName'] . '" name="update" id="btnUpdate" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
			<button style="display: none;" value="' . $row['SettingName'] . '" name="delete" id="btnDelete" class="btn btn-danger"><span class="icon icon-pencil-case" aria-hidden="true"></span></button>
			</td>
			</tr>';
        }
        $output .= '</tbody>
	</table>';
        return array('response' => 200, 'HtmlTabel' => $output);
    } else {
        return array('response' => 500);
    }
}

function GetOneDataSetting($conn, $KodeOPD, $SettingName)
{
    $output = '';
    $sql = "SELECT SettingName, SettingValue, TipeSetting, KodeOPD FROM sistemsettingopd WHERE SettingName = '$SettingName' AND KodeOPD = '$KodeOPD' GROUP BY SettingName";
    $res = $conn->query($sql);
    if ($res) {
        while ($row = $res->fetch_assoc()) {
            $output['SettingName'] = $row['SettingName'];
            $output['SettingValue'] = $row['SettingValue'];
            $output['TipeSetting'] = $row['TipeSetting'];
        }
        $output['response'] = 200;
        return $output;
    } else {
        return array('response' => 500);
    }
}

function InsertDataSetting($conn, $KodeOPD, $SettingName, $SettingValue, $TipeSetting)
{
    $sql = "INSERT INTO sistemsettingopd (SettingName, SettingValue, TipeSetting, KodeOPD) VALUES ('$SettingName', '$SettingValue', '$TipeSetting','$KodeOPD')";
    $res = $conn->query($sql);
    if ($res) {
        return array('response' => 200);
    } else {
        return array('response' => 500);
    }
}

function UpdateDataSetting($conn, $KodeOPD, $SettingName, $SettingValue, $TipeSetting)
{
    $sql = "UPDATE sistemsettingopd SET SettingValue = '$SettingValue', TipeSetting = '$TipeSetting' WHERE SettingName = '$SettingName'";
    $res = $conn->query($sql);
    if ($res) {
        return array('response' => 200);
    } else {
        return array('response' => 500);
    }
}

function DeleteDataSetting($conn, $KodeOPD, $SettingName)
{
    $sql = "DELETE FROM sistemsettingopd WHERE SettingName = '$SettingName' AND KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if ($res) {
        return array('response' => 200);
    } else {
        return array('response' => 500);
    }
}

?>