<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Home</h2>
		</div>
	</header>
	<section class="dashboard-header">
		<div class="container-fluid">
			<div class="row">
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_pegawai">
						<div class="icon bg-red"><i class="icon icon-user"></i></div>
						<div class="text"><strong>Data</strong><br>Pegawai</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12" style="display: none;">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_setting">
						<div class="icon bg-red"><i class="fa fa-cogs" aria-hidden="true"></i></div>
						<div class="text"><strong>Sistem</strong><br>Setting</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_produktivitas">
						<div class="icon bg-orange"><i class="fa fa-pie-chart" aria-hidden="true"></i></div>
						<div class="text"><strong>TPP</strong><br>Kinerja</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_import">
						<div class="icon bg-green"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Import</strong><br>Absensi</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_absen">
						<div class="icon bg-green"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Absensi</strong><br>Pegawai</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_hadir">
						<div class="icon bg-green"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Verifikasi</strong><br>Kehadiran</div>
					</div>
				</div>							
			</div>
			<br>
			<div class="row">				
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_telat">
						<div class="icon bg-green"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Verifikasi</strong><br>TL/PA</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_apel">
						<div class="icon bg-green"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Verifikasi</strong><br>Apel</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_senam">
						<div class="icon bg-green"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Verifikasi</strong><br>Senam</div>
					</div>
				</div>								
			</div>			
			<br>
			<div class="row" style="display: none;">				
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_produktivitas">
						<div class="icon bg-orange"><i class="fa fa-pie-chart" aria-hidden="true"></i></div>
						<div class="text"><strong>TPP</strong><br>Produktivitas</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_kinerja">
						<div class="icon bg-orange"><i class="fa fa-pie-chart" aria-hidden="true"></i></div>
						<div class="text"><strong>TPP</strong><br>Kinerja</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_makan">
						<div class="icon bg-orange"><i class="fa fa-pie-chart" aria-hidden="true"></i></div>
						<div class="text"><strong>TPP</strong><br>Uang Makan</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	$(document).on('click', '#btn_pegawai', function () {
		location.href='pegawai_view.php';
	});

	$(document).on('click', '#btn_setting', function () {
		location.href='sistem_setting_view.php';
	});

	$(document).on('click', '#btn_import', function () {
		location.href='import_finger.php';
	});

	$(document).on('click', '#btn_absen', function () {
		location.href='absensi_pegawai_view.php';
	});

	$(document).on('click', '#btn_hadir', function () {
		location.href='verifikasi_absen_hadir.php';
	});

	$(document).on('click', '#btn_telat', function () {
		location.href='verifikasi_kedatangan_view.php';
	});

	$(document).on('click', '#btn_apel', function () {
		location.href='verifikasi_absen_apel.php';
	});

	$(document).on('click', '#btn_senam', function () {
		location.href='verifikasi_absen_senam.php';
	});

	$(document).on('click', '#btn_produktivitas', function () {
		location.href='data_tpp_view.php';
	});

	$(document).on('click', '#btn_kinerja', function () {
		location.href='capaian_opd_view.php';
	});

	$(document).on('click', '#btn_makan', function () {
		location.href='tpp_uang_makan_view.php';
	});
</script>