<?php 
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if($_POST['action'] == 'DataStruktur'){
		$result = GetDataStrukturOPD($conn, 'OPD-0000001');
		echo json_encode($result);
	}
}

function GetDataStrukturOPD($conn, $KodeOPD){
	$args = [];
	$sql = "SELECT j.KodeOPD, j.KodeJabatan, j.NamaJabatan, p.NamaPegawai, (SELECT COUNT(mj.KodeJabatan) FROM mstjabatan mj WHERE mj.AtasanLangsungKodeJab = j.KodeJabatan AND mj.AtasanLangsungOPD = j.KodeOPD) AS Bawahan FROM mstjabatan j LEFT JOIN mstpegawai p ON p.KodeJabatan = j.KodeJabatan AND p.KodeOPD = j.KodeOPD WHERE j.IsKepala = '1' AND j.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_array($res);
		$args[] = array('id' => $row['KodeJabatan'], 'parent' => null, 'title' => $row['NamaJabatan'], 'description' => $row['NamaPegawai']);
		if ($row['Bawahan'] > 0) {
			$res1 = GetBawahan($conn, $row['KodeJabatan'], $row['KodeOPD'], $KodeOPD);
			if ($res1) {
				while ($row1 = $res1->fetch_assoc()) {
					$args[] = array('id' => $row1['KodeJabatan'], 'parent' => $row1['AtasanLangsungKodeJab'], 'title' => $row1['NamaJabatan'], 'description' => $row1['NamaPegawai']);
					if ($row1['Bawahan'] > 0) {
						$res2 = GetBawahan($conn, $row1['KodeJabatan'], $row1['KodeOPD'], $KodeOPD);
						if ($res2) {
							while ($row2 = $res2->fetch_assoc()) {
								$args[] = array('id' => $row2['KodeJabatan'], 'parent' => $row2['AtasanLangsungKodeJab'], 'title' => $row2['NamaJabatan'], 'description' => $row2['NamaPegawai']);
								$res3 = GetBawahan($conn, $row2['KodeJabatan'], $row2['KodeOPD'], $KodeOPD);
								if($res3){
									while ($row3 = $res3->fetch_assoc()) {
										$args[] = array('id' => $row3['KodeJabatan'], 'parent' => $row3['AtasanLangsungKodeJab'], 'title' => $row3['NamaJabatan'], 'description' => $row3['NamaPegawai']);
									}
								}
							}
						}
					}
				}
			}
		}
		return $args;
	}
}

function GetBawahan($conn, $KodeAtasanJab, $KodeAtasanOPD, $KodeOPD)
{
	$sql = "SELECT j.AtasanLangsungKodeJab, j.KodeOPD, j.KodeJabatan, j.NamaJabatan, p.NamaPegawai, (SELECT COUNT(mj.KodeJabatan) FROM mstjabatan mj WHERE mj.AtasanLangsungKodeJab = j.KodeJabatan AND mj.AtasanLangsungOPD = j.KodeOPD) AS Bawahan 
	FROM mstjabatan j LEFT JOIN mstpegawai p ON p.KodeJabatan = j.KodeJabatan AND p.KodeOPD = j.KodeOPD WHERE j.KodeOPD = '$KodeOPD' AND j.AtasanLangsungKodeJab = '$KodeAtasanJab' AND j.AtasanLangsungOPD = '$KodeAtasanOPD' GROUP BY j.KodeJabatan";
	$res = $conn->query($sql);
	return $res;
}
?>