<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Jabatan</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 text-right">
				</br>
				<button onclick="location.href = '../opd/jabatan_tambah.php'" class="btn btn-primary">Tambah Data</button>
			</div>
			<div class="col-lg-12">
				<div id="tabel_jabatan" class="table-responsive">
				</div>					
			</div>
		</div>
	</div>
</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="text-align:center;">Anda yakin untuk menghapus data ini ?</h4>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var KodeOPD;
	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		LoadData();  
	});

	var KodeJabatanDelete,KodeJabatanEdit;

	function LoadData(){
		var action = "LoadData";
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD},
			dataType: 'json',
			success: function (data) {
				if(data.Status == 200){
					$('#tabel_jabatan').html(data.Tabel);
					$('#tabeldata').DataTable();	
				}else{
					alert('request failed');
				}				
			}
		});
	}

	$(document).on('click', '#btnDelete', function () {
		var KodeJabatan = $(this).val();
		var dataValue = $(this).attr("data-value");
		KodeJabatanDelete = KodeJabatan;
		console.log("KodeJabatan : "+KodeJabatan);
		console.log("KodeOPD : "+dataValue);
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btHapus', function () {
		var action = "HapusData";
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {id: KodeJabatanDelete, kodeOPD: KodeOPD,action: action},
			dataType: 'json',
			success: function (data) {
				if(data.Status == 200){
					LoadData();
					$('#ModalHapus').modal("toggle");
				}else{
					alert('request failed');
				}
			}
		});
	});

	$(document).on('click', '.btn-warning', function () {
		var id = $(this).attr("id");
		KodeJabatanEdit = id;
		var action = "AmbilData";
		$.ajax({
			url: "instansi_aksi.php",
			method: "POST",
			data: {id: id, action: action},
			dataType: 'json',
			success: function (data) {
				$('#etNamaOPD').val(data.NamaOPD);
				$('#etAlamatOPD').val(data.AlamatOPD);
				$('#etKodeSuratOPD').val(data.KodeSuratOPD);
				$('#etKeteranganOPD').val(data.Keterangan);
				$('#ModalEdit').modal("show");
			}
		});
	});

	$(document).on('click', '#btSimpan', function () {
		var NamaOPD = $("[name='etNamaOPD']").val();
		var AlamatOPD = $("[name='etAlamatOPD']").val();
		var KodeSuratOPD = $("[name='etKodeSuratOPD']").val();
		var Keterangan = $("[name='etKeteranganOPD']").val();
		var action = "UpdateData";
		$.ajax({
			url: "instansi_aksi.php",
			method: 'POST',
			data: {id: KodeJabatanEdit, namaOPD: NamaOPD, alamatOPD: AlamatOPD, kodeSuratOPD: KodeSuratOPD, keteranganOPD: Keterangan, action: action},
			success: function () {
				$("[name='etNamaOPD']").val("");
				$("[name='etAlamatOPD']").val("");
				$("[name='etKodeSuratOPD']").val("");
				$("[name='etKeteranganOPD']").val("");
				LoadData();
				$('#ModalEdit').modal("toggle");
			}
		});
	});

	$(document).on('click', '#btnDetail', function () {
		var json = $(this).val();
		window.location = "jabatan_detail.php?"+json;
	});
</script>
