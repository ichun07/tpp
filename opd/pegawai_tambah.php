<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Tambah Pegawai</h2>
        </div>
    </header>
    <section class="forms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div id="sukses"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../opd/pegawai_view.php'" class="btn btn-success">
                        Kembali
                    </button>
                </br></br>
                <div class="card">                        
                    <div class="card-body">
                        <form id="form_pegawai" method="post" action="">
                            <div class="form-group" style="display: none" id="select_opd">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama Pegawai</label>
                                        <input type="text" placeholder="Nama Pegawai" class="form-control"
                                        name="txtNamaPegawai" id="txtNamaPegawai" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Status</label>
                                        <select id="cbStatus" name="cbStatus" class="form-control">
                                            <option value="PNS" selected>PNS</option>
                                            <option value="CPNS">CPNS</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">NIP</label>
                                        <input type="text" placeholder="NIP" class="form-control" name="txtNip"
                                        id="txtNip" autocomplete="off" required>
                                    </div>                         
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">No Akun</label>
                                        <input type="number" placeholder="No Akun" class="form-control" name="txtAkun"
                                        id="txtAkun" autocomplete="off" required>
                                    </div>    
                                    <div class="form-group" id="select_pangkat"></div>
                                    <div class="form-group" id="select_kode_jabatan"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <!-- <button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button> -->
                                <input class="btn btn-primary" type="submit" name="submit" value="Simpan">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
    var KodeOPD;
    var HargaJabatan,NilaiJabatan,PokokTPP;
    var Golongan = 0,Pangkat = 0;
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
        LoadDataJabatan(KodeOPD);
    });

    function LoadDataJabatan(KodeOPD) {
        var action = "GetJabatan";
        $.ajax({
            url: "pegawai_aksi.php",
            method: "POST",
            data: {action: action, KodeOPD: KodeOPD},
            dataType: 'json',
            success: function (data) {
                if (data.response = 200) {
                    $('#select_pangkat').html(data.cbPangkat);
                    $('#select_kode_jabatan').html(data.cbJabatan);

                    $('#cbJabatan').change(function () {
                        NilaiJabatan = $(this).find('option:selected').attr('data-value');
                        // $(this).attr("data-value");
                        HargaJabatan = $(this).find('option:selected').attr('data-value2');
                        // $(this).attr("data-value2");
                        PokokTPP = NilaiJabatan * HargaJabatan;
                        console.log("NilaiJabatan : " + NilaiJabatan);
                        console.log("HargaJabatan : " + HargaJabatan);
                        console.log("PokokTPP : " + PokokTPP);
                    });

                    $('#cbPangkat').change(function () {
                        Pangkat = $(this).find('option:selected').attr('value');
                        Golongan = $(this).find('option:selected').attr('data-value');
                        console.log("Pangkat : " + Pangkat);
                        console.log("Golongan : " + Golongan);
                    });
                } else {
                    alert('Load Data Jabatan Failed');
                }
            }
        });
    }

    function simpanData() {
        var NIP = $("[name='txtNip']").val();
        console.log("NIP : " + NIP);
        var NamaPegawai = $("[name='txtNamaPegawai']").val();
        console.log("NamaPegawai : " + NamaPegawai);
        var Status = $("[name='cbStatus']").val();
        console.log("Pangkat : " + Pangkat);
        console.log("Golongan : " + Golongan);
        var KodeJabatan = $("[name='cbJabatan']").val();
        var NoAkun = $("[name='txtAkun']").val();
        console.log("KodeJabatan : " + KodeJabatan);
        var action = "ReplacePegawai";
        if(Status !='PNS'){
            PokokTPP = PokokTPP * 0.8;
        } 
        $.ajax({
            url: "pegawai_aksi.php",
            method: "POST",
            data: {
                NIP: NIP,
                NamaPegawai: NamaPegawai,
                Pangkat: Pangkat,
                Golongan: Golongan,
                KodeJabatan: KodeJabatan,
                KodeOPD: KodeOPD,
                Status: Status,
                PokokTPP: PokokTPP,
                NoAkun:NoAkun,
                action: action
            },
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    $("[name='txtNip']").val(""); 
                    $("[name='txtNamaPegawai']").val("");                      
                    $("[name='txtAkun']").val("");
                    SelectElement("cbStatus", "PNS");
                    LoadDataJabatan(KodeOPD);
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/pegawai_view.php'>lihat semua data</a>.</div>");
                    swal('Informasi' ,  'Berhasil menambah pegawai!' ,  'success');
                } else {
                    swal('Error' ,  'Gagal menambah pegawai!' ,  'error');
                }
            }
        });
    }

    $("#form_pegawai").submit(function(e) {
        e.preventDefault();
        var NIP = $("[name='txtNip']").val();
        console.log("NIP : " + NIP);
        var NamaPegawai = $("[name='txtNamaPegawai']").val();
        console.log("NamaPegawai : " + NamaPegawai);
        var Status = $("[name='cbStatus']").val();
        console.log("Pangkat : " + Pangkat);
        console.log("Golongan : " + Golongan);
        var KodeJabatan = $("[name='cbJabatan']").val();
        console.log("KodeJabatan : " + KodeJabatan);        
        var NoAkun = $("[name='txtAkun']").val();
        var action = "InsertData";
        if(KodeJabatan == 0){
            swal('Peringatan' ,  'Anda harus memilih jabatan!' ,  'warning');
        }else if(Pangkat == 0){
            swal('Peringatan' ,  'Anda harus memilih Pangkat & Golongan!' ,  'warning');
        }else{
            if(Status !='PNS'){
                PokokTPP = PokokTPP * 0.8;
            }            
            var formData = new FormData();
            formData.append("NIP", NIP);
            formData.append("NamaPegawai", NamaPegawai);
            formData.append("Pangkat", Pangkat);
            formData.append("Golongan", Golongan);
            formData.append("KodeJabatan", KodeJabatan);
            formData.append("KodeOPD", KodeOPD);
            formData.append("Status", Status);
            formData.append("PokokTPP", PokokTPP);
            formData.append("NoAkun", NoAkun);
            formData.append("action", action);
            $.ajax({
                url: "pegawai_aksi.php",
                method: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'json',
                success: function (data) {
                    if (data.response === 200) {
                        $("[name='txtNip']").val(""); 
                        $("[name='txtNamaPegawai']").val("");
                        SelectElement("cbStatus", "PNS");
                        LoadDataJabatan(KodeOPD);
                        $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/pegawai_view.php'>lihat semua data</a>.</div>");
                        swal('Sukses' ,  'Berhasil menambah pegawai' ,  'success');
                    } else {
                        swal({
                            title: "Peringatan",
                            text: "Jabatan tersebut sudah ditempati, Apakah anda ingin menggantinya dengan pegawai baru?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                          if (willDelete) {
                            simpanData();
                        } else {
                            $("[name='txtNip']").val(""); 
                            $("[name='txtNamaPegawai']").val("");                            
                            $("[name='txtAkun']").val("");
                            SelectElement("cbStatus", "PNS");
                            LoadDataJabatan(KodeOPD);
                            swal("Tambah pegawai dibatalkan.");
                        }
                    });
                    }
                }
            });
        }        
    });

    function SelectElement(id, valueToSelect)
    {    
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }
</script>