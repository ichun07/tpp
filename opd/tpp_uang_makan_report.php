<?php
include_once '../config/koneksi.php';
ob_start();
$NamaBulan = array (1 =>   'Januari',
	'Februari',
	'Maret',
	'April',
	'Mei',
	'Juni',
	'Juli',
	'Agustus',
	'September',
	'Oktober',
	'November',
	'Desember'
);
$KodeOPD = $_GET['KodeOPD'];
$Bulan = $_GET['Bulan'];
$Tahun = $_GET['Tahun'];
$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai,(SELECT SettingValue FROM sistemsetting WHERE SettingName = 'UANG_MAKAN' AND IsAnjab = 0) AS PokokUangMakan, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, (tb.JmlHadir * (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'UANG_MAKAN' AND IsAnjab = 0)) AS JmlUangMakan
FROM tppbulanan tb
LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
ORDER BY tb.Bulan, tb.Tahun";
$res = $conn->query($sql);
$no = 1;
$JmlUangMakanHarian = 0;
$JmlKehadiran = 0;
$JmlUangMakanTotal = 0;
$JmlPotongan = 0;
$JmlTppDiterima = 0;
?>
<page style="width:100%;" backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
	<h2 style="width:100%; margin-bottom: 0px; text-align: center;">DAFTAR REKAPITULASI TAMBAHAN PENGHASILAN BERUPA UANG MAKAN</h2>
	<h3 style="width:100%; margin-top: 0px; text-align: center;"><?php echo "BULAN ".strtoupper($NamaBulan[(int)$Bulan])." TAHUN ".$Tahun; ?></h3>
	<!-- TABEL ABSENSI -->
	<font size="2" face="Courier New" >
		<table border="1" style="width:100%; margin-top: 20px; border-collapse: collapse; font-size: 11px;">
			<tr>
				<th style="width:5%; text-align: center; padding: 4px;">No</th>
				<th style="width:20%; text-align: center; padding: 4px;">Nama NIP</th>
				<th style="width:12%; text-align: center; padding: 4px;">Pangkat/Gol</th>
				<th style="width:8%; text-align: center; padding: 4px;">Uang Makan Harian</th>
				<th style="width:8%; text-align: center; padding: 4px;">Jumlah Kehadiran</th>
				<th style="width:8%; text-align: center; padding: 4px;">Jumlah Uang Makan</th>
				<th style="width:5%; text-align: center; padding: 4px;">PPH 21%</th>
				<th style="width:8%; text-align: center; padding: 4px;">Potongan Pajak</th>
				<th style="width:8%; text-align: center; padding: 4px;">TPP Diterima</th>
				<th style="width:10%; text-align: center; padding: 4px;">Nomor Rekening Pegawai</th>
			</tr>
			<tr>
				<th style="text-align: center; padding: 4px;">1</th>
				<th style="text-align: center; padding: 4px;">2</th>
				<th style="text-align: center; padding: 4px;">3</th>
				<th style="text-align: center; padding: 4px;">4</th>
				<th style="text-align: center; padding: 4px;">5</th>
				<th style="text-align: center; padding: 4px;">6</th>
				<th style="text-align: center; padding: 4px;">7</th>
				<th style="text-align: center; padding: 4px;">8</th>
				<th style="text-align: center; padding: 4px;">9</th>
				<th style="text-align: center; padding: 4px;">10</th>
			</tr>
			<?php
			while ($row = $res->fetch_assoc()) {
				$PotonganPPH = $row['JmlUangMakan'] * 21/100;
				$TppDiterima = $row['JmlUangMakan'] - $PotonganPPH; ?>
				<tr>
					<td style="text-align: center; padding: 4px;"><?php echo $no++; ?></td>
					<td style="padding: 4px;"><?php echo ucwords(strtolower($row['NamaPegawai'])); ?></td>
					<td style="padding: 4px;"><?php echo $row['Pangkat']; ?></td>
					<td style="text-align: right; padding: 4px;"><?php echo number_format($row['PokokUangMakan']); ?></td>
					<td style="text-align: right; padding: 4px;"><?php echo $row['JmlHadir']; ?></td>
					<td style="text-align: right; padding: 4px;"><?php echo number_format($row['JmlUangMakan']); ?></td>
					<td style="text-align: center; padding: 4px;">21%</td>
					<td style="text-align: right; padding: 4px;"><?php echo number_format($PotonganPPH); ?></td>
					<td style="text-align: right; padding: 4px;"><?php echo number_format($TppDiterima); ?></td>
					<td style="text-align: center; padding: 4px;"></td>
				</tr>
				<?php
				$JmlUangMakanHarian += $row['PokokUangMakan'];
				$JmlKehadiran += $row['JmlHadir'];
				$JmlUangMakanTotal += $row['JmlUangMakan'];
				$JmlPotongan += $PotonganPPH;
				$JmlTppDiterima += $TppDiterima;
			}
			?>
			<tr>
				<td colspan="3" style="text-align: center; padding: 4px;">Jumlah</td>
				<td style="text-align: right; padding: 4px;"><?php echo number_format($JmlUangMakanHarian); ?></td>
				<td style="text-align: right; padding: 4px;"><?php echo $JmlKehadiran; ?></td>
				<td style="text-align: right; padding: 4px;"><?php echo number_format($JmlUangMakanTotal); ?></td>
				<td style="text-align: right; padding: 4px;"></td>
				<td style="text-align: right; padding: 4px;"><?php echo number_format($JmlPotongan); ?></td>
				<td style="text-align: right; padding: 4px;"><?php echo number_format($JmlTppDiterima); ?></td>
				<td style="text-align: center; padding: 4px;"></td>
			</tr>
		</table>
	</font>

	<table style="width: 100%; margin-top: 30px;">
		<tr>
			<td style="width: 50%"></td>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			$tgl=date('d-m-Y');
			$TanggalTxt = date('d');
			$BulanTxt = date('m');
			$TahunTxt = date('Y');
			?>
			<td style="text-align: center; width: 50%">Jombang, <?php echo $TanggalTxt.' '.$NamaBulan[(int)$BulanTxt].' '.$TahunTxt; ?></td>
		</tr>
		<tr style="text-align: center;">
			<td style="text-align: center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Kepala Perangkat Daerah,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
			<td style="text-align: center; center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Bendahara Pengeluaran,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>	
</page>
<?php
$filename="tpp_uangmakan_".$Bulan."_".$Tahun.".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF
//==========================================================================================================

require_once('../html2pdf/html2pdf.class.php');
$content = ob_get_clean();
//$content = '<page style="font-family: freeserif">'.nl2br($content).'</page>';
try
{
	$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(5, 5, 5, 5));
	$html2pdf->setDefaultFont('Arial');
    $html2pdf->pdf->SetTitle('TPP Uang Makan');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }
?>