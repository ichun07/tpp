<?php
session_start();
if (!isset($_SESSION['KodeOPD'])) {
  echo "<script>location.href='login_opd_view.php'</script>";
}

if (isset($_GET['nama'])) {
  $NamaUser = $_GET['nama'];    
  $_SESSION['Nama'] = $NamaUser;
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bagian Organisasi Kab.Jombang</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">    
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
  <!-- Fontastic Custom icon font-->
  <link rel="stylesheet" href="../komponen/css/fontastic.css">
  <!-- Google fonts - Poppins -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">  
  <!-- Favicon-->
  <link rel="shortcut icon" href="../komponen/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <!-- <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css"> -->

        <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
        <script src="../komponen/sweetalert/sweetalert.js"></script>
        <!-- C:\xampp\htdocs\TPP\komponen\sweetalert\src -->
        <!-- css table datatables/dataTables -->
        
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="../komponen/font-awesome/css/font-awesome.min.css">
      </head>
      <body>
        <div class="page">
          <!-- Main Navbar-->
          <header class="header">
            <nav class="navbar" style="background-color: #007bff;">
              <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                  <!-- Navbar Header-->
                  <div class="navbar-header">
                    <!-- Navbar Brand --><a href="index.php" class="navbar-brand d-none d-sm-inline-block">
                      <div class="brand-text d-none d-lg-inline-block"><span>Sistem Informasi </span><strong> TPP</strong></div>
                      <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>BD</strong></div></a>
                      <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                    </div>
                    <!-- Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                      <!-- Logout    -->
                      <li class="nav-item"><a href="logout.php" class="nav-link logout"> <span class="d-none d-sm-inline">Logout</span><i class="fa fa-sign-out"></i></a></li>
                    </ul>
                  </div>
                </div>
              </nav>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
              <!-- Side Navbar -->
              <nav class="side-navbar">
                <!-- Sidebar Header-->
                <div class="sidebar-header d-flex align-items-center">
                  <div onclick="HalamanProfil()" class="title" style="cursor: pointer;">
                    <h1 class="h4"><?php echo $_SESSION['Nama']; ?></h1>
                    <p><?php echo $_SESSION['NamaOPD']; ?></p>
                  </div>
                </div>
                <!-- Sidebar Navidation Menus--><span class="heading">Menu</span>
                <ul class="list-unstyled">
                  <li><a href="../opd/index.php"> <i class="icon-home"></i>Home </a></li>
                  <!-- <li><a href="../opd/pegawai_view.php"> <i class="icon icon-user"></i>Pegawai </a></li> -->
                  <li><a href="#DropdownPegawai" aria-expanded="false" data-toggle="collapse"> <i class="icon icon-user" aria-hidden="true"></i>Pegawai </a>
                    <ul id="DropdownPegawai" class="collapse list-unstyled ">
                      <li><a href="../opd/pegawai_view.php">Data Pegawai</a></li>
                      <li><a href="../opd/skp_view.php">SKP Pegawai</a></li>
                    </ul>
                  </li>
                  <li><a href="#DropdownAbsensi" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-calendar-check-o" aria-hidden="true"></i>Absensi Pegawai </a>
                    <ul id="DropdownAbsensi" class="collapse list-unstyled ">
                      <li><a href="../opd/import_finger.php">Import Data Absensi</a></li>
                      <li><a href="../opd/verifikasi_absen_hadir.php">Verifikasi Kehadiran</a></li>
                      <li><a href="../opd/verifikasi_kedatangan_view.php">Verifikasi TL/PA</a></li>
                      <li><a href="../opd/verifikasi_absen_apel.php">Verifikasi Apel</a></li>
                      <li><a href="../opd/verifikasi_absen_senam.php">Verifikasi Senam</a></li>
                      <li><a href="../opd/absensi_pegawai_view.php">Absensi Pegawai</a></li>
                    </ul>
                  </li>
                  <!-- TPP BERBASIS KINERJA -->
                  <li><a href="../opd/data_tpp_view.php"> <i class="fa fa-pie-chart"></i>TPP Berbasis Kinerja</a></li>
                  <!-- <li><a href="#DropdownTPP" aria-expanded="false" data-toggle="collapse"><i class="fa fa-pie-chart" aria-hidden="true"></i>Data TPP </a>
                    <ul id="DropdownTPP" class="collapse list-unstyled ">
                      <li><a href="../opd/tpp_bulanan_view.php">TPP Produktivitas</a></li>
                      <li><a href="../opd/capaian_opd_view.php">TPP Kinerja</a></li>
                      <li><a href="../opd/tpp_uang_makan_view.php">TPP Uang Makan</a></li>
                    </ul>
                  </li> -->
                  <li><a href="../opd/struktur_view.php"><i class="fa fa-sitemap" aria-hidden="true"></i>Struktur Organisasi </a></li>
                  <li style="display: none;"><a href="../opd/sistem_setting_view.php"><i class="fa fa-cogs" aria-hidden="true"></i>Sistem Setting </a></li>
                  <li><a href="../opd/log_view.php"><i class="fa fa-server" aria-hidden="true"></i></i>Log Server </a></li>
                </nav>