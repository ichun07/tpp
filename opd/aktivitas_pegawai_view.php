<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
$KodePegawai = '';

if (isset($_GET['KodePegawai'])) {
    $KodePegawai = $_GET['KodePegawai'];
} else {
    echo "<script>alert('request failed');</script>";
}
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Jurnal Aktivitas Pegawai</h2>
        </div>
    </header>
    
    <section class="forms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <a href="pegawai_view.php" class="btn btn-success">Kembali</a>
                </div>
                <div class="col-lg-6 text-right">
                    <button id="btnCetak" class="btn btn-primary">Cetak Aktivitas Pegawai</button>
                </div>
            </div>         
            <br>   
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="h4">Identitas Pegawai</h3>
                        </div>
                        <div class="card-body">
                            <!-- Draw Html -->
                            <div class="col-md-12">
                                <div class="row">
                                    <label class="text-info"> Nama</label>
                                </div>
                                <div class="row">
                                    <h5 id="txtNama" style="padding-bottom: 10px; padding-left: 5px;"></h5>
                                </div>
                                <div class="row">
                                    <label class="text-info"> Pangkat / Golongan</label>
                                </div>
                                <div class="row">
                                    <h5 id="txtPangkat" style="padding-bottom: 10px; padding-left: 5px;"></h5>
                                </div>
                                <div class="row">
                                    <label class="text-info"> NIP</label>
                                </div>
                                <div class="row">
                                    <h5 id="txtNIP" style="padding-bottom: 10px; padding-left: 5px;"></h5>
                                </div>
                                <div class="row">
                                    <label class="text-info"> Jabatan</label>
                                </div>
                                <div class="row">
                                    <h5 id="txtJabatan" style="padding-bottom: 10px; padding-left: 5px;"></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-12 text-left">
                                <h3 class="h4">Jurnal Aktivitas Kerja Pegawai</h3>
                            </div>                        
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 text-right">                                
                                </div>
                                <div class="col-md-3 text-right">
                                    <?php 
                                    date_default_timezone_set('Asia/Jakarta');
                                    $year = date('Y');
                                    $month = date('m');
                                    ?>
                                    <select id="cb_bulan" name="cb_bulan" class="form-control">             
                                        <option value="1"<?php if ((int)$month == 1) echo ' selected="selected"'; ?>>Januari</option>
                                        <option value="2"<?php if ((int)$month == 2) echo ' selected="selected"'; ?>>Februari</option>
                                        <option value="3"<?php if ((int)$month == 3) echo ' selected="selected"'; ?>>Maret</option>
                                        <option value="4"<?php if ((int)$month == 4) echo ' selected="selected"'; ?>>April</option>
                                        <option value="5"<?php if ((int)$month == 5) echo ' selected="selected"'; ?>>Mei</option>
                                        <option value="6"<?php if ((int)$month == 6) echo ' selected="selected"'; ?>>Juni</option>
                                        <option value="7"<?php if ((int)$month == 7) echo ' selected="selected"'; ?>>Juli</option>
                                        <option value="8"<?php if ((int)$month == 8) echo ' selected="selected"'; ?>>Agustus</option>
                                        <option value="9"<?php if ((int)$month == 9) echo ' selected="selected"'; ?>>September</option>
                                        <option value="10"<?php if ((int)$month == 10) echo ' selected="selected"'; ?>>Oktober</option>
                                        <option value="11"<?php if ((int)$month == 11) echo ' selected="selected"'; ?>>November</option>
                                        <option value="12"<?php if ((int)$month == 12) echo ' selected="selected"'; ?>>Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-3 text-right">
                                    <select id="cb_tahun" name="cb_tahun" class="form-control">
                                        <option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                        <?php
                                        for($i = $year-1; $i > $year-10; $i--){
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="dataAktivitas" class="table-responsive">                            
                                    </div>
                                </div>                            
                            </div>                        
                        </div>
                    </div>
                </div>  
            </div>            
        </div>
    </div>
</div>
</section>
</div>
<div id="ModalFoto" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Dokumen Verifikasi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="form-group" style="padding-bottom: 20px; height: 400px; width: 500px;">
                    <div width="100%" class="text-center" >
                        <div id="image_preview">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="ModalVerifikasi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Verifikasi Aktivitas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="form-group" style="padding-bottom: 20px;">
                <label class="form-control-label">Status ACC</label>
                <select class='form-control' id='cbStatusACC' name='cbStatusACC'>
                    <option value='DISETUJUI' selected>Disetujui</option>
                    <option value='PENYESUAIAN'>Penyesuaian</option>
                    <option value='DITOLAK'>Ditolak</option>
                </select>
            </div>
            <div class="form-group" style="padding-bottom: 20px;">
                <div id="view_sesuai">
                    <label>Durasi Kerja ACC</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="txtDurasiACC" id="txtDurasiACC" class="form-control" required
                            data-msg="Durasi Kerja ACC tidak boleh kosong!"/>
                        </div>
                        <div class="col-md-6 text-right">
                            <label id="txtDurasiAsli"></label>
                        </div>
                    </div>    
                </div>                
            </div>            
            <div class="form-group" style="padding-bottom: 20px;">
                <div id="view_keterangan">
                    <label>Keterangan</label>
                    <textarea type="text" name="txtKeterangan" id="txtKeterangan" class="form-control" required
                    data-msg="Keterangan tidak boleh kosong!" rows="7" cols="80"></textarea>
                </div>                
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" id="btSimpan">
                    Simpan
                </button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
            </div>

        </div>
    </div>
</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">

    var KodeOPD, KodePegawai, KodeAktivitas, DurasiAsli,Bulan,Tahun;

    var DurasiACC;
    var KeteranganACC;
    var StatusACC;
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
        KodePegawai = "<?php echo $KodePegawai; ?>";
        Tahun = $("[name='cb_tahun']").val();
        Bulan = $("[name='cb_bulan']").val();
        DurasiAsli = 0;
        LoadData();
    });

    $('#cb_tahun').change(function () {
        Tahun = $(this).val();
        console.log("Tahun : "+Tahun);
        LoadData();
    });

    $('#cb_bulan').change(function () {
        Bulan = $(this).val();
        console.log("Bulan : "+Bulan);
        LoadData();
    });

    function LoadData() {
        var action = "LoadData";
        $.ajax({
            url: "aktivitas_pegawai_aksi.php",
            method: "POST",
            data: {action: action, KodePegawai: KodePegawai, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun},
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    var DataPegawai = data.DataPegawai;
                    document.getElementById('txtNama').innerHTML = DataPegawai.NamaPegawai;
                    document.getElementById('txtPangkat').innerHTML = DataPegawai.Pangkat + " / " + DataPegawai.Golongan;
                    document.getElementById('txtNIP').innerHTML = DataPegawai.NIP;
                    document.getElementById('txtJabatan').innerHTML = DataPegawai.NamaJabatan;
                    $('#dataAktivitas').html(data.DataHtml);
                    $('#tabeldata').DataTable();
                } else {
                    alert('request failed');
                }
            }
        });
    }

    $(document).on('click', '#btnVerifikasi', function () {
        KodeAktivitas = $(this).val();
        var action = "AmbilData";
        $.ajax({
            url: "aktivitas_pegawai_aksi.php",
            method: "POST",
            data: {action: action, KodePegawai: KodePegawai, KodeOPD: KodeOPD, KodeAktivitas: KodeAktivitas},
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    var row = data.DataAktivitas;
                    var pesan = "Durasi Asli " + row.DurasiKerja + " Menit";
                    console.log("Pesan : " + pesan);
                    $('#txtDurasiACC').val("0");
                    document.getElementById('txtDurasiAsli').innerHTML = pesan;
                    //$('#txtDurasiAsli').val(pesan);
                    DurasiAsli = row.DurasiKerja;
                    $('#ModalVerifikasi').modal("show");
                    document.getElementById("view_sesuai").style.display = "none";
                    document.getElementById("view_keterangan").style.display = "none";
                    $('#cbStatusACC').change(function () {
                        StatusACC = $(this).val();
                        if(StatusACC == 'DISETUJUI'){
                            document.getElementById("view_sesuai").style.display = "none";
                            document.getElementById("view_keterangan").style.display = "none";
                        }else if(StatusACC == 'PENYESUAIAN'){
                            document.getElementById("view_sesuai").style.display = "block";
                            document.getElementById("view_keterangan").style.display = "block";
                        }else{
                            document.getElementById("view_sesuai").style.display = "none";
                            document.getElementById("view_keterangan").style.display = "block";
                        }
                    });
                } else {
                    alert('request failed');
                }
            }
        });
    });

    $(document).on('click', '#btSimpan', function () {
        DurasiACC = $("[name='txtDurasiACC']").val();
        console.log("Durasi Asli : "+DurasiAsli);
        console.log("Durasi ACC : "+DurasiACC);
        console.log("Status ACC : "+StatusACC);
        KeteranganACC = '';
        StatusACC = $("[name='cbStatusACC']").val(); 
        if(StatusACC == 'DISETUJUI'){
            DurasiACC = DurasiAsli;
        }else if(StatusACC == 'PENYESUAIAN'){
            DurasiACC = $("[name='txtDurasiACC']").val(); 
            KeteranganACC = $("[name='txtKeterangan']").val();
        }else{
            DurasiACC = 0;
            KeteranganACC = $("[name='txtKeterangan']").val();
        }
        var action = "UpdateData";
        $.ajax({
            url: "aktivitas_pegawai_aksi.php",
            method: 'POST',
            data: {
                action: action,
                KodePegawai: KodePegawai,
                KodeOPD: KodeOPD,
                KodeAktivitas: KodeAktivitas,
                DurasiKerjaACC: DurasiACC,
                StatusACC: StatusACC,
                KeteranganACC: KeteranganACC
            },
            success: function () {
                $("[name='txtDurasiACC']").val("0");
                $("[name='txtKeterangan']").val("");
                LoadData();
                $('#ModalVerifikasi').modal("toggle");

                SelectElement("cbStatusACC", "DISETUJUI");
                swal('Informasi' ,  'Berhasil verifikasi.' ,  'success');
            }
        });
    });

    $(document).on('click', '#btnCetak', function () {
        console.log("Klik");
        var Url = "KodeOPD="+KodeOPD+"&Bulan="+Bulan+"&Tahun="+Tahun+"&KodePegawai="+KodePegawai;
        window.open("aktivitas_pegawai_report.php?"+Url);
    });


    $(document).on('click', '#btnDokumen', function () {
        var KodeAktivitas = $(this).val();
        var action = "AmbilFoto";
        $.ajax({
            url: "aktivitas_pegawai_aksi.php",
            method: "POST",
            data: {action: action, KodePegawai: KodePegawai, KodeAktivitas: KodeAktivitas},
            dataType: 'json',
            success: function (data) {
                if(data != 500){
                    $('#image_preview').html(data);     
                }else{
                    var noimage = '<img id="previewing" src="noimage.png" />';
                    $('#image_preview').html(noimage);
                }           
                $('#previewing').attr('width', '400px');
                $('#previewing').attr('height', '400px');               
            }
        });

        $('#previewing').attr('width', '400px');
        $('#previewing').attr('height', '400px');   

        $('#ModalFoto').modal('show');
    });

    function SelectElement(id, valueToSelect)
    {    
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }
</script>
<style type="text/css">
h1
{
    text-align: center;
    background-color: #FEFFED;
    height: 70px;
    color: rgb(95, 89, 89);
    margin: 0 0 -29px 0;
    padding-top: 14px;
    border-radius: 10px 10px 0 0;
    font-size: 35px;
}
.main {
    position: absolute;
    top: 50px;
    left: 20%;
    width: 450px;
    height:530px;
    border: 2px solid gray;
    border-radius: 10px;
}
.main label{
    color: rgba(0, 0, 0, 0.71);
    margin-left: 60px;
}
/*#image_preview{
    position: absolute;
    font-size: 30px;
    left: 100px;
    top: 30px;
    width: 400px;
    height: 400px;
    text-align: center;
    line-height: 180px;
    font-weight: bold;
    color: #C0C0C0;
    background-color: #FFFFFF;
    overflow: auto;
    }*/
    .submit{
        font-size: 16px;
        background: linear-gradient(#ffbc00 5%, #ffdd7f 100%);
        border: 1px solid #e5a900;
        color: #4E4D4B;
        font-weight: bold;
        cursor: pointer;
        width: 300px;
        border-radius: 5px;
        padding: 10px 0;
        outline: none;
        margin-top: 20px;
        margin-left: 15%;
    }
    .submit:hover{
        background: linear-gradient(#ffdd7f 5%, #ffbc00 100%);
    }
    #file {
        color: red;
        padding: 5px;
        margin-top: 10px;
    }
    #message{
        position:absolute;
        top:120px;
        left:815px;
    }
    #success
    {
        color:green;
    }
    #invalid
    {
        color:red;
    }
    #line
    {
        margin-top: 274px;
    }
    #error
    {
        color:red;
    }
    #error_message
    {
        color:blue;
    }
    #loading
    {
        display:none;
        position:absolute;
        top:50px;
        left:850px;
        font-size:25px;
    }
</style>