<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		$KodeOPD = $_POST['KodeOPD'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = DataKehadiranPegawaiBulanan($conn, $KodeOPD, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if ($_POST["action"] == "DetailPegawai") {
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetAbsenPerPegawai($conn,$KodePegawai,$KodeOPD,$Bulan,$Tahun);
		echo json_encode($result);
	}
}

function GetDataAbsensi($conn, $KodeOPD, $Bulan, $Tahun){
	$result = '';
	$sql = "
	SELECT peg.KodePegawai, peg.NIP, peg.NamaPegawai, peg.Pangkat, peg.Golongan, peg.Alamat, peg.PokokTPP, peg.KodeJabatan,
	(SELECT COUNT(apeg.IsHadir) AS JmlAlpa 
	FROM absensipegawai apeg
	INNER JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE apeg.IsHadir = '0' 
	AND apeg.KetTidakHadir = 'ALPA' 
	AND MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD) 
	AS JmlAlpa,

	(SELECT COUNT(apeg.IsHadir) AS JmlSakit 
	FROM absensipegawai apeg 
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE apeg.IsHadir = '0' 
	AND apeg.KetTidakHadir = 'SAKIT' 
	AND MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD) 
	AS JmlSakit,

	(SELECT COUNT(apeg.IsHadir) AS JmlCuti 
	FROM absensipegawai apeg 
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE apeg.IsHadir = '0' 
	AND apeg.KetTidakHadir = 'CUTI' 
	AND MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD) 
	AS JmlCuti,

	(SELECT COUNT(apeg.IsHadir) AS JmlAbsen 
	FROM absensipegawai apeg 
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE apeg.IsHadir = '0' 
	AND MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlAbsen,

	(SELECT COUNT(apeg.IsSenam) AS JmlSenam 
	FROM absensipegawai apeg 
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE apeg.IsSenam = '1' 
	AND MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlSenam,

	(SELECT COUNT(apeg.IsApel) AS JmlApel 
	FROM absensipegawai apeg 
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE apeg.IsApel = '1' 
	AND MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlApel,

	(SELECT IFNULL(SUM(apeg.TL1),'0')*1 AS JmlTL1 
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlTL1,

	(SELECT IFNULL(SUM(apeg.TL2),'0')*2 AS JmlTL2
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlTL2,

	(SELECT IFNULL(SUM(apeg.TL3),'0')*3 AS JmlTL3 
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlTL3,

	(SELECT IFNULL(SUM(apeg.PA1),'0')*1 AS JmlPA1 
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlPA1,

	(SELECT IFNULL(SUM(apeg.PA2),'0')*2 AS JmlPA2
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlPA2,

	(SELECT IFNULL(SUM(apeg.PA3),'0')*3 AS JmlPA3
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlPA3,

	(SELECT IFNULL(SUM(apeg.LF1),'0')*3 AS JmlLF1
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlLF1,

	(SELECT IFNULL(SUM(apeg.LF2),'0')*3 AS JmlLF2
	FROM absensipegawai apeg
	LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
	WHERE MONTH(apeg.Tanggal) = '$Bulan' 
	AND YEAR(apeg.Tanggal) = '$Tahun' 
	AND apeg.KodePegawai = peg.KodePegawai
	AND p.KodeOPD = peg.KodeOPD)  
	AS JmlLF2, peg.KodePegawai, peg.NamaPegawai, peg.NIP
	FROM mstpegawai peg WHERE peg.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th rowspan="2" width="30px">No</th>
		<th class="text-center" rowspan="2">Nama NIP</th>
		<th class="text-center" colspan="4">Tidak Hadir</th>
		<th class="text-center" colspan="10">Pelanggaran</th>
		<th class="text-center" rowspan="2">Aksi</th>
		</tr>
		<tr>
		<th>Alpa</th>
		<th>Sakit</th>
		<th>Cuti</th>
		<th>Jml</th>
		<th>TL1</th>
		<th>TL2</th>
		<th>TL3</th>
		<th>PA1</th>
		<th>PA2</th>
		<th>PA3</th>
		<th>LF1</th>
		<th>LF2</th>
		<th>TA</th>
		<th>TS</th>
		</tr>
		</thead>
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$url = "KodePegawai=" . $row['KodePegawai'] . "&Bulan=" . $Bulan. "&Tahun=" . $Tahun;
			$output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . $row['NamaPegawai'] . '</td>
			<td>' . $row['JmlAlpa'] . '</td>
			<td>' . $row['JmlSakit'] . '</td>
			<td>' . $row['JmlCuti'] . '</td>
			<td>' . $row['JmlAbsen'] . '</td>
			<td>' . $row['JmlTL1'] . '</td>
			<td>' . $row['JmlTL2'] . '</td>
			<td>' . $row['JmlTL3'] . '</td>
			<td>' . $row['JmlPA1'] . '</td>
			<td>' . $row['JmlPA2'] . '</td>
			<td>' . $row['JmlPA3'] . '</td>
			<td>' . $row['JmlLF1'] . '</td>
			<td>' . $row['JmlLF2'] . '</td>
			<td>' . $row['JmlApel'] . '</td>
			<td>' . $row['JmlSenam'] . '</td>
			<td class="text-center">			
			<button type="button" value = ' . $url . ' id="btnDetail" name="btnDetail" class="btn btn-warning"><span class="icon icon-pencil-case" aria-hidden="true"></span></button>
			</td>
			</tr>';
		}
		$output .= '</tbody>
		</table>';
		$result['response'] = 200;
		$result['HtmlTabel'] = $output;
	}else{
		$result['response'] = 500;
	}
	return $result;
}

function GetAbsenPerPegawai($conn,$KodePegawai,$KodeOPD,$Bulan,$Tahun){
	$sql_peg = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.NamaJabatan 
	FROM mstpegawai p 
	LEFT JOIN mstjabatan j ON p.KodeOPD = j.KodeOPD AND p.KodeJabatan = j.KodeJabatan
	WHERE p.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD'";
	$res_peg = $conn->query($sql_peg);
	if($res_peg){
		while ($row_peg = $res_peg->fetch_assoc()) {
			# code...
			$result['DataPegawai'] = $row_peg;
		}
		$sql_abs = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, ap.KetTidakHadir, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, ap.DokumenPendukung
		FROM absensipegawai ap 
		INNER JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		WHERE ap.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun'
		ORDER BY ap.Tanggal ASC";
		$res_abs = $conn->query($sql_abs);
		if($res_abs){
			$result['response'] = 200;
			$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata" style="font-size: 12px;style="width:100%;"">
			<thead>
			<tr>
			<th rowspan="2" width="30px">No</th>
			<th rowspan="2" style="width:10%;">Tanggal</th>
			<th rowspan="2" style="width:10%;" class="text-center">Hadir</th>
			<th rowspan="2" style="width:10%;" class="text-center">Tidak Hadir</th>
			<th rowspan="2" style="width:10%;" class="text-center">Keterangan</th>
			<th colspan="10" style="width:10%;" class="text-center">Pelanggaran</th>
			</tr>
			<tr>
			<th class="text-center">TL1</th>
			<th class="text-center">TL2</th>
			<th class="text-center">TL3</th>
			<th class="text-center">PA1</th>
			<th class="text-center">PA2</th>
			<th class="text-center">PA3</th>
			<th class="text-center">LF1</th>
			<th class="text-center">LF2</th>
			<th class="text-center">TA</th>
			<th class="text-center">TS</th>
			</tr>
			</thead>
			<tbody>';
			$no = 1;
			while ($row = $res_abs->fetch_assoc()) {
				# code...
				$output .= '<tr>
				<td class="text-center">' . $no++ . '</td>
				<td>' . date('d - m - Y', strtotime($row['Tanggal'])) . '</td>';
				if($row['IsHadir'] == 1){
					$output .= '<td class="text-center">&#10004;</td><td></td><td class="text-center">-</td>';
				}else{
					$output .= '<td></td><td class="text-center">&#10004;</td><td class="text-center">' . $row['KetTidakHadir'] . '</td>';
				}
				$TidakApel = $row['IsApel'] > 0 ? '&#10004;' : '';
				$TidakSenam = $row['IsSenam'] > 0 ? '&#10004;' : '';
				$TL1 = $row['TL1'] > 0 ? '&#10004;' : '';
				$TL2 = $row['TL2'] > 0 ? '&#10004;' : '';
				$TL3 = $row['TL3'] > 0 ? '&#10004;' : '';
				$PA1 = $row['PA1'] > 0 ? '&#10004;' : '';
				$PA2 = $row['PA2'] > 0 ? '&#10004;' : '';
				$PA3 = $row['PA3'] > 0 ? '&#10004;' : '';
				$LF1 = $row['LF1'] > 0 ? '&#10004;' : '';
				$LF2 = $row['LF2'] > 0 ? '&#10004;' : '';

				$output .= '<td>' . $TL1 . '</td>
				<td class="text-center">' . $TL2 . '</td>
				<td class="text-center">' . $TL3 . '</td>
				<td class="text-center">' . $PA1 . '</td>
				<td class="text-center">' . $PA2 . '</td>
				<td class="text-center">' . $PA3 . '</td>
				<td class="text-center">' . $LF1 . '</td>
				<td class="text-center">' . $LF2 . '</td>
				<td class="text-center">' . $TidakApel .'</td>
				<td class="text-center">' . $TidakSenam . '</td>
				</tr>';
			}
			$output .= '</tbody>
			</table>';
			$result['DataHtml'] = $output;
		}else{
			$result['response'] = 500;
		}
	}else{
		$result['response'] = 500;
	}
	return $result;
}

function DataKehadiranPegawaiBulanan($conn, $KodeOPD, $Bulan, $Tahun){
	$JmlHariEfektif = 0;
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir,(tb.JmlSakit + tb.JmlCuti + tb.JmlAlpha) AS JmlAbsen, tb.JmlSenam, tb.JmlApel, ((tb.JmlSenam + tb.JmlApel) * 3) AS JmlAS, (tb.JmlTL1 * 1) AS JmlTL1, (tb.JmlTL2 * 2) AS JmlTL2, (tb.JmlTL3 * 3) AS JmlTL3, (tb.JmlPA1 * 1) AS JmlPA1, (tb.JmlPA2 * 2) AS JmlPA2, (tb.JmlPA3 * 3) AS JmlPA3, (tb.JmlLF1 * 3) AS JmlLF1, (tb.JmlLF2 * 3) AS JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun";
	$res = $conn->query($sql);
	if($res){
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th rowspan="2" width="30px">No</th>
		<th class="text-center" rowspan="2">Nama NIP</th>
		<th class="text-center" colspan="4">Tidak Hadir</th>
		<th class="text-center" colspan="10">Pelanggaran</th>
		<th class="text-center" rowspan="2">Jml Hadir</th>
		<th class="text-center" rowspan="2">Aksi</th>
		</tr>
		<tr>
		<th>Alpa</th>
		<th>Sakit</th>
		<th>Cuti</th>
		<th>Jml</th>
		<th>TL1</th>
		<th>TL2</th>
		<th>TL3</th>
		<th>PA1</th>
		<th>PA2</th>
		<th>PA3</th>
		<th>LF1</th>
		<th>LF2</th>
		<th>AS</th>		
		<th>Jml</th>
		</tr>
		</thead>
		<tbody>';
		$no = 1;	
		while ($row = $res->fetch_assoc()) {
    		# code...
    		$JmlHariEfektif = $row['JmlHariEfektif'];
    		$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'] - $row['JmlSakit'] - $row['JmlCuti'];
    		$JmlPel = $row['JmlTL1']+$row['JmlTL2']+$row['JmlTL3']+$row['JmlPA1']+$row['JmlPA2']+$row['JmlPA3']+$row['JmlLF1']+$row['JmlLF2']+$row['JmlAS'];
			$url = "KodePegawai=" . $row['KodePegawai'] . "&Bulan=" . $Bulan. "&Tahun=" . $Tahun;
			$output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . $row['NamaPegawai'] . '</td>
			<td>' . $row['JmlAlpha'] . '</td>
			<td>' . $row['JmlSakit'] . '</td>
			<td>' . $row['JmlCuti'] . '</td>
			<td>' . $row['JmlAbsen'] . ' Hari</td>
			<td>' . $row['JmlTL1'] . '%</td>
			<td>' . $row['JmlTL2'] . '%</td>
			<td>' . $row['JmlTL3'] . '%</td>
			<td>' . $row['JmlPA1'] . '%</td>
			<td>' . $row['JmlPA2'] . '%</td>
			<td>' . $row['JmlPA3'] . '%</td>
			<td>' . $row['JmlLF1'] . '%</td>
			<td>' . $row['JmlLF2'] . '%</td>
			<td>' . $row['JmlAS'] . '%</td>
			<td>' . $JmlPel . '%</td>
			<td>' . $JmlHadir . ' Hari</td>
			<td class="text-center">			
			<button type="button" value = ' . $url . ' id="btnDetail" name="btnDetail" class="btn btn-warning"><span class="fa fa-arrow-right" aria-hidden="true"></span></button>
			</td>
			</tr>';
		}
		$output .= '</tbody>
		</table>';
		$result['response'] = 200;
		$result['HtmlTabel'] = $output;
		$result['JmlHariEfektif'] = $JmlHariEfektif;
	}else{
		$result['response'] = 500;
	}
	return $result;
}