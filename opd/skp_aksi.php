<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';


if (isset($_POST["action"])) {
	if ($_POST['action'] == 'InsertData') {
		$KodePegawai = $_POST['KodePegawai'];
		$Tahun = $_POST['Tahun'];
		$NilaiSKP = $_POST['NilaiSKP'];
		$result = InsertDataSKP($conn, $KodePegawai, $Tahun, $NilaiSKP);
		echo json_encode($result);
	}

	if($_POST["action"] == "LoadData"){
		$requestData = $_REQUEST;
		$Tahun = $_POST['Tahun'];
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetDataSKP($conn, $requestData, $Tahun, $KodeOPD);
		echo json_encode($result);
	}

	if($_POST['action'] == 'DataPegawai'){
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetDataPegawai($conn, $KodeOPD);
		echo json_encode($result);
	}

	if($_POST['action'] == 'DeleteData'){
		$KodePegawai = $_POST['KodePegawai'];
		$Tahun = $_POST['Tahun'];
		$result = DeleteDataSKP($conn, $KodePegawai, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'AmbilData'){
		$KodePegawai = $_POST['KodePegawai'];
		$Tahun = $_POST['Tahun'];
		$result = GetOneData($conn, $KodePegawai, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'UpdateData'){
		$KodePegawai = $_POST['KodePegawai'];
		$Tahun = $_POST['Tahun'];
		$NilaiSKP = $_POST['NilaiSKP'];
		$result = UpdateDataSKP($conn, $KodePegawai, $Tahun, $NilaiSKP);
		echo json_encode($result);
	}
}

function UpdateDataSKP($conn, $KodePegawai, $Tahun, $NilaiSKP){
	$PersenSKP = NilaiSKP($NilaiSKP);
	$sql = "UPDATE skppegawai SET NilaiSKP = '$NilaiSKP', PersenSKP = '$PersenSKP' WHERE Tahun = '$Tahun' AND KodePegawai = '$KodePegawai';";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'UPDATE', 'Mengubah data SKP pegawai '.$KodePegawai. ' Tahun '.$Tahun, $_SESSION['KodeUser']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function GetOneData($conn, $KodePegawai, $Tahun){
	$sql = "SELECT s.KodePegawai, s.Tahun, s.PersenSKP, s.NilaiSKP, p.NamaPegawai, p.NIP, j.NamaJabatan 
	FROM skppegawai s
	LEFT JOIN mstpegawai p ON p.KodePegawai = s.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE s.Tahun = '$Tahun' AND s.KodePegawai = '$KodePegawai'";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_assoc($res);
		$row['response'] = 200;
	}else{
		$row['response'] = 500;
	}
	return $row;
}

function InsertDataSKP($conn, $KodePegawai, $Tahun, $NilaiSKP){
	$sql1 = "SELECT COUNT(KodePegawai) AS Jumlah FROM skppegawai WHERE KodePegawai = '$KodePegawai' AND Tahun = '$Tahun'";
	$res1 = $conn->query($sql1);
	$row = mysqli_fetch_array($res1);
	if($row['Jumlah'] < 1){
		$PersenSKP = NilaiSKP($NilaiSKP);
		$sql = "INSERT INTO skppegawai (KodePegawai, Tahun, PersenSKP, NilaiSKP) VALUES ('$KodePegawai', '$Tahun', '$PersenSKP', '$NilaiSKP')";
		$res = $conn->query($sql);
		if($res){
			session_start();
			InsertLog($conn, 'INSERT', 'Menambah data SKP pegawai '.$KodePegawai. ' Tahun '.$Tahun, $_SESSION['KodeUser']);
			return array('response' => 200);
		}else{
			return array('response' => 500);
		}	
	}else{
		return array('response' => 403);
	}
}

function GetDataSKP($conn, $requestData, $Tahun, $KodeOPD){
	$columns = array( 
		0 => 'Tahun',
		1 => 'NIP', 
		2 => 'NamaPegawai',
		3 => 'NamaJabatan',
		4 => 'NilaiSKP'
	);

	$sql = "SELECT s.KodePegawai, s.Tahun, s.PersenSKP, s.NilaiSKP, p.NamaPegawai, p.NIP, j.NamaJabatan ";
	$sql.="FROM skppegawai s
	LEFT JOIN mstpegawai p ON p.KodePegawai = s.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD'";
	$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
	$totalFiltered = $totalData;


	if( !empty($requestData['search']['value']) ) {
		$sql = "SELECT s.KodePegawai, s.Tahun, s.PersenSKP, s.NilaiSKP, p.NamaPegawai, p.NIP, j.NamaJabatan ";
		$sql.="FROM skppegawai s
		LEFT JOIN mstpegawai p ON p.KodePegawai = s.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		";
		$sql.="WHERE IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD' AND p.NamaPegawai LIKE '%".$requestData['search']['value']."%' ";
		$sql.="OR IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD' AND s.Tahun LIKE '%".$requestData['search']['value']."%' ";
		$sql.="OR IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD' AND j.NamaJabatan LIKE '%".$requestData['search']['value']."%' ";
		$sql.="OR IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD' AND s.PersenSKP LIKE '%".$requestData['search']['value']."%' ";		
		$sql.="OR IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD' AND s.NilaiSKP LIKE '%".$requestData['search']['value']."%' ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
		$totalFiltered = mysqli_num_rows($query);
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");	
	} else {
		$sql = "SELECT s.KodePegawai, s.Tahun, s.PersenSKP, s.NilaiSKP, p.NamaPegawai, p.NIP, j.NamaJabatan ";
		$sql.="FROM skppegawai s
		LEFT JOIN mstpegawai p ON p.KodePegawai = s.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE IF(LENGTH('$Tahun')> 0, s.Tahun = '$Tahun', TRUE) AND p.KodeOPD = '$KodeOPD' 
		";
		$sql.="ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	}

	$data = array();
	$no = 1;
	while( $row=mysqli_fetch_array($query) ) {
		$nestedData=array();
		$Aksi = '<button value = "'.$row['KodePegawai'].'" data-value = "'.$row['Tahun'].'" name="btnUpdate" id="btnUpdate" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
		<button value="'.$row['KodePegawai'].'" data-value = "'.$row['Tahun'].'" name="delete" id="btnDelete" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></button>';
		$nestedData[] = $row['Tahun'];
		$nestedData[] = $row['NIP'];
		$nestedData[] = $row['NamaPegawai'];
		$nestedData[] = $row['NamaJabatan'];
		$nestedData[] = $row['NilaiSKP'];	
		$nestedData[] = $Aksi;		

		$data[] = $nestedData;

	}



	$json_data = array(
		"draw"            => intval( $requestData['draw'] ),
		"recordsTotal"    => intval( $totalData ),
		"recordsFiltered" => intval( $totalFiltered ),
		"data"            => $data   		);

	return $json_data;
}

function GetDataPegawai($conn, $KodeOPD){
	$arrayOutput = '';
	$output = '';
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, j.NamaJabatan 
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE p.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Nama Pegawai</label>';
		$output .= "<select class='form-control' id='txtKodePegawaiAdd' name='txtKodePegawaiAdd'>";
		$output .= "<option value=''> - Pilih Pegawai - </option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option value='" . $row['KodePegawai'] . "'>" . $row['NamaPegawai'] . " (".$row['NIP'].")</option>";
		}
		$output .= '</select>';
		$arrayOutput['DataPegawai'] = $output;
		$arrayOutput['response'] = 200;
		return $arrayOutput;
	}else{
		return array('response' => 500);
	}
}

function DeleteDataSKP($conn, $KodePegawai, $Tahun){
	$sql = "DELETE FROM skppegawai WHERE KodePegawai = '$KodePegawai' AND Tahun = '$Tahun'";
	$res = $conn->query($sql);
	if($res){		
		session_start();
		InsertLog($conn, 'DELETE', 'Menghapus data SKP pegawai '.$KodePegawai. ' Tahun '.$Tahun, $_SESSION['KodeUser']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}
?>