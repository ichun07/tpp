<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];

require "../komponen/excel_reader.php";
include_once '../config/koneksi.php';

if(isset($_POST['submit'])){
    $target = basename($_FILES['filepegawaiall']['name']) ;
    move_uploaded_file($_FILES['filepegawaiall']['tmp_name'], $target);
    
    $data = new Spreadsheet_Excel_Reader($_FILES['filepegawaiall']['name'],false);
    
//    menghitung jumlah baris file xls
    $baris = $data->rowcount($sheet_index=0);

//    import data excel mulai baris ke-2 (karena tabel xls ada header pada baris 1)
    for ($i=2; $i<=$baris; $i++)
    {

        $barisreal = $baris-1;
        $k = $i-1;

// menghitung persentase progress
        $percent = intval($k/$barisreal * 100)."%";
        echo '<script language="javascript">
        document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.'; background-color:lightblue\">&nbsp;</div>";
        </script>';
//       membaca data (kolom ke-1 sd terakhir)
        $Tanggal = $data->val($i, 1);
        $IsHadir = $data->val($i, 2);
        $KodePegawai = $data->val($i, 3);
        $KetTidakHadir = $data->val($i, 4);
        $IsSenam = $data->val($i, 5);
        $IsApel = $data->val($i, 6);
        $TL1 = $data->val($i, 7);
        $TL2 = $data->val($i, 8);
        $TL3 = $data->val($i, 9);
        $PA1 = $data->val($i, 10);
        $PA2 = $data->val($i, 11);
        $PA3 = $data->val($i, 12);
        $LF1 = $data->val($i, 13);
        $LF2 = $data->val($i, 13);
        $DokumenPendukung = $data->val($i, 14);

//      setelah data dibaca, masukkan ke tabel pegawai sql
        $query = "INSERT INTO absensipegawai (Tanggal, IsHadir, KodePegawai, KetTidakHadir, IsSenam, IsApel, TL1, TL2, TL3, PA1, PA2, PA3, LF1, LF2, DokumenPendukung) VALUES ('$Tanggal', b'".$IsHadir."', '$KodePegawai', '$KetTidakHadir', b'".$IsSenam."', b'".$IsApel."', '$TL1', '$TL2', '$TL3', '$PA1', '$PA2', '$PA3', '$LF1', '$LF2', '$DokumenPendukung')";
        $hasil = $conn->query($query);
    }

    if(!$hasil){
//          jika import gagal
      die(mysqli_error($conn));
  }else{
    $div = "<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Mengubah Profil!</strong></div>";
    echo '<script type="text/javascript">
    $("#sukses").html("'.$div.'");
    </script>';
}

//    hapus file xls yang udah dibaca
unlink($_FILES['filepegawaiall']['name']);
}
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Absensi Pegawai</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>        
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../opd/absensi_pegawai_view.php'" class="btn btn-success">
                        Kembali
                    </button>
                    <br></br>
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Tambah Jabatan</h3>
                        </div>
                        <div class="card-body">
                            <form name="myForm" id="myForm" onSubmit="return validateForm()" action="absensi_pegawai_import.php" method="post" enctype="multipart/form-data">
                                <input type="file" id="filepegawaiall" name="filepegawaiall"/>
                                <input class="btn btn-primary" type="submit" name="submit" value="Import" />
                            </form>
                        </div>
                    </div>
                </br></br>
                    <div id="progress" style="border:1px solid #ccc;"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    var KodeOPD;
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
    });

    function validateForm()
    {
        function hasExtension(inputID, exts) {
            var fileName = document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
        }

        if(!hasExtension('filepegawaiall', ['.xls'])){
            alert("Hanya file XLS (Excel 2003) yang diijinkan.");
            return false;
        }
    }
    
</script>