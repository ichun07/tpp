<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Capaian OPD</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../opd/capaian_opd_view.php'" class="btn btn-success">
                        Kembali
                    </button>
                    <br></br>
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Tambah Data Capaian</h3>
                        </div>
                        <div class="card-body">
                            <form method="post">
                                <?php 
                                date_default_timezone_set('Asia/Jakarta');
                                $year = date('Y');
                                $month = date('m');
                                ?>   
                                <div class="form-group">
                                    <label class="form-control-label">Tahun</label>
                                    <input disabled type="text" placeholder="Tahun" class="form-control" name="txtTahun" value="<?php echo $year; ?>">
                                </div>                                                         
                                <div class="form-group">
                                    <label class="form-control-label">Triwulan</label>
                                </br>
                                <select id="cb_triwulan" name="cb_triwulan" class="form-control">             
                                    <option value="1" <?php if (3 >= (int)$month and (int)$month >= 1) echo ' selected="selected"'; ?>>Triwulan 1</option>
                                    <option value="2"<?php if (6 >= (int)$month and (int)$month >= 4) echo ' selected="selected"'; ?>>Triwulan 2</option>
                                    <option value="3"<?php if (9 >= (int)$month and (int)$month >= 7) echo ' selected="selected"'; ?>>Triwulan 3</option>
                                    <option value="4"<?php if (12 >= (int)$month and (int)$month >= 10) echo ' selected="selected"'; ?>>Triwulan 4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Realisasi Anggaran (%)</label>
                                <input type="number" onchange="handleChange(this);" placeholder="Realisasi Anggaran" class="form-control"
                                name="txtRealisasiAnggaran" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Rata Rata Capaian Output (%)</label>
                                <input type="number" onchange="handleChange(this);" placeholder="Rata Rata Capaian Output" class="form-control"
                                name="txtNilaiOutputKegiatan" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Evalusasi SAKIP</label>
                            </br>
                            <select id="txtEvalusasiSAKIP" name="txtEvalusasiSAKIP" class="form-control">             
                                <option selected="selected" value="AA">AA</option>
                                <option value="A">A</option>
                                <option value="BB">BB</option>
                                <option value="B">B</option>
                                <option value="CC">CC</option>
                                <option value="C">C</option>
                                <option value="DD">DD</option>
                                <option value="D">D</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    var KodeOPD;
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
    });

    function handleChange(input) {
        if (input.value < 0) input.value = 0;
        if (input.value > 100) input.value = 100;
    }
    function simpanData() {
        var Tahun = $("[name='txtTahun']").val();
        var Triwulan = $("[name='cb_triwulan']").val();
        var RealisasiAnggaran = $("[name='txtRealisasiAnggaran']").val();
        var NilaiOutputKegiatan = $("[name='txtNilaiOutputKegiatan']").val();
        var EvalusasiSAKIP = $("[name='txtEvalusasiSAKIP']").val();
        console.log("EvalusasiSAKIP : "+EvalusasiSAKIP);
        var action = "InsertData";
        $.ajax({
            url: "capaian_opd_aksi.php",
            method: "POST",
            data: {
                Tahun: Tahun,
                Triwulan: Triwulan,
                RealisasiAnggaran: RealisasiAnggaran,
                NilaiOutputKegiatan: NilaiOutputKegiatan,
                EvalusasiSAKIP: EvalusasiSAKIP,
                KodeOPD: KodeOPD,
                action: action
            },
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    $("[name='txtRealisasiAnggaran']").val("");
                    $("[name='txtNilaiOutputKegiatan']").val("");
                    $('#cb_triwulan').prop('selected', function() {
                        return this.defaultSelected;
                    });
                    $('#txtEvalusasiSAKIP').prop('selected', function() {
                        return this.defaultSelected;
                    });
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/capaian_opd_view.php'>lihat semua data</a>.</div>");
                } else {
                    alert('Insert Data failed');
                }

            }
        });
    }
</script>