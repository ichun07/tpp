<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';
//MENIT AKTIVITAS ADALAH WAKTU PENYELESAIAN YANG DISETUJUI OLEH ATASAN
//MENIT KERJA EFEKTIF DALAM 1 HARI ADALAH 300 MENIT

if (isset($_POST["action"])) {
    if ($_POST["action"] == "TppProduktifitas") {
        $KodeOPD = $_POST['KodeOPD'];
        $Bulan = $_POST['Bulan'];
        $Tahun = $_POST['Tahun'];
        $result = GetTppProduktifitas($conn, $KodeOPD, $Bulan, $Tahun);
        echo json_encode($result);
    }

    if($_POST["action"] == "TppUangMakan"){
        $KodeOPD = $_POST['KodeOPD'];
        $Bulan = $_POST['Bulan'];
        $Tahun = $_POST['Tahun'];
        $result = DataTppUangMakan($conn, $KodeOPD, $Bulan, $Tahun);
        echo json_encode($result);
    }

    if($_POST["action"] == "RekapAbsen"){
        $KodeOPD = $_POST['KodeOPD'];
        $Bulan = $_POST['Bulan'];
        $Tahun = $_POST['Tahun'];
        $JmlHariEfektif = $_POST['JmlHariEfektif'];
        $result = RekapAbsensiBulanan($conn, $KodeOPD, $Bulan, $Tahun, $JmlHariEfektif);
        echo json_encode($result);
    }
}

function GetTppProduktifitas($conn, $KodeOPD, $Bulan, $Tahun)
{
    $sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan != 'DITOLAK') AS MenitAktivitas
    FROM tppbulanan tb
    LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
    LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
    WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
    ORDER BY tb.Bulan, tb.Tahun";
    $res = $conn->query($sql);
    if ($res) {
        $output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
        <thead>
        <tr>
        <th rowspan="2" width="30px" class="text-center">No</th>
        <th rowspan="2" class="text-center">Nama / NIP</th>
        <th rowspan="2" class="text-center">Jabatan</th>
        <th rowspan="2" width="30px" class="text-center">Kelas Jabatan</th>
        <th rowspan="2" class="text-center">Harga Jabatan</th>
        <th rowspan="2" class="text-center">TPP Pokok</th>
        <th rowspan="2" width="30px" class="text-center">Tingkat Kehadiran</th>
        <th rowspan="2" class="text-center">Aktivitas Harian</th>
        <th colspan="2" class="text-center">TPP Produktivitas</th>
        <th rowspan="2" class="text-center">PPH 21%</th>
        <th rowspan="2" class="text-center">Potongan Pajak</th>
        <th rowspan="2" class="text-center">TPP Diterima</th>
        </tr>
        <tr>
        <th width="30px" class="text-center">%</th>
        <th class="text-center">Rp.</th>
        </tr>
        </thead>		
        <tbody>';
        $no = 1;
        while ($row = $res->fetch_assoc()) {
            # code...
            $JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
            $PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
            //NilaiTingkatKehadiran adalah ProsenKehadiran - ProsenPelanggaran
            $NilaiTingkatKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $KodeOPD, $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);
            $MenitKerjaEfektif = 300 * $row['JmlHariEfektif'];
            //Jika Staff atau Fungsional
            if($row['TipeJabatan'] == 'FUNGSIONAL'){
                $NilaiAktivitasKerjaPegawai = NilaiAktivitasKerjaPegawaiStaff($row['MenitAktivitas'], $MenitKerjaEfektif);
            }else{
            // Jika Pimpinan
                $NilaiAktivitasKerjaPegawai = HitungNilaiAktivitasPegawaiTerpimpin($conn, $row['MenitAktivitas'], $MenitKerjaEfektif,$KodeOPD,$row['KodeJabatan'],$KodeOPD,$Bulan,$Tahun);
            }            
            $RpTPP = TTPProduktivitas($row['PokokTPPPegawai'], $NilaiTingkatKehadiran, $NilaiAktivitasKerjaPegawai);
            $PersenTPP = $row['PokokTPPPegawai'] == 0 ? 0 : ($RpTPP / $row['PokokTPPPegawai'] * 100);
            $PPNTPP = $RpTPP * 0.21;
            $TotalTPPBersih = $RpTPP - $PPNTPP;
            $output .= '<tr>
            <td width="30px">' . $no++ . '</td>
            <td>' . $row['NamaPegawai'] . '</td>
            <td>' . $row['NamaJabatan'] . '</td>
            <td width="30px">' . $row['KelasJabatan'] . '</td>
            <td class="text-right">' . number_format($row['HargaJabatan']) . '</td>
            <td class="text-right">' . number_format($row['PokokTPPPegawai']) . '</td>
            <td class="text-right">' . number_format($PersenHadir,2) . '</td>
            <td class="text-right">' . number_format($NilaiAktivitasKerjaPegawai,2) . '</td>
            <td class="text-right">' . number_format($PersenTPP,2) . '</td>
            <td class="text-right">' . number_format($RpTPP) . '</td>
            <td>21%</td>
            <td class="text-right">' . number_format($PPNTPP) . '</td>
            <td class="text-right">' . number_format($TotalTPPBersih) . '</td>
            </tr>';
        }
        $output .= '</tbody>
        </table>';
        return array('response' => 200, 'DataHtml' => $output);
    } else {
        return array('response' => 500);
    }
}

function DataTppUangMakan($conn, $KodeOPD, $Bulan, $Tahun){
    $sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai,(SELECT SettingValue FROM sistemsetting WHERE SettingName = 'UANG_MAKAN' AND IsAnjab = 0) AS PokokUangMakan, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, (tb.JmlHadir * (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'UANG_MAKAN' AND IsAnjab = 0)) AS JmlUangMakan
    FROM tppbulanan tb
    LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
    LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
    WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
    ORDER BY tb.Bulan, tb.Tahun";
    $res = $conn->query($sql);
    if($res){
        $output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
        <thead>
        <tr>
        <th width="30px">No</th>
        <th>Nama / NIP</th>
        <th width="30px">Pangkat</th>
        <th width="30px">Uang Makan Harian</th>
        <th width="30px">Jumlah Kehadiran</th>
        <th width="30px">Jumlah Uang Makan</th>
        <th width="30px">PPH 21%</th>
        <th width="30px">Potongan Pajak</th>
        <th width="30px">TPP Diterima</th>
        </tr>
        </thead>  
        <tbody>';
        $no = 1;
        while ($row = $res->fetch_assoc()) {
            # code...
            $PotonganPPH = $row['JmlUangMakan'] * 21/100;
            $TppDiterima = $row['JmlUangMakan'] - $PotonganPPH;
            $output .= '<tr>
            <td>' . $no++ . '</td>
            <td>' . $row['NamaPegawai'] . '</td>
            <td>' . $row['Pangkat'] . '</td>
            <td class="text-right">' . number_format($row['PokokUangMakan']) . '</td>
            <td class="text-right">' . $row['JmlHadir'] . '</td>
            <td class="text-right">' . number_format($row['JmlUangMakan']) . '</td>
            <td class="text-right">21%</td>
            <td class="text-right">' . number_format($PotonganPPH) . '</td>
            <td class="text-right">' . number_format($TppDiterima) . '</td>
            </tr>';
        }
        $output .= '</tbody>
        </table>';
        return array('response' => 200, 'DataHtml' => $output);
    }else{
        return array('response' => 500);
    }
}

function RekapAbsensiBulanan($conn, $KodeOPD, $Bulan, $Tahun, $JmlHariEfektif){
    $res_insert = '';
    $sql_select = "SELECT peg.KodePegawai,peg.PokokTPP,
    (SELECT COUNT(apeg.IsHadir) AS JmlAlpa 
    FROM absensipegawai apeg
    INNER JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsHadir = '0' 
    AND apeg.KetTidakHadir = 'ALPHA' 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD) 
    AS JmlAlpha,

    (SELECT COUNT(apeg.IsHadir) AS JmlSakit 
    FROM absensipegawai apeg 
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsHadir = '0' 
    AND apeg.KetTidakHadir = 'SAKIT' 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD) 
    AS JmlSakit,

    (SELECT COUNT(apeg.IsHadir) AS JmlCuti 
    FROM absensipegawai apeg 
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsHadir = '0' 
    AND apeg.KetTidakHadir = 'CUTI' 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD) 
    AS JmlCuti,

    (SELECT COUNT(apeg.IsHadir) AS JmlAbsen 
    FROM absensipegawai apeg 
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsHadir = '0' 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlAbsen,

    (SELECT COUNT(apeg.IsSenam) AS JmlSenam 
    FROM absensipegawai apeg 
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsSenam = '1' AND apeg.DokumenPendukung IS NULL 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlSenam,

    (SELECT COUNT(apeg.IsApel) AS JmlApel 
    FROM absensipegawai apeg 
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsApel = '1' AND apeg.DokumenPendukung IS NULL 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlApel,

    (SELECT COUNT(apeg.IsHadir) AS JmlHadir 
    FROM absensipegawai apeg 
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE apeg.IsHadir = '1' 
    AND MONTH(apeg.Tanggal) = '$Bulan' 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD) 
    AS JmlHadir,

    (SELECT IFNULL(SUM(apeg.TL1),'0') AS JmlTL1 
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlTL1,

    (SELECT IFNULL(SUM(apeg.TL2),'0') AS JmlTL2
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlTL2,

    (SELECT IFNULL(SUM(apeg.TL3),'0') AS JmlTL3 
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlTL3,

    (SELECT IFNULL(SUM(apeg.PA1),'0') AS JmlPA1 
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlPA1,

    (SELECT IFNULL(SUM(apeg.PA2),'0') AS JmlPA2
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlPA2,

    (SELECT IFNULL(SUM(apeg.PA3),'0') AS JmlPA3
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlPA3,

    (SELECT IFNULL(SUM(apeg.LF1),'0') AS JmlLF1
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlLF1,

    (SELECT IFNULL(SUM(apeg.LF2),'0') AS JmlLF2
    FROM absensipegawai apeg
    LEFT JOIN mstpegawai p ON apeg.KodePegawai = p.KodePegawai 
    WHERE MONTH(apeg.Tanggal) = '$Bulan' AND apeg.DokumenPendukung IS NULL 
    AND YEAR(apeg.Tanggal) = '$Tahun' 
    AND apeg.KodePegawai = peg.KodePegawai
    AND p.KodeOPD = peg.KodeOPD)  
    AS JmlLF2
    FROM mstpegawai peg 
    WHERE peg.KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql_select);
    if($res){
        while ($row = $res->fetch_assoc()) {
            $sql_insert = "INSERT INTO tppbulanan 
            (Tahun, Bulan, KodePegawai, PokokTPP, JmlHariEfektif, JmlSakit, JmlCuti, JmlAlpha, JmlHadir, JmlSenam, JmlApel, JmlTL1, JmlTL2, JmlTL3, JmlPA1, JmlPA2, JmlPA3, JmlLF1, JmlLF2)
            VALUES 
            ('".$Tahun."', '".$Bulan."', '".$row['KodePegawai']."', '".$row['PokokTPP']."', '".$JmlHariEfektif."', '".$row['JmlSakit']."', '".$row['JmlCuti']."', '".$row['JmlAlpha']."', '".$row['JmlHadir']."', '".$row['JmlSenam']."', '".$row['JmlApel']."', '".$row['JmlTL1']."', '".$row['JmlTL2']."', '".$row['JmlTL3']."', '".$row['JmlPA1']."', '".$row['JmlPA2']."', '".$row['JmlPA3']."', '".$row['JmlLF1']."', '".$row['JmlLF2']."')
            ON DUPLICATE KEY UPDATE
            PokokTPP = '".$row['PokokTPP']."', JmlHariEfektif = '".$JmlHariEfektif."', JmlSakit = '".$row['JmlSakit']."', JmlCuti = '".$row['JmlCuti']."', JmlAlpha = '".$row['JmlAlpha']."', JmlHadir = '".$row['JmlHadir']."', JmlSenam = '".$row['JmlSenam']."', JmlApel = '".$row['JmlApel']."', JmlTL1 = '".$row['JmlTL1']."', JmlTL2 = '".$row['JmlTL2']."', JmlTL3 = '".$row['JmlTL3']."', JmlPA1 = '".$row['JmlPA1']."', JmlPA2 = '".$row['JmlPA2']."', JmlPA3 = '".$row['JmlPA3']."', JmlLF1 = '".$row['JmlLF1']."', JmlLF2 = '".$row['JmlLF2']."'";
            $res_insert = $conn->query($sql_insert);            
        }
        if($res_insert){            
            session_start();
            InsertLog($conn, 'INSERT', 'Rekap absensi pegawai Bulan '.$Bulan.' Tahun '.$Tahun, $_SESSION['KodeUser']);
            return array('response' => 200);
        }else{
            return array('response' => 500);    
        }        
    }else{
        return array('response' => 500);
    }
}

?>