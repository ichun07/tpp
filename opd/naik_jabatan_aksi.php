<?php 
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {

	if ($_POST["action"] == "LoadData") {
		$KodePegawai = $_POST['KodePegawai'];
		$result = GetDataPegawai($conn, $KodePegawai);
		echo json_encode($result);
	}	

	if ($_POST["action"] == "LoadDataJabatan") {
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetOneJabatan($conn, $KodeJabatan, $KodeOPD);
		echo json_encode($result);
	}	

	if($_POST['action'] == 'NaikJabatan'){
		$KodePegawai = $_POST['KodePegawai'];
		$NoSKPengangkatan = $_POST['NoSKPengangkatan'];
		$TanggalPengangkatan = $_POST['TanggalPengangkatan'];
		$Keterangan = $_POST['Keterangan'];
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$PokokTPP = $_POST['PokokTPP'];
		$KodeJabatanLama = $_POST['KodeJabatanLama'];
		$result = NaikJabatanPegawai($conn, $KodePegawai, $NoSKPengangkatan, $TanggalPengangkatan, $TanggalPengangkatan, $Keterangan, $KodeJabatan, $KodeOPD, $PokokTPP, $KodeJabatanLama);
		echo json_encode($result);
	}
}

function GetDataPegawai($conn, $KodePegawai){
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.Alamat, p.PokokTPP, p.PokokUangMakan, p.KodeJabatan, p.KodeOPD, p.Username, p.Password, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE p.KodePegawai = '$KodePegawai'";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_array($res,MYSQLI_ASSOC);
		$row['response'] = 200;
		$row['CBJabatan'] = GetDataJabatan($conn, $row['KodeOPD'], $row['KodeJabatan']);
		return $row;
	}else{
		return array('response' => 500);
	}
}

function NaikJabatanPegawai($conn, $KodePegawai, $NoSKPengangkatan, $TanggalPengangkatan, $TanggalSampai, $Keterangan, $KodeJabatan, $KodeOPD, $PokokTPP, $KodeJabatanLama){

	$sql_awal = "SELECT COUNT(KodePegawai) AS Jumlah FROM mstpegawai WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
	$res_awal = $conn->query($sql_awal);
	if($res_awal){
		$result = mysqli_fetch_array($res_awal);
		if ($result['Jumlah'] > 0) {
			return array('response' => 403, 'error' => 'Jabatan sudah terisi!');
		} else {
			$KodeHistory = AutoKodeHistory($conn,$KodePegawai);
			$sql = "INSERT INTO historyjabatan (KodePegawai, KodeHistory, TanggalPengangkatan, NoSKPengangkatan, Keterangan, KodeJabatan, KodeOPD) VALUES ('$KodePegawai', '$KodeHistory', '$TanggalPengangkatan', '$NoSKPengangkatan', '$Keterangan', '$KodeJabatan', '$KodeOPD')";
			$res = $conn->query($sql);
			if($res){
				$sql1 = "UPDATE mstpegawai SET KodeJabatan = '$KodeJabatan', PokokTPP = '$PokokTPP' WHERE KodePegawai = '$KodePegawai'";
				$res1 = $conn->query($sql1);
				if($res1){
					$sql_up_jab = "UPDATE historyjabatan SET TanggalSampai = '$TanggalSampai' WHERE KodePegawai = '$KodePegawai' AND KodeJabatan = '$KodeJabatanLama' AND KodeOPD = '$KodeOPD'";
					$res_up = $conn->query($sql_up_jab);
					if($res_up){						
						session_start();
						InsertLog($conn, 'UPDATE', 'Ubah jabatan pegawai '.$KodePegawai, $_SESSION['KodeUser']);
						return array('response' => 200);
					}else{
						return array('response' => 500);	
					}			
				}else{
					return array('response' => 500);
				}
			}else{
				return array('response' => 500);
			}	
		}
	}
}

function AutoKodeHistory($conn,$KodePegawai){
	$sql = "SELECT RIGHT(KodeHistory,16) AS kode FROM historyjabatan WHERE KodePegawai = '$KodePegawai' ORDER BY KodeHistory DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
	return 'HTR-' . $bikin_kode;
}

function GetDataJabatan($conn, $KodeOPD, $KodeJabatan){
	$arrayOutput = '';
	$output = '';
	$sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD, o.NamaOPD 
	FROM mstjabatan j
	LEFT JOIN mstopd o
	ON o.KodeOPD = j.KodeOPD
	WHERE j.KodeOPD = '$KodeOPD' AND j.KodeJabatan != '$KodeJabatan'
	GROUP BY j.KodeJabatan, j.KodeOPD";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Jabatan</label>';
		$output .= "<select class='form-control' id='cbJabatan' name='cbJabatan'>";
		$output .= "<option value='0'> - Pilih Jabatan - </option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
		}
		$output .= '</select>';
		return $output;
	}else{
		return array(500);
	}
}

function GetOneJabatan($conn, $KodeJabatan, $KodeOPD){
	$sql = "SELECT mstjabatan.*,(SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan FROM mstjabatan WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD' LIMIT 0,1";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_array($res,MYSQLI_ASSOC);
		$row['response'] = 200;
		return $row;
	}else{
		return array('response' => 500);
	}
}