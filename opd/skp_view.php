<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];

date_default_timezone_set('Asia/Jakarta');
$year = date('Y');
$month = date('m');
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data SKP Pegawai</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<select class="form-control" id="cb_tahun" name="cb_tahun">
									<option selected="selected" value="">Semua</option>
									<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
									<?php
									for($i = $year-1; $i > $year-10; $i--){
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6 text-right">
							<button id="btnTambah" class="btn btn-primary">Tambah Data</button>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">				
							<div id="tabel_pegawai" class="table-responsive">
								<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
									<thead>
										<tr>
											<th width="50px">Tahun</th>
											<th>NIP</th>
											<th>Nama Pegawai</th>
											<th>Jabatan</th>
											<th width="70px">Nilai SKP</th>
											<th width="80px">Aksi</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Modal Popup untuk Tambah--> 
<div id="ModalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Tambah SKP Pegawai</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Tahun</label>
							<div class="form-group">
								<select class="form-control" id="txtTahunAdd" name="txtTahunAdd">
									<option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
									<?php
									for($i = $year-1; $i > $year-10; $i--){
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
									?>
								</select>
							</div>
						</div>			
						<div class="form-group" id="view_nama_pegawai">
						</div>
						<div class="form-group">
							<label class="form-control-label">Nilai SKP</label>
							<input type="number" placeholder="Nilai SKP" class="form-control" name="txtNilaiSKPAdd"
							id="txtNilaiSKPAdd" autocomplete="off" step="any" required>
						</div>
					</div>						
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-info" id="btnSimpanAdd">
					Simpan
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">
					Batal
				</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">				
				<h4 class="modal-title">Anda yakin untuk menghapus data ini ?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Popup untuk Edit--> 
<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Edit SKP Pegawai</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Tahun</label>
							<input type="number" placeholder="Tahun" class="form-control" name="txtTahunEdit"
							id="txtTahunEdit" autocomplete="off" step="any" required disabled>
						</div>			
						<div class="form-group">							
							<label class="form-control-label">Nama Pegawai</label>
							<input type="number" placeholder="Nama Pegawai" class="form-control" name="txtNamaPegawaiEdit"
							id="txtNamaPegawaiEdit" autocomplete="off" step="any" required disabled>
						</div>
						<div class="form-group">
							<label class="form-control-label">Nilai SKP</label>
							<input type="number" placeholder="Nilai SKP" class="form-control" name="txtNilaiSKPEdit"
							id="txtNilaiSKPEdit" autocomplete="off" step="any" required>
						</div>
					</div>						
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-info" id="btnSimpanEdit">
					Simpan
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">
					Batal
				</button>
			</div>
		</div>
	</div>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	var KodeOPD;
	var dataTable1;
	var Tahun;

	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		Tahun = '';
		LoadData();
	});

	$('#cb_tahun').change(function () {
		Tahun = $(this).find('option:selected').attr('value');
		dataTable1.ajax.reload();
	});

	function LoadData(){		
		var action = "LoadData";
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-right'
			},
			{
				targets: 5,
				className: 'text-center'
			}],
			"ajax":{
				url :"skp_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action ,
					d.Tahun = Tahun,
					d.KodeOPD = KodeOPD;
				},
				error: function(){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");
				}
			}
		});						
	}

	$(document).on('click', '#btnTambah', function () {
		var action = "DataPegawai";
		var KodePegawai = '';
		$.ajax({
			url: "skp_aksi.php",
			method: "POST",
			data: {KodeOPD: KodeOPD, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#view_nama_pegawai').html(data.DataPegawai);
					$('#ModalTambah').modal('show');

					$('#txtKodePegawaiAdd').change(function () {
						KodePegawai = $(this).find('option:selected').attr('value');
					});

					$(document).on('click', '#btnSimpanAdd', function () {
						var action = "InsertData";
						var Tahun = $("[name='txtTahunAdd']").val();
						var NilaiSKP = $("[name='txtNilaiSKPAdd']").val();
						if(KodePegawai != ''){
							$.ajax({
								url: "skp_aksi.php",
								method: "POST",
								data: {KodePegawai: KodePegawai, Tahun: Tahun, NilaiSKP: NilaiSKP, action: action},
								dataType: 'json',
								success: function (data) {
									if(data.response == 200){
										swal('Sukses','Berhasil menambah SKP Pegawai.', 'success');			
										dataTable1.ajax.reload();
										$('#ModalTambah').modal("toggle");
										SelectElement('txtTahunAdd','2018');
										$("[name='txtNilaiSKPAdd']").val('');
									}else if(data.response == 403){
										swal('Peringatan', 'Data tersebut telah ada.', 'warning');
									}else{
										swal('Error', 'Terjadi Kesalahan', 'error');
									}
								}
							});
						}else{
							swal('Peringatan', 'Anda harus memilih pegawai', 'warning');		
						}						
					});
				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});		
	});

	function SelectElement(id, valueToSelect){    
		var element = document.getElementById(id);
		element.value = valueToSelect;
	}

	$(document).on('click', '#btnDelete', function () {
		var KodePegawai = $(this).val();
		var Tahun = $(this).attr("data-value");
		$('#ModalHapus').modal('show');

		$(document).on('click', '#btHapus', function () {
			var action = "DeleteData";
			$.ajax({
				url: "skp_aksi.php",
				method: "POST",
				data: {KodePegawai: KodePegawai, Tahun: Tahun, action: action},
				dataType: 'json',
				success: function (data) {
					if(data.response == 200){
						swal('Sukses', 'Berhasil menghapus data', 'success');
						dataTable1.ajax.reload();
						$('#ModalHapus').modal("toggle");
					}else{
						swal('Error', 'Terjadi Kesalahan', 'error');
					}
				}
			});
		});
	});

	$(document).on('click', '#btnUpdate', function () {
		var KodePegawai = $(this).val();
		var Tahun = $(this).attr("data-value");
		var action = 'AmbilData';
		$.ajax({
			url: "skp_aksi.php",
			method: "POST",
			data: {KodePegawai: KodePegawai, Tahun: Tahun, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$("[name='txtTahunEdit']").val(data.Tahun);
					$("[name='txtNamaPegawaiEdit']").val(data.NamaPegawai + "(" + data.NIP + ")");
					$("[name='txtNilaiSKPEdit']").val(data.NilaiSKP);

					$('#ModalEdit').modal("show");

					$(document).on('click', '#btnSimpanEdit', function () {
						var action = "UpdateData";
						var NilaiSKP = $("[name='txtNilaiSKPEdit']").val();
						$.ajax({
							url: "skp_aksi.php",
							method: "POST",
							data: {KodePegawai: KodePegawai, Tahun: Tahun, NilaiSKP: NilaiSKP, action: action},
							dataType: 'json',
							success: function (data) {
								if(data.response == 200){
									dataTable1.ajax.reload();
									swal('Sukses', 'Berhasil mengubah data', 'success');
									$('#ModalEdit').modal("toggle");
								}else{
									swal('Error', 'Terjadi Kesalahan', 'error');
								}
							}
						});
					});

				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});

		$('#ModaEdit').modal('show');

		$(document).on('click', '#btHapus', function () {
			var action = "DeleteData";
			$.ajax({
				url: "skp_aksi.php",
				method: "POST",
				data: {KodePegawai: KodePegawai, Tahun: Tahun, action: action},
				dataType: 'json',
				success: function (data) {
					if(data.response == 200){
						swal('Sukses', 'Berhasil menghapus data', 'success');
						dataTable1.ajax.reload();
						$('#ModalHapus').modal("toggle");
					}else{
						swal('Error', 'Terjadi Kesalahan', 'error');
					}
				}
			});
		});
	});
</script>