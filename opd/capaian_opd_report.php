<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';
ob_start();
$NamaBulan = array (1 =>   'Januari',
	'Februari',
	'Maret',
	'April',
	'Mei',
	'Juni',
	'Juli',
	'Agustus',
	'September',
	'Oktober',
	'November',
	'Desember'
);
$KodeOPD = $_GET['KodeOPD'];
$Triwulan = $_GET['Triwulan'];
$Tahun = $_GET['Tahun'];
$sql = "SELECT Tahun, Triwulan, KodeOPD, RealisasiAnggaran, NilaiKategoriNRA, NRABobot, NilaiOutputKegiatan, NilaiKategoriNOK, NOKBobot, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot FROM capaiankinerjaopd WHERE 
KodeOPD = '$KodeOPD' AND Tahun = '$Tahun' AND Triwulan = '$Triwulan' ORDER BY Tahun DESC";
$res = $conn->query($sql);
$no = 1;
?>
<page style="width:100%;" backtop="7mm" backbottom="7mm" backleft="7mm" backright="7mm"> 
	<h2 style="width:100%; margin-bottom: 0px; text-align: center;">DAFTAR REKAPITULASI PEMBERIAN TPP BERDASARKAN PRODUKTIVITAS KERJA</h2>
	<h3 style="width:100%; margin-top: 0px; text-align: center;"><?php echo "TRIBULAN ".$Triwulan." TAHUN ".$Tahun; ?></h3>
	<!-- TABEL ABSENSI -->
	<h5 style="margin-top: 20px; margin-bottom: 0px">A. NILAI KINERJA PERANGKAT DAERAH</h5>
	<table border="1" style="width:100%; border-collapse: collapse; font-size: 11px;">
		<tr>
			<th style="width:25%; text-align: center; padding: 4px;">Nilai Realisasi Anggaran</th>
			<th style="width:25%; text-align: center; padding: 4px;">Nilai Output Kegiatan</th>
			<th style="width:25%; text-align: center; padding: 4px;">Nilai SAKIP</th>
			<th style="width:18%; text-align: center; padding: 4px;">Nilai Kinerja Perangkat Daerah</th>
		</tr>
		<?php
		while ($row = $res->fetch_assoc()) { ?>
			<tr>
				<td>
					<table style="width:100%;">
						<tr style="padding: 4px;"><td style="padding: 4px;">Realisasi Anggaran</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['RealisasiAnggaran']; ?> %</td></tr>
						<tr style="padding: 4px;"><td style="padding: 4px;">Nilai Kategori NRA</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['NilaiKategoriNRA']; ?></td></tr>
						<tr style="padding: 4px;"><td style="padding: 4px;">NRA x Bobot 30%</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['NRABobot']; ?></td></tr>
					</table>
				</td>
				<td>
					<table style="width:100%;">
						<tr style="padding: 4px;"><td style="padding: 4px;">Rata Rata Capaian Output</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['NilaiOutputKegiatan']; ?>%</td></tr>
						<tr style="padding: 4px;"><td style="padding: 4px;">Nilai Kategori NOK</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['NilaiKategoriNOK']; ?></td></tr>
						<tr style="padding: 4px;"><td style="padding: 4px;">NOK x Bobot 30%</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['NOKBobot']; ?></td></tr>
					</table>
				</td>
				<td>
					<table style="width:100%;">
						<tr style="padding: 4px;"><td style="padding: 4px;">Kategori Nilai SAKIP</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['EvalusasiSAKIP']; ?></td></tr>
						<tr style="padding: 4px;"><td style="padding: 4px;">Nilai Kategori S</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['NilaiKategoriSAKIP']; ?></td></tr>
						<tr style="padding: 4px;"><td style="padding: 4px;">NS x Bobot 40%</td><td style="padding: 4px;">:</td><td style="padding: 4px;"><?php echo $row['SAKIPBobot']; ?></td></tr>
					</table>
				</td>
				<td></td>			
			</tr>
			<?php
			
			$res_pegawai = GetDataPegawai($conn,$KodeOPD,$row['NilaiKategoriNRA'],$row['NilaiKategoriNOK'],$row['NilaiKategoriSAKIP']);	
		}
		?>
	</table>
	<?php 
	$rowcount=mysqli_num_rows($res);
	if($rowcount > 0){
		echo $res_pegawai;
	} ?>
	<table style="width: 100%; margin-top: 30px;">
		<tr>
			<td style="width: 50%"></td>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			$tgl=date('d-m-Y');
			$TanggalTxt = date('d');
			$BulanTxt = date('m');
			$TahunTxt = date('Y');
			?>
			<td style="text-align: center; width: 50%">Jombang, <?php echo $TanggalTxt.' '.$NamaBulan[(int)$BulanTxt].' '.$TahunTxt; ?></td>
		</tr>
		<tr style="text-align: center;">
			<td style="text-align: center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Kepala Perangkat Daerah,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
			<td style="text-align: center; center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Bendahara Pengeluaran,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>	
</page>
<?php
$filename="tpp_produktivitas_".$Triwulan."_".$Tahun.".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF
//==========================================================================================================

require_once('../html2pdf/html2pdf.class.php');
$content = ob_get_clean();
//$content = '<page style="font-family: freeserif">'.nl2br($content).'</page>';
try
{
	$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(5, 5, 5, 5));
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->pdf->SetTitle('TPP Kinerja');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }

function GetDataPegawai($conn,$KodeOPD,$NRA,$NOK,$SAKIP){
	$sql ="SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE p.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output = '<h5 style="margin-top: 20px; margin-bottom: 0px">B. REKAPITULASI TPP</h5>
		<table border="1" style="width:100%; border-collapse: collapse; font-size: 11px;">
		<tr>
		<th style="width:4%; text-align: center; padding: 4px; font-size: 11px;">No</th>
		<th style="width:12%; text-align: center; padding: 4px; font-size: 11px;">Nama / NIP</th>
		<th style="width:8%; text-align: center; padding: 4px; font-size: 11px;">Jabatan</th>
		<th style="width:5%; text-align: center; padding: 4px; font-size: 11px;">Kelas<br>Jabatan</th>
		<th style="width:5%; text-align: center; padding: 4px; font-size: 11px;">Harga<br>Jabatan</th>
		<th style="width:6%; text-align: center; padding: 4px; font-size: 11px;">TPP<br>Pokok</th>
		<th style="width:6%; text-align: center; padding: 4px; font-size: 11px;">TPP<br>Kinerja</th>
		<th style="width:5%; text-align: center; padding: 4px; font-size: 11px;">PPH<br>21%</th>
		<th style="width:6%; text-align: center; padding: 4px; font-size: 11px;">Potongan<br>Pajak</th>
		<th style="width:8%; text-align: center; padding: 4px; font-size: 11px;">Diterimakan</th>
		<th style="width:7%; text-align: center; padding: 4px; font-size: 11px;">Tanda<br>Tangan</th>
		<th style="width:8%; text-align: center; padding: 4px; font-size: 11px;">No. Rekening</th>
		</tr>
		<tr>
		<th style="text-align: center; padding: 4px; font-size: 11px;">1</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">2</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">3</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">4</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">5</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">6</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">7</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">8</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">9</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">10</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">11</th>
		<th style="text-align: center; padding: 4px; font-size: 11px;">12</th>
		</tr>';
		$no = 1;
		$JmlTppPokok = 0;
		$JmlTppKinerja = 0;
		$JmlPotongan = 0;
		$JmlDiterimakan = 0;
		while ($row = $res->fetch_assoc()) {
			# code...
			$PokokTPP = $row['PokokTPPPegawai'];
			$TPPKinerja = TPPKinerja($PokokTPP, $NRA, $NOK, $SAKIP);
			$PotonganPajak = $TPPKinerja * 21/100;
			$Diterima = ($TPPKinerja - $PotonganPajak);
			$output .= '<tr>
			<td style="text-align: center; padding: 4px;">' . $no++ . '</td>
			<td style="padding: 4px;">' . ucwords(strtolower($row['NamaPegawai'])) . '</td>
			<td style="padding: 4px;">' . ucwords(strtolower($row['NamaJabatan'])) . '</td>
			<td style="text-align: center; padding: 4px;">' . $row['KelasJabatan'] . '</td>
			<td style="text-align: right; padding: 4px;">' . number_format($row['HargaJabatan']) . '</td>
			<td style="text-align: right; padding: 4px;">' . number_format($PokokTPP) . '</td>
			<td style="text-align: right; padding: 4px;">' . number_format($TPPKinerja) . '</td>
			<td style="text-align: right; padding: 4px;">21</td>
			<td style="text-align: right; padding: 4px;">'.number_format($PotonganPajak).'</td>
			<td style="text-align: right; padding: 4px;">' . number_format($Diterima) . '</td>
			<td style="text-align: center; padding: 4px;"></td>
			<td style="text-align: center; padding: 4px;"></td>
			</tr>';
			$JmlTppPokok += $PokokTPP;
			$JmlTppKinerja += $TPPKinerja;
			$JmlPotongan += $PotonganPajak;
			$JmlDiterimakan += $Diterima;
		}
		$output .= '<tr>
		<td colspan="5" style="text-align: center; padding: 4px;">Jumlah</td>
		<td style="text-align: center; padding: 4px;">' . number_format($JmlTppPokok) . '</td>
		<td style="text-align: center; padding: 4px;">' . number_format($JmlTppKinerja) . '</td>
		<td style="text-align: center; padding: 4px;"></td>
		<td style="text-align: center; padding: 4px;">' . number_format($JmlPotongan) . '</td>
		<td style="text-align: center; padding: 4px;">' . number_format($JmlDiterimakan) . '</td>
		<td style="text-align: center; padding: 4px;"></td>
		<td style="text-align: center; padding: 4px;"></td>
		</tr>
		</table>';
		return $output;
	}
}
?>