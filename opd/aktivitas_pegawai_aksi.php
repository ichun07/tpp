<?php 
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetDataAktivitas($conn, $KodePegawai, $KodeOPD, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST["action"] == "AmbilData"){
		$KodeAktivitas = $_POST['KodeAktivitas'];
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetOneDataAktivitas($conn, $KodeAktivitas, $KodePegawai, $KodeOPD);
		echo json_encode($result);
	}

	if($_POST["action"] == "UpdateData"){
		$KodeAktivitas = $_POST['KodeAktivitas'];
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$DurasiKerjaACC = $_POST['DurasiKerjaACC'];
		$StatusACC = $_POST['StatusACC'];
		$KeteranganACC = $_POST['KeteranganACC'];
		$result = UpdateVerifikasi($conn, $KodeAktivitas, $KodePegawai, $KodeOPD, $DurasiKerjaACC, $StatusACC, $KeteranganACC);
		echo json_encode($result);
	}

	if($_POST['action'] == 'AmbilFoto'){
		$KodePegawai = $_POST['KodePegawai'];
		$KodeAktivitas = $_POST['KodeAktivitas'];
		$result = GetDokumenPendukung($conn, $KodePegawai, $KodeAktivitas);
		echo json_encode($result);
	}
}

function GetDataAktivitas($conn, $KodePegawai,$KodeOPD, $Bulan, $Tahun){
	$dataPegawai = '';
	$output = '';
	$sql_pegawai ="SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password 
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
	WHERE p.KodeOPD = '$KodeOPD' AND p.KodePegawai = '$KodePegawai'";
	$res = $conn->query($sql_pegawai);
	if($res){
		while ($row_pegawai = $res->fetch_assoc()) {
			$dataPegawai = $row_pegawai;
		}
		$DataAktivitas = DataTabelAktivitas($conn, $KodePegawai, $KodeOPD, $Bulan, $Tahun);
		return array('DataPegawai' => $dataPegawai, 'DataHtml' => $DataAktivitas, 'response' => 200);
	}else{
		return array('response' => 500);
	}
}

function GetOneDataAktivitas($conn, $KodeAktivitas, $KodePegawai, $KodeOPD){
	$output = '';
	$sql = "SELECT ap.KodeAktivitas, ap.NamaAktivitas, ap.Deskripsi, ap.DurasiKerja, ap.ACCAtasan, ap.KodePegawaiAtasan, peg.NamaPegawai AS NamaAtasan, ap.KeteranganACC, ap.DurasiKerjaACC, ap.KodePegawai, p.NamaPegawai
	FROM aktivitaspegawai ap
	LEFT JOIN mstpegawai p ON ap.KodePegawai = p.KodePegawai
	LEFT JOIN mstpegawai peg ON ap.KodePegawaiAtasan = peg.KodePegawai
	WHERE ap.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD' AND ap.KodeAktivitas = '$KodeAktivitas'";
	$res = $conn->query($sql);
	if($res){
		$output['response'] = 200;
		while ($row = $res->fetch_assoc()) {
			$output['DataAktivitas'] = $row;
		}
		return $output;
	}else{
		return array('response' => 500);
	}
}

function UpdateVerifikasi($conn, $KodeAktivitas, $KodePegawai, $KodeOPD, $DurasiKerjaACC, $StatusACC, $KeteranganACC){
	$sql = "UPDATE aktivitaspegawai ap
	LEFT JOIN mstpegawai p ON ap.KodePegawai = p.KodePegawai 
	SET ap.ACCAtasan = '$StatusACC', ap.KeteranganACC = '$KeteranganACC', ap.DurasiKerjaACC = '$DurasiKerjaACC' 
	WHERE ap.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD' AND ap.KodeAktivitas = '$KodeAktivitas'";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function DataTabelAktivitas($conn, $KodePegawai, $KodeOPD, $Bulan, $Tahun){
	$sql = "SELECT ap.KodeAktivitas, ap.Tanggal, ap.NamaAktivitas, ap.Deskripsi, ap.DurasiKerja, ap.ACCAtasan, ap.KodePegawaiAtasan, ap.KeteranganACC, ap.DurasiKerjaACC, ap.KodePegawai, ap.DokumenPendukung, peg.NamaPegawai AS NamaAtasan, p.NamaPegawai, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung, j.IsKepala
	FROM aktivitaspegawai ap
	LEFT JOIN mstpegawai p ON ap.KodePegawai = p.KodePegawai
	LEFT JOIN mstpegawai peg ON ap.KodePegawaiAtasan = peg.KodePegawai
	LEFT JOIN mstjabatan j ON p.KodeJabatan = j.KodeJabatan AND p.KodeOPD =  j.KodeOPD
	WHERE ap.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' GROUP BY ap.KodeAktivitas ORDER BY ap.Tanggal ASC ";
	$res = $conn->query($sql);
	if($res){
		$JmlRMKE = 0;
		$JmlRMKEsetuju = 0;
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr style = "border-bottom:none;">
		<th rowspan="2" width="30px">No</th>
		<th rowspan="2" width="100px">Tanggal</th>
		<th rowspan="2">Keterangan</th>
		<th rowspan="2" width="90px" class="text-center">Realisasi Menit Kerja</th>
		<th colspan="4" class="text-center">Validasi Aktivitas Kerja Pegawai</th>
		<th rowspan="2">Aksi</th>
		</tr>
		<tr>
		<th class="text-center">Status</th>
		<th width="90px" class="text-center">Nilai Penetapan</th>
		<th class="text-center">Alasan Penyesuaian</th>
		<th class="text-center">Penilai</th>
		</tr>
		</thead>		
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			# code...
			$Verifikasi = '';
			$Aksi = '';
			if($row['ACCAtasan']!=null && $row['ACCAtasan']!=''){
				$Verifikasi = $row['ACCAtasan'];
				$Aksi = '';
			}else{
				$Verifikasi =  'UNVERIFIED';
				if($row['IsKepala'] > 0){
					$Aksi = '<button value = '. $row['KodeAktivitas'] .' name="btnVerifikasi" id="btnVerifikasi" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>';
				}else{
					$Aksi = '';
				}				
			}
			$output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . date('d - m - Y', strtotime($row['Tanggal'])) . '</td>
			<td>' . $row['NamaAktivitas'] . '</td>
			<td class="text-center">' . $row['DurasiKerja'] . '</td>
			<td class="text-center">' . $Verifikasi . '</td>
			<td class="text-center">' . $row['DurasiKerjaACC'] . '</td>
			<td>' . $row['KeteranganACC'] . '</td>
			<td>' . $row['NamaAtasan'] . '</td>
			<td>' . $Aksi . '<button value = '. $row['KodeAktivitas'] .' name="btnDokumen" id="btnDokumen" class="btn btn-success"><span class="fa fa-file-image-o" aria-hidden="true"></span></button></td>
			</tr>';
			if($row['DurasiKerja']!=null && $row['DurasiKerja']!=''){
				$JmlRMKE+= $row['DurasiKerja'];
			}
			if($row['DurasiKerjaACC']!=null && $row['DurasiKerjaACC']!=''){
				$JmlRMKEsetuju += $row['DurasiKerjaACC'];
			}
		}
		$output .= '</tbody>
		<tfoot>
		<tr>
		<th></th>
		<th></th>
		<th class="text-center">JUMLAH RMKE</th>
		<th class="text-center">'.$JmlRMKE.'</th>
		<th></th>
		<th class="text-center">'.$JmlRMKEsetuju.'</th>
		<th></th>
		<th></th>
		<th></th>
		</tr>
		</tfoot>
		</table>';
		return $output;
	} else {
		return 500;
	}
}

function GetDokumenPendukung($conn, $KodePegawai, $KodeAktivitas){
	$sql = "SELECT DokumenPendukung FROM aktivitaspegawai WHERE KodeAktivitas = '$KodeAktivitas' AND KodePegawai = '$KodePegawai'";
	$sth = $conn->query($sql);
	if($sth){
		$result=mysqli_fetch_array($sth);
		if($result['DokumenPendukung'] != null){
			return '<img id="previewing" src="data:image/jpeg;base64,'.base64_encode( $result['DokumenPendukung'] ).'"/>';
		}else{
			return 500;
		}
	}else{
		return 500;
	}	
}