<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Jabatan</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../opd/jabatan_view.php'" class="btn btn-success">
                        Kembali
                    </button>
                </br></br>
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Tambah Jabatan</h3>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            <div class="form-group">
                                <label class="form-control-label">Nama Jabatan</label>
                                <input type="text" placeholder="Nama Jabatan" class="form-control"
                                name="txtNamaJabatan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Kode Jabatan</label>
                                <input type="text" placeholder="Kode Jabatan" class="form-control"
                                name="txtKodeManual">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Tipe Jabatan</label>
                                <select class='form-control' id='txtTipeJabatan' name='txtTipeJabatan'>
                                    <option value='STRUKTURAL'> Struktural </option>
                                    <option value='FUNGSIONAL'> Fungsional </option>
                                </select>
                            </div>
                            <div class="form-group" id="select_kelas_jabatan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Harga Jabatan</label>
                                <input type="number" placeholder="Harga Jabatan" class="form-control"
                                name="txtHargaJabatan">
                            </div>                            
                            <div class="form-group" id="select_atasan_opd">
                            </div>
                            <div class="form-group" id="select_atasan_jab">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Keterangan</label>
                                <input type="text" placeholder="Keterangan" class="form-control"
                                name="txtKeterangan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Deskripsi</label>
                                <textarea placeholder="Deskripsi" name="txtDeskripsi" id="txtDeskripsi"
                                class="form-control" rows="7" cols="80"></textarea>
                            </div>
                            <div class="form-group" id="radio_kepala">
                            </div>
                            <div class="form-group">
                                <button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    var KodeOPD, JumlahKepala, IsKepala;
    $(document).ready(function () {
        IsKepala = 0;
        KodeOPD = "<?php echo $KodeOPD; ?>";
        LoadDataAtasan(KodeOPD);
    });

    function LoadDataAtasan(KodeOPD) {
        var action = "LoadDataAtasan";
        $.ajax({
            url: "jabatan_aksi.php",
            method: "POST",
            data: {action: action, name: "cbAtasanLangsungKodeJab", text: "Atasan Langsung Jabatan", KodeOPD: KodeOPD},
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {                    
                    $('#select_kelas_jabatan').html(data.HtmlKelas);
                    if (data.Jumlah < 1) {
                        $('#radio_kepala').html(data.HtmlRadio); 
                        $('#select_atasan_opd').html(data.HtmlOPD);
                        $('#select_atasan_opd').show();
                        $('#cbKodeOpd').change(function () {
                            var selectedItem = $(this).val();
                            console.log("KodeOPD : " + selectedItem);
                            LoadDataJabatan(selectedItem);
                        });
                    } else {
                        $('#select_atasan_opd').hide();
                        $('#select_atasan_jab').html(data.HtmlAtasan);
                    }
                    JumlahKepala = data.Jumlah;
                } else {
                    alert('Load atasan opd failed');
                }
            }
        });
    }

    function LoadDataJabatan(KodeOPD) {
        var action = "GetJabatan";
        $.ajax({
            url: "jabatan_aksi.php",
            method: "POST",
            data: {action: action, KodeOPD: KodeOPD},
            dataType: 'json',
            success: function (data) {
                if (data.response = 200) {
                    $('#select_atasan_jab').html(data.cbJabatan);
                } else {
                    alert('Load Data Jabatan Failed');
                }
            }
        });
    }

    function simpanData() {
        var AtasanLangsungOPD;
        var AtasanLangsungKodeJab;
        var KodeManual = $("[name='txtKodeManual']").val();
        var NamaJabatan = $("[name='txtNamaJabatan']").val();
        var Keterangan = $("[name='txtKeterangan']").val();
        var Deskripsi = $("[name='txtDeskripsi']").val();
        var TipeJabatan = $("[name='txtTipeJabatan']").val();
        var KelasJabatan = $("[name='txtKelasJabatan']").val();
        var HargaJabatan = $("[name='txtHargaJabatan']").val();
        var action = "InsertData";
        if (JumlahKepala < 1) {
            if(IsKepala < 1){
                alert("Kepala OPD harus ditetapkan dahulu!");
            }else{
                AtasanLangsungOPD = $("[name='cbKodeOpd']").val();
                AtasanLangsungKodeJab = $("[name='cbJabatan']").val();
                $.ajax({
                    url: "jabatan_aksi.php",
                    method: "POST",
                    data: {
                        kodeManual: KodeManual,
                        namaJabatan: NamaJabatan,
                        keterangan: Keterangan,
                        deskripsi: Deskripsi,
                        tipeJabatan: TipeJabatan,
                        kelasJabatan: KelasJabatan,
                        hargaJabatan: HargaJabatan,
                        atasanLangsungKodeJab: AtasanLangsungKodeJab,
                        atasanLangsungOPD: AtasanLangsungOPD,
                        kodeOPD: KodeOPD,
                        isKepala: IsKepala,
                        action: action
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.Status === 200) {
                            $("[name='txtKodeManual']").val("");
                            $("[name='txtNamaJabatan']").val("");
                            $("[name='txtKeterangan']").val("");
                            $("[name='txtDeskripsi']").val("");
                    //$("[name='txtTipeJabatan']").val("");
                    SelectElement("txtTipeJabatan", "STRUKTURAL");
                    $("[name='txtKelasJabatan']").val("");
                    $("[name='txtHargaJabatan']").val("");
                    LoadDataAtasan();
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/jabatan_view.php'>lihat semua data</a>.</div>");
                } else {
                    alert('Insert Data failed');
                }

            }
        });
            }
        }else{
            AtasanLangsungOPD = KodeOPD;
            AtasanLangsungKodeJab = $("[name='cbAtasanLangsungKodeJab']").val();
            $.ajax({
                url: "jabatan_aksi.php",
                method: "POST",
                data: {
                    kodeManual: KodeManual,
                    namaJabatan: NamaJabatan,
                    keterangan: Keterangan,
                    deskripsi: Deskripsi,
                    tipeJabatan: TipeJabatan,
                    kelasJabatan: KelasJabatan,
                    hargaJabatan: HargaJabatan,
                    atasanLangsungKodeJab: AtasanLangsungKodeJab,
                    atasanLangsungOPD: AtasanLangsungOPD,
                    kodeOPD: KodeOPD,
                    isKepala: IsKepala,
                    action: action
                },
                dataType: 'json',
                success: function (data) {
                    if (data.Status === 200) {
                        $("[name='txtKodeManual']").val("");
                        $("[name='txtNamaJabatan']").val("");
                        $("[name='txtKeterangan']").val("");
                        $("[name='txtDeskripsi']").val("");
                    //$("[name='txtTipeJabatan']").val("");
                    SelectElement("txtTipeJabatan", "STRUKTURAL");
                    $("[name='txtKelasJabatan']").val("");
                    $("[name='txtHargaJabatan']").val("");
                    LoadDataAtasan();
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../opd/jabatan_view.php'>lihat semua data</a>.</div>");
                } else {
                    alert('Insert Data failed');
                }

            }
        });
        }        
        
    }

    function SelectElement(id, valueToSelect)
    {    
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }

    function validate() {
        if (document.getElementById('r1').checked) {
            IsKepala = 1;
        } else {
            IsKepala = 0;
        }
    }
</script>