<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
    if ($_POST["action"] == "LoadData") {
        $KodeOPD = $_POST['KodeOPD'];
        $result = GetDataJabatan($conn, $KodeOPD);
        echo json_encode($result);
    }
    if ($_POST["action"] == "InsertData") {
        $KodeManual = $_POST['kodeManual'];
        $NamaJabatan = $_POST['namaJabatan'];
        $Keterangan = $_POST['keterangan'];
        $Deskripsi = $_POST['deskripsi'];
        $TipeJabatan = $_POST['tipeJabatan'];
        $KelasJabatan = $_POST['kelasJabatan'];
        $HargaJabatan = $_POST['hargaJabatan'];
        $AtasanLangsungKodeJab = $_POST['atasanLangsungKodeJab'];
        $AtasanLangsungOPD = $_POST['atasanLangsungOPD'];
        $KodeOPD = $_POST['kodeOPD'];
        $IsKepala = "b'" . $_POST['isKepala'] . "'";
        $result = InsertDataJabatan($conn, $KodeManual, $NamaJabatan, $Keterangan, $Deskripsi, $TipeJabatan, $KelasJabatan, $HargaJabatan, $AtasanLangsungKodeJab, $KodeOPD, $AtasanLangsungOPD, $IsKepala);
        echo json_encode($result);
    }
    if ($_POST["action"] == "AmbilData") {
        $KodeJabatan = $_POST["kodeJabatan"];
        $KodeOPD = $_POST["kodeOPD"];
        $result = GetOneDataJabatan($conn, $KodeJabatan, $KodeOPD);
        echo json_encode($result);
    }
    if ($_POST["action"] == "UpdateData") {
        $KodeJabatan = $_POST['kodeJabatan'];
        $KodeManual = $_POST['kodeManual'];
        $NamaJabatan = $_POST['namaJabatan'];
        $Keterangan = $_POST['keterangan'];
        $Deskripsi = $_POST['deskripsi'];
        $TipeJabatan = $_POST['tipeJabatan'];
        $KelasJabatan = $_POST['kelasJabatan'];
        $HargaJabatan = $_POST['hargaJabatan'];
        $AtasanLangsungKodeJab = $_POST['atasanLangsungKodeJab'];
        $AtasanLangsungOPD = $_POST['atasanLangsungOPD'];
        $KodeOPD = $_POST['kodeOPD'];
        $result = UpdateDataJabatan($conn, $KodeJabatan, $KodeManual, $NamaJabatan, $Keterangan, $Deskripsi, $TipeJabatan, $KelasJabatan, $HargaJabatan, $AtasanLangsungKodeJab, $KodeOPD, $AtasanLangsungOPD);
        echo json_encode($result);
    }
    if ($_POST["action"] == "HapusData") {
        $KodeJabatan = $_POST["id"];
        $KodeOPD = $_POST["kodeOPD"];
        $result = DeleteDataJabatan($conn, $KodeJabatan, $KodeOPD);
        echo json_encode($result);
    }
    if ($_POST["action"] == "LoadDataAtasan") {
        $text = $_POST["text"];
        $name = $_POST["name"];
        $KodeOPD = $_POST["KodeOPD"];
        $result = GetDataAtasanJabatan($conn, $text, $name, $KodeOPD);
        echo json_encode($result);
    }

    if ($_POST["action"] == "LoadDataAtasanEdit") {
        $text = $_POST["text"];
        $name = $_POST["name"];
        $KodeJabatan = $_POST["kodeJabatan"];
        $result = GetDataAtasanJabatanEdit($conn, $text, $name, $KodeJabatan);
        echo json_encode($result);
    }

    if ($_POST["action"] == "GetJabatan") {
        $KodeOPD = $_POST['KodeOPD'];
        $result = GetDataJabatan1($conn, $KodeOPD);
        echo json_encode($result);
    }

    if($_POST["action"] == "GetAtasanOPD"){
        $KodeOPD = $_POST['KodeOPD'];
        $NamaOPD = $_POST['NamaOPD'];
        $result = GetOPDEditAtasan($conn,$KodeOPD,$NamaOPD);
        echo json_encode($result);
    }

    if($_POST["action"] == "GetAtasanJabatan"){
        $KodeOPD = $_POST['KodeOPD'];
        $KodeJabatan = $_POST['KodeJabatan'];
        $NamaJabatan = $_POST['NamaJabatan'];
        $result = GetJabatanEditAtasan($conn, $KodeOPD, $KodeJabatan, $NamaJabatan);
        echo json_encode($result);
    }

    if($_POST["action"] == "GetKelasEdit"){
        $Kode = $_POST['Kode'];
        $result = GetDataKelasJabatanEdit($conn,$Kode);
        echo json_encode($result);
    }
}

function GetDataJabatan1($conn, $KodeOPD){
    $arrayOutput = '';
    $output = '';
    $sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD, o.NamaOPD 
    FROM mstjabatan j
    LEFT JOIN mstopd o
    ON o.KodeOPD = j.KodeOPD
    WHERE j.KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if($res){
        $arrayOutput['response'] = 200;
        $output .='<label class="form-control-label">Atasan Jabatan</label>';
        $output .= "<select class='form-control' id='cbJabatan' name='cbJabatan'>";
        $output .= "<option value='0'> - Pilih Jabatan - </option>";
        while ($row = $res->fetch_assoc()) {
            $output .= "<option value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
        }
        $output .= '</select>';
        $arrayOutput['cbJabatan'] = $output;
        return $arrayOutput;
    }else{
        return array('response' => 500);
    }
}

function InsertDataJabatan($conn, $KodeManual, $NamaJabatan, $Keterangan, $Deskripsi, $TipeJabatan, $KelasJabatan, $HargaJabatan, $AtasanLangsungKodeJab, $KodeOPD, $AtasanLangsungOPD, $IsKepala)
{
    $arrayOutput = '';
    $KodeJabatan = AutoKodeJabatan($conn, $KodeOPD);
    $sql = "INSERT INTO mstjabatan (KodeJabatan, KodeManual, NamaJabatan, Keterangan, Deskripsi, TipeJabatan, KelasJabatan, HargaJabatan, AtasanLangsungKodeJab, KodeOPD, AtasanLangsungOPD, IsKepala) VALUES ('$KodeJabatan', '$KodeManual', '$NamaJabatan', '$Keterangan', '$Deskripsi', '$TipeJabatan', '$KelasJabatan', '$HargaJabatan', '$AtasanLangsungKodeJab', '$KodeOPD', '$AtasanLangsungOPD'," . $IsKepala . ")";
    $res = $conn->query($sql);
    if ($res) {
        $arrayOutput['Status'] = 200;
    } else {
        $arrayOutput['Status'] = 500;
    }
    return $arrayOutput;
}

function UpdateDataJabatan($conn, $KodeJabatan, $KodeManual, $NamaJabatan, $Keterangan, $Deskripsi, $TipeJabatan, $KelasJabatan, $HargaJabatan, $AtasanLangsungKodeJab, $KodeOPD, $AtasanLangsungOPD)
{
    $arrayOutput = '';
    $sql = "UPDATE mstjabatan SET KodeManual = '$KodeManual', NamaJabatan = '$NamaJabatan', Keterangan = '$Keterangan', Deskripsi = '$Deskripsi', TipeJabatan = '$TipeJabatan', KelasJabatan = '$KelasJabatan', HargaJabatan = '$HargaJabatan', AtasanLangsungKodeJab = '$AtasanLangsungKodeJab', AtasanLangsungOPD = '$AtasanLangsungOPD' WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if ($res) {
        $arrayOutput['Status'] = 200;
    } else {
        $arrayOutput['Status'] = 500;
    }
    return $arrayOutput;
}

function GetDataJabatan($conn, $KodeOPD)
{
    $arrayOutput = '';
    $output = '';
    $sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD
    FROM mstjabatan j
    WHERE j.KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if ($res) {
        $arrayOutput['Status'] = 200;
        $output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
        <thead>
        <tr>
        <th width="30px">No</th>
        <th width="60px">Kode Manual</th>
        <th>Nama Jabatan</th>
        <th>Tipe Jabatan</th>
        <th>Harga Jabatan</th>
        <th>Kelas Jabatan</th>
        <th width="130px"class="text-center">Aksi</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
        <th>No</th>
        <th>Kode Manual</th>
        <th>Nama Jabatan</th>
        <th>Tipe Jabatan</th>
        <th>Harga Jabatan</th>
        <th>Kelas Jabatan</th>
        <th class="text-center">Aksi</th>
        </tr>
        </tfoot>
        <tbody>';
        $no = 1;
        while ($row = $res->fetch_assoc()) {
            $json = json_encode($row);
            $url = "KodeJabatan=" . $row['KodeJabatan'] . "&KodeOPD=" . $row['KodeOPD'];
            $output .= '<tr>
            <td>' . $no++ . '</td>
            <td>' . $row['KodeManual'] . '</td>
            <td>' . $row['NamaJabatan'] . '</td>
            <td>' . $row['TipeJabatan'] . '</td>
            <td>' . $row['HargaJabatan'] . '</td>
            <td>' . $row['KelasJabatan'] . '</td>
            <td class="text-center">			
            <button type="button" value = ' . $url . ' id="btnDetail" name="btnDetail" class="btn btn-warning"><span class="icon icon-pencil-case" aria-hidden="true"></span></button>
            <button data-value="' . $row['KodeOPD'] . '" value="' . $row['KodeJabatan'] . '" name="delete" id="btnDelete" class="btn btn-danger"><span class="icon icon-pencil-case" aria-hidden="true"></span></button>
            </td>
            </tr>';
        }
        $output .= '</tbody>
        </table>';
        $arrayOutput['Tabel'] = $output;
    } else {
        $arrayOutput['Status'] = 500;
    }
    return $arrayOutput;
}

function GetOneDataJabatan($conn, $KodeJabatan, $KodeOPD)
{
    $arrayOutput = '';
    $output = '';
    $sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD, o.NamaOPD, mj.NamaJabatan AS NamaAtasan 
    FROM mstjabatan j
    LEFT JOIN mstopd o ON o.KodeOPD = j.AtasanLangsungOPD
    LEFT JOIN mstjabatan mj ON mj.KodeJabatan = j.AtasanLangsungKodeJab AND mj.KodeOPD = j.atasanLangsungOPD    
    WHERE j.KodeJabatan = '$KodeJabatan' AND j.KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if ($res) {
        $output['Status'] = 200;
        while ($row = $res->fetch_assoc()) {
            # code...
            $output['KodeJabatan'] = $row['KodeJabatan'];
            $output['KodeManual'] = $row['KodeManual'];
            $output['NamaJabatan'] = $row['NamaJabatan'];
            $output['Keterangan'] = $row['Keterangan'];
            $output['Deskripsi'] = $row['Deskripsi'];
            $output['TipeJabatan'] = $row['TipeJabatan'];
            $output['KelasJabatan'] = $row['KelasJabatan'];
            $output['HargaJabatan'] = $row['HargaJabatan'];
            $output['AtasanLangsungKodeJab'] = $row['AtasanLangsungKodeJab'];
            $output['KodeOPD'] = $row['KodeOPD'];
            $output['AtasanLangsungOPD'] = $row['AtasanLangsungOPD'];
            $output['NamaOPD'] = $row['NamaOPD'];
            $output['NamaAtasan'] = $row['NamaAtasan'];
            //$output['HtmlOPD'] = GetDataOPDEdit($conn,$row['KodeOPD'],$row['NamaOPD']);
        }
    } else {
        $output['Status'] = 500;
    }
    return $output;
}

function DeleteDataJabatan($conn, $KodeJabatan, $KodeOPD)
{
    $arrayOutput = '';
    $sql = "DELETE FROM mstjabatan WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if ($res) {
        $arrayOutput['Status'] = 200;
    } else {
        $arrayOutput['Status'] = 500;
    }
    return $arrayOutput;
}

function GetDataAtasanJabatan($conn, $text, $name, $KodeOPD)
{
    $arrayOutput = '';
    $output = '';
    $sql_cek_ketua = "SELECT COUNT(j.KodeJabatan) AS Jumlah FROM mstjabatan j WHERE j.IsKepala = '1' AND j.KodeOPD = '$KodeOPD'";
    $res_cek = $conn->query($sql_cek_ketua);
    $row_cek = mysqli_fetch_assoc($res_cek);
    if ($row_cek['Jumlah'] < 1) {
        $sql_opd = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
        $res_opd = $conn->query($sql_opd);
        if ($res_opd) {
            $outputRadio = '<div id="rates">
            <input id="r1" name="r1" type="checkbox" onclick="validate()" /> Ketua OPD</br>
            </div>';                            
            $arrayOutput['HtmlRadio'] = $outputRadio;
            $output .= '<label class="form-control-label">Nama OPD</label>';
            $output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
            $output .= "<option value='0'> - Pilih OPD - </option>";
            while ($row = $res_opd->fetch_assoc()) {
                $output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
            }
            $output .= '</select>';
            $arrayOutput['HtmlOPD'] = $output;
            $arrayOutput['Jumlah'] = 0;
            $arrayOutput['response'] = 200;
        } else {
            $arrayOutput['response'] = 500;
        }
    } else {
        $sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.KodeOPD, o.NamaOPD 
        FROM mstjabatan j
        LEFT JOIN mstopd o
        ON o.KodeOPD = j.KodeOPD
        WHERE j.KodeOPD = '$KodeOPD'";
        $res = $conn->query($sql);
        if ($res) {
            $output .= '<label class="form-control-label">' . $text . '</label>';
            $output .= "<select class='form-control' id='" . $name . "' name='" . $name . "'>";
            $output .= "<option value='0'> - Pilih Kode Atasan - </option>";
            while ($row = $res->fetch_assoc()) {
                $output .= "<option value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
            }
            $output .= '</select>';
            $arrayOutput['HtmlAtasan'] = $output;
            $arrayOutput['Jumlah'] = 1;
            $arrayOutput['response'] = 200;
        } else {
            $arrayOutput['response'] = 500;
        }
    }
    $KelasJabatan = GetDataKelasJabatan($conn);
    $arrayOutput['HtmlKelas'] = $KelasJabatan;
    return $arrayOutput;
}

function AutoKodeJabatan($conn, $KodeOPD)
{
    date_default_timezone_set('Asia/Jakarta');
    $sql = "SELECT RIGHT(KodeJabatan,7) AS kode FROM mstjabatan WHERE KodeOPD = '$KodeOPD' ORDER BY KodeJabatan DESC LIMIT 1";
    $res = mysqli_query($conn, $sql);
    $result = mysqli_fetch_array($res);
    if ($result['kode'] == null) {
        $kode = 1;
    } else {
        $kode = ++$result['kode'];
    }
    $bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
    return 'JBT-' . $bikin_kode;
}

function GetDataOPDEdit($conn, $KodeOPD, $NamaOPD)
{
    $output = '';
    $sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
    $res = $conn->query($sql);
    if ($res) {
        $output .= '<label class="form-control-label">Nama OPD</label>';
        $output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
        $output .= "<option value='" . $KodeOPD . "'>" . $NamaOPD . "</option>";
        while ($row = $res->fetch_assoc()) {
            $output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
        }
        $output .= '</select>';
    }
    return $output;
}

function GetDataAtasanJabatanEdit($conn, $text, $name, $KodeJabatan, $NamaAtasan)
{
    $arrayOutput = '';
    $output = '';
    $sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.KodeOPD, o.NamaOPD 
    FROM mstjabatan j
    LEFT JOIN mstopd o
    ON o.KodeOPD = j.KodeOPD";
    $res = $conn->query($sql);
    if ($res) {
        $arrayOutput['Status'] = 200;
        $output .= '<label class="form-control-label">' . $text . '</label>';
        $output .= "<select class='form-control' id='" . $name . "' name='" . $name . "'>";
        $output .= "<option value='" . $KodeJabatan . "'>" . $NamaAtasan . "</option>";
        while ($row = $res->fetch_assoc()) {
            $output .= "<option value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan']  . "</option>";
        }
        $output .= '</select>';
        $arrayOutput['CBAtasan'] = $output;
    } else {
        $arrayOutput['Status'] = 500;
    }
    return $arrayOutput;
}

function GetDataKelasJabatan($conn){
    $output = '';
    $sql = "SELECT Kode FROM listdata WHERE JenisListData = 'KELAS_JABATAN' ORDER BY Kode";
    $res = $conn->query($sql);
    if($res){
        $output .= '<label class="form-control-label">Kelas Jabatan</label>';
        $output .= "<select class='form-control' id='txtKelasJabatan' name='txtKelasJabatan'>";        
        while ($row = $res->fetch_assoc()) {
            # code...
            $output .= "<option value='" . $row['Kode'] . "'>" . $row['Kode'] . "</option>";
        }
        $output .= '</select>';
        return $output;
    }else{
        return 500;
    }
}

function GetOPDEditAtasan($conn,$KodeOPD,$NamaOPD){
    $output = '';
    $sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
    $res = $conn->query($sql);
    if($res){
        $output .= '<label class="form-control-label">Atasan Langsung OPD</label>';
        $output .= "<select class='form-control' id='cb_atasan_opd' name='cb_atasan_opd'>";
        $output .= "<option value='" . $KodeOPD . "'>" . $NamaOPD . "</option>";
        while ($row = $res->fetch_assoc()) {
            # code...
            $output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
        }
        $output .= '</select>';
        return $output;
    }else{
        return 500;
    }
}

function GetJabatanEditAtasan($conn, $KodeOPD, $KodeJabatan, $NamaJabatan){
    $output = '';
    $sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD 
    FROM mstjabatan j
    WHERE j.KodeOPD = '$KodeOPD'";
    $res = $conn->query($sql);
    if($res){
        $output .='<label class="form-control-label">Atasan Langsung Jabatan</label>';
        $output .= "<select class='form-control' id='cbAtasanLangsungKodeJab' name='cbAtasanLangsungKodeJab'>";
        $output .= "<option value='" . $KodeJabatan . "'>" . $NamaJabatan . "</option>";
        while ($row = $res->fetch_assoc()) {
            $output .= "<option value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
        }
        $output .= '</select>';
        return $output;
    }else{
        return 500;
    }
}

function GetDataKelasJabatanEdit($conn,$Kode){
    $output = '';
    $sql = "SELECT Kode FROM listdata WHERE JenisListData = 'KELAS_JABATAN' ORDER BY Kode";
    $res = $conn->query($sql);
    if($res){
        $output .= '<label class="form-control-label">Kelas Jabatan</label>';
        $output .= "<select class='form-control' id='txtKelasJabatan' name='txtKelasJabatan'>";
        $output .= "<option value='" . $Kode . "'>" . $Kode . "</option>";        
        while ($row = $res->fetch_assoc()) {
            # code...
            $output .= "<option value='" . $row['Kode'] . "'>" . $row['Kode'] . "</option>";
        }
        $output .= '</select>';
        return $output;
    }else{
        return 500;
    }
}
?>