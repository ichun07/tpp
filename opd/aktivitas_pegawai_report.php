<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';
ob_start();
$NamaBulan = array(1 => 'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
);
$KodePegawai = $_GET['KodePegawai'];
$KodeOPD = $_GET['KodeOPD'];
$Bulan = $_GET['Bulan'];
$Tahun = $_GET['Tahun'];
$sql = "SELECT ap.KodeAktivitas, ap.Tanggal, ap.NamaAktivitas, ap.Deskripsi, ap.DurasiKerja, ap.ACCAtasan, ap.KodePegawaiAtasan, ap.KeteranganACC, ap.DurasiKerjaACC, ap.KodePegawai, ap.DokumenPendukung, peg.NamaPegawai AS NamaAtasan, p.NamaPegawai, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung
FROM aktivitaspegawai ap
LEFT JOIN mstpegawai p ON ap.KodePegawai = p.KodePegawai
LEFT JOIN mstpegawai peg ON ap.KodePegawaiAtasan = peg.KodePegawai
WHERE ap.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' GROUP BY ap.KodeAktivitas ORDER BY ap.Tanggal ASC";
$res = $conn->query($sql);
$no = 1;
$JmlRMKE = 0;
$JmlRMKEsetuju = 0;
$dataPegawai = '';

$sql_pegawai = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password 
FROM mstpegawai p
LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
WHERE p.KodeOPD = '$KodeOPD' AND p.KodePegawai = '$KodePegawai'";
$res_pegawai = $conn->query($sql_pegawai);
while ($row_pegawai = $res_pegawai->fetch_assoc()) {
    $dataPegawai = $row_pegawai;
}
?>
<page style="width:100%;" backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <h2 style="margin-top: 0px; width:100%; margin-bottom: 0px; text-align: center;">
        JURNAL AKTIVITAS KERJA PEGAWAI
    </h2>
    <h3 style="width:100%; margin-top: 0px; text-align: center;">
        <?php echo "BULAN " . strtoupper($NamaBulan[(int)$Bulan]) . " TAHUN " . $Tahun; ?>

    </h3>
    <h5 style="margin-top: 20px; margin-bottom: 0px;">
        A. IDENTITAS PEGAWAI
    </h5>
    <table style="width:100%; font-size: 12px;">
        <tr>
            <td style="padding-right: 10px; padding-left: 30px;"> 1.</td>
            <td style="width:15%; padding-right: 4px;">Nama</td>
            <td style="padding-right: 4px;">:</td>
            <td><?php echo ucwords(strtolower($dataPegawai['NamaPegawai'])); ?></td>
        </tr>
        <tr>
            <td style="padding-right: 10px; padding-left: 30px;">2.</td>
            <td style="width:15%; padding-right: 4px;">Pangkat / Gol</td>
            <td style="padding-right: 4px;">:</td>
            <td><?php echo $dataPegawai['Pangkat'] . " / " . $dataPegawai['Golongan']; ?></td>
        </tr>
        <tr>
            <td style="padding-right: 10px; padding-left: 30px;">3.</td>
            <td style="width:15%; padding-right: 4px;">NIP</td>
            <td style="padding-right: 4px;">:</td>
            <td><?php echo $dataPegawai['NIP']; ?></td>
        </tr>
        <tr>
            <td style="padding-right: 10px; padding-left: 30px;">4.</td>
            <td style="width:15%; padding-right: 4px;">Jabatan</td>
            <td style="padding-right: 4px;">:</td>
            <td><?php echo $dataPegawai['NamaJabatan']; ?></td>
        </tr>
    </table>
    <h5 style="margin-top: 20px; margin-bottom: 0px">B. JURNAL AKTIVITAS KERJA PEGAWAI</h5>
    <table border="1" style="width:100%; border-collapse: collapse; font-size: 11px;">
        <tr>
            <th rowspan="2" style="width:4%; text-align: center; padding: 4px;">No</th>
            <th rowspan="2" style="width:8%; text-align: center; padding: 4px;">Tanggal</th>
            <th rowspan="2" style="width:20%; text-align: center; padding: 4px;">Keterangan Aktivitas</th>
            <th rowspan="2" style="width:7%; text-align: center; padding: 4px;">Realisasi Menit Kerja Efektif</th>
            <th colspan="4" style="width:45%; text-align: center; padding: 4px;">Validasi Aktivitas Kerja Pegawai</th>
            <th rowspan="2" style="width:8%; text-align: center; padding: 4px;">Data Pendukung</th>
        </tr>
        <tr>
            <th style="width:4%; text-align: center; padding: 4px;">Status</th>
            <th style="width:6.5%; text-align: center; padding: 4px;">Nilai Penetapan</th>
            <th style="width:20%; text-align: center; padding: 4px;">Alasan Penyesuaian</th>
            <th style="width:5%; text-align: center; padding: 4px;">Penilai</th>
        </tr>
        <tr>
            <th style="text-align: center; padding: 4px;">1</th>
            <th style="text-align: center; padding: 4px;">2</th>
            <th style="text-align: center; padding: 4px;">3</th>
            <th style="text-align: center; padding: 4px;">4</th>
            <th style="text-align: center; padding: 4px;">5</th>
            <th style="text-align: center; padding: 4px;">6</th>
            <th style="text-align: center; padding: 4px;">7</th>
            <th style="text-align: center; padding: 4px;">8</th>
            <th style="text-align: center; padding: 4px;">9</th>
        </tr>
        <?php
        while ($row = $res->fetch_assoc()) {
            ?>
            <tr>
                <td style="text-align: center; padding: 4px;"><?php echo $no++; ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo date('d - m - Y', strtotime($row['Tanggal'])); ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo $row['NamaAktivitas']; ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo $row['DurasiKerja']; ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo $row['ACCAtasan']; ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo $row['DurasiKerjaACC']; ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo $row['KeteranganACC']; ?></td>
                <td style="text-align: center; padding: 4px;"><?php echo $row['NamaAtasan']; ?></td>
                <td style="text-align: center; padding: 4px;"></td>
            </tr>
            <?php
            if ($row['DurasiKerja'] != null && $row['DurasiKerja'] != '') {
                $JmlRMKE += $row['DurasiKerja'];
            }
            if ($row['DurasiKerjaACC'] != null && $row['DurasiKerjaACC'] != '') {
                $JmlRMKEsetuju += $row['DurasiKerjaACC'];
            }
        }
        ?>
        <tr>
            <td style="text-align: center; padding: 4px;"></td>
            <td style="text-align: center; padding: 4px;"></td>
            <td style="text-align: center; padding: 4px;">JUMLAH RMKE</td>
            <td style="text-align: center; padding: 4px;"><?php echo $JmlRMKE; ?></td>
            <td style="text-align: center; padding: 4px;"></td>
            <td style="text-align: center; padding: 4px;"><?php echo $JmlRMKEsetuju; ?></td>
            <td style="text-align: center; padding: 4px;"></td>
            <td style="text-align: center; padding: 4px;"></td>
            <td style="text-align: center; padding: 4px;"></td>
        </tr>
    </table>
    <table style="width: 100%; margin-top: 30px;">
        <tr>
            <td style="width: 40%"></td>
            <td style="width: 25%"></td>
            <?php
            date_default_timezone_set('Asia/Jakarta');
            $tgl = date('d-m-Y');
            $TanggalTxt = date('d');
            $BulanTxt = date('m');
            $TahunTxt = date('Y');
            ?>
            <td style="text-align: center; width: 25%">Jombang, <?php echo $TanggalTxt . ' ' . $NamaBulan[(int)$BulanTxt] . ' ' . $TahunTxt; ?></td>
        </tr>
        <tr style="text-align: center;">
            <td style="text-align: center; width: 40%;">
                <table style="text-align: center; width: 100%;">
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            Atasan pejabat penilai,
                        </td>
                    </tr>
                    <tr style="height: 100px;width: 100%;">
                        <td style="height: 50px;width: 100%;"></td>
                    </tr>
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            Nama
                        </td>
                    </tr>
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            NIP
                        </td>
                    </tr>
                </table>
            </td>
            <td style="text-align: center; width: 25%;">
                <table style="text-align: center; width: 100%;">
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            Bendahara Pengeluaran,
                        </td>
                    </tr>
                    <tr style="height: 100px;width: 100%;">
                        <td style="height: 50px;width: 100%;"></td>
                    </tr>
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            Nama
                        </td>
                    </tr>
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            NIP
                        </td>
                    </tr>
                </table>
            </td>
            <td style="text-align: center; width: 25%;">
                <table style="text-align: center; width: 100%;">
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            Pegawai,
                        </td>
                    </tr>
                    <tr style="height: 100px;width: 100%;">
                        <td style="height: 50px;width: 100%;"></td>
                    </tr>
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            Nama
                        </td>
                    </tr>
                    <tr style="text-align: center; width: 100%;">
                        <td style="text-align: center; width: 100%;">
                            NIP
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</page>
<?php
$filename = "aktivitas_pegawai_" . $KodePegawai . "_" . $Bulan . "_" . $Tahun . ".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF
//==========================================================================================================

require_once('../html2pdf/html2pdf.class.php');
$content = ob_get_clean();
//$content = '<page style="font-family: freeserif">'.nl2br($content).'</page>';
try {
    $html2pdf = new HTML2PDF('L', 'A4', 'en', false, 'ISO-8859-15', array(5, 5, 5, 5));
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->pdf->SetTitle('Jurnal Aktivitas Pegawai');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output($filename);
} catch (HTML2PDF_exception $e) {
    echo $e;
}
?>