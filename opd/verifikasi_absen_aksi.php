<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		$result = '';
		$KodeOPD = $_POST['KodeOPD'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$Pelanggaran = $_POST['Pelanggaran'];
		if($Pelanggaran == "TS"){
			$result = GetDataSenamBulanan($conn, $KodeOPD, $Bulan, $Tahun, $Pelanggaran);
		}else if($Pelanggaran == "TA"){
			$result = GetDataApelBulanan($conn, $KodeOPD, $Bulan, $Tahun, $Pelanggaran);
		}else{
			$result = GetDataAbsen($conn, $KodeOPD, $Bulan, $Tahun, $Pelanggaran);
		}		
		echo json_encode($result);
	}

	if ($_POST["action"] == "AmbilFoto") {
		$KodePegawai = $_POST['KodePegawai'];
		$Tanggal = $_POST['Tanggal'];
		$result = GetDokumenPendukung($conn, $KodePegawai, $Tanggal);
		echo json_encode($result);
	}

	if($_POST["action"] == "UploadDokumen"){
		$KodePegawai = $_POST['KodePegawai'];
		$Tanggal = $_POST['Tanggal'];
		$Dokumen = $_FILES["file"];
		$validextensions = array("jpeg", "jpg", "png");
		$temporary = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		$result = UploadDokumen($conn, $KodePegawai, $Tanggal, $Dokumen,$file_extension, $validextensions);
		echo json_encode($result);
	}
	

	if($_POST["action"] == "TidakApel"){
		$requestData= $_REQUEST;
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$KodeOPD = $_POST['KodeOPD'];
		$Pelanggaran = $_POST['Pelanggaran'];
		$result = GetDataTidakApel($conn,$requestData,$Bulan,$Tahun,$KodeOPD,$Pelanggaran);
		echo json_encode($result);
	}

	if($_POST["action"] == "TidakSenam"){
		$requestData= $_REQUEST;
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$KodeOPD = $_POST['KodeOPD'];
		$Pelanggaran = $_POST['Pelanggaran'];
		$result = GetDataTidakSenam($conn,$requestData,$Bulan,$Tahun,$KodeOPD,$Pelanggaran);
		echo json_encode($result);
	}

	if($_POST["action"] == "UpdateApel"){
		$KodePegawai = $_POST['KodePegawai'];
		$Tanggal = $_POST['Tanggal'];
		$Aksi = $_POST['Aksi'];
		$result = UbahStatusApel($conn, $KodePegawai, $Tanggal, $Aksi);
		echo json_encode($result);
	}

	if($_POST["action"] == "UpdateSenam"){
		$KodePegawai = $_POST['KodePegawai'];
		$Tanggal = $_POST['Tanggal'];
		$Aksi = $_POST['Aksi'];
		$result = UbahStatusSenam($conn, $KodePegawai, $Tanggal, $Aksi);
		echo json_encode($result);
	}

	if($_POST["action"] == "TidakHadir"){
		$requestData= $_REQUEST;
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$KodeOPD = $_POST['KodeOPD'];
		$Pelanggaran = $_POST['Pelanggaran'];
		$result = GetDataTidakHadir($conn,$requestData,$Bulan,$Tahun,$KodeOPD,$Pelanggaran);
		echo json_encode($result);
	}

	if($_POST["action"] == "SenamSemua"){
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$KodeOPD = $_POST['KodeOPD'];
		$result = JadikanSenamSemua($conn,$Bulan,$Tahun,$KodeOPD);
		echo json_encode($result);
	}

	if($_POST["action"] == "ApelSemua"){
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$KodeOPD = $_POST['KodeOPD'];
		$result = JadikanApelSemua($conn,$Bulan,$Tahun,$KodeOPD);
		echo json_encode($result);
	}	
}

function UbahStatusSenam($conn, $KodePegawai, $Tanggal, $Aksi){
	if($Aksi == 'senam'){
		$sql = "UPDATE absensipegawai SET IsSenam = b'0' WHERE KodePegawai = '$KodePegawai' AND Tanggal = '$Tanggal'";
	}else{
		$sql = "UPDATE absensipegawai SET IsSenam = b'1' WHERE KodePegawai = '$KodePegawai' AND Tanggal = '$Tanggal'";
	}
	$res = $conn->query($sql);
	if ($res) {
		session_start();
		InsertLog($conn, 'UPDATE', 'Verifikasi senam pegawai '.$KodePegawai. ' Tanggal '.$Tanggal, $_SESSION['KodeUser']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function GetDataAbsen($conn, $KodeOPD, $Bulan, $Tahun, $Pelanggaran){
	$sql = '';
	if($Pelanggaran == "ALPHA"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '0' AND ap.KetTidakHadir = 'ALPHA'
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "SAKIT"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '0' AND ap.KetTidakHadir = 'SAKIT'
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "CUTI"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '0' AND ap.KetTidakHadir = 'CUTI'
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "TL"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '1' AND IF(ap.TL1 = '1',TRUE,IF(ap.TL2 = '1',TRUE,IF(ap.TL3 = '1',TRUE, FALSE)))
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "PA"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '1' AND IF(ap.PA1 = '1',TRUE,IF(ap.PA2 = '1',TRUE,IF(ap.PA3 = '1',TRUE, FALSE)))
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "LF"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '1' AND IF(ap.LF1 = '1',TRUE,IF(ap.LF2 = '1',TRUE, FALSE))
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "TA"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '1' AND ap.IsJumat = '0' AND ap.IsApel = '1'
		ORDER BY Tanggal ASC";
	}else if($Pelanggaran == "TS"){
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		AND ap.IsHadir = '1' AND ap.IsJumat = '1' AND ap.IsSenam = '1'
		ORDER BY Tanggal ASC";
	}else {
		$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2 
		FROM absensipegawai ap 
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
		ORDER BY Tanggal ASC";
	}
	
	$res = $conn->query($sql);
	if($res){
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th rowspan="2">No</th>
		<th rowspan="2">Tanggal</th>
		<th rowspan="2">Nama Pegawai</th>
		<th rowspan="2" width="30px" class="text-center">Hadir</th>
		<th colspan="3" width="30px" class="text-center">Tidak Hadir</th>
		<th colspan="5" class="text-center">Pelanggaran</th>
		<th rowspan="2" class="text-center">File</th>
		</tr>
		<tr>
		<th width="25px" class="text-center">Sakit</th>
		<th width="25px" class="text-center">Cuti</th>
		<th width="25px" class="text-center">Alpha</th>
		<th width="25px" class="text-center">Telat</th>
		<th width="25px" class="text-center">Pulang Awal</th>
		<th width="25px" class="text-center">Lupa Finger</th>
		<th width="25px" class="text-center">Tidak Apel</th>
		<th width="25px" class="text-center">Tidak Senam</th>
		</tr>
		</thead>
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . date('d - m - Y', strtotime($row['Tanggal'])) . '</td>
			<td>' . $row['NamaPegawai'] . '</td>';
			if($row['IsHadir'] < 1){
				$output.='<td class="text-center"><input type="hidden" value="0"></td>';
			}else{
				$output.='<td class="text-center">&#10004;<input type="hidden" value="1"></td>';
			}			
			if($row['IsHadir'] < 1){
				if($row['KetTidakHadir'] == 'ALPHA'){
					$output.='<td class="text-center"></td>
					<td class="text-center"></td>
					<td class="text-center">&#10004;</td>';
				}else if($row['KetTidakHadir'] == 'CUTI'){
					$output.='<td class="text-center"></td>
					<td class="text-center">&#10004;</td>
					<td class="text-center"></td>';
				}else if($row['KetTidakHadir'] == 'SAKIT'){
					$output.='<td class="text-center">&#10004;</td>
					<td class="text-center"></td>
					<td class="text-center"></td>';
				}
				
			}else{
				$output.='<td class="text-center"></td>
				<td class="text-center"></td>
				<td class="text-center"></td>';
			}
			if($row['TL1'] == 1 || $row['TL2'] == 1 || $row['TL3'] == 1){
				$output.='<td class="text-center">&#10004;</td>';
			}else{
				$output.='<td class="text-center"></td>';
			}
			if($row['PA1'] == 1 || $row['PA2'] == 1 || $row['PA3'] == 1){
				$output.='<td class="text-center">&#10004;</td>';
			}else{
				$output.='<td class="text-center"></td>';
			}
			if($row['LF1'] == 1 || $row['LF2'] == 1){
				$output.='<td class="text-center">&#10004;</td>';
			}else{
				$output.='<td class="text-center"></td>';
			}
			if($row['IsApel'] == 1){
				$output.='<td class="text-center">&#10004;</td>';
			}else{
				$output.='<td class="text-center"></td>';
			}
			if($row['IsSenam'] == 1){
				$output.='<td class="text-center">&#10004;</td>';
			}else{
				$output.='<td class="text-center"></td>';
			}
			$output.='<td><button data-value2="'.$row['IsHadir'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnVerify" id="btnVerify" class="btn btn-primary"><span class="icon icon-pencil-case" aria-hidden="true"></span></button></td>
			</tr>';
		}
		$output .= '</tbody>
		</table>';
		return array('response' => 200, 'HtmlTabel' => $output);
	}else{
		return 500;
	}
}

function GetDokumenPendukung($conn, $KodePegawai, $Tanggal){
	$sql = "SELECT DokumenPendukung FROM absensipegawai WHERE Tanggal = '$Tanggal' AND KodePegawai = '$KodePegawai'";
	$sth = $conn->query($sql);
	if($sth){
		$result=mysqli_fetch_array($sth);
		if($result['DokumenPendukung'] != null){
			return '<img id="previewing" src="data:image/jpeg;base64,'.base64_encode( $result['DokumenPendukung'] ).'"/>';
		}else{
			return 500;
		}
	}else{
		return 500;
	}	
}

function GetDataSenamBulanan($conn, $KodeOPD, $Bulan, $Tahun, $Pelanggaran){
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung
	FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.IsJumat = '1' AND ap.IsSenam = '1'
	ORDER BY Tanggal ASC";
	$res = $conn->query($sql);
	if($res){
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th>No</th>
		<th>Tanggal</th>
		<th>Nama Pegawai</th>
		<th>Jabatan</th>
		<th width="30px" class="text-center">Senam</th>
		<th width="30px" class="text-center">Tidak Senam</th>
		<th>Dokumen</th>
		</tr>
		</thead>
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . date('d - m - Y', strtotime($row['Tanggal'])) . '</td>
			<td>' . $row['NamaPegawai'] . '</td>
			<td>' . $row['NamaJabatan'] . '</td>';
			if($row['IsSenam'] == null){
				if($row['DokumenPendukung'] < 1){
					$output .= '<td><button data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnJdSenam" id="btnJdSenam" class="btn btn-primary"><span aria-hidden="true">&#10004;</span></button>
					</td>
					<td class="text-center">&#10004;</td>';
				}else{
					$output .= '<td></td>
					<td class="text-center">&#10004;</td>';
				}
			}else{
				if($row['IsSenam'] == '0'){
					$output .= '<td class="text-center">&#10004;</td>
					<td></td>';
				}else{					
					if($row['DokumenPendukung'] < 1){
						$output .= '<td><button data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnJdSenam" id="btnJdSenam" class="btn btn-primary"><span aria-hidden="true">&#10004;</span></button>
						</td>
						<td class="text-center">&#10004;</td>';
					}else{
						$output .= '<td></td>
						<td class="text-center">&#10004;</td>';
					}
				}				
			}

			if($row['IsSenam'] == null){
				if($row['DokumenPendukung'] < 1){
					$output.='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnSenam" id="btnSenam" class="btn btn-primary"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="0"></td>
					</tr>';
				}else{
					$output.='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnSenam" id="btnSenam" class="btn btn-success"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="1"></td>
					</tr>';
				}
			}else{
				if($row['IsSenam'] == '1'){
					if($row['DokumenPendukung'] < 1){
						$output.='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnSenam" id="btnSenam" class="btn btn-primary"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="0"></td>
						</tr>';
					}else{
						$output.='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnSenam" id="btnSenam" class="btn btn-success"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="1"></td>
						</tr>';
					}
				}else{
					$output.='<td></td>
					</tr>';
				}
			}
		}
		$output .= '</tbody>
		</table>';
		return array('response' => 200, 'HtmlTabel' => $output);
	}else{
		return 500;
	}
}

function UploadDokumen($conn, $KodePegawai, $Tanggal, $Dokumen,$file_extension, $validextensions){
	if ((($Dokumen["type"] == "image/png") || ($Dokumen["type"] == "image/jpg") || ($Dokumen["type"] == "image/jpeg")) && ($Dokumen["size"] < 5000000) && in_array($file_extension, $validextensions)){
		if ($Dokumen["error"] > 0){
			$error_message = "Return Code: " . $Dokumen["error"]."";
			return array('response' => 500, 'error_response' => 'File Error');
		}else{
			if (file_exists($Dokumen["name"])) {
				$error_message = "".$Dokumen["name"] . " already exists";
				return array('response' => 500, 'error_response' => 'File Exists');
			}else{
				$sourcePath = $Dokumen['tmp_name'];
				$targetPath = $Dokumen['name'];
				move_uploaded_file($sourcePath,$targetPath);
				$lalala = mysqli_real_escape_string($conn, file_get_contents($targetPath));
				$sql = "UPDATE absensipegawai SET DokumenPendukung = '$lalala' 
				WHERE Tanggal = '$Tanggal' AND KodePegawai = '$KodePegawai'";
				$res = $conn->query($sql);
				if($res){
					unlink($targetPath);
					return array('response' => 200);
				}else{
					unlink($targetPath);
					return array('response' => 500, 'error_response' => 'Upload Failed');
				}
				
			}
		}
	}else{
		return array('response' => 404, 'error_response' => 404);
	}
}


function GetDataTidakApel($conn,$requestData,$Bulan,$Tahun,$KodeOPD,$Pelanggaran){
	$columns = array( 
// datatable column index  => database column name
		0 => 'Tanggal',
		1 => 'Tanggal', 
		2 => 'NamaPegawai',
		3 => 'NamaJabatan',
		4 => 'IsApel',
		5 => 'IsApel'
	);

// getting total number records without any search
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD 
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.IsJumat = '0' AND IF(LENGTH('$Pelanggaran') > 0, IsApel = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0'";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD ";
	$sql.="WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsApel = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '0' AND p.NamaPegawai LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsApel = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '0' AND p.NIP LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsApel = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '0' AND j.NamaJabatan LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsApel = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '0' AND ap.Tanggal LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); // again run query with limit
	
} else {	

	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.IsJumat = '0' AND IF(LENGTH('$Pelanggaran') > 0, IsApel = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0'";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
$no = 1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 
	if($row['IsApel'] == '0'){
		$Senam = '<td class="text-center">&#10004;</td>';
		$TidakSenam = '<td><button data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnJdTdkApel" id="btnJdTdkApel" class="btn btn-primary">Jadikan Tidak Apel</button>
		</td>';
		$Dokumen = '<td class="text-center"></td>';
	}else{
		if($row['DokumenPendukung'] < 1){
			$Senam = '<td><button data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnJdApel" id="btnJdApel" class="btn btn-primary">Jadikan Apel</button>
			</td>';
			$TidakSenam = '<td class="text-center">&#10004;</td>';
			$Dokumen ='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnDokumen" id="btnDokumen" class="btn btn-primary"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="0"></td>';
		}else{
			$Senam = '<td></td>';
			$TidakSenam = '<td class="text-center">&#10004;</td>';
			$Dokumen ='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnDokumen" id="btnDokumen" class="btn btn-success"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="1"></td>';
		}
	}
	$nestedData[] = $no++;
	$nestedData[] = date('d - m - Y', strtotime($row['Tanggal']));
	$nestedData[] = $row['NamaPegawai'];
	$nestedData[] = $row['NamaJabatan'];
	$nestedData[] = $TidakSenam;
	$nestedData[] = $Senam;
	$nestedData[] = $Dokumen;		
	
	$data[] = $nestedData;

}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
		);

return $json_data;  // send data as json format
}

function UbahStatusApel($conn, $KodePegawai, $Tanggal, $Aksi){
	if($Aksi == 'apel'){
		$sql = "UPDATE absensipegawai SET IsApel = b'0' WHERE KodePegawai = '$KodePegawai' AND Tanggal = '$Tanggal'";
	}else{
		$sql = "UPDATE absensipegawai SET IsApel = b'1' WHERE KodePegawai = '$KodePegawai' AND Tanggal = '$Tanggal'";
	}
	$res = $conn->query($sql);
	if ($res) {
		session_start();
		InsertLog($conn, 'UPDATE', 'Verifikasi apel pegawai '.$KodePegawai. ' Tanggal '.$Tanggal, $_SESSION['KodeUser']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function GetDataTidakSenam($conn,$requestData,$Bulan,$Tahun,$KodeOPD, $Pelanggaran){
	$columns = array( 
// datatable column index  => database column name
		0 => 'Tanggal',
		1 => 'NamaPegawai', 
		2 => 'NamaJabatan',
	);

// getting total number records without any search
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD 
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.IsJumat = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsSenam = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0'";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD ";
	$sql.="WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsSenam = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '1' AND p.NamaPegawai LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsSenam = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '1' AND p.NIP LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsSenam = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '1' AND j.NamaJabatan LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsSenam = '1', TRUE) AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0' AND ap.IsJumat = '1' AND ap.Tanggal LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); // again run query with limit
	
} else {
	$sql = "SELECT ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND IF(LENGTH('$Pelanggaran') > 0, IsSenam = '1', TRUE) AND ap.IsJumat = '1' AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0'";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
$no = 1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 
	if($row['IsSenam'] == '0'){
		$Senam = '<td class="text-center">&#10004;</td>';
		$TidakSenam = '<td><button data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnJdTdkSenam" id="btnJdTdkSenam" class="btn btn-primary">Jadikan Tidak Senam</button>
		</td>';
		$Dokumen = '<td class="text-center"></td>';
	}else{
		if($row['DokumenPendukung'] < 1){
			$Senam = '<td><button data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnJdSenam" id="btnJdSenam" class="btn btn-primary">Jadikan Senam</button>
			</td>';
			$TidakSenam = '<td class="text-center">&#10004;</td>';
			$Dokumen ='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnDokumen" id="btnDokumen" class="btn btn-primary">Jadikan Senam</button><input type="hidden" value="0"></td>';
		}else{
			$Senam = '<td></td>';
			$TidakSenam = '<td class="text-center">&#10004;</td>';
			$Dokumen ='<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnDokumen" id="btnDokumen" class="btn btn-success"><span class="icon icon-pencil-case" aria-hidden="true"></span></button><input type="hidden" value="1"></td>';
		}
	}
	$nestedData[] = $no++;
	$nestedData[] = date('d - m - Y', strtotime($row['Tanggal']));
	$nestedData[] = $row['NamaPegawai'];
	$nestedData[] = $row['NamaJabatan'];
	$nestedData[] = $TidakSenam;
	$nestedData[] = $Senam;
	$nestedData[] = $Dokumen;		
	
	$data[] = $nestedData;

}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
		);

return $json_data;  // send data as json format
}

function GetDataTidakHadir($conn,$requestData,$Bulan,$Tahun,$KodeOPD,$Pelanggaran){
	$columns = array( 
// datatable column index  => database column name
		0 => 'Tanggal',
		1 => 'Tanggal', 
		2 => 'NamaPegawai',
		3 => 'IsHadir',
		4 => "ap.KetTidakHadir = 'SAKIT'",
		5 => "ap.KetTidakHadir = 'CUTI'",
		6 => "ap.KetTidakHadir = 'ALPHA'",
		7 => "ap.IsDinas"
	);

// getting total number records without any search
	$sql = "SELECT ap.IsDinas, ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD 
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND IF(ap.IsHadir != '0', ap.IsDinas = '1', ap.IsHadir = '0') AND IF(LENGTH('$Pelanggaran')>0,IF('$Pelanggaran' = 'DINAS', IsHadir = '1' AND IsDinas = '1',ap.KetTidakHadir = '$Pelanggaran'),TRUE)";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT ap.IsDinas, ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD ";
	$sql.="WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND IF(ap.IsHadir != '0', ap.IsDinas = '1', ap.IsHadir = '0') AND IF(LENGTH('$Pelanggaran')>0,IF('$Pelanggaran' = 'DINAS', IsHadir = '1' AND IsDinas = '1',ap.KetTidakHadir = '$Pelanggaran'),TRUE) AND p.NamaPegawai LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND IF(ap.IsHadir != '0', ap.IsDinas = '1', ap.IsHadir = '0') AND IF(LENGTH('$Pelanggaran')>0,IF('$Pelanggaran' = 'DINAS', IsHadir = '1' AND IsDinas = '1',ap.KetTidakHadir = '$Pelanggaran'),TRUE) AND p.NIP LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND IF(ap.IsHadir != '0', ap.IsDinas = '1', ap.IsHadir = '0') AND IF(LENGTH('$Pelanggaran')>0,IF('$Pelanggaran' = 'DINAS', IsHadir = '1' AND IsDinas = '1',ap.KetTidakHadir = '$Pelanggaran'),TRUE) AND j.NamaJabatan LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND IF(ap.IsHadir != '0', ap.IsDinas = '1', ap.IsHadir = '0') AND IF(LENGTH('$Pelanggaran')>0,IF('$Pelanggaran' = 'DINAS', IsHadir = '1' AND IsDinas = '1',ap.KetTidakHadir = '$Pelanggaran'),TRUE) AND ap.Tanggal LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); // again run query with limit
	
} else {	

	$sql = "SELECT ap.IsDinas, ap.Tanggal, ap.IsHadir, ap.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Status, p.PokokTPP, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, ap.KetTidakHadir, IF(ap.IsJumat < 1,'Senin Kamis', 'Jumat') AS HariKerja, ap.IsSenam, ap.IsApel, ap.TL1, ap.TL2, ap.TL3, ap.PA1, ap.PA2, ap.PA3, ap.LF1, ap.LF2, IF(ap.DokumenPendukung IS NULL, 0, 1) AS DokumenPendukung ";
	$sql.=" FROM absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND IF(ap.IsHadir != '0', ap.IsDinas = '1', ap.IsHadir = '0') AND IF(LENGTH('$Pelanggaran')>0,IF('$Pelanggaran' = 'DINAS', IsHadir = '1' AND IsDinas = '1',ap.KetTidakHadir = '$Pelanggaran'),TRUE)";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
$no = 1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 
	if($row['IsHadir'] < 1){
		$Hadir ='<td class="text-center"><input type="hidden" value="0"></td>';
		if($row['KetTidakHadir'] == 'ALPHA'){
			$Sakit = '<td class="text-center"></td>';
			$Cuti = '<td class="text-center"></td>';
			$Alpha = '<td class="text-center">&#10004;</td>';
		}else if($row['KetTidakHadir'] == 'CUTI'){
			$Sakit = '<td class="text-center"></td>';
			$Cuti = '<td class="text-center">&#10004;</td>';
			$Alpha = '<td class="text-center"></td>';
		}else if($row['KetTidakHadir'] == 'SAKIT'){
			$Sakit = '<td class="text-center">&#10004;</td>';
			$Cuti = '<td class="text-center"></td>';
			$Alpha = '<td class="text-center"></td>';
		}
	}else{
		$Hadir ='<td class="text-center">&#10004;<input type="hidden" value="1"></td>';
		$Sakit = '<td class="text-center"></td>';
		$Cuti = '<td class="text-center"></td>';
		$Alpha = '<td class="text-center"></td>';
	}

	if($row['TL1'] == 1 || $row['TL2'] == 1 || $row['TL3'] == 1){
		$Telat = '<td class="text-center">&#10004;</td>';
	}else{
		$Telat = '<td class="text-center"></td>';
	}

	if($row['PA1'] == 1 || $row['PA2'] == 1 || $row['PA3'] == 1){
		$Pulang = '<td class="text-center">&#10004;</td>';
	}else{
		$Pulang = '<td class="text-center"></td>';
	}
	if($row['LF1'] == 1 || $row['LF2'] == 1){
		$Finger = '<td class="text-center">&#10004;</td>';
	}else{
		$Finger = '<td class="text-center"></td>';
	}
	if($row['IsApel'] == 1){
		$Apel = '<td class="text-center">&#10004;</td>';
	}else{
		$Apel ='<td class="text-center"></td>';
	}
	if($row['IsSenam'] == 1){
		$Senam = '<td class="text-center">&#10004;</td>';
	}else{
		$Senam = '<td class="text-center"></td>';
	}
	if($row['IsDinas'] == 1){
		$Dinas = '<td class="text-center">&#10004;</td>';
	}else{
		$Dinas = '<td class="text-center"></td>';
	}
	$nestedData[] = $no++;
	$nestedData[] = date('d - m - Y', strtotime($row['Tanggal']));
	$nestedData[] = $row['NamaPegawai'];
	$nestedData[] = $Hadir;
	$nestedData[] = $Sakit;
	$nestedData[] = $Cuti;
	$nestedData[] = $Alpha;
	$nestedData[] = $Dinas;
	// $nestedData[] = $Telat;
	// $nestedData[] = $Pulang;
	// $nestedData[] = $Finger;
	// $nestedData[] = $Apel;
	// $nestedData[] = $Senam;	
	$nestedData[] = '<td><button data-value2="'.$row['DokumenPendukung'].'" data-value="' . $row['Tanggal'] . '" value="' . $row['KodePegawai'] . '" name="btnVerify" id="btnVerify" class="btn btn-primary"><span class="icon icon-pencil-case" aria-hidden="true"></span></button></td>';
	
	$data[] = $nestedData;

}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
		);

return $json_data;  // send data as json format
}

function JadikanSenamSemua($conn,$Bulan,$Tahun,$KodeOPD){
	$sql = "UPDATE absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD 
	SET IsSenam = b'0'
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.IsJumat = '1' AND IsSenam = '1' AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0'";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'UPDATE', 'Verifikasi senam semua pegawai Bulan '.$Bulan.' Tahun '.$Tahun, $_SESSION['KodeUser']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function JadikanApelSemua($conn,$Bulan,$Tahun,$KodeOPD){
	$sql = "UPDATE absensipegawai ap 
	LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD 
	SET IsApel = b'0' 
	WHERE MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.KodeOPD = '$KodeOPD'
	AND ap.IsHadir = '1' AND ap.IsJumat = '0' AND IsApel = '1' AND DokumenPendukung IS NULL AND ap.TL1 = '0' AND ap.TL2 = '0' AND ap.TL3 = '0'";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'UPDATE', 'Verifikasi apel semua pegawai Bulan '.$Bulan.' Tahun '.$Tahun, $_SESSION['KodeUser']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}