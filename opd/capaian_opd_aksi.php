<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$Triwulan = $_POST['Triwulan'];
		$result = GetDataCapaian($conn, $KodeOPD, $Tahun, $Triwulan);
		echo json_encode($result);
	}

	if ($_POST["action"] == "InsertData") {
		$Tahun = $_POST['Tahun'];
		$Triwulan = $_POST['Triwulan'];
		$KodeOPD = $_POST['KodeOPD'];
		$RealisasiAnggaran = $_POST['RealisasiAnggaran'];
		$NilaiKategoriNRA = NilaiRealisasiAnggaran($RealisasiAnggaran);
		$NilaiOutputKegiatan = $_POST['NilaiOutputKegiatan'];
		$NilaiKategoriNOK = NilaiOutputKegiatan($NilaiOutputKegiatan);
		$EvalusasiSAKIP = $_POST['EvalusasiSAKIP'];
		$NilaiKategoriSAKIP = NilaiSAKIP($EvalusasiSAKIP);
		$NRABobot = ($NilaiKategoriNRA * 30) / 100;
		$NOKBobot = ($NilaiKategoriNOK * 30) / 100;
		$SAKIPBobot = ($NilaiKategoriSAKIP * 40) / 100;
		$result = InsertDataCapaian($conn, $Tahun, $Triwulan, $KodeOPD, $RealisasiAnggaran, $NilaiKategoriNRA, $NRABobot, $NilaiOutputKegiatan, $NilaiKategoriNOK, $NOKBobot, $EvalusasiSAKIP, $NilaiKategoriSAKIP, $SAKIPBobot);
		echo json_encode($result);
	}
}

function InsertDataCapaian($conn, $Tahun, $Triwulan, $KodeOPD, $RealisasiAnggaran, $NilaiKategoriNRA, $NRABobot, $NilaiOutputKegiatan, $NilaiKategoriNOK, $NOKBobot, $EvalusasiSAKIP, $NilaiKategoriSAKIP, $SAKIPBobot)
{
	$sql = "INSERT INTO capaiankinerjaopd (Tahun, Triwulan, KodeOPD, RealisasiAnggaran, NilaiKategoriNRA, NRABobot, NilaiOutputKegiatan, NilaiKategoriNOK, NOKBobot, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot) VALUES ('$Tahun', '$Triwulan', '$KodeOPD', '$RealisasiAnggaran', '$NilaiKategoriNRA', '$NRABobot', '$NilaiOutputKegiatan', '$NilaiKategoriNOK', '$NOKBobot', '$EvalusasiSAKIP', '$NilaiKategoriSAKIP', '$SAKIPBobot')";
	$res = $conn->query($sql);
	if ($res) {
		return array('response' => 200);
	} else {
		return array('response' => 500);
	}
}

function GetDataCapaian($conn, $KodeOPD, $Tahun, $Triwulan)
{
	$output = '';
	$sql = '';
	$res_pegawai = '';
		$sql = "SELECT Tahun, Triwulan, KodeOPD, RealisasiAnggaran, NilaiKategoriNRA, NRABobot, NilaiOutputKegiatan, NilaiKategoriNOK, NOKBobot, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot FROM capaiankinerjaopd WHERE 
		KodeOPD = '$KodeOPD' AND Tahun = '$Tahun' AND Triwulan = '$Triwulan' ORDER BY Tahun DESC";
	$res = $conn->query($sql);
	if ($res) {
		$output = '<table width="100%" class="table table-striped table-bordered dataTable">
		<thead>
		<tr>
		<th class="text-center">Nilai Realisasi Anggaran</th>
		<th class="text-center">Nilai Output Kegiatan</th>
		<th class="text-center">Nilai SAKIP</th>
		<th class="text-center" width="30px">Nilai Kinerja Perangkat Daerah</th>
		</tr>
		</thead>
		<tbody>';
		while ($row = $res->fetch_assoc()) {
			$output .= '<tr>
			<td>
			Realisasi Anggaran : ' . $row['RealisasiAnggaran'] . '%<br>
			Nilai Kategori NRA : ' . $row['NilaiKategoriNRA'] . '<br>
			NRA x Bobot 30% : ' . $row['NRABobot'] . '
			</td>
			<td>
			Rata Rata Capaian Output : ' . $row['NilaiOutputKegiatan'] . '%<br>
			Nilai Kategori NOK : ' . $row['NilaiKategoriNOK'] . '<br>
			NOK x Bobot 30% : ' . $row['NOKBobot'] . '<br>
			</td>
			<td>
			Kategori Nilai SAKIP : ' . $row['EvalusasiSAKIP'] . '<br>
			Nilai Kategori S : ' . $row['NilaiKategoriSAKIP'] . '<br>
			NS x Bobot 40% : ' . $row['SAKIPBobot'] . '<br>
			</td>
			<td class="text-center">0</td>			
			</tr>';
			$res_pegawai = GetDataPegawai($conn,$KodeOPD,$row['NilaiKategoriNRA'],$row['NilaiKategoriNOK'],$row['NilaiKategoriSAKIP']);
		}
		$output .= '</tbody>
		</table>';
		return array('response' => 200, 'Tabel' => $output, 'Tabel2' => $res_pegawai);
	} else {
		return array('response' => 500);
	}
}

function GetDataPegawai($conn,$KodeOPD,$NRA,$NOK,$SAKIP){
	$sql ="SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE p.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th width="30px">No</th>
		<th>Nama / NIP</th>
		<th>Jabatan</th>
		<th width="30px">Kelas Jabatan</th>
		<th width="30px">Harga Jabatan</th>
		<th>TPP Pokok</th>
		<th>TPP Kinerja</th>
		<th>PPH 21%</th>
		<th>Potongan Pajak</th>
		<th>TPP Diterima</th>
		</tr>
		</thead>		
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			# code...
			$PokokTPP = $row['PokokTPPPegawai'];
			$TPPKinerja = TPPKinerja($PokokTPP, $NRA, $NOK, $SAKIP);
			$PotonganPajak = $TPPKinerja * 21/100;
			$Diterima = ($TPPKinerja - $PotonganPajak);
			$output .= '<tr>
			<td>' . $no++ . '</td>
			<td>' . $row['NamaPegawai'] . '</td>
			<td>' . $row['NamaJabatan'] . '</td>
			<td>' . $row['KelasJabatan'] . '</td>
			<td class="text-right">' . number_format($row['HargaJabatan']) . '</td>
			<td class="text-right">' . number_format($PokokTPP) . '</td>
			<td class="text-right">' . number_format($TPPKinerja) . '</td>
			<td class="text-right">21%</td>
			<td class="text-right">'.number_format($PotonganPajak).'</td>
			<td class="text-right">' . number_format($Diterima) . '</td>
			</tr>';
		}
		$output .= '</tbody>
		</table>';
		return $output;
	}
}

?>