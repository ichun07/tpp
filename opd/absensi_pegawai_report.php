<?php
include_once '../config/koneksi.php';
ob_start();
$NamaBulan = array (1 =>   'Januari',
	'Februari',
	'Maret',
	'April',
	'Mei',
	'Juni',
	'Juli',
	'Agustus',
	'September',
	'Oktober',
	'November',
	'Desember'
);
$KodeOPD = $_GET['KodeOPD'];
$Bulan = $_GET['Bulan'];
$Tahun = $_GET['Tahun'];
$HKE = $_GET['HKE'];
$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir,(tb.JmlSakit + tb.JmlCuti + tb.JmlAlpha) AS JmlAbsen, tb.JmlSenam, tb.JmlApel, ((tb.JmlSenam + tb.JmlApel) * 3) AS JmlAS, (tb.JmlTL1 * 1) AS JmlTL1, (tb.JmlTL2 * 2) AS JmlTL2, (tb.JmlTL3 * 3) AS JmlTL3, (tb.JmlPA1 * 1) AS JmlPA1, (tb.JmlPA2 * 2) AS JmlPA2, (tb.JmlPA3 * 3) AS JmlPA3, (tb.JmlLF1 * 3) AS JmlLF1, (tb.JmlLF2 * 3) AS JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas
FROM tppbulanan tb
LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
ORDER BY tb.Bulan, tb.Tahun";
$res = $conn->query($sql);
$no = 1;
?>
<page style="width:100%;" backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm"> 
	<h2 style="width:100%; text-align: center; page-break-after:always; margin-top: 0px;">DAFTAR REKAPITULASI TINGKAT KEHADIRAN PEGAWAI</h2>
	<table style="width:100%; margin-top: 20px;">
		<tr>
			<td style="width:20%;">Bulan/Tahun</td>
			<td width="10px"> : </td>
			<td> <?php echo $NamaBulan[(int)$Bulan]." / ".$Tahun;?></td>
		</tr>
		<tr>
			<td style="width:20%;">Hari Kerja Efektif (HKE)</td>
			<td width="10px"> : </td>
			<td> <?php echo $HKE." hari";?></td>
		</tr>
	</table>
	<!-- TABEL ABSENSI -->
	<table border="1" style="width:100%; margin-top: 20px; border-collapse: collapse; font-size: 11px;">
		<tr>
			<th rowspan="2" style="width:5%; text-align: center; padding: 4px;">No</th>
			<th rowspan="2" style="width:20%; text-align: center; padding: 4px;">Nama NIP</th>
			<th colspan="4" style="width:15%; text-align: center; padding: 4px;">Tidak Hadir</th>
			<th colspan="10" style="width:35%; text-align: center; padding: 4px;">Pelanggaran</th>
			<th rowspan="2" style="width:10%; text-align: center; padding: 4px;">Jml. Hadir dan Persentase</th>
			<th rowspan="2" style="width:8%; text-align: center; padding: 4px;">Tingkat Kehadiran</th>
		</tr>
		<tr>
			<th style="text-align: center; padding: 4px;">Alpa</th>
			<th style="text-align: center; padding: 4px;">Sakit</th>
			<th style="text-align: center; padding: 4px;">Cuti</th>
			<th style="text-align: center; padding: 4px;">Jml</th>
			<th style="text-align: center; padding: 4px;">TL1</th>
			<th style="text-align: center; padding: 4px;">TL2</th>
			<th style="text-align: center; padding: 4px;">TL3</th>
			<th style="text-align: center; padding: 4px;">PA1</th>
			<th style="text-align: center; padding: 4px;">PA2</th>
			<th style="text-align: center; padding: 4px;">PA3</th>
			<th style="text-align: center; padding: 4px;">LF1</th>
			<th style="text-align: center; padding: 4px;">LF2</th>
			<th style="text-align: center; padding: 4px;">AS</th>
			<th style="text-align: center; padding: 4px;">Jml</th>
		</tr>
		<?php
		while ($row = $res->fetch_assoc()) {
			$JmlHariEfektif = $row['JmlHariEfektif'];
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'] - $row['JmlSakit'] - $row['JmlCuti'];
			$JmlPel = $row['JmlTL1']+$row['JmlTL2']+$row['JmlTL3']+$row['JmlPA1']+$row['JmlPA2']+$row['JmlPA3']+$row['JmlLF1']+$row['JmlLF2']+$row['JmlAS']; ?>
			<tr>
				<td style="text-align: center; padding: 4px;"><?php echo $no++; ?></td>
				<td style="padding: 4px; font-size: 12px;"><?php echo ucwords(strtolower($row['NamaPegawai'])); ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlAlpha']; ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlSakit']; ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlCuti']; ?></td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlAbsen']; ?> Hari</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlTL1']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlTL2']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlTL3']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlPA1']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlPA2']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlPA3']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlLF1']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlLF2']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $row['JmlAS']; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $JmlPel; ?>%</td>
				<td style="text-align: center; padding: 4px;"><?php echo $JmlHadir; ?> Hari</td>
				<td style="text-align: center; padding: 4px;"></td>
			</tr>
			<?php
		}
		?>
	</table>

	<table style="width: 100%; margin-top: 30px;">
		<tr>
			<td style="width: 50%"></td>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			$tgl=date('d-m-Y');
			$TanggalTxt = date('d');
			$BulanTxt = date('m');
			$TahunTxt = date('Y');
			?>
			<td style="text-align: center; width: 50%">Jombang, <?php echo $TanggalTxt.' '.$NamaBulan[(int)$BulanTxt].' '.$TahunTxt; ?></td>
		</tr>
		<tr style="text-align: center;">
			<td style="text-align: center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Kepala Perangkat Daerah,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
			<td style="text-align: center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Sekretaris,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>
	<table style="margin-top: 30px;">
		<tr>
			<td colspan="3">Keterangan :</td>
		</tr>
		<tr>
			<td colspan="3">* Jml. Hadir = Jumlah hari kerja efektif (HKE) - Alpa</td>
		</tr>
		<tr>
			<td width="100px">TL1, 2, 3</td>
			<td width="20px">:</td>
			<td>Kategori Terlambat</td>
		</tr>
		<tr>
			<td width="100px">PA1, 2, 3</td>
			<td width="20px">:</td>
			<td>Kategori Pulang Awal</td>
		</tr>
		<tr>
			<td width="100px"> LF1, 2</td>
			<td width="20px">:</td>
			<td>Lupa Finger Masuk atau Pulang</td>
		</tr>
		<tr>
			<td width="100px">AS</td>
			<td width="20px">:</td>
			<td>Tidak Apel/ Senam</td>
		</tr>
	</table>
</page>
<?php
$filename="absensi_".$Bulan."_".$Tahun.".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF
//==========================================================================================================

require_once('../html2pdf/html2pdf.class.php');
$content = ob_get_clean();
//$content = '<page style="font-family: freeserif">'.nl2br($content).'</page>';
try
{
	$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(5, 5, 5, 5));
	$html2pdf->setDefaultFont('Arial');
    $html2pdf->pdf->SetTitle('Absensi Pegawai');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }
?>