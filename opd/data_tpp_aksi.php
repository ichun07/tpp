<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		$KodeOPD = $_POST['KodeOPD'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetTpp($conn, $KodeOPD, $Bulan, $Tahun);
		echo json_encode($result);
	}
}

function GetTpp($conn, $KodeOPD, $Bulan, $Tahun)
{
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, p.PPH, j.KodeManual, j.NilaiJabatan, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IFNULL(SUM(atv.DurasiKerjaACC),0) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, s.PersenSKP, IFNULL(s.NilaiSKP, 0) AS NilaiSKP, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	LEFT JOIN skppegawai s ON s.KodePegawai = tb.KodePegawai AND s.Tahun = (tb.Tahun - 1)
	LEFT JOIN capaiankinerjaopd c ON c.KodeOPD = p.KodeOPD AND c.Tahun = tb.Tahun AND c.Triwulan = IF(3 >= tb.Bulan AND tb.Bulan >= 1, 1, IF(6 >= tb.Bulan AND tb.Bulan >= 4, 2, IF(9 >= tb.Bulan AND tb.Bulan >= 7, 3, IF(12 >= tb.Bulan and tb.Bulan >= 10, 4, TRUE))))
	WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun";
	$res = $conn->query($sql);
	if ($res) {
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th class="text-center">No</th>
		<th class="text-center">Nama / NIP</th>
		<th class="text-center">Jabatan</th>
		<th class="text-center">Kelas Jabatan</th>
		<th class="text-center">Nilai Jabatan</th>
		<th class="text-center">TPP Pokok</th>
		<th class="text-center">Tingkat Kedisiplinan (%)</th>
		<th class="text-center">TPP Kedisiplinan (30%)</th>
		<th class="text-center">Nilai Aktivitas</th>
		<th class="text-center">TPP Aktivitas (30%)</th>
		<th class="text-center">Nilai SKP</th>
		<th class="text-center">TPP SKP (20%)</th>
		<th class="text-center">Nilai SAKIP</th>
		<th class="text-center">TPP SAKIP (20%)</th>
		<th class="text-center">Jumlah TPP Bruto</th>
		<th class="text-center">PPH 21%</th>
		<th class="text-center">TPP Diterima</th>
		</tr>
		</thead>		
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
            # code...
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
			$ProsenKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $KodeOPD, $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);
			
			$NilaiJabatan = $row['NilaiJabatan'];
			$PokokTPP = $row['PokokTPPPegawai'];
			$PersenSKP = $row['PersenSKP'];
			$NilaiSKP = $row['NilaiSKP'];
			$NilaiSAKIP = $row['NilaiKategoriSAKIP'];

			$BebanAktivitas = $row['MenitAktivitas'];

			//jika pegawai mempunyai bawahan maka mendapat 1% dari beban tiap aktivitas bawahan
			$NilaiTambah = HitungNilaiAktivitasBawahan($conn, $KodeOPD, $row['KodeJabatan'], $Bulan, $Tahun);
			//cek apakah beban pegawai melebihi nilai jabatan
			//jika beban melebihi nilai jabatan maka yang dipakai adalah nilai jabatan 
			$JmlPoinAktivitas = ($BebanAktivitas + $NilaiTambah) > $row['NilaiJabatan'] ? $row['NilaiJabatan'] : ($BebanAktivitas + $NilaiTambah);
			//menghitung nilai aktivitas dengan membagi jml poin aktivitas dengan nilai jabatan
			$NilaiAktivitas = $JmlPoinAktivitas == 0 ? 0 : ($JmlPoinAktivitas / $NilaiJabatan);

			$TppDisiplin = Disiplin($ProsenKehadiran, $PokokTPP);
			$TppAktivitas = Aktivitas($JmlPoinAktivitas, $NilaiJabatan, $PokokTPP);
			$TppSKP = SKP($PersenSKP, $PokokTPP);
			$TppSakip = Sakip($NilaiSAKIP, $PokokTPP);

			$BrutoTPP = TPP($TppDisiplin, $TppAktivitas, $TppSKP, $TppSakip);
			$Pph21 = $row['PPH'] == 0 ? 0 : ($BrutoTPP * ($row['PPH'] / 100));
			$TppDiterima = $BrutoTPP - $Pph21;

			$output .= '<tr>
			<td width="30px">' . $no++ . '</td>
			<td>' . $row['NamaPegawai'] . '</td>
			<td>' . $row['NamaJabatan'] . '</td>
			<td width="30px">' . $row['KelasJabatan'] . '</td>
			<td width="30px">' . $row['NilaiJabatan'] . '</td>
			<td class="text-right">' . number_format($row['PokokTPPPegawai']) . '</td>
			<td class="text-right">' . number_format($ProsenKehadiran,2) . '</td>
			<td class="text-right">' . number_format($TppDisiplin) . '</td>
			<td class="text-right">' . number_format($NilaiAktivitas,2) . '</td>
			<td class="text-right">' . number_format($TppAktivitas) . '</td>
			<td class="text-right">' . number_format($NilaiSKP,2) . '</td>
			<td class="text-right">' . number_format($TppSKP) . '</td>
			<td class="text-right">' . $NilaiSAKIP . '</td>
			<td class="text-right">' . number_format($TppSakip) . '</td>
			<td class="text-right">' . number_format($BrutoTPP) . '</td>
			<td class="text-right">' . number_format($Pph21) . '</td>
			<td class="text-right">' . number_format($TppDiterima) . '</td>
			</tr>';
		}
		$output .= '</tbody>
		</table>';
		return array('response' => 200, 'DataHtml' => $output);
	} else {
		return array('response' => 500);
	}
}

function HitungNilaiAktivitasBawahan($conn, $AtasanKodeOPD, $KodeJabatanAtasan, $Bulan, $Tahun){
	$NilaiTambahan = 0;
	$sql = "SELECT IFNULL(atv.DurasiKerjaACC, 0) AS DurasiKerjaACC
	FROM aktivitaspegawai atv
	LEFT JOIN mstpegawai p ON p.KodePegawai = atv.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE j.AtasanLangsungKodeJab = '$KodeJabatanAtasan' AND j.AtasanLangsungOPD = '$AtasanKodeOPD' AND atv.ACCAtasan = 'DISETUJUI' AND MONTH(atv.Tanggal) = '$Bulan' AND YEAR(atv.Tanggal) = '$Tahun'
	GROUP BY atv.KodeAktivitas";
	$res = $conn->query($sql);
	if ($res) {
		while ($row = $res->fetch_assoc()) {
			$NilaiTambahan += (0.01 * $row['DurasiKerjaACC']);
		}
		return $NilaiTambahan;
	}else{
		return $NilaiTambahan;
	}
}