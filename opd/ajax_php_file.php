<?php
include_once '../config/koneksi.php';
if(isset($_FILES["file"]["type"])){
	$KodePegawai = $_POST['KodePegawai'];
	$Tanggal = $_POST['Tanggal'];
	$KetAbsen = $_POST['rbKetAbsen'];
	$validextensions = array("jpeg", "jpg", "png");
	$temporary = explode(".", $_FILES["file"]["name"]);
	$file_extension = end($temporary);
	if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 5000000) && in_array($file_extension, $validextensions)){
		if ($_FILES["file"]["error"] > 0){
			$error_message = "Return Code: " . $_FILES["file"]["error"]."";
			echo json_encode(array('response' => 500, 'error_response' => 'File Error'));
		}else{
			if (file_exists("" . $_FILES["file"]["name"])) {
				$error_message = "".$_FILES["file"]["name"] . " already exists";
				echo json_encode(array('response' => 500, 'error_response' => 'File Exists'));
			}else{
				$sourcePath = $_FILES['file']['tmp_name'];
				$targetPath = "".$_FILES['file']['name'];
				move_uploaded_file($sourcePath,$targetPath);
				$lalala = mysqli_real_escape_string($conn, file_get_contents($targetPath));
				if($KetAbsen == 'DINAS'){
					$sql = "UPDATE absensipegawai SET IsHadir = b'1', IsDinas = b'1', DokumenPendukung = '$lalala', KetTidakHadir = NULL 
					WHERE Tanggal = '$Tanggal' AND KodePegawai = '$KodePegawai'";
				}else{
					$sql = "UPDATE absensipegawai SET KetTidakHadir = '$KetAbsen', DokumenPendukung = '$lalala' 
					WHERE Tanggal = '$Tanggal' AND KodePegawai = '$KodePegawai'";	
				}				
				$res = $conn->query($sql);
				if($res){
					session_start();
					InsertLog($conn, 'UPDATE', 'Verifikasi kehadiran pegawai '.$KodePegawai, $_SESSION['KodeUser']);
					echo json_encode(array('response' => 200));
				}else{
					echo json_encode(array('response' => 500, 'error_response' => 'Upload Failed'));
				}

				unlink($targetPath);
			}
		}
	}else{
		echo json_encode(array('response' => 404, 'error_response' => 404));
	}
}else{
	echo json_encode(array('response' => 500, 'error_response' => 'Not Set File'));
}
?>