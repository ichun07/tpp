<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Log Server</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">				
							<div id="tabel_pegawai" class="table-responsive">
								<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
									<thead>
										<tr>
											<th width="30px">No</th>
											<th width="130px">Tanggal</th>
											<th>Nama User</th>
											<th width="80px">Aksi</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th width="30px">No</th>
											<th>Tanggal</th>
											<th>Nama User</th>
											<th>Aksi</th>
											<th>Keterangan</th>
										</tr>
									</tfoot>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var dataTable1;
	var KodeOPD;

	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		LoadData();  
	});

	function LoadData(){		
		var action = "LoadData";
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			}],
			"ajax":{
				url :"log_aksi.php",
				type: "post",	
				data: function ( d ) {
					d.action = action ,
					d.KodeOPD = KodeOPD;
				},
				error: function(xhr, status, error){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");
					var err = xhr.responseText;
					console.log(error);
				}
			}
		} );						
	}

</script>
