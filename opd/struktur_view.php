<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Struktur Organisasi</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div id="strukturorganisasi" class="table-responsive" style="width: 100%; height: 400px;"></div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	var KodeOPD;

	$(document).ready(function () {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		LoadDataJabatan(KodeOPD);
	});

	function LoadDataJabatan(KodeOPD) {
		var action = "DataStruktur";
		$.ajax({
			url: "struktur_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD},
			dataType: 'json',
			success: function (data) {
				var myData = data;
				console.log("Result : "+myData);
				var options = new primitives.orgdiagram.Config();
				var items = [];
				for (var i = 0; i < myData.length; i++) {
					var a = new primitives.orgdiagram.ItemConfig({
						id: myData[i]['id'],
						parent: myData[i]['parent'],
						title: "",
						description: myData[i]['title']+"  ("+myData[i]['description']+")",
						image: "../komponen/img/ic_person.png"
					});
					items.push(a);
				}
				options.items = items;
				options.cursorItem = 0;
				options.hasSelectorCheckbox = primitives.common.Enabled.True;

				jQuery("#strukturorganisasi").orgDiagram(options);
			}
		});
	}
</script>