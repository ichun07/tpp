<?php
include_once 'header.php';
$KodePegawai = '';
$KodeOPD = $_SESSION['KodeOPD'];
if (isset($_GET['KodePegawai'])) {
    $KodePegawai = $_GET['KodePegawai'];
}
date_default_timezone_set('Asia/Jakarta');
$year = date('Y');
$month = date('m');
$date = date('d');
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Pengangkatan Jabatan Pegawai</h2>
        </div>
    </header>
    <section class="forms">
        <form id="form_history" method="post" action="">
            <div class="container-fluid">
                <div class="row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2 text-left">
                                <button type="button" onclick="location.href = '../opd/pegawai_view.php'"
                                class="btn btn-success">Kembali</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="h3">
                                            Identitas Pegawai
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Nama Pegawai</label>
                                                    <input type="text" placeholder="Nama Pegawai" class="form-control"
                                                    name="txtNamaPegawai" id="txtNamaPegawai" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">NIP</label>
                                                    <input type="text" placeholder="NIP" class="form-control" name="txtNip" id="txtNip" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Status</label>
                                                    <input type="text" placeholder="Status" class="form-control"
                                                    name="txtStatus" id="txtStatus" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Pangkat</label>
                                                    <input type="text" placeholder="Pangkat" class="form-control"
                                                    name="txtPangkat"
                                                    id="txtPangkat" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Golongan</label>
                                                    <input type="number" placeholder="Golongan" class="form-control"
                                                    name="txtGolongan"
                                                    id="txtGolongan" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="h3">
                                            Jabatan Sekarang
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Jabatan</label>
                                                    <input type="text" placeholder="Nama Jabatan" class="form-control" name="txtNamaJabatanLama" id="txtNamaJabatanLama" disabled>
                                                </div>
                                                <div class="form-group">       
                                                    <label class="form-control-label">Kode Manual</label>
                                                    <input type="text" placeholder="Kode Manual" class="form-control" name="txtKodeManualLama" id="txtKodeManualLama" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Tipe Jabatan</label>
                                                    <input type="text" class='form-control' id='txtTipeJabatanLama' name='txtTipeJabatanLama' disabled>
                                                </div>                              
                                                <div class="form-group">
                                                    <label class="form-control-label">Kelas Jabatan</label>
                                                    <input type="text" class='form-control' id='txtKelasJabatanLama' name='txtKelasJabatanLama' disabled>
                                                </div>
                                                <div class="form-group">       
                                                    <label class="form-control-label">Harga Jabatan</label>
                                                    <input type="number" placeholder="Harga Jabatan" class="form-control" name="txtHargaJabatanLama" id="txtHargaJabatanLama" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="h3">
                                            Jabatan Baru
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group" id="view_cb_jabatan">
                                                </div>
                                                <div class="form-group">       
                                                    <label class="form-control-label">Kode Manual</label>
                                                    <input type="text" class="form-control" name="txtKodeManualBaru" id="txtKodeManualBaru" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Tipe Jabatan</label>
                                                    <input type="text" class='form-control' id='txtTipeJabatanBaru' name='txtTipeJabatanBaru' disabled>
                                                </div>                              
                                                <div class="form-group">
                                                    <label class="form-control-label">Kelas Jabatan</label>
                                                    <input type="text" class='form-control' id='txtKelasJabatanBaru' name='txtKelasJabatanBaru' disabled>
                                                </div>
                                                <div class="form-group">       
                                                    <label class="form-control-label">Harga Jabatan</label>
                                                    <input type="number" class="form-control" name="txtHargaJabatanBaru" id="txtHargaJabatanBaru" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="h3">
                                            Formulir Pengangkatan
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">       
                                                    <label class="form-control-label">No. SK Pengangkatan</label>
                                                    <input type="text" class="form-control" placeholder="Nomor SK Pengangkatan" name="txtNoSk" id="txtNoSk" required autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Tanggal Pengangkatan</label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <select id="cb_tanggal_angkat" name="cb_tanggal_angkat" class="form-control" required>
                                                                <?php 
                                                                for($i = 1; $i < 32; $i++){
                                                                    if($i == $date){
                                                                        echo "<option value='".$i."' selected>".$i."</option>";
                                                                    }else{
                                                                        echo "<option value='".$i."'>".$i."</option>";
                                                                    }               
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select id="cb_bulan_angkat" name="cb_bulan_angkat" class="form-control" required>     
                                                                <option value="1"<?php if ((int)$month == 1) echo ' selected="selected"'; ?>>Januari</option>
                                                                <option value="2"<?php if ((int)$month == 2) echo ' selected="selected"'; ?>>Februari</option>
                                                                <option value="3"<?php if ((int)$month == 3) echo ' selected="selected"'; ?>>Maret</option>
                                                                <option value="4"<?php if ((int)$month == 4) echo ' selected="selected"'; ?>>April</option>
                                                                <option value="5"<?php if ((int)$month == 5) echo ' selected="selected"'; ?>>Mei</option>
                                                                <option value="6"<?php if ((int)$month == 6) echo ' selected="selected"'; ?>>Juni</option>
                                                                <option value="7"<?php if ((int)$month == 7) echo ' selected="selected"'; ?>>Juli</option>
                                                                <option value="8"<?php if ((int)$month == 8) echo ' selected="selected"'; ?>>Agustus</option>
                                                                <option value="9"<?php if ((int)$month == 9) echo ' selected="selected"'; ?>>September</option>
                                                                <option value="10"<?php if ((int)$month == 10) echo ' selected="selected"'; ?>>Oktober</option>
                                                                <option value="11"<?php if ((int)$month == 11) echo ' selected="selected"'; ?>>November</option>
                                                                <option value="12"<?php if ((int)$month == 12) echo ' selected="selected"'; ?>>Desember</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select id="cb_tahun_angkat" name="cb_tahun_angkat" class="form-control" required>
                                                                <option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                                <?php
                                                                for($i = $year; $i < $year+10; $i++){
                                                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="display: none;">
                                                    <label class="form-control-label">Tanggal Sampai</label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <select id="cb_tanggal_sampai" name="cb_tanggal_sampai" class="form-control" required>
                                                                <?php 
                                                                for($i = 1; $i < 32; $i++){
                                                                    if($i == $date){
                                                                        echo "<option value='".$i."' selected>".$i."</option>";
                                                                    }else{
                                                                        echo "<option value='".$i."'>".$i."</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select id="cb_bulan_sampai" name="cb_bulan_sampai" class="form-control" required>     
                                                                <option value="1"<?php if ((int)$month == 1) echo ' selected="selected"'; ?>>Januari</option>
                                                                <option value="2"<?php if ((int)$month == 2) echo ' selected="selected"'; ?>>Februari</option>
                                                                <option value="3"<?php if ((int)$month == 3) echo ' selected="selected"'; ?>>Maret</option>
                                                                <option value="4"<?php if ((int)$month == 4) echo ' selected="selected"'; ?>>April</option>
                                                                <option value="5"<?php if ((int)$month == 5) echo ' selected="selected"'; ?>>Mei</option>
                                                                <option value="6"<?php if ((int)$month == 6) echo ' selected="selected"'; ?>>Juni</option>
                                                                <option value="7"<?php if ((int)$month == 7) echo ' selected="selected"'; ?>>Juli</option>
                                                                <option value="8"<?php if ((int)$month == 8) echo ' selected="selected"'; ?>>Agustus</option>
                                                                <option value="9"<?php if ((int)$month == 9) echo ' selected="selected"'; ?>>September</option>
                                                                <option value="10"<?php if ((int)$month == 10) echo ' selected="selected"'; ?>>Oktober</option>
                                                                <option value="11"<?php if ((int)$month == 11) echo ' selected="selected"'; ?>>November</option>
                                                                <option value="12"<?php if ((int)$month == 12) echo ' selected="selected"'; ?>>Desember</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select id="cb_tahun_sampai" name="cb_tahun_sampai" class="form-control" required>
                                                                <option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                                                <?php
                                                                for($i = $year; $i < $year+10; $i++){
                                                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-control-label">Keterangan</label>
                                                    <textarea id="txtKeterangan" name="txtKeterangan" class="form-control" placeholder="Keterangan" rows="6" cols="50" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                   <input type="submit" name="submit" value="Simpan" class="btn btn-primary">
                                               </div>
                                           </div>
                                       </div>     
                                   </div>
                               </div>
                           </div>                     
                       </div>                       
                   </div>
               </div>
           </div>
       </form>
   </section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    var KodePegawai,KodeOPD,Status;
    var HargaJabatan,NilaiJabatan,PokokTPP = 0;
    var KodeJabatanLama = '';

    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
        KodePegawai = "<?php echo $KodePegawai; ?>";
        LoadData();
    });

    function LoadData(){
        var action = "LoadData";
        $.ajax({
            url: "naik_jabatan_aksi.php",
            method: "POST",
            data: {action: action, KodePegawai: KodePegawai},
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    $('#txtNamaPegawai').val(data.NamaPegawai);
                    $('#txtNip').val(data.NIP);
                    $('#txtPangkat').val(data.Pangkat);
                    $('#txtGolongan').val(data.Golongan);
                    $('#txtStatus').val(data.Status);
                    Status = data.Status;

                    $('#txtNamaJabatanLama').val(data.NamaJabatan);
                    $('#txtKodeManualLama').val(data.KodeManual);
                    $('#txtTipeJabatanLama').val(data.TipeJabatan);
                    $('#txtKelasJabatanLama').val(data.KelasJabatan);
                    $('#txtHargaJabatanLama').val(data.HargaJabatan);
                    KodeJabatanLama = data.KodeJabatan;
                    $('#view_cb_jabatan').html(data.CBJabatan);

                    $('#cbJabatan').change(function () {
                        var selectedItem = $(this).val();
                        LoadJabatanBaru(selectedItem);
                    });
                } else {
                    alert('Load data failed');
                }
            }
        });
    }

    function LoadJabatanBaru(kodeJabBaru){
        var action = "LoadDataJabatan";
        $.ajax({
            url: "naik_jabatan_aksi.php",
            method: "POST",
            data: {action: action, KodeJabatan: kodeJabBaru, KodeOPD: KodeOPD},
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    $('#txtNamaJabatanBaru').val(data.NamaJabatan);
                    $('#txtKodeManualBaru').val(data.KodeManual);
                    $('#txtTipeJabatanBaru').val(data.TipeJabatan);
                    $('#txtKelasJabatanBaru').val(data.KelasJabatan);
                    $('#txtHargaJabatanBaru').val(data.HargaJabatan);
                    NilaiJabatan = data.NilaiJabatan;
                    HargaJabatan = data.HargaJabatan;
                } else {
                    swal("Error","Terjadi kesalahan.","error")
                    .then((value) => {
                        if(value){ window.close(); }
                    });                    
                }
            }
        });
    }

    $("#form_history").submit(function(e) {
        console.log("NilaiJabatan : "+NilaiJabatan);
        console.log("HargaJabatan : "+HargaJabatan);
        e.preventDefault();
        var action = 'NaikJabatan';
        var NomorSK = $("[name='txtNoSk']").val();
        var thnSK = $("[name='cb_tahun_angkat']").val();
        var blnSK = $("[name='cb_bulan_angkat']").val();
        var tglSK = $("[name='cb_tanggal_angkat']").val();
        var TanggalSK = thnSK+"-"+blnSK+"-"+tglSK;
        var thnSampai = $("[name='cb_tahun_sampai']").val();
        var blnSampai = $("[name='cb_bulan_sampai']").val();
        var tglSampai = $("[name='cb_tanggal_sampai']").val();
        var TanggalSampai = thnSampai+"-"+blnSampai+"-"+tglSampai;
        var Keterangan = $("[name='txtKeterangan']").val();
        var KodeJabatanBaru = $("[name='cbJabatan']").val();
        if(KodeJabatanBaru == 0){
            swal('Peringatan' ,  'Pilih jabatan baru' ,  'warning');
        }else{
            var IntStatus = 1;
            if(Status != "PNS"){
                IntStatus = 0.8;
            }else{
                IntStatus = 1;
            }
            PokokTPP = NilaiJabatan * HargaJabatan * IntStatus;
            var formData = new FormData();
            formData.append("KodePegawai", KodePegawai);
            formData.append("NoSKPengangkatan", NomorSK);
            formData.append("TanggalSampai", TanggalSampai);
            formData.append("TanggalPengangkatan", TanggalSK);
            formData.append("Keterangan", Keterangan);
            formData.append("KodeJabatan", KodeJabatanBaru);
            formData.append("KodeJabatanLama", KodeJabatanLama);
            formData.append("KodeOPD", KodeOPD);
            formData.append("PokokTPP", PokokTPP);
            formData.append("action", action);

            $.ajax({
                url: "naik_jabatan_aksi.php",
                method: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData:false,
                dataType: 'json',
                success: function (data) {
                    if (data.response == 200) {
                        swal("Berhasil","Berhasil mengubah jabatan.","success")
                        .then((value) => {
                            if(value){ location.href='pegawai_view.php'; }
                        });
                    } else if(data.response == 403){
                        swal('Peringatan' ,  'Jabatan tersebut telah ditempati.' ,  'warning');
                    }else {
                        swal('Error' ,  'Gagal mengubah jabatan' ,  'error');
                    }
                }
            });
        }        
    });
</script>
<style type="text/css">
.row {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
  flex-wrap: wrap;
}
.row > [class*='col-'] {
  display: flex;
  flex-direction: column;
}
</style>