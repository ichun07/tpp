<?php
include_once 'header.php';
$KodeOPD = $_SESSION['KodeOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Absensi Pegawai Bulanan</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-2 text-left">
							Bulan/Tahun
						</div>
						<div class="col-md-3 text-left">
							<?php 
							date_default_timezone_set('Asia/Jakarta');
							$year = date('Y');
							$month = date('m');
							?>
							<select id="cb_bulan" name="cb_bulan" class="form-control">				
								<option value="1"<?php if ((int)$month == 1) echo ' selected="selected"'; ?>>Januari</option>
								<option value="2"<?php if ((int)$month == 2) echo ' selected="selected"'; ?>>Februari</option>
								<option value="3"<?php if ((int)$month == 3) echo ' selected="selected"'; ?>>Maret</option>
								<option value="4"<?php if ((int)$month == 4) echo ' selected="selected"'; ?>>April</option>
								<option value="5"<?php if ((int)$month == 5) echo ' selected="selected"'; ?>>Mei</option>
								<option value="6"<?php if ((int)$month == 6) echo ' selected="selected"'; ?>>Juni</option>
								<option value="7"<?php if ((int)$month == 7) echo ' selected="selected"'; ?>>Juli</option>
								<option value="8"<?php if ((int)$month == 8) echo ' selected="selected"'; ?>>Agustus</option>
								<option value="9"<?php if ((int)$month == 9) echo ' selected="selected"'; ?>>September</option>
								<option value="10"<?php if ((int)$month == 10) echo ' selected="selected"'; ?>>Oktober</option>
								<option value="11"<?php if ((int)$month == 11) echo ' selected="selected"'; ?>>November</option>
								<option value="12"<?php if ((int)$month == 12) echo ' selected="selected"'; ?>>Desember</option>
							</select>
						</div>
						<div class="col-md-2 text-left">
							<select id="cb_tahun" name="cb_tahun" class="form-control">
								<option selected="selected" value="<?php echo $year; ?>"><?php echo $year; ?></option>
								<?php
								for($i = $year-1; $i > $year-10; $i--){
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
								?>
							</select>
						</div>
						<div class="col-md-5 text-right">
							<button id="btnRekap" class="btn btn-primary">Rekap Absensi Bulanan</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 text-left">
							Hari Kerja Efektif (HKE)
						</div>
						<div class="col-md-6 text-left">
							<label id="txtJmlHariEfektif" name="JmlHariEfektif"></label>
						</div>
						<div class="col-md-4 text-right">
							<button id="btnCetak" class="btn btn-primary">Cetak Absensi Bulanan</button>
						</div>
					</div>
					<br>
					<div class="row">
					<div class="col-lg-12">
						<div id="tabel_absensi" class="table-responsive">
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<div id="ModalHKE" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Hari Kerja Efektif</h4>
			</div>
			<div class="modal-body">
				<div class="form-group" style="padding-bottom: 20px;">
					<label id="txtTextJmlHari" name="txtTextJmlHari">Jumlah Hari Efektif</label>
					<input type="number" name="txtJmlHari" id="txtJmlHari" class="form-control" required data-msg="Jumlah hari tidak boleh kosong!"/>
				</div>				
				<div class="modal-footer">
					<button class="btn btn-info" id="btnSimpanRekap">
						Simpan
					</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">
						Batal
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var KodeOPD,Bulan,Tahun,HKE;
	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		Tahun = $("[name='cb_tahun']").val();
		Bulan = $("[name='cb_bulan']").val();
		LoadData();  
	});

	$('#cb_tahun').change(function () {
		Tahun = $(this).val();
		console.log("Tahun : "+Tahun);
		LoadData();
	});

	$('#cb_bulan').change(function () {
		Bulan = $(this).val();
		console.log("Bulan : "+Bulan);
		LoadData();
	});

	function LoadData(){
		var action = "LoadData";
		$.ajax({
			url: "absensi_pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					console.log("KodeOPD: "+KodeOPD+" Bulan : "+Bulan+" Tahun : "+Tahun);
					$('#tabel_absensi').html(data.HtmlTabel);
					$('#tabeldata').DataTable( {
						scrollX: 200
					} );
					document.getElementById("txtJmlHariEfektif").innerHTML = data.JmlHariEfektif + ' Hari';
					HKE = data.JmlHariEfektif;
				}else{
					alert('request failed');
				}				
			}
		});
	}

	$(document).on('click', '#btnDetail', function () {
		var json = $(this).val();
		window.location = "absensi_pegawai_detail.php?"+json;
	});

	$(document).on('click', '#btnRekap', function () {
		document.getElementById("txtTextJmlHari").innerHTML = 'Jumlah HKE Bulan ke-'+Bulan+' Tahun '+Tahun;
		$('#ModalHKE').modal('show');	
	});

	$(document).on('click', '#btnSimpanRekap', function () {
		var action = "RekapAbsen";
		Tahun = $("[name='cb_tahun']").val();
		Bulan = $("[name='cb_bulan']").val();
		var JmlHariEfektif = $("[name='txtJmlHari']").val();
		$.ajax({
			url: "tpp_bulanan_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun, JmlHariEfektif:JmlHariEfektif},
			dataType: 'json',
			success: function (data) {
				if(data.response === 200){
					$("[name='txtJmlHari']").val("");
					LoadData();
					$('#ModalHKE').modal('toggle');
				}else{
					alert("Gagal Rekap Absensi");
				}				
			}
		});	
	});

	$(document).on('click', '#btnCetak', function () {
		console.log("Klik");
		var Url = "KodeOPD="+KodeOPD+"&Bulan="+Bulan+"&Tahun="+Tahun+"&HKE="+HKE;
		window.open("absensi_pegawai_report.php?"+Url);
	});
</script>