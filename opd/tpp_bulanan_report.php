<?php
include_once '../config/koneksi.php';
include_once 'rumus.php';
ob_start();
$NamaBulan = array (1 =>   'Januari',
	'Februari',
	'Maret',
	'April',
	'Mei',
	'Juni',
	'Juli',
	'Agustus',
	'September',
	'Oktober',
	'November',
	'Desember'
);
$KodeOPD = $_GET['KodeOPD'];
$Bulan = $_GET['Bulan'];
$Tahun = $_GET['Tahun'];
$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan != 'DITOLAK') AS MenitAktivitas
FROM tppbulanan tb
LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
ORDER BY tb.Bulan, tb.Tahun";
$res = $conn->query($sql);
$no = 1;
$JmlTppPersen = 0;
$JmlTppRp = 0;
$JmlPotongan = 0;
$JmlTppDiterima = 0;
?>
<page style="width:100%; padding: 10px;" backtop="7mm" backbottom="7mm" backleft="7mm" backright="7mm"> 
	<h2 style="width:100%; margin-top: 0px; margin-bottom: 0px; text-align: center;">DAFTAR REKAPITULASI PEMBERIAN TPP BERDASARKAN PRODUKTIVITAS KERJA</h2>
	<h3 style="width:100%; margin-top: 6px; margin-bottom: 6px; text-align: center;"><?php echo "BULAN ".strtoupper($NamaBulan[(int)$Bulan])." TAHUN ".$Tahun; ?></h3>
	<table class="tbl" border="1" style="width:100%; margin-top: 10px; border-collapse: collapse; font-size: 11px;">
		<tr>
			<th rowspan="2" style="width:3%; text-align: center; padding: 4px; font-size: 11px;">No</th>
			<th rowspan="2" style="width:7%; text-align: center; padding: 4px; font-size: 11px;">Nama / NIP</th>
			<th rowspan="2" style="width:7%; text-align: center; padding: 4px; font-size: 11px;">Jabatan</th>
			<th rowspan="2" style="width:5%; text-align: center; padding: 4px; font-size: 11px;">Kelas<br>Jabatan</th>
			<th rowspan="2" style="width:5%; text-align: center; padding: 4px; font-size: 11px;">Harga<br>Jabatan</th>
			<th rowspan="2" style="width:6%; text-align: center; padding: 4px; font-size: 11px;">TPP<br>Pokok</th>
			<th rowspan="2" style="width:6%; text-align: center; padding: 4px; font-size: 11px;">Tingkat<br>Kehadiran</th>
			<th rowspan="2" style="width:5%; text-align: center; padding: 4px; font-size: 11px;">Aktivitas<br>Harian</th>
			<th colspan="2" style="width:7%; text-align: center; padding: 4px; font-size: 11px;">TPP Produktivitas</th>
			<th rowspan="2" style="width:4%; text-align: center; padding: 4px; font-size: 11px;">PPH<br>21%</th>
			<th rowspan="2" style="width:5.5%; text-align: center; padding: 4px; font-size: 11px;">Potongan<br>Pajak</th>
			<th rowspan="2" style="width:6%; text-align: center; padding: 4px; font-size: 11px;">TPP<br>Diterima</th>
			<th rowspan="2" style="width:4%; text-align: center; padding: 4px; font-size: 11px;">TTD</th>
			<th rowspan="2" style="width:5%; text-align: center; padding: 4px; font-size: 11px;">No. Rek</th>
		</tr>
		<tr>
			<th style="width:4%; text-align: center; padding: 4px;">%</th>
			<th style="width:4%; text-align: center; padding: 4px;">Rp.</th>
		</tr>
		<tr>
			<th style="text-align: center; padding: 4px;">1</th>
			<th style="text-align: center; padding: 4px;">2</th>
			<th style="text-align: center; padding: 4px;">3</th>
			<th style="text-align: center; padding: 4px;">4</th>
			<th style="text-align: center; padding: 4px;">5</th>
			<th style="text-align: center; padding: 4px;">6</th>
			<th style="text-align: center; padding: 4px;">7</th>
			<th style="text-align: center; padding: 4px;">8</th>
			<th style="text-align: center; padding: 4px;">9</th>
			<th style="text-align: center; padding: 4px;">10</th>
			<th style="text-align: center; padding: 4px;">11</th>
			<th style="text-align: center; padding: 4px;">12</th>
			<th style="text-align: center; padding: 4px;">13</th>
			<th style="text-align: center; padding: 4px;">14</th>
			<th style="text-align: center; padding: 4px;">15</th>
		</tr>
		<?php
		while ($row = $res->fetch_assoc()) {
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
            //NilaiTingkatKehadiran adalah ProsenKehadiran - ProsenPelanggaran
			$NilaiTingkatKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $KodeOPD, $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);
			$MenitKerjaEfektif = 300 * $row['JmlHariEfektif'];
            //Jika Staff atau Fungsional
			if($row['TipeJabatan'] == 'FUNGSIONAL'){
				$NilaiAktivitasKerjaPegawai = NilaiAktivitasKerjaPegawaiStaff($row['MenitAktivitas'], $MenitKerjaEfektif);
			}else{
            // Jika Pimpinan
				$NilaiAktivitasKerjaPegawai = HitungNilaiAktivitasPegawaiTerpimpin($conn, $row['MenitAktivitas'], $MenitKerjaEfektif,$KodeOPD,$row['KodeJabatan'],$KodeOPD,$Bulan,$Tahun);
			}            
			$RpTPP = TTPProduktivitas($row['PokokTPPPegawai'], $NilaiTingkatKehadiran, $NilaiAktivitasKerjaPegawai);
			$PersenTPP = $row['PokokTPPPegawai'] == 0 ? 0 : ($RpTPP / $row['PokokTPPPegawai'] * 100);
			$PPNTPP = $RpTPP * 0.21;
			$TotalTPPBersih = $RpTPP - $PPNTPP; 

			$JmlTppPersen += $PersenTPP;
			$JmlTppRp += $RpTPP;
			$JmlPotongan += $PPNTPP;
			$JmlTppDiterima += $TotalTPPBersih;
			?>
			<tr>
				<td style="text-align: center; padding: 4px; font-size: 11px;"><?php echo $no++; ?></td>
				<td style="padding: 4px; font-size: 11px;"><?php echo ucwords(strtolower($row['NamaPegawai'])).'<br>'.$row['NIP']; ?></td>
				<td style="text-align: left; padding: 4px; font-size: 11px; white-space:nowrap;  overflow:hidden;  text-overflow:ellipsis;"><?php echo ucwords(strtolower($row['NamaJabatan'])); ?></td>
				<td style="text-align: center; padding: 4px; font-size: 11px;"><?php echo $row['KelasJabatan']; ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($row['HargaJabatan']); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($row['PokokTPPPegawai']); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($PersenHadir,2); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($NilaiAktivitasKerjaPegawai,2); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($PersenTPP,2); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($RpTPP); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;">21%</td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($PPNTPP); ?></td>
				<td style="text-align: right; padding: 4px; font-size: 11px;"><?php echo number_format($TotalTPPBersih); ?></td>
				<td style="text-align: center; padding: 4px;"></td>
				<td style="text-align: center; padding: 4px;"></td>
			</tr>
			<?php
		}
		?>
		<tr>
			<td colspan="8" style="text-align: center; padding: 4px;">Jumlah</td>
			<td style="text-align: center; padding: 4px;"><?php echo number_format($JmlTppPersen,2); ?></td>
			<td style="text-align: center; padding: 4px;"><?php echo number_format($JmlTppRp); ?></td>
			<td style="text-align: center; padding: 4px;"></td>
			<td style="text-align: center; padding: 4px;"><?php echo number_format($JmlPotongan); ?></td>
			<td style="text-align: center; padding: 4px;"><?php echo number_format($JmlTppDiterima); ?></td>
			<td colspan="2" style="text-align: center; padding: 4px;"></td>
		</tr>
	</table>

	<table style="width: 100%; margin-top: 30px;">
		<tr>
			<td style="width: 50%"></td>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			$tgl=date('d-m-Y');
			$TanggalTxt = date('d');
			$BulanTxt = date('m');
			$TahunTxt = date('Y');
			?>
			<td style="text-align: center; width: 50%">Jombang, <?php echo $TanggalTxt.' '.$NamaBulan[(int)$BulanTxt].' '.$TahunTxt; ?></td>
		</tr>
		<tr style="text-align: center;">
			<td style="text-align: center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Kepala Perangkat Daerah,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
			<td style="text-align: center; center; width: 50%;">
				<table style="text-align: center; width: 100%;">
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Bendahara Pengeluaran,
						</td>
					</tr>
					<tr style="height: 100px;width: 100%;"><td style="height: 50px;width: 100%;"></td></tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							Nama
						</td>
					</tr>
					<tr style="text-align: center; width: 100%;">
						<td style="text-align: center; width: 100%;">
							NIP
						</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>	
</page>
<style>
.tbl{
	width: 595pt;
	border: 1px solid #000;
}
</style>
<?php
$filename="tpp_produktivitas_".$Bulan."_".$Tahun.".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF
//==========================================================================================================

require_once('../html2pdf/html2pdf.class.php');
$content = ob_get_clean();
//$content = '<page style="font-family: freeserif">'.nl2br($content).'</page>';
try
{
	$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(5, 5, 5, 5));
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->pdf->SetTitle('TPP Produktifitas');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output($filename);
}
catch(HTML2PDF_exception $e) { echo $e; }
?>