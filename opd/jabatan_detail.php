<?php
include_once 'header.php';
$KodeJabatan = '';
$KodeOPD = '';
if(isset($_GET['KodeJabatan'])){
	$KodeJabatan = $_GET['KodeJabatan'];
	$KodeOPD = $_GET['KodeOPD'];
}
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Detail Jabatan</h2>
		</div>
	</header>	
	<section class="forms"> 
		<div id="sukses"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 text-left">
						</br>
						<button type="button" onclick="location.href = '../opd/jabatan_view.php'" class="btn btn-success">Kembali</button>
					</div>
					<div class="col-md-6 text-right">
					</br>
					<button style="visibility: hidden;" type="button" id="btEdit" class="btn btn-success">Edit</button>
				</div>
				<br><br><br>		
				<div class="col-lg-12">
					<div class="card">						
						<div class="card-body">
							<form method="post">
								<div class="form-group">
									<label class="form-control-label">Nama Jabatan</label>
									<input type="text" placeholder="Nama Jabatan" class="form-control" name="txtNamaJabatan" id="txtNamaJabatan">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Kode Manual</label>
									<input type="text" placeholder="Kode Manual" class="form-control" name="txtKodeManual" id="txtKodeManual">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Tipe Jabatan</label>
									<select class='form-control' id='txtTipeJabatan' name='txtTipeJabatan'>
										<option value='STRUKTURAL'> Struktural </option>
										<option value='FUNGSIONAL'> Fungsional </option>
									</select>
								</div>								
								<div class="form-group" id="select_kelas_jabatan">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Harga Jabatan</label>
									<input type="number" placeholder="Harga Jabatan" class="form-control" name="txtHargaJabatan" id="txtHargaJabatan">
								</div>
								<div class="form-group" id="select_atasan_opd">
								</div>
								<div class="form-group" id="select_atasan_jab">
								</div>								
								<div class="form-group">       
									<label class="form-control-label">Keterangan</label>
									<input type="text" placeholder="Keterangan" class="form-control" name="txtKeterangan" id="txtKeterangan">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Deskripsi</label>
									<textarea placeholder="Deskripsi" name="txtDeskripsi" id="txtDeskripsi" class="form-control" rows="7" cols="80"></textarea>
								</div>

								<div class="form-group">       
									<button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
								</div>
							</form>							
						</div>					
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

	$(document).ready(function() {		
		KodeJabatan = "<?php echo $KodeJabatan; ?>";
		KodeOPD = "<?php echo $KodeOPD; ?>";
		console.log("KodeJabatan " + KodeJabatan);
		LoadDataJabatan();
	});

	var KodeJabatan, KodeOPD;

	var AtasanLangsungOPD, AtasanLangsungKodeJab, NamaOPDAtasan, NamaJabatanAtasan;
	function LoadDataJabatan(){
		var action = "AmbilData";
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {action: action, kodeJabatan: KodeJabatan, kodeOPD: KodeOPD},
			dataType: 'json',
			success: function (data) {
				if(data.Status == 200){
					console.log("NamaJabatan " + data.NamaJabatan);
					$('#txtNamaJabatan').val(data.NamaJabatan);
					$('#txtKodeManual').val(data.KodeManual);
					SelectElement("txtTipeJabatan", data.TipeJabatan);
					$('#txtHargaJabatan').val(data.HargaJabatan);
					$('#txtKeterangan').val(data.Keterangan);
					$('#txtDeskripsi').val(data.Deskripsi);
					AtasanLangsungOPD = data.AtasanLangsungOPD;
					AtasanLangsungKodeJab = data.AtasanLangsungKodeJab;
					NamaOPDAtasan = data.NamaOPD;
					NamaJabatanAtasan = data.NamaAtasan;
					LoadDataAtasan(AtasanLangsungOPD,AtasanLangsungKodeJab,NamaJabatanAtasan)	
					LoadDataOPD();
					LoadDataKelas(data.KelasJabatan);
				}else{
					alert('Load data opd failed');
				}
			}
		});
	}

	function SelectElement(id, valueToSelect)
	{    
		var element = document.getElementById(id);
		element.value = valueToSelect;
	}

	function LoadDataOPD(){
		var action = "GetAtasanOPD";
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD:AtasanLangsungOPD, NamaOPD: NamaOPDAtasan},
			dataType: 'json',
			success: function (data) {
				$('#select_atasan_opd').html(data);
				$('#cb_atasan_opd').change(function () {
					var selectedItem = $(this).val();
					console.log("KodeOPD : " + selectedItem);
					LoadDataAtasan(selectedItem,"","Pilih Atasan Jabatan");
				});
			}
		});
	}

	function LoadDataAtasan(KodeOPDSelected,kodeJabatan,NamaJabatan){
		var action = "GetAtasanJabatan";		
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPDSelected, KodeJabatan: kodeJabatan, NamaJabatan: NamaJabatan},
			dataType: 'json',
			success: function (data) {
				$('#select_atasan_jab').html(data);
			}
		});
	}

	function LoadDataKelas(KodeKelas){
		var action = "GetKelasEdit";		
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {action: action, Kode: KodeKelas},
			dataType: 'json',
			success: function (data) {
				$('#select_kelas_jabatan').html(data);
			}
		});
	}
	
	
	function simpanData() {
		var KodeManual = $("[name='txtKodeManual']").val(); 
		var NamaJabatan = $("[name='txtNamaJabatan']").val(); 
		var Keterangan = $("[name='txtKeterangan']").val(); 
		var Deskripsi = $("[name='txtDeskripsi']").val(); 
		var TipeJabatan = $("[name='txtTipeJabatan']").val(); 
		var KelasJabatan = $("[name='txtKelasJabatan']").val(); 
		var HargaJabatan = $("[name='txtHargaJabatan']").val(); 
		var AtasanJab = $("[name='cbAtasanLangsungKodeJab']").val(); 
		var AtasanOPD = $("[name='cb_atasan_opd']").val();
		var action = "UpdateData";
		console.log("UPDATE mstjabatan SET KodeManual = '"+KodeManual+"', NamaJabatan = '"+NamaJabatan+"', Keterangan = '"+Keterangan+"', Deskripsi = '"+Deskripsi+"', TipeJabatan = '"+TipeJabatan+"', KelasJabatan = '"+KelasJabatan+"', HargaJabatan = '"+HargaJabatan+"', AtasanLangsungKodeJab = '"+AtasanJab+"', KodeOPD = '"+KodeOPD+"', AtasanLangsungOPD = '"+AtasanOPD+"' WHERE KodeJabatan = '"+KodeJabatan+"'");
		$.ajax({
			url: "jabatan_aksi.php",
			method: "POST",
			data: {kodeJabatan: KodeJabatan, kodeManual: KodeManual, namaJabatan: NamaJabatan, keterangan: Keterangan, deskripsi: Deskripsi, tipeJabatan: TipeJabatan, kelasJabatan: KelasJabatan, hargaJabatan: HargaJabatan, atasanLangsungKodeJab: AtasanJab, atasanLangsungOPD: AtasanOPD, kodeOPD: KodeOPD, action: action},
			dataType: 'json',
			success: function (data) {				
				if(data.Status == 200){						
					$("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil MEngubah Data!</strong> <a href='../opd/jabatan_view.php'>lihat semua data</a>.</div>");
				}else{
					alert('Insert Data failed');
				}
				
			}
		});
	}	
</script>