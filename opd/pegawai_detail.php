<?php
include_once 'header.php';
$KodePegawai = '';
$KodeOPD = '';
if (isset($_GET['KodePegawai'])) {
    $KodePegawai = $_GET['KodePegawai'];
    $KodeOPD = $_GET['KodeOPD'];
}
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Detail Pegawai</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 text-left">
                        </br>
                        <button type="button" onclick="location.href = '../opd/pegawai_view.php'"
                        class="btn btn-success">Kembali
                    </button>
                </div>
                <div class="col-md-6 text-right">
                </br>
                <button style="visibility: hidden;" type="button" id="btEdit" class="btn btn-success">Edit</button>
            </div>
            <br><br><br>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post">                                        
                            <div class="form-group">
                                <label class="form-control-label">Nama Pegawai</label>
                                <input type="text" placeholder="Nama Pegawai" class="form-control"
                                name="txtNamaPegawai" id="txtNamaPegawai">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Status</label>
                                <select id="cbStatus" name="cbStatus" class="form-control">
                                    <option value="PNS">PNS</option>
                                    <option value="CPNS">CPNS</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">NIP</label>
                                <input type="text" placeholder="NIP" class="form-control" name="txtNip" id="txtNip">
                            </div>
                            <div class="form-group" id="select_kode_jabatan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Pangkat</label>
                                <input type="text" placeholder="Pangkat" class="form-control"
                                name="txtPangkat"
                                id="txtPangkat">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Golongan</label>
                                <input type="number" placeholder="Golongan" class="form-control"
                                name="txtGolongan"
                                id="txtGolongan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Username</label>
                                <input type="text" placeholder="Username" class="form-control"
                                name="txtUsername"
                                id="txtUsername">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Password</label>
                                <input type="password" placeholder="Password" class="form-control"
                                name="txtPassword"
                                id="txtPassword">
                            </div>
                            <div class="form-group">
                                <button type="button" onclick="simpanData()" class="btn btn-primary">
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
    var KodePegawai = '';
    var KodeOPD = '';
    var Status = '';
    var HargaJabatan,NilaiJabatan,PokokTPP = 0;
    var IntStatus = 1;
    var UbahJabatan = 'false';
    $(document).ready(function () {
        KodeOPD = "<?php echo $KodeOPD; ?>";
        KodePegawai = "<?php echo $KodePegawai; ?>";
        LoadDataPegawai();
    });

    $('#cbStatus').change(function () {
        var selectedItem = $(this).val();        
        Status = selectedItem;
    });

    function LoadDataPegawai() {
        var action = "AmbilData";
        $.ajax({
            url: "pegawai_aksi.php",
            method: "POST",
            data: {action: action, KodePegawai: KodePegawai, KodeOPD: KodeOPD},
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    var row = data.Data
                    console.log("NamaPegawai " + row.NamaPegawai);
                    $('#txtNamaPegawai').val(row.NamaPegawai);
                    $('#txtNip').val(row.NIP);
                    $('#txtPangkat').val(row.Pangkat);
                    $('#txtGolongan').val(row.Golongan);
                    $('#txtUsername').val(row.Username);
                    $('#txtPassword').val(row.Password);
                    $('#select_kode_jabatan').html(row.HtmlJabatan);

                    SelectElement('cbStatus', row.Status);
                    console.log("Status : "+row.Status);
                    Status = row.Status;
                    NilaiJabatan = row.NilaiJabatan;
                    HargaJabatan = row.HargaJabatan;
                    $('#cbJabatan').change(function () {
                        NilaiJabatan = $(this).find('option:selected').attr('data-value');
                        // $(this).attr("data-value");
                        HargaJabatan = $(this).find('option:selected').attr('data-value2');
                        // $(this).attr("data-value2");
                        
                        UbahJabatan = 'true';
                    });
                } else {
                    alert('Load data failed');
                }
            }
        });
    }

    function simpanData() {
        if(Status != "PNS"){
            IntStatus = 0.8;
        }else{
            IntStatus = 1;
        }
        PokokTPP = NilaiJabatan * HargaJabatan * IntStatus;
        console.log("NilaiJabatan : " + NilaiJabatan);
        console.log("HargaJabatan : " + HargaJabatan);
        console.log("PokokTPP : " + PokokTPP);
        var NIP = $("[name='txtNip']").val();
        var NamaPegawai = $("[name='txtNamaPegawai']").val();
        var Pangkat = $("[name='txtPangkat']").val();
        var Golongan = $("[name='txtGolongan']").val();
        var KodeJabatan = $("[name='cbJabatan']").val();
        var Username = $("[name='txtUsername']").val();
        var Password = $("[name='txtPassword']").val();
        var action = "UpdateData";
        if (KodeJabatan == 0) {
            swal('Peringatan' ,  'Anda harus memilih jabatan!' ,  'warning');
        } else {
            $.ajax({
                url: "pegawai_aksi.php",
                method: "POST",
                data: {
                    KodePegawai: KodePegawai,
                    NIP: NIP,
                    NamaPegawai: NamaPegawai,
                    Pangkat: Pangkat,
                    Golongan: Golongan,
                    KodeJabatan: KodeJabatan,
                    KodeOPD: KodeOPD,
                    Username: Username,
                    Password: Password,
                    PokokTPP: PokokTPP,
                    UbahJabatan : UbahJabatan,
                    Status: Status,
                    action: action
                },
                dataType: 'json',
                success: function (data) {
                    if (data.response == 200) {
                        $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Ubah Data!</strong> <a href='../opd/pegawai_view.php'>lihat semua data</a>.</div>");
                        swal('Sukses' ,  'Berhasil mengubah data pegawai' ,  'success');
                    } else {
                     swal('Peringatan' ,  'Gagal mengubah data pegawai' ,  'error');
                 }
             }
         });
        }
    }

    function SelectElement(id, valueToSelect)
    {    
        var element = document.getElementById(id);
        element.value = valueToSelect;
    }
</script>