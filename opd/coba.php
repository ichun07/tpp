<link href="../komponen/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.12.4.min.js" 
integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" 
crossorigin="anonymous">
</script>
<script src="../komponen/jquery-ui/jquery-ui.min.js"></script>
<link href="../komponen/tree/CSS/jHTree.css" rel="stylesheet">
<script src="../komponen/tree/js/jQuery.jHTree.js"></script>


<div id="tree">
</div>

<script type="text/javascript">

	var myData = [{
		"head": "A",
		"id": "aa",
		"contents": "A Contents",
		"children": [
		{
			"head": "A-1",
			"id": "a1",
			"contents": "A-1 Contents",
			"children": [
			{ "head": "A-1-1", "id": "a11", "contents": "A-1-1 Contents" }
			]
		},
		{
			"head": "A-2",
			"id": "a2",
			"contents": "A-2 Contents",
			"children": [
			{ "head": "A-2-1", "id": "a21", "contents": "A-2-1 Contents" },
			{ "head": "A-2-2", "id": "a22", "contents": "A-2-2 Contents" }
			]
		}
		]
	}];

	$("#tree").jHTree({
		callType: 'obj',
		structureObj: myData,
		nodeDropComplete: function (event, data) {
    //----- Do Something @ Server side or client side -----------
    //alert("Node ID: " + data.nodeId + " Parent Node ID: " + data.parentNodeId);
    //-----------------------------------------------------------
}
});	


	//via ajax


// 	$("#tree").jHTree({
// 		callType: 'url',
// 		url: 'myData.json',
// 		nodeDropComplete: function (event, data) {
//     //----- Do Something @ Server side or client side -----------
//     //alert("Node ID: " + data.nodeId + " Parent Node ID: " + data.parentNodeId);
//     //-----------------------------------------------------------
// }
// });


// data.json
// [{
//   "head": "A",
//   "id": "aa",
//   "contents": "A Contents",
//   "children": [
//     {
//       "head": "A-1",
//       "id": "a1",
//       "contents": "A-1 Contents",
//       "children": [
//         { "head": "A-1-1", "id": "a11", "contents": "A-1-1 Contents" }
//       ]
//     },
//     {
//       "head": "A-2",
//       "id": "a2",
//       "contents": "A-2 Contents",
//       "children": [
//         { "head": "A-2-1", "id": "a21", "contents": "A-2-1 Contents" },
//         { "head": "A-2-2", "id": "a22", "contents": "A-2-2 Contents" }
//       ]
//     }
//   ]
// }]
</script>
