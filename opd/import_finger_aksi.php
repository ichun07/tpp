<?php
require "../komponen/excel_reader.php";
include_once '../config/koneksi.php';
include_once 'rumus.php';
if(isset($_POST)){
	$target = basename($_FILES['filepegawaiall']['name']) ;
	move_uploaded_file($_FILES['filepegawaiall']['tmp_name'], $target);
	$data = new Spreadsheet_Excel_Reader($_FILES['filepegawaiall']['name'],false);
	$baris = $data->rowcount($sheet_index = 0);
	for ($i=2; $i<=$baris; $i++){
		$TL1 = 0;$TL2 = 0;$TL3 = 0;$PA1 = 0;$PA2 = 0;$PA3 = 0;$LF1 = 0;$LF2 = 0;

		$barisreal = $baris-1;
		$k = $i-1;
		$percent = intval($k/$barisreal * 100)."%";

		$kode = $data->val($i, 1);
		$NoAkun = $data->val($i, 2);
		$No = $data->val($i, 3);
		$Nama = $data->val($i, 4);
		$AutoAssign = $data->val($i, 5);
		$Tanggal = $data->val($i, 6);
		$JamKerja = $data->val($i, 7);
		$AwalTugas = $data->val($i, 8);
		$AkhirTugas = $data->val($i, 9);
		$Masuk = $data->val($i, 10);
		$Keluar = $data->val($i, 11);
		$Normal = $data->val($i, 12);
		$WaktuReal = $data->val($i, 13);
		$Telat = $data->val($i, 14);
		$PlgAwal = $data->val($i, 15);
		$Bolos = $data->val($i, 16);
		$WaktuLembur = $data->val($i, 17);
		$WaktuKerja = $data->val($i, 18);
		$Status = $data->val($i, 19);
		$HrsCekIn = $data->val($i, 20);
		$HrsCekOut = $data->val($i, 21);
		$Departemen = $data->val($i, 22);
		$NDays = $data->val($i, 23);
		$AkhirPekan = $data->val($i, 24);
		$HariLibur = $data->val($i, 25);
		$LamaHadir = $data->val($i, 26);
		$NDays_OT = $data->val($i, 27);
		$LemburAkhirPekan = $data->val($i, 28);
		$LiburLembur = $data->val($i, 29);
		if($JamKerja == 'jumat'){
			$IsJumat = 1;
		}else{
			$IsJumat = 0;
		}

		if($Bolos == "True"){
			$IsHadir = 0;
		}else{
			$IsHadir = 1;
			if($Telat != null && $Telat != ""){
				$JamDatang = GetJamDatang($Keluar, $LamaHadir);
				$start_date = new DateTime($AwalTugas);
				$since_start = $start_date->diff(new DateTime($JamDatang));
				$TimeDiff = (($since_start->h * 60) + $since_start->i);
				$Telat = KategoriTelat($TimeDiff);
				$TL1 = $Telat['TL1']; $TL2 = $Telat['TL2']; $TL3 = $Telat['TL3'];
			}

			if($PlgAwal != null && $PlgAwal != ""){
				$JamPulang = GetJamPulang($Masuk, $LamaHadir);
				$start_date = new DateTime($AkhirTugas);
				$since_start = $start_date->diff(new DateTime($JamPulang));
				$TimeDiff = (($since_start->h * 60) + $since_start->i);
				$Pulang = KategoriPulang($TimeDiff);
				$PA1 = $Pulang['PA1']; $PA2 = $Pulang['PA2']; $PA3 = $Pulang['PA3'];
			}
		}

		$query = "";
		$var = $Tanggal;
		$date = str_replace('/', '-', $var);
		$TanggalUpload = date('Y-m-d', strtotime($date));

		if($IsHadir < 1){
			$query = "INSERT INTO absensipegawai (Tanggal, IsHadir, KodePegawai, IsJumat, TL1, TL2, TL3, PA1, PA2, PA3, LF1, LF2, KetTidakHadir) VALUES ('$TanggalUpload', b'".$IsHadir."', (SELECT p.KodePegawai FROM mstpegawai p WHERE p.NoAkun = '$NoAkun'), b'".$IsJumat."', '$TL1', '$TL2', '$TL3', '$PA1', '$PA2', '$PA3', '$LF1', '$LF2','ALPHA')";
		}else{
			if($IsJumat > 0){
				$query = "INSERT INTO absensipegawai (Tanggal, IsHadir, KodePegawai, IsJumat, TL1, TL2, TL3, PA1, PA2, PA3, LF1, LF2, IsSenam) VALUES ('$TanggalUpload', b'".$IsHadir."', (SELECT p.KodePegawai FROM mstpegawai p WHERE p.NoAkun = '$NoAkun'), b'".$IsJumat."', '$TL1', '$TL2', '$TL3', '$PA1', '$PA2', '$PA3', '$LF1', '$LF2', b'1')";
			}else{
				$query = "INSERT INTO absensipegawai (Tanggal, IsHadir, KodePegawai, IsJumat, TL1, TL2, TL3, PA1, PA2, PA3, LF1, LF2, IsApel) VALUES ('$TanggalUpload', b'".$IsHadir."', (SELECT p.KodePegawai FROM mstpegawai p WHERE p.NoAkun = '$NoAkun'), b'".$IsJumat."', '$TL1', '$TL2', '$TL3', '$PA1', '$PA2', '$PA3', '$LF1', '$LF2',b'1')";
			}            
		}

		$hasil = $conn->query($query);
	}

	
	unlink($_FILES['filepegawaiall']['name']);
	if(!$hasil){
		die(mysqli_error($conn));
	}else{
		session_start();
		InsertLog($conn, 'INSERT', 'Import data absensi ', $_SESSION['KodeUser']);
		echo json_encode(array('response' => 200));
	}
}

function GetJamDatang($JamPulang, $DurasiKerja){
	$time1 = $JamPulang;
	$time2 = $DurasiKerja;

	$time1_unix = strtotime(date('Y-m-d').' '.$time1.':00');
	$time2_unix = strtotime(date('Y-m-d').' '.$time2.':00');

	$begin_day_unix = strtotime(date('Y-m-d').' 00:00:00');

	$jumlah_time = date('H:i', ($time1_unix - ($time2_unix - $begin_day_unix)));

	return $jumlah_time;
}

function GetJamPulang($JamDatang, $DurasiKerja){
	$time1 = $JamDatang;
	$time2 = $DurasiKerja;

	$time1_unix = strtotime(date('Y-m-d').' '.$time1.':00');
	$time2_unix = strtotime(date('Y-m-d').' '.$time2.':00');

	$begin_day_unix = strtotime(date('Y-m-d').' 00:00:00');

	$jumlah_time = date('H:i', ($time1_unix + ($time2_unix - $begin_day_unix)));

	return $jumlah_time;
}

?>