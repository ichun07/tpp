<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {

	if($_POST['action'] == 'LoadData'){
		$requestData = $_REQUEST;
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetDataLog($conn, $requestData, $KodeOPD);
		echo json_encode($result);
	}
}

function GetDataLog($conn, $requestData, $KodeOPD){
	$columns = array( 
		0 => 'Tanggal',
		1 => 'Tanggal', 
		2 => 'Nama',
		3 => 'Aksi',
		4 => 'Keterangan',
	);

	$sql = "SELECT l.KodeLogServer, l.Aksi, l.Tanggal, l.Keterangan, l.KodeUser, u.Nama ";
	$sql.="FROM logserver l
	INNER JOIN mstuser u ON u.KodeUser = l.KodeUser
	WHERE u.IsKabupaten = '0' AND u.IsDihapus != '1' AND u.KodeOPD = '$KodeOPD'";
	$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
	$totalFiltered = $totalData;


	if( !empty($requestData['search']['value']) ) {
		$sql = "SELECT l.KodeLogServer, l.Aksi, l.Tanggal, l.Keterangan, l.KodeUser, u.Nama ";
		$sql.="FROM logserver l
		INNER JOIN mstuser u ON u.KodeUser = l.KodeUser ";
		$sql.="WHERE u.IsKabupaten = '0' AND u.KodeOPD = '$KodeOPD' AND u.IsDihapus != '1' AND l.Tanggal LIKE '".$requestData['search']['value']."%' ";
		$sql.=" OR u.IsKabupaten = '0' AND u.KodeOPD = '$KodeOPD' AND u.IsDihapus != '1' AND l.Aksi LIKE '".$requestData['search']['value']."%' ";
		$sql.=" OR u.IsKabupaten = '0' AND u.KodeOPD = '$KodeOPD' AND u.IsDihapus != '1' AND l.Keterangan LIKE '".$requestData['search']['value']."%' ";
		$sql.=" OR u.IsKabupaten = '0' AND u.KodeOPD = '$KodeOPD' AND u.IsDihapus != '1' AND u.Nama LIKE '".$requestData['search']['value']."%' ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
		$totalFiltered = mysqli_num_rows($query);
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");	
	} else {
		$sql = "SELECT l.KodeLogServer, l.Aksi, l.Tanggal, l.Keterangan, l.KodeUser, u.Nama ";
		$sql.="FROM logserver l
		INNER JOIN mstuser u ON u.KodeUser = l.KodeUser WHERE u.IsKabupaten = '0' AND u.KodeOPD = '$KodeOPD' AND u.IsDihapus != '1' ";
		$sql.="ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	}

	$data = array();
	$no = 1;
	while( $row=mysqli_fetch_array($query) ) {
		$nestedData=array();
		$nestedData[] = $no++;
		$nestedData[] = 'Jam '.date('h:i', strtotime($row['Tanggal'])).'<br>Tgl. '.date('d-m-Y', strtotime($row['Tanggal']));
		$nestedData[] = $row['Nama'];
		$nestedData[] = $row['Aksi'];
		$nestedData[] = $row['Keterangan'];

		$data[] = $nestedData;

	}



	$json_data = array(
		"draw"            => intval( $requestData['draw'] ),
		"recordsTotal"    => intval( $totalData ),
		"recordsFiltered" => intval( $totalFiltered ),
		"data"            => $data   		);

	return $json_data;  
}


