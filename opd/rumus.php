<?php

function NilaiAktivitasKerjaPegawaiStaff($JmlMenitKerja,$MenitKerjaEfektif){
	$angka = $JmlMenitKerja == 0 ? 0 : ($JmlMenitKerja / $MenitKerjaEfektif);
	return $angka;
}

function NilaiAktivitasKerjaPegawaiStruktur($JmlMenitKerja,$MenitKerjaEfektif,$JmlNilaiAktivitasPegawaiTerpimpin,$JmlPegawaiTerpimpin){
	$value1 = $JmlMenitKerja == 0 ? 0 : ($JmlMenitKerja / $MenitKerjaEfektif) * 60/100;
	$value2 = $JmlNilaiAktivitasPegawaiTerpimpin == 0 ? 0 : ($JmlNilaiAktivitasPegawaiTerpimpin / $JmlPegawaiTerpimpin) * 40/100;
	$angka = $value1 + $value2;
	return $angka;
}

function TTPProduktivitas($PokokTPP,$TingkatKehadiran,$AktivitasKerjaPegawai){
	$value1 = $PokokTPP == 0 ? 0 : ($PokokTPP * ($TingkatKehadiran / 100) * 0.5);
	$value2 = $PokokTPP == 0 ? 0 : ($PokokTPP * ($AktivitasKerjaPegawai / 100) * 0.5);
	$angka = $value1 + $value2;
	return $angka;
}

function TPPKinerja($PokokTPP, $NRA, $NOK, $SAKIP){
	return ($PokokTPP * $NRA * 0.3) + ($PokokTPP * $NOK * 0.3) + ($PokokTPP * $SAKIP * 0.4);
}

function HitungNilaiAktivitasPegawaiTerpimpin($conn, $JmlMenitKerja, $MenitKerjaEfektif,$AtasanKodeOPD,$KodeJabatanAtasan,$KodeOPD,$Bulan,$Tahun){
	$JmlNilaiAktivitasPegawaiTerpimpin = 0;
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan != 'DITOLAK') AS MenitAktivitas
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	WHERE p.KodeOPD = '$KodeOPD' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun' AND j.AtasanLangsungOPD = '$AtasanKodeOPD' AND j.AtasanLangsungKodeJab = '$KodeJabatanAtasan'
	ORDER BY tb.Bulan, tb.Tahun";
	$res = $conn->query($sql);
	if ($res) {
		$JmlPegawaiTerpimpin = 0;
		while ($row = $res->fetch_assoc()) {
			$MenitKerjaEfektif = 300 * $row['JmlHariEfektif'];
			$JmlNilaiAktivitasPegawaiTerpimpin += NilaiAktivitasKerjaPegawaiStaff($row['MenitAktivitas'], $MenitKerjaEfektif);
			$JmlPegawaiTerpimpin+=1;
		}
		$NilaiAktivitasKerjaPegawaiPimpinan = NilaiAktivitasKerjaPegawaiStruktur($JmlMenitKerja,$MenitKerjaEfektif,$JmlNilaiAktivitasPegawaiTerpimpin,$JmlPegawaiTerpimpin);
		return $NilaiAktivitasKerjaPegawaiPimpinan;
	}else{
		return 0;
	}
}

function GetNilaiPelanggaran($conn, $KodeOPD, $JmlSenam, $JmlApel, $JmlTL1, $JmlTL2, $JmlTL3, $JmlPA1, $JmlPA2, $JmlPA3, $JmlLF1, $JmlLF2)
{
	$Jml = 0;
	$sql = "SELECT SettingName, SettingValue, TipeSetting FROM sistemsetting WHERE TipeSetting = 'PELANGGARAN' ORDER BY SettingName ASC";
	$res = $conn->query($sql);
	if ($res) {
		while ($row = $res->fetch_assoc()) {
			if ($row['SettingName'] == 'AS') {
				$Jml += $row['SettingValue'] * $JmlSenam;
				$Jml += $row['SettingValue'] * $JmlApel;
			}
			if ($row['SettingName'] == 'TL1') {
				$Jml += $row['SettingValue'] * $JmlTL1;
			}
			if ($row['SettingName'] == 'TL2') {
				$Jml += $row['SettingValue'] * $JmlTL2;
			}
			if ($row['SettingName'] == 'TL3') {
				$Jml += $row['SettingValue'] * $JmlTL3;
			}
			if ($row['SettingName'] == 'PA1') {
				$Jml += $row['SettingValue'] * $JmlPA1;
			}
			if ($row['SettingName'] == 'PA2') {
				$Jml += $row['SettingValue'] * $JmlPA2;
			}
			if ($row['SettingName'] == 'PA3') {
				$Jml += $row['SettingValue'] * $JmlPA3;
			}
			if ($row['SettingName'] == 'LF1') {
				$Jml += $row['SettingValue'] * $JmlLF1;
			}
			if ($row['SettingName'] == 'LF2') {
				$Jml += $row['SettingValue'] * $JmlLF2;
			}
		}
		return $Jml;
	} else {
		return 0;
	}
}

function NilaiRealisasiAnggaran($value){
	if($value > 95){
		return 1.00;
	}else if (96 > $value and $value > 90) {
		return 0.95;
	}else if (91 > $value and $value > 80) {
		return 0.90;
	}else if (81 > $value and $value > 70) {
		return 0.80;
	}else if (71 > $value and $value > 60) {
		return 0.70;
	}else if (61 > $value and $value > 50) {
		return 0.60;
	}else if (51 > $value and $value > 45) {
		return 0.50;
	}else {
		return 0;
	}
}

function NilaiOutputKegiatan($value){
	if($value > 85){
		return 1.00;
	}else if (86 > $value and $value > 75) {
		return 0.85;
	}else if (76 > $value and $value > 65) {
		return 0.75;
	}else if (66 > $value and $value > 50) {
		return 0.65;
	}else if (51 > $value and $value > 40) {
		return 0.50;
	}else {
		return 0;
	}
}

function NilaiSAKIP($value){
	if($value == 'AA'){
		return 1.10;
	}else if ($value == 'A') {
		return 1.00;
	}else if ($value == 'BB') {
		return 0.90;
	}else if ($value == 'B') {
		return 0.85;
	}else if ($value == 'CC') {
		return 0.60;
	}else if ($value == 'C') {
		return 0.55;
	}else {
		return 0;
	}
}

function NilaiSKP($value){
	if($value > 90){
		return 0;
	}else if(90 >= $value and $value >= 76){
		return 25;
	}else if(75 >= $value and $value >= 61){
		return 50;
	}else if(60 >= $value and $value >= 50){
		return 75;
	}else{
		return 100;
	}

}

function KategoriTelat($TimeDiff){
	if($TimeDiff >= 1 && $TimeDiff <= 10){
		return array('TL1' => 1, 'TL2' => 0, 'TL3' => 0);
	}else if($TimeDiff >= 11 && $TimeDiff <= 60){
		return array('TL1' => 0, 'TL2' => 1, 'TL3' => 0);
	}else if($TimeDiff > 60){
		return array('TL1' => 0, 'TL2' => 0, 'TL3' => 1);
	}else{
		return array('TL1' => 0, 'TL2' => 0, 'TL3' => 0);
	}
}

function KategoriPulang($TimeDiff){
	if($TimeDiff >= 1 && $TimeDiff <= 10){
		return array('PA1' => 1, 'PA2' => 0, 'PA3' => 0);
	}else if($TimeDiff >= 11 && $TimeDiff <= 60){
		return array('PA1' => 0, 'PA2' => 1, 'PA3' => 0);
	}else if($TimeDiff > 60){
		return array('PA1' => 0, 'PA2' => 0, 'PA3' => 1);
	}else{
		return array('PA1' => 0, 'PA2' => 0, 'PA3' => 0);
	}
}


// revisi tpp terbaru

function HitungPokokTPP($conn, $KodeJabatan, $KodeOPD){
	$sql = "SELECT * FROM mstjabatan WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_array($res);	
	}	
}

function TPP($Disiplin, $Aktivitas, $SKP, $SAKIP){
	$TPP = $Disiplin + $Aktivitas + $SKP + $SAKIP;
	return $TPP;
}

function Disiplin($ProsenKehadiran, $PokokTPP){
	$Disiplin = 0.3 * $ProsenKehadiran * $PokokTPP;
	return $Disiplin;
}

function Aktivitas($JmlPoinAktivitas, $NilaiJabatan, $PokokTPP){
	$Aktivitas = $JmlPoinAktivitas == 0 ? 0 : (0.3 * ($JmlPoinAktivitas / $NilaiJabatan) * $PokokTPP);
	return $Aktivitas;
}

function SKP($PersenSKP, $PokokTPP){
	$PenguranganSKP = ($PersenSKP / 100) * $PokokTPP;
	$SKP = 0.2 * ($PokokTPP - $PenguranganSKP);
	return $SKP;
}

function Sakip($NilaiSAKIP, $PokokTPP){
	$Sakip = 0.2 * $NilaiSAKIP * $PokokTPP;
	return $Sakip;
}