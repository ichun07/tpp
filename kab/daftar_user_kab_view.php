<?php
include_once 'header.php';
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Data User Kabupaten</h2>
        </div>
    </header>
    <section class="forms">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 text-right">                        
                            <button onclick="location.href = '../kab/daftar_user_tambah.php?IsKabupaten=1'" class="btn btn-primary">Tambah
                                Data
                            </button>
                        </div>
                    </div>
                </br>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="tabel_user_opd" class="table-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Modal Popup untuk delete-->
<div class="modal fade" id="ModalHapus">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-title" style="text-align:center;">Anda yakin untuk menghapus data ini ?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" class="btn btn-info" id="btHapus">Hapus</a>
                <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Popup untuk Edit-->
<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Edit Data Instansi</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" style="padding-bottom: 20px;">
                    <label for="etNamaUser">Nama User</label>
                    <input type="text" name="etNamaUser" id="etNamaUser" class="form-control" required
                    data-msg="Nama User tidak boleh kosong!"/>
                </div>
                <div class="form-group" style="padding-bottom: 20px;" id="select_opd">

                </div>
                <div class="form-group" style="padding-bottom: 20px;">
                    <label for="etUsername">Username</label>
                    <input type="text" name="etUsername" id="etUsername" class="form-control" required
                    data-msg="Username tidak boleh kosong!"/>
                </div>
                <div class="form-group" style="padding-bottom: 20px;">
                    <label for="etPassword">Password</label>
                    <input type="password" name="etPassword" id="etPassword" class="form-control" required
                    data-msg="Password tidak boleh kosong!"/>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" id="btSimpan">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
                </div>

            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
    $(document).ready(function () {
        LoadData();
    });

    var KodeUserdelete, KodeUseredit;

    function LoadData() {
        var action = "LoadData";
        $.ajax({
            url: "daftar_user_aksi.php",
            method: "POST",
            data: {action: action, IsKabupaten: 1},
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    $('#tabel_user_opd').html(data.DataHtml);
                    $('#tabeldata').DataTable();
                } else {
                    alert('request failed');
                }
            }
        });
    }

    $(document).on('click', '.btn-danger', function () {
        KodeUserdelete = $(this).attr("id");
        $('#ModalHapus').modal('show');
    });

    $(document).on('click', '#btHapus', function () {
        var action = "HapusData";
        $.ajax({
            url: "daftar_user_aksi.php",
            method: "POST",
            data: {id: KodeUserdelete, action: action},
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    LoadData();
                    $('#ModalHapus').modal("toggle");
                } else {
                    alert('request failed');
                }
            }
        });
    });

    $(document).on('click', '.btn-warning', function () {
        var id = $(this).attr("id");
        KodeUseredit = id;
        var action = "AmbilData";
        $.ajax({
            url: "daftar_user_aksi.php",
            method: "POST",
            data: {id: id, action: action, IsKabupaten:1},
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    $('#etNamaUser').val(data.Nama);
                    $('#etUsername').val(data.Username);
                    $('#etPassword').val(data.Password);
                    $('#select_opd').html(data.HtmlCB);
                    $('#ModalEdit').modal("show");
                } else {
                    alert('request failed');
                }
            }
        });
    });

    $(document).on('click', '#btSimpan', function () {
        var NamaUser = $("[name='etNamaUser']").val();
        var Username = $("[name='etUsername']").val();
        var Password = $("[name='etPassword']").val();
        var KodeOPD = "";
        var action = "UpdateData";
        $.ajax({
            url: "daftar_user_aksi.php",
            method: 'POST',
            data: {
                id: KodeUseredit,
                namaUser: NamaUser,
                username: Username,
                password: Password,
                kodeOPD: KodeOPD,
                action: action,
                IsKabupaten:1
            },
            dataType: 'json',
            success: function (data) {
                if (data.response === 200) {
                    $("[name='txtNamaUser']").val("");
                    $("[name='txtUsername']").val("");
                    $("[name='txtPassword']").val("");
                    LoadData();
                    $('#ModalEdit').modal("toggle");
                } else {
                    alert('request failed');
                }
            }
        });
    });
</script>

