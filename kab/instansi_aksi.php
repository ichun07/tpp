<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		echo GetDataInstansi($conn);
	}
	if ($_POST["action"] == "InsertData") {
		$NamaOPD = $_POST["namaOPD"];
		$AlamatOPD = $_POST["alamatOPD"];
		$KodeSuratOPD = $_POST["kodeSuratOPD"];
		$Keterangan = $_POST["keteranganOPD"];
		AddInstansi($conn,$NamaOPD,$AlamatOPD,$KodeSuratOPD,$Keterangan);
	}
	if ($_POST["action"] == "AmbilData") {
		$KodeOPD = $_POST["id"];
		$output = GetOneDataInstansi($conn, $KodeOPD);
		echo json_encode($output);
	}
	if ($_POST["action"] == "UpdateData") {
		$KodeOPD = $_POST["id"];
		$NamaOPD = $_POST["namaOPD"];
		$AlamatOPD = $_POST["alamatOPD"];
		$KodeSuratOPD = $_POST["kodeSuratOPD"];
		$Keterangan = $_POST["keteranganOPD"];
		SetEditData($conn,$KodeOPD, $NamaOPD,$AlamatOPD,$KodeSuratOPD,$Keterangan);
	}
	if ($_POST["action"] == "HapusData") {
		$KodeOPD = $_POST["id"];
		SetHapusData($conn, $KodeOPD);
	}
}

function AddInstansi($conn,$NamaOPD,$AlamatOPD,$KodeSuratOPD,$Keterangan){
	$KodeOPD = AutoKodeOPD($conn);
	$sql = "INSERT INTO mstopd (KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan) VALUES ('$KodeOPD', '$NamaOPD', '$AlamatOPD', '$KodeSuratOPD', '$Keterangan')";
	$res = $conn->query($sql);
	session_start();
	InsertLog($conn, 'INSERT', 'Menambah Data OPD', $_SESSION['KodeUserKab']);
	return $res;
}

function GetDataInstansi($conn){
	$output = '';
	$sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
	$res = $conn->query($sql);
	$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
	<thead>
	<tr>
	<th width="30px">No</th>
	<th>Nama OPD</th>
	<th>Alamat OPD</th>
	<th width="90px">Kode Surat</th>
	<th>Keterangan</th>
	<th>Aksi</th>
	</tr>
	</thead>
	<tfoot>
	<tr>
	<th>No</th>
	<th>Nama OPD</th>
	<th>Alamat OPD</th>
	<th>Kode Surat</th>
	<th>Keterangan</th>
	<th>Aksi</th>
	</tr>
	</tfoot>
	<tbody>';	
	if($res){
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$output.= '<tr>
			<td>'.$no++.'</td>
			<td>'.$row['NamaOPD'].'</td>
			<td>'.$row['AlamatOPD'].'</td>
			<td>'.$row['KodeSuratOPD'].'</td>
			<td>'.$row['Keterangan'].'</td>
			<td class="text-center">
			<button id="btnUpdate" name="btnUpdate" value="' . $row['KodeOPD'] . '" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
			<button id="btnDelete" name="btnDelete" value="' . $row['KodeOPD'] . '" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></button>
			<button id="btnInfo" name="btnInfo" value="' . $row['KodeOPD'] . '" data-value="' . $row['NamaOPD'] . '" class="btn btn-info"><span class="fa fa-arrow-right" aria-hidden="true"></span></button>
			</td>
			</tr>';
		}
	}

	$output .='</tbody>
	</table>';
	return $output;
}

function GetOneDataInstansi($conn, $KodeOPD){
	$output = '';
	$sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd WHERE KodeOPD = '$KodeOPD' GROUP BY KodeOPD";
	$res = $conn->query($sql);
	while ($row = $res->fetch_assoc()) {
		$output['KodeOPD'] = $row['KodeOPD'];
		$output['NamaOPD'] = $row['NamaOPD'];
		$output['AlamatOPD'] = $row['AlamatOPD'];
		$output['KodeSuratOPD'] = $row['KodeSuratOPD'];
		$output['Keterangan'] = $row['Keterangan'];
	}
	return $output;
}

function SetHapusData($conn, $KodeOPD){
	$sql = "DELETE FROM mstopd WHERE KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	session_start();
	InsertLog($conn, 'DELETE', 'Menghapus data OPD', $_SESSION['KodeUserKab']);
	return $res;
}

function SetEditData($conn,$KodeOPD, $NamaOPD,$AlamatOPD,$KodeSuratOPD,$Keterangan){
	$sql = "UPDATE mstopd SET NamaOPD = '$NamaOPD',AlamatOPD = '$AlamatOPD',KodeSuratOPD = '$KodeSuratOPD',Keterangan = '$Keterangan' WHERE KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	session_start();
	InsertLog($conn, 'UPDATE', 'Mengubah Data OPD', $_SESSION['KodeUserKab']);
	return $res;
}

function AutoKodeOPD($conn){
	$sql = "SELECT RIGHT(KodeOPD,7) AS kode FROM mstopd ORDER BY KodeOPD DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
	return 'OPD-' . $bikin_kode;
}

?>