<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Instansi OPD</h2>
		</div>
	</header>
	<section class="forms"> 
		<div class="container-fluid">			
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 text-right">
						<button onclick="location.href = '../kab/instansi_tambah.php'" class="btn btn-primary">Tambah Data</button>
					</div>
				</div></br>
				<div class="row">
					<div class="col-lg-12">
						<div id="tabel_instansi" class="table-responsive">
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="text-align:center;">Anda yakin untuk menghapus data ini ?</h4>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Popup untuk Edit--> 
<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Edit Data Instansi</h4>
			</div>
			<div class="modal-body">
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="etNamaOPD">Nama Instansi</label>
					<input type="text" name="etNamaOPD" id="etNamaOPD" class="form-control" required data-msg="Nama Instansi tidak boleh kosong!"/>
				</div>
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="etAlamatOPD">Alamat Instansi</label>
					<input type="text" name="etAlamatOPD" id="etAlamatOPD" class="form-control" required data-msg="Alamat tidak boleh kosong!"/>
				</div>
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="etKodeSuratOPD">Kode Surat Instansi</label>
					<input type="text" name="etKodeSuratOPD" id="etKodeSuratOPD" class="form-control" required data-msg="Kode Surat tidak boleh kosong!"/>
				</div>
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="etKeteranganOPD">Keterangan</label>
					<input type="text" name="etKeteranganOPD" id="etKeteranganOPD" class="form-control" required data-msg="Keterangan tidak boleh kosong!"/>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info" id="btSimpan">
						Simpan
					</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
				</div>

			</div>
		</div>
	</div>
</div>
<?php
include_once 'footer.php';
?>

<script type="text/javascript">
	$(document).ready(function() {
		LoadData();  
	});

	var KodeOPDdelete,KodeOPDedit;

	function LoadData(){
		var action = "LoadData";
		$.ajax({
			url: "instansi_aksi.php",
			method: "POST",
			data: {action: action},
			success: function (data) {
				$('#tabel_instansi').html(data);
				$('#tabeldata').DataTable();
			}
		});
	}

	$(document).on('click', '#btnDelete', function () {
		var id = $(this).val();
		KodeOPDdelete = id;
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btHapus', function () {
		var action = "HapusData";
		$.ajax({
			url: "instansi_aksi.php",
			method: "POST",
			data: {id: KodeOPDdelete, action: action},
			success: function () {
				LoadData();
				$('#ModalHapus').modal("toggle");
			}
		});
	});

	$(document).on('click', '#btnUpdate', function () {
		var id = $(this).val();
		KodeOPDedit = id;
		var action = "AmbilData";
		$.ajax({
			url: "instansi_aksi.php",
			method: "POST",
			data: {id: id, action: action},
			dataType: 'json',
			success: function (data) {
				$('#etNamaOPD').val(data.NamaOPD);
				$('#etAlamatOPD').val(data.AlamatOPD);
				$('#etKodeSuratOPD').val(data.KodeSuratOPD);
				$('#etKeteranganOPD').val(data.Keterangan);
				$('#ModalEdit').modal("show");
			}
		});
	});

	$(document).on('click', '#btSimpan', function () {
		var NamaOPD = $("[name='etNamaOPD']").val();
		var AlamatOPD = $("[name='etAlamatOPD']").val();
		var KodeSuratOPD = $("[name='etKodeSuratOPD']").val();
		var Keterangan = $("[name='etKeteranganOPD']").val();
		var action = "UpdateData";
		$.ajax({
			url: "instansi_aksi.php",
			method: 'POST',
			data: {id: KodeOPDedit, namaOPD: NamaOPD, alamatOPD: AlamatOPD, kodeSuratOPD: KodeSuratOPD, keteranganOPD: Keterangan, action: action},
			success: function () {
				$("[name='etNamaOPD']").val("");
				$("[name='etAlamatOPD']").val("");
				$("[name='etKodeSuratOPD']").val("");
				$("[name='etKeteranganOPD']").val("");
				LoadData();
				$('#ModalEdit').modal("toggle");
			}
		});
	});

	$(document).on('click', '#btnInfo', function () {
		var KodeOPD = $(this).val();
		var NamaOPD = $(this).attr("data-value");
		location.href='instansi_detail.php?KodeOPD='+KodeOPD+'&NamaOPD='+NamaOPD;
	});
</script>
