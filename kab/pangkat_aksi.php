<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		$requestData = $_REQUEST;
		$result = GetDataPangkat($conn,$requestData);
		echo json_encode($result);
	}

	if($_POST['action'] == 'HapusData'){
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$result = DeletePangkat($conn, $Pangkat, $Golongan);
		echo json_encode($result);
	}

	if($_POST['action'] == 'InsertData'){
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$PPH = $_POST['PPH'];
		$result = InsertPangkat($conn, $Pangkat, $Golongan, $PPH);
		echo json_encode($result); 
	}
}

function GetDataPangkat($conn,$requestData){
	$columns = array( 
		0 => 'Pangkat',
		1 => 'Pangkat', 
		2 => 'Golongan',
		3 => 'PPH'
	);

	$sql = "SELECT * ";
	$sql.="FROM mstpangkat";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
	$totalFiltered = $totalData; 


	if( !empty($requestData['search']['value']) ) {
		$sql = "SELECT * ";
		$sql.=" FROM mstpangkat ";
		$sql.="WHERE Pangkat LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR Golongan '%".$requestData['search']['value']."%' ";
		$sql.=" OR PPH '%".$requestData['search']['value']."%' ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
		$totalFiltered = mysqli_num_rows($query);

		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; 
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	} else {	

		$sql = "SELECT * ";
		$sql.=" FROM mstpangkat";
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	}

	$data = array();
	$no = 1;
	while( $row=mysqli_fetch_array($query) ) {  
		$nestedData=array();
		$nestedData[] = $no++;
		$nestedData[] = $row['Pangkat'];
		$nestedData[] = $row['Golongan'];
		$nestedData[] = $row['PPH'].'%';
		$nestedData[] = '<td class="text-center">		
		<button value = "'.$row['Pangkat'].'" data-value = "'.$row['Golongan'].'" name="btnHapus" id="btnHapus" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></button>
		</td>';

		$data[] = $nestedData;
		

	}



	$json_data = array(
		"draw"            => intval( $requestData['draw'] ),   
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data 
	);

	return $json_data; 

	//<button value = '.$row['Pangkat'].' data-value = '.$row['Golongan'].' name="btnEdit" id="btnEdit" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
}

function DeletePangkat($conn, $Pangkat, $Golongan){
	$sql = "DELETE FROM mstpangkat WHERE Pangkat = '$Pangkat' AND Golongan = '$Golongan'";
	$res = $conn->query($sql);
	if($res){		
		session_start();
		InsertLog($conn, 'DELETE', 'Menghapus Pangkat dan Golongan', $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function CekDataPangkat($conn, $Pangkat, $Golongan){
	$sql ="SELECT COUNT(Pangkat) AS Jumlah FROM mstpangkat WHERE Pangkat = '$Pangkat' AND Golongan = '$Golongan'";
	$res = $conn->query($sql);
	if($res){
		$result = mysqli_fetch_array($res);
		return $result['Jumlah'];
	}
}

function InsertPangkat($conn, $Pangkat, $Golongan, $PPH){
	$check = CekDataPangkat($conn, $Pangkat, $Golongan);
	if($check > 0){
		return array('response' => 404);
	}else{
		$KodePangkat = AutoKodePangkat($conn);
		$sql = "INSERT INTO mstpangkat (KodePangkat, Pangkat, Golongan, PPH) VALUES ('$KodePangkat', '$Pangkat', '$Golongan', '$PPH')";
		$res = $conn->query($sql);
		if($res){
			session_start();
			InsertLog($conn, 'INSERT', 'Menambah Pangkat dan Golongan', $_SESSION['KodeUserKab']);
			return array('response' => 200);
		}else{
			return array('response' => 500);
		}
	}
}


function AutoKodePangkat($conn){
	$sql = "SELECT RIGHT(KodePangkat,6) AS kode FROM mstpangkat ORDER BY KodePangkat DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 6, "0", STR_PAD_LEFT);
	return 'GOL-' . $bikin_kode;
}


?>