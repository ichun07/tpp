<?php
date_default_timezone_set("Asia/Jakarta");
$year = date("Y");
$month = date("m");

if($_POST['action']==2){
	$output = '
	<div class="row" id="data_absen">
	<div class="col-md-2 text-left">
	Bulan/Tahun
	</div>
	<div class="col-md-3 text-left">
	<select id="cb_bulan" name="cb_bulan" class="form-control">				
	<option value="1"'; if ((int)$month == 1) { $output.=' selected="selected" '; } $output.='>Januari</option>
	<option value="2"'; if ((int)$month == 2) { $output.=' selected="selected" ';} $output.='>Februari</option>
	<option value="3"'; if ((int)$month == 3) { $output.=' selected="selected" ';} $output.='>Maret</option>
	<option value="4"'; if ((int)$month == 4) { $output.=' selected="selected" ';} $output.='>April</option>
	<option value="5"'; if ((int)$month == 5) { $output.=' selected="selected" ';} $output.='>Mei</option>
	<option value="6"'; if ((int)$month == 6) { $output.=' selected="selected" ';} $output.='>Juni</option>
	<option value="7"'; if ((int)$month == 7) { $output.=' selected="selected" ';} $output.='>Juli</option>
	<option value="8"'; if ((int)$month == 8) { $output.=' selected="selected" ';} $output.='>Agustus</option>
	<option value="9"'; if ((int)$month == 9) { $output.=' selected="selected" ';} $output.='>September</option>
	<option value="10"'; if ((int)$month == 10) { $output.=' selected="selected" ';} $output.='>Oktober</option>
	<option value="11"'; if ((int)$month == 11) { $output.=' selected="selected" ';} $output.='>November</option>
	<option value="12"'; if ((int)$month == 12) { $output.=' selected="selected" ';} $output.='>Desember</option>
	</select>
	</div>
	<div class="col-md-2 text-left">
	<select id="cb_tahun" name="cb_tahun" class="form-control">
	<option selected="selected" value="'.$year.'">'.$year.'</option>';
	for($i = $year-1; $i > $year-10; $i--){
		$output .= "<option value='".$i."'>".$i."</option>";
	}
	$output.='</select>
	</div>
	<div class="col-md-5 text-right">
	</div>
	<div class="col-md-2 text-left">
	Hari Kerja Efektif (HKE)
	</div>
	<div class="col-md-10 text-left">
	<label id="txtJmlHariEfektif" name="JmlHariEfektif"></label>
	</div>
	<div class="col-lg-12">
	<div id="tabel_absensi">
	</div>					
	</div>
	</div>';

	echo json_encode($output);
}else if($_POST['action'] == 3){
	$output = '
	<div class="row" id="data_tpp">
	<div class="col-md-2 text-left">
	Bulan/Tahun
	</div>
	<div class="col-md-3 text-left">
	<select id="cb_bulan" name="cb_bulan" class="form-control">				
	<option value="1"'; if ((int)$month == 1) { $output.=' selected="selected" '; } $output.='>Januari</option>
	<option value="2"'; if ((int)$month == 2) { $output.=' selected="selected" ';} $output.='>Februari</option>
	<option value="3"'; if ((int)$month == 3) { $output.=' selected="selected" ';} $output.='>Maret</option>
	<option value="4"'; if ((int)$month == 4) { $output.=' selected="selected" ';} $output.='>April</option>
	<option value="5"'; if ((int)$month == 5) { $output.=' selected="selected" ';} $output.='>Mei</option>
	<option value="6"'; if ((int)$month == 6) { $output.=' selected="selected" ';} $output.='>Juni</option>
	<option value="7"'; if ((int)$month == 7) { $output.=' selected="selected" ';} $output.='>Juli</option>
	<option value="8"'; if ((int)$month == 8) { $output.=' selected="selected" ';} $output.='>Agustus</option>
	<option value="9"'; if ((int)$month == 9) { $output.=' selected="selected" ';} $output.='>September</option>
	<option value="10"'; if ((int)$month == 10) { $output.=' selected="selected" ';} $output.='>Oktober</option>
	<option value="11"'; if ((int)$month == 11) { $output.=' selected="selected" ';} $output.='>November</option>
	<option value="12"'; if ((int)$month == 12) { $output.=' selected="selected" ';} $output.='>Desember</option>
	</select>
	</div>
	<div class="col-md-2 text-left">
	<select id="cb_tahun" name="cb_tahun" class="form-control">
	<option selected="selected" value="'.$year.'">'.$year.'</option>';
	for($i = $year-1; $i > $year-10; $i--){
		$output .= "<option value='".$i."'>".$i."</option>";
	}
	$output.='</select>
	</div>
	<div class="col-md-5 text-right">
	<button id="btnCetak" class="btn btn-primary" style="visibility:hidden;">Cetak Tpp Produktifitas</button>
	</div>
	<div class="col-lg-12">
	<div id="tabel_produktifitas">
	</div>					
	</div>
	</div>';
	echo json_encode($output);
}else if($_POST['action'] == 4){
	$output ='<div class="row">
	<div class="col-md-4 text-left">
	<div class="form-group">
	<select id="cb_triwulan" name="cb_triwulan" class="form-control">             
	<option value="1"'; if (3 >= (int)$month and (int)$month >= 1) $output.= ' selected="selected"'; $output.='>Triwulan 1</option>
	<option value="2"'; if (6 >= (int)$month and (int)$month >= 4) $output.= ' selected="selected"'; $output.='>Triwulan 2</option>
	<option value="3"'; if (9 >= (int)$month and (int)$month >= 7) $output.= ' selected="selected"'; $output.='>Triwulan 3</option>
	<option value="4"'; if (12 >= (int)$month and (int)$month >= 10) $output.= ' selected="selected"'; $output.='>Triwulan 4</option>
	</select>
	</div>
	</div>
	<div class="col-md-4 text-left">
	<div class="form-group">
	<select class="form-control" id="cb_tahun" name="cb_tahun">
	<option selected="selected" value="'.$year.'">'.$year.'</option>';

	for($i = $year-1; $i > $year-10; $i--){
		$output.= '<option value="'.$i.'">'.$i.'</option>';
	}
	$output.='
	</select>
	</div>
	</div>
	<div class="col-md-4 text-right">
	</div>

	<div class="col-lg-12">
	<div class="card">
	<div class="card-header d-flex align-items-center">
	<h3 class="h4">Nilai Kinerja Perangkat Daerah</h3>
	</div>
	<div class="card-body">
	<div id="tabel_capaian" class="table-responsive">
	</div>
	</div>
	</div>		
	</br>
	<div class="card">
	<div class="card-header d-flex align-items-center">
	<h3 class="h4">Rekapitulasi TPP</h3>
	</div>
	<div class="card-body">
	<div id="tabel_pegawai" class="table-responsive">
	</div>				
	</div>
	</div>
	</div>';
	echo json_encode($output);
}else if($_POST['action'] == 5){
	$output = '
	<div class="row" id="data_makan">
	<div class="col-md-2 text-left">
	Bulan/Tahun
	</div>
	<div class="col-md-3 text-left">
	<select id="cb_bulan" name="cb_bulan" class="form-control">				
	<option value="1"'; if ((int)$month == 1) { $output.=' selected="selected" '; } $output.='>Januari</option>
	<option value="2"'; if ((int)$month == 2) { $output.=' selected="selected" ';} $output.='>Februari</option>
	<option value="3"'; if ((int)$month == 3) { $output.=' selected="selected" ';} $output.='>Maret</option>
	<option value="4"'; if ((int)$month == 4) { $output.=' selected="selected" ';} $output.='>April</option>
	<option value="5"'; if ((int)$month == 5) { $output.=' selected="selected" ';} $output.='>Mei</option>
	<option value="6"'; if ((int)$month == 6) { $output.=' selected="selected" ';} $output.='>Juni</option>
	<option value="7"'; if ((int)$month == 7) { $output.=' selected="selected" ';} $output.='>Juli</option>
	<option value="8"'; if ((int)$month == 8) { $output.=' selected="selected" ';} $output.='>Agustus</option>
	<option value="9"'; if ((int)$month == 9) { $output.=' selected="selected" ';} $output.='>September</option>
	<option value="10"'; if ((int)$month == 10) { $output.=' selected="selected" ';} $output.='>Oktober</option>
	<option value="11"'; if ((int)$month == 11) { $output.=' selected="selected" ';} $output.='>November</option>
	<option value="12"'; if ((int)$month == 12) { $output.=' selected="selected" ';} $output.='>Desember</option>
	</select>
	</div>
	<div class="col-md-2 text-left">
	<select id="cb_tahun" name="cb_tahun" class="form-control">
	<option selected="selected" value="'.$year.'">'.$year.'</option>';
	for($i = $year-1; $i > $year-10; $i--){
		$output .= "<option value='".$i."'>".$i."</option>";
	}
	$output.='</select>
	</div>
	<div class="col-md-5 text-right">
	<button id="btnCetak" class="btn btn-primary" style="visibility:hidden;">Cetak Tpp Produktifitas</button>
	</div>
	<div class="col-lg-12">
	<div id="tabel_makan">
	</div>					
	</div>
	</div>';
	echo json_encode($output);
}