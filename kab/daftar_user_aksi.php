<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
    if ($_POST["action"] == "LoadData") {
        $IsKabupaten = $_POST['IsKabupaten'];
        $result = GetDataUser($conn, $IsKabupaten);
        echo json_encode($result);
    }
    if ($_POST["action"] == "LoadDataOPD") {
        $result = GetDataOPD($conn);
        echo json_encode($result);
    }
    if ($_POST["action"] == "InsertData") {
        $Nama = $_POST["namaUser"];
        $Username = $_POST["username"];
        $Password = base64_encode($_POST["password"]);
        $IsDihapus = "b'0'";
        $KodeOPD = $_POST["kodeOPD"];
        $IsTpp = 1;
        $IsAnjab = "b'0'";
        $IsKabupaten = "b'".$_POST['IsKabupaten']."'";

        if($_POST['IsKabupaten'] == 0){
            $Opd = true;
        }else{
            $Opd = false;
        }
        $result = InsertDataUser($conn, $Nama, $Username, $Password, $IsDihapus, $KodeOPD, $IsKabupaten, $IsTpp, $IsAnjab, $Opd);
        echo json_encode($result);
    }
    if ($_POST["action"] == "AmbilData") {
        $KodeUser = $_POST["id"];
        $IsKabupaten = $_POST['IsKabupaten'];
        $output = GetOneDataUser($conn, $KodeUser,$IsKabupaten);
        echo json_encode($output);
    }
    if ($_POST["action"] == "UpdateData") {
        $KodeUser = $_POST["id"];
        $Nama = $_POST["namaUser"];
        $Username = $_POST["username"];
        $Password = base64_encode($_POST["password"]);
        $KodeOPD = $_POST["kodeOPD"];
        $IsKabupaten = $_POST['IsKabupaten'];
        $result = SetUpdateDataUser($conn, $KodeUser, $Nama, $Username, $Password, $KodeOPD, $IsKabupaten);
        echo json_encode($result);
    }
    if ($_POST["action"] == "HapusData") {
        $KodeUser = $_POST["id"];
        $result = SetHapusDataUser($conn, $KodeUser);
        echo json_encode($result);
    }
}

function GetDataUser($conn, $IsKabupaten)
{
    if ($IsKabupaten < 1) {
        $no = 1;
        $output = '';
        $sql = "SELECT u.KodeUser,u.Nama,u.Username,u.Password,u.IsDihapus,u.KodeOPD,u.IsKabupaten,u.IsTpp,u.IsAnjab,o.NamaOPD
        FROM mstuser u 
        LEFT JOIN mstopd o ON u.KodeOPD = o.KodeOPD
        WHERE u.IsDihapus = '0'
        AND u.IsTpp = '1'
        AND u.IsKabupaten = '0'
        ORDER BY u.KodeUser ASC";
        $res = $conn->query($sql);
        if ($res) {
            $output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
            <thead>
            <tr>
            <th width="30px">No</th>
            <th>Nama User</th>
            <th>Username</th>
            <th>Nama OPD</th>
            <th width="200px">Aksi</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
            <th>No</th>
            <th>Nama User</th>
            <th>Username</th>
            <th>Nama OPD</th>
            <th>Aksi</th>
            </tr>
            </tfoot>
            <tbody>';
            while ($row = $res->fetch_assoc()) {
                $output .= '<tr>
                <td>' . $no++ . '</td>
                <td>' . $row['Nama'] . '</td>
                <td>' . $row['Username'] . '</td>
                <td>' . $row['NamaOPD'] . '</td>
                <td class="text-center">
                <a href="#" name="update" id="' . $row['KodeUser'] . '" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                <a href="#" name="delete" id="' . $row['KodeUser'] . '" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></a>
                </td>
                </tr>';
            }
            $output .= '</tbody></table>';
            return array('response' => 200, 'DataHtml' => $output);
        } else {
            return array('response' => 500);
        }
    } else {
        $no = 1;
        $output = '';
        $sql = "SELECT u.KodeUser,u.Nama,u.Username,u.Password,u.IsDihapus,u.KodeOPD,u.IsKabupaten,u.IsTpp,u.IsAnjab
        FROM mstuser u 
        WHERE u.IsDihapus = '0'
        AND u.IsTpp = '1'
        AND u.IsKabupaten = '1'
        ORDER BY u.KodeUser ASC";
        $res = $conn->query($sql);
        if ($res) {
            $output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
            <thead>
            <tr>
            <th width="30px">No</th>
            <th>Nama User</th>
            <th>Username</th>
            <th width="200px">Aksi</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
            <th>No</th>
            <th>Nama User</th>
            <th>Username</th>
            <th>Aksi</th>
            </tr>
            </tfoot>
            <tbody>';
            while ($row = $res->fetch_assoc()) {
                $output .= '<tr>
                <td>' . $no++ . '</td>
                <td>' . $row['Nama'] . '</td>
                <td>' . $row['Username'] . '</td>
                <td class="text-center">
                <a href="#" name="update" id="' . $row['KodeUser'] . '" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                <a href="#" name="delete" id="' . $row['KodeUser'] . '" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></span></a>
                </td>
                </tr>';
            }
            $output .= '</tbody></table>';
            return array('response' => 200, 'DataHtml' => $output);
        } else {
            return array('response' => 500);
        }
    }
}

function GetOneDataUser($conn, $KodeUser, $IsKabupaten)
{
    $output = '';
    if($IsKabupaten > 0){
        $sql = "SELECT u.KodeUser,u.Nama,u.Username,u.Password,u.IsDihapus,u.KodeOPD,u.IsKabupaten,u.IsTpp,u.IsAnjab
        FROM mstuser u 
        WHERE u.KodeUser = '$KodeUser' AND u.IsKabupaten = '$IsKabupaten'
        GROUP BY u.KodeUser";

        $res = $conn->query($sql);
        if ($res) {
            while ($row = $res->fetch_assoc()) {
                $output['KodeUser'] = $row['KodeUser'];
                $output['Nama'] = $row['Nama'];
                $output['Username'] = $row['Username'];
                $output['Password'] = base64_decode($row['Password']);
                $output['IsDihapus'] = $row['IsDihapus'];
                $output['IsKabupaten'] = $row['IsKabupaten'];
                $output['IsTpp'] = $row['IsTpp'];
                $output['IsAnjab'] = $row['IsAnjab'];
            }
            $output['response'] = 200;
            return $output;
        } else {
            return array('response' => 500);
        }
    }else{
        $sql = "SELECT u.KodeUser,u.Nama,u.Username,u.Password,u.IsDihapus,u.KodeOPD,u.IsKabupaten,u.IsTpp,u.IsAnjab,o.NamaOPD
        FROM mstuser u 
        LEFT JOIN mstopd o ON u.KodeOPD = o.KodeOPD
        WHERE u.KodeUser = '$KodeUser'
        GROUP BY u.KodeUser";

        $res = $conn->query($sql);
        if ($res) {
            while ($row = $res->fetch_assoc()) {
                $output['KodeUser'] = $row['KodeUser'];
                $output['Nama'] = $row['Nama'];
                $output['Username'] = $row['Username'];
                $output['Password'] = base64_decode($row['Password']);
                $output['IsDihapus'] = $row['IsDihapus'];
                $output['KodeOPD'] = $row['KodeOPD'];
                $output['IsKabupaten'] = $row['IsKabupaten'];
                $output['IsTpp'] = $row['IsTpp'];
                $output['IsAnjab'] = $row['IsAnjab'];
                $output['NamaOPD'] = $row['NamaOPD'];
                if($IsKabupaten < 1){
                    $output['HtmlCB'] = GetDataOPDEdit($conn, $row['KodeOPD'], $row['NamaOPD']);
                }else{
                    $output['HtmlCB'] = '';
                }            
            }
            $output['response'] = 200;
            return $output;
        } else {
            return array('response' => 500);
        }
    }   

}

function InsertDataUser($conn, $Nama, $Username, $Password, $IsDihapus, $KodeOPD, $IsKabupaten, $IsTpp, $IsAnjab, $Opd)
{
    $sql = "";
    $KodeUser = AutoKodeUser($conn);
    if(!$Opd){
        $sql = "INSERT INTO mstuser (KodeUser,Nama,Username,Password,IsDihapus,IsKabupaten,IsTpp,IsAnjab) VALUES ('$KodeUser', '$Nama', '$Username', '$Password', " . $IsDihapus . "," . $IsKabupaten . ", " . $IsTpp . ", " . $IsAnjab . ")";        
    }else{
        $sql = "INSERT INTO mstuser (KodeUser,Nama,Username,Password,IsDihapus,KodeOPD,IsKabupaten,IsTpp,IsAnjab) VALUES ('$KodeUser', '$Nama', '$Username', '$Password', " . $IsDihapus . ", '$KodeOPD', " . $IsKabupaten . ", " . $IsTpp . ", " . $IsAnjab . ")";
    }
    $res = $conn->query($sql);
    if ($res) {        
        session_start();
        InsertLog($conn, 'INSERT', 'Menambah Data User', $_SESSION['KodeUserKab']);
        return array('response' => 200);
    } else {
        return array('response' => 500);
    }
}

function SetUpdateDataUser($conn, $KodeUser, $Nama, $Username, $Password, $KodeOPD, $IsKabupaten)
{
    $sql = '';
    if($IsKabupaten < 1){
        $sql = "UPDATE mstuser SET Nama = '$Nama', Username = '$Username', Password = '$Password', KodeOPD = '$KodeOPD' WHERE KodeUser = '$KodeUser'";
    }else{
        $sql = "UPDATE mstuser SET Nama = '$Nama', Username = '$Username', Password = '$Password' WHERE KodeUser = '$KodeUser'";
    }
    
    $res = $conn->query($sql);
    if ($res) {
        session_start();
        InsertLog($conn, 'UPDATE', 'Mengubah Data User', $_SESSION['KodeUserKab']);
        return array('response' => 200);
    } else {
        return array('response' => 500);
    }
}

function SetHapusDataUser($conn, $KodeUser)
{
    $sql = "UPDATE mstuser SET IsDihapus = '1' WHERE KodeUser = '$KodeUser'";
    $res = $conn->query($sql);
    if ($res) {
        session_start();
        InsertLog($conn, 'DELETE', 'Menghapus Data User', $_SESSION['KodeUserKab']);
        return array('response' => 200);
    } else {
        return array('response' => 500);
    }
}

function GetDataOPDEdit($conn, $KodeOPD, $NamaOPD)
{
    $output = '';
    $sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
    $res = $conn->query($sql);
    if ($res) {
        $output .= '<label class="form-control-label">Nama OPD</label>';
        $output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
        $output .= "<option value='" . $KodeOPD . "'>" . $NamaOPD . "</option>";
        while ($row = $res->fetch_assoc()) {
            $output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
        }
        $output .= '</select>';
        return  $output;
    } else {
        return array('response' => 500);
    }
}

function GetDataOPD($conn)
{
    $output = '';
    $sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
    $res = $conn->query($sql);
    if ($res) {
        $output .= '<label class="form-control-label">Nama OPD</label>';
        $output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
        $output .= "<option value='0'> - Pilih OPD - </option>";
        while ($row = $res->fetch_assoc()) {
            $output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
        }
        $output .= '</select>';
        return $output;
    } else {
        return $output;
    }
}

function AutoKodeUser($conn)
{
    $sql = "SELECT RIGHT(KodeUser,16) AS kode FROM mstuser ORDER BY KodeUser DESC LIMIT 1";
    $res = mysqli_query($conn, $sql);
    $result = mysqli_fetch_array($res);
    if ($result['kode'] == null) {
        $kode = 1;
    } else {
        $kode = ++$result['kode'];
    }
    $bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
    return 'USR-' . $bikin_kode;
}

?>