<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {

	if($_POST["action"] == "LoadData"){
		$requestData = $_REQUEST;
		$result = DataAktivitasPegawai($conn, $requestData);
		echo json_encode($result);
	}

	if($_POST['action'] == 'InsertData'){
		$NamaAktivitas = $_POST['NamaAktivitas'];
		$Satuan = $_POST['Satuan'];
		$TingkatKesulitan = $_POST['TingkatKesulitan'];
		$Waktu = $_POST['Waktu'];
		$Beban = $_POST['Beban'];
		$result = InsertDataAktivitas($conn, $NamaAktivitas, $Satuan, $TingkatKesulitan, $Waktu, $Beban);
		echo json_encode($result);
	}

	if($_POST['action'] == 'AmbilData'){
		$KodeAktivitasPegawai = $_POST['KodeAktivitas'];
		$result = GetOneDataAktivitas($conn, $KodeAktivitasPegawai);
		echo json_encode($result);
	}

	if($_POST['action'] == 'UpdateData'){
		$KodeAktivitasPegawai = $_POST['KodeAktivitas'];
		$NamaAktivitas = $_POST['NamaAktivitas'];
		$Satuan = $_POST['Satuan'];
		$TingkatKesulitan = $_POST['TingkatKesulitan'];
		$Waktu = $_POST['Waktu'];
		$Beban = $_POST['Beban'];
		$result = UdateDataAktivitas($conn, $KodeAktivitasPegawai, $NamaAktivitas, $Satuan, $TingkatKesulitan, $Waktu, $Beban);
		echo json_encode($result);	
	}

	if($_POST['action'] == 'DeleteData'){
		$KodeAktivitasPegawai = $_POST['KodeAktivitas'];
		$result = DeleteDataAktivitas($conn, $KodeAktivitasPegawai);
		echo json_encode($result);
	}
}

function DataAktivitasPegawai($conn, $requestData){
	$columns = array( 
		0 => 'KodeAktivitasPegawai',
		1 => 'NamaAktivitas', 
		2 => 'TingkatKesulitan',
		3 => 'Waktu',
		4 => 'Beban'
	);

	$sql = "SELECT KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban ";
	$sql.="FROM mstaktivitas";
	$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
	$totalFiltered = $totalData;


	if( !empty($requestData['search']['value']) ) {
		$sql = "SELECT KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban ";
		$sql.="FROM mstaktivitas ";
		$sql.="WHERE NamaAktivitas LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR TingkatKesulitan LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR Waktu LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR Beban LIKE '%".$requestData['search']['value']."%' ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
		$totalFiltered = mysqli_num_rows($query);
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");	
	} else {
		$sql = "SELECT KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban ";
		$sql.="FROM mstaktivitas ";
		$sql.="ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	}

	$data = array();
	$no = 1;
	while( $row=mysqli_fetch_array($query) ) {
		$nestedData=array();
		$Aksi = '<button value = "'.$row['KodeAktivitasPegawai'].'" name="btnUpdate" id="btnUpdate" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
		<button value="'.$row['KodeAktivitasPegawai'].'" name="delete" id="btnDelete" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></button>';
		$nestedData[] = $no++;
		$nestedData[] = $row['NamaAktivitas'];
		$nestedData[] = $row['TingkatKesulitan'];
		$nestedData[] = $row['Waktu'];
		$nestedData[] = $row['Beban'];	
		$nestedData[] = $Aksi;		

		$data[] = $nestedData;

	}



	$json_data = array(
		"draw"            => intval( $requestData['draw'] ),
		"recordsTotal"    => intval( $totalData ),
		"recordsFiltered" => intval( $totalFiltered ),
		"data"            => $data   		);

	return $json_data; 
}

function InsertDataAktivitas($conn, $NamaAktivitas, $Satuan, $TingkatKesulitan, $Waktu, $Beban){
	$KodeAktivitasPegawai = AutoKodeAktivitas($conn);
	$sql = "INSERT INTO mstaktivitas (KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban) VALUES ('$KodeAktivitasPegawai', '$NamaAktivitas', '$Satuan', '$TingkatKesulitan', '$Waktu', '$Beban')";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'INSERT', 'Menambah data aktivitas pegawai', $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function AutoKodeAktivitas($conn){
	$sql = "SELECT RIGHT(KodeAktivitasPegawai,21) AS kode FROM mstaktivitas ORDER BY KodeAktivitasPegawai DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 21, "0", STR_PAD_LEFT);
	return 'AKT-' . $bikin_kode;
}

function GetOneDataAktivitas($conn, $KodeAktivitasPegawai){
	$sql = "SELECT KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban 
	FROM mstaktivitas
	WHERE KodeAktivitasPegawai = '$KodeAktivitasPegawai'";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_assoc($res);
		$row['response'] = 200;
	}else{
		$row['response'] = 500;
	}
	return $row;
}

function UdateDataAktivitas($conn, $KodeAktivitasPegawai, $NamaAktivitas, $Satuan, $TingkatKesulitan, $Waktu, $Beban){
	$sql = "UPDATE mstaktivitas 
	SET NamaAktivitas = '$NamaAktivitas', Satuan = '$Satuan', TingkatKesulitan = '$TingkatKesulitan', Waktu = '$Waktu', Beban = '$Beban' 
	WHERE KodeAktivitasPegawai = '$KodeAktivitasPegawai'";
	$res = $conn->query($sql);
	if($res){		
		session_start();
		InsertLog($conn, 'UPDATE', 'Mengubah data aktivitas pegawai', $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500, 'error' => mysqli_error($conn));
	}
}

function DeleteDataAktivitas($conn, $KodeAktivitasPegawai){
	$sql = "DELETE FROM mstaktivitas WHERE KodeAktivitasPegawai = '$KodeAktivitasPegawai'";
	$res = $conn->query($sql);
	if($res){		
		session_start();
        InsertLog($conn, 'DELETE', 'Menghapus data aktivitas pegawai', $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500, 'error' => mysqli_error($conn));
	}
}