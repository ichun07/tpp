<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Aktivitas Pegawai</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 text-right">
							<button onclick="location.href = '../kab/aktivitas_tambah.php'" class="btn btn-primary">Tambah Data</button>
						</div>
					</div>
				</br>
				<div class="row">
					<div class="col-lg-12">				
						<div id="tabel_akyivitas" class="table-responsive">
							<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
								<thead>
									<tr>
										<th width="30px">No</th>
										<th>Nama Aktivitas</th>
										<th>Tingkat Kesulitan</th>
										<th>Waktu</th>
										<th>Beban</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Nama Aktivitas</th>
										<th>Tingkat Kesulitan</th>
										<th>Waktu</th>
										<th>Beban</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">				
				<h4 class="modal-title">Anda yakin untuk menghapus data ini ?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Popup untuk Edit--> 
<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Edit Data Aktivitas</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label class="form-control-label">Nama Aktivitas</label>
							<input type="text" placeholder="Nama Aktivitas" class="form-control"
							name="txtNamaAktivitas" id="txtNamaAktivitas" autocomplete="off" required>
						</div>
					</div>
				</div>				
				<div class="row">
					<div class="col-lg-6">						
						<div class="form-group">
							<label class="form-control-label">Satuan</label>                     
							<input type="number" placeholder="Satuan" class="form-control"
							name="txtSatuan" id="txtSatuan" autocomplete="off" required>
						</div>
						<div class="form-group">
							<label class="form-control-label">Tingkat Kesulitan</label>
							<input type="number" placeholder="Tingkat Kesulitan" class="form-control" name="txtTingkatKesulitan"
							id="txtTingkatKesulitan" autocomplete="off" step="any" required>
						</div>                         
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label class="form-control-label">Waktu</label>
							<input type="number" placeholder="Waktu" class="form-control" name="txtWaktu"
							id="txtWaktu" autocomplete="off" required>
						</div>    
						<div class="form-group">
							<label class="form-control-label">Beban</label>
							<input type="number" placeholder="Beban" step="any" class="form-control" name="txtBeban"
							id="txtBeban" autocomplete="off" required>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-info" id="btnSimpan">
					Simpan
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">
					Batal
				</button>
			</div>
		</div>
	</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">

	var dataTable1;
	var KodeAktivitas;

	$(document).ready(function() {
		LoadData();  
	});

	function LoadData(){		
		var action = "LoadData";
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-right'
			},
			{
				targets: 5,
				className: 'text-center'
			}],
			"ajax":{
				url :"aktivitas_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action;
				},
				error: function(){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");

				}
			}
		} );						
	}

	$(document).on('click', '#btnDelete', function () {
		KodeAktivitas = $(this).val();
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btHapus', function () {
		var action = "DeleteData";
		$.ajax({
			url: "aktivitas_aksi.php",
			method: "POST",
			data: {KodeAktivitas: KodeAktivitas, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					swal('Sukses', 'Berhasil menghapus data', 'success');
					dataTable1.ajax.reload();
					$('#ModalHapus').modal("toggle");
				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});
	});

	$(document).on('click', '#btnUpdate', function () {
		KodeAktivitas = $(this).val();
		var action = "AmbilData";
		$.ajax({
			url: "aktivitas_aksi.php",
			method: "POST",
			data: {KodeAktivitas: KodeAktivitas, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#txtNamaAktivitas').val(data.NamaAktivitas);
					$('#txtSatuan').val(data.Satuan);
					$('#txtWaktu').val(data.Waktu);
					$('#txtTingkatKesulitan').val(data.TingkatKesulitan);
					$('#txtBeban').val(data.Beban);
					$('#ModalEdit').modal("show");
				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});
	});

	$(document).on('click', '#btnSimpan', function () {
		var action = "UpdateData";
		var NamaAktivitas = $("[name='txtNamaAktivitas']").val();
		var Satuan = $("[name='txtSatuan']").val();
		var TingkatKesulitan = $("[name='txtTingkatKesulitan']").val();
		var Waktu = $("[name='txtWaktu']").val();
		var Beban = $("[name='txtBeban']").val();
		$.ajax({
			url: "aktivitas_aksi.php",
			method: "POST",
			data: {KodeAktivitas: KodeAktivitas, NamaAktivitas: NamaAktivitas, Satuan: Satuan, TingkatKesulitan: TingkatKesulitan, Waktu: Waktu, Beban: Beban, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#txtNamaAktivitas').val("");
					$('#txtSatuan').val("");
					$('#txtWaktu').val("");
					$('#txtTingkatKesulitan').val("");
					$('#txtBeban').val("");					
					dataTable1.ajax.reload();
					swal('Sukses', 'Berhasil mengubah data', 'success');
					$('#ModalEdit').modal("toggle");
				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});
	});
</script>
