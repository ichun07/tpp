<?php
include_once 'header.php';
$IsKabupaten = '';
if (isset($_GET['IsKabupaten'])) {
    $IsKabupaten = $_GET['IsKabupaten'];
}
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Tambah User</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../kab/daftar_user_view.php'"
                    class="btn btn-success">Kembali
                </button>
                <br></br>
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <?php
                        if ($IsKabupaten < 1) {
                            ?>
                            <h3 class="h4">Daftar User OPD</h3>
                            <?php
                        } else {
                            ?>
                            <h3 class="h4">Daftar User Kabupaten</h3>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            <div class="form-group">
                                <label class="form-control-label">Nama User</label>
                                <input type="text" placeholder="Nama User" class="form-control" name="txtNamaUser"
                                required data-msg="Nama User tidak boleh kosong!" autocomplete="off">
                            </div>
                            <div class="form-group" id="select_opd">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Username</label>
                                <input type="text" placeholder="Username" class="form-control" name="txtUsername"
                                required data-msg="Username tidak boleh kosong!" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Password</label>
                                <input type="password" placeholder="Password" class="form-control"
                                name="txtPassword" required data-msg="Password tidak boleh kosong!" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    var IsKabupaten;
    $(document).ready(function () {
        IsKabupaten = "<?php echo $IsKabupaten; ?>";
        if (IsKabupaten < 1) {
            LoadDataOPD();
        }

        $("[name='txtUsername']").val("");
        $("[name='txtPassword']").val("");
    });

    function LoadDataOPD() {
        var action = "LoadDataOPD";
        $.ajax({
            url: "daftar_user_aksi.php",
            method: "POST",
            data: {action: action}, dataType: 'json',
            success: function (data) {
                $('#select_opd').html(data);
            }
        });
    }

    function simpanData() {
        var KodeOPD = 0;
        var action = "InsertData";
        var NamaUser = $("[name='txtNamaUser']").val();
        var Username = $("[name='txtUsername']").val();
        var Password = $("[name='txtPassword']").val();
        if (IsKabupaten < 1) {
            KodeOPD = $("[name='cbKodeOpd']").val();
            console.log("KOde OPD : "+KodeOPD);
            if (KodeOPD == 0) {
                $("#sukses").html("<div  class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Pilih Data OPD!</strong></div>");
            } else {
                $.ajax({
                    url: "daftar_user_aksi.php",
                    method: "POST",
                    data: {
                        namaUser: NamaUser,
                        username: Username,
                        password: Password,
                        kodeOPD: KodeOPD,
                        action: action,
                        IsKabupaten: IsKabupaten
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.response === 200) {
                            $("[name='txtNamaUser']").val("");
                            $("[name='txtUsername']").val("");
                            $("[name='txtPassword']").val("");
                            LoadDataOPD();
                            $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../kab/daftar_user_view.php'>lihat semua data</a>.</div>");
                        } else {
                            alert('request failed');
                        }
                    }
                });
            }
        } else {
            $.ajax({
                url: "daftar_user_aksi.php",
                method: "POST",
                data: {
                    namaUser: NamaUser,
                    username: Username,
                    password: Password,
                    kodeOPD: KodeOPD,
                    action: action,
                    IsKabupaten: IsKabupaten
                },
                dataType: 'json',
                success: function (data) {
                    if (data.response === 200) {
                        $("[name='txtNamaUser']").val("");
                        $("[name='txtUsername']").val("");
                        $("[name='txtPassword']").val("");
                        $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../kab/daftar_user_kab_view.php'>lihat semua data</a>.</div>");
                    } else {
                        alert('request failed');
                    }
                }
            });
        }
    }
</script>