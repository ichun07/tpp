<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Sistem Setting</h2>
		</div>
	</header>
	<section class="forms"> 
		<div id="sukses"></div>
		<div class="container-fluid">
			<div class="row">				
				<div class="col-lg-12">
					<button type="button" onclick="location.href = '../kab/sistem_setting_view.php'" class="btn btn-success">Kembali</button>
					<div class="card">
						<div class="card-close">
							<div class="dropdown">
								<button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
								<div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
							</div>
						</div>
						<div class="card-header d-flex align-items-center">
							<h3 class="h4">Tambah Sistem Setting</h3>
						</div>
						<div class="card-body">
							<form method="post">
								<div class="form-group">
									<label class="form-control-label">Nama Setting</label>
									<input type="text" placeholder="Nama Setting" class="form-control" name="txtSettingName" required data-msg="Nama Setting tidak boleh kosong!">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Nilai Setting</label>
									<input type="text" placeholder="Nilai Setting" class="form-control" name="txtSettingValue" required data-msg="Nilai Setting tidak boleh kosong!">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Tipe Setting</label>
									<input type="text" placeholder="Tipe Setting" class="form-control" name="txtTipeSetting" required data-msg="Tipe Setting tidak boleh kosong!">
								</div>								
								<div class="form-group">       
									<button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
								</div>
							</form>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

	function simpanData() {
		var SettingName = $("[name='txtSettingName']").val();
		var SettingValue = $("[name='txtSettingValue']").val();
		var TipeSetting = $("[name='txtTipeSetting']").val();
		var action = "InsertData";
		$.ajax({
			url: "sistem_setting_aksi.php",
			method: "POST",
			data: {settingName: SettingName, settingValue: SettingValue, tipeSetting: TipeSetting, action: action},
			success: function () {
				$("[name='txtSettingName']").val("");
				$("[name='txtSettingValue']").val("");
				$("[name='txtTipeSetting']").val("");
				$("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../kab/sistem_setting_view.php'>lihat semua data</a>.</div>");
			}
		});
	}	
</script>