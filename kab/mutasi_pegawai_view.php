<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Pegawai</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">
			<div class="card">
				<div class="card-body">
					<div class="row">				
						<div class="col-lg-12">
							<div id="tabel_pegawai" class="table-responsive">
								<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
									<thead>
										<tr>
											<th width="20px">No</th>
											<th>NIP</th>
											<th>Nama Pegawai</th>
											<th>Pangkat</th>
											<th width="15px">Gol.</th>
											<th>Jabatan</th>
											<th>Nama OPD</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>No</th>
											<th>NIP</th>
											<th>Nama Pegawai</th>
											<th>Pangkat</th>
											<th>Gol.</th>
											<th>Jabatan</th>
											<th>Nama OPD</th>
											<th>Aksi</th>
										</tr>
									</tfoot>
									<tbody>
									</tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="text-align:center;">Anda yakin untuk menghapus data ini ?</h4>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">
	$(document).ready(function() {
		LoadData();  
	});

	var KodePegawai,KodeOPD;

	$(document).on('click', '#btnDelete', function () {
		KodePegawai = $(this).val();
		KodeOPD = $(this).attr("data-value");
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btHapus', function () {
		var action = "HapusData";
		$.ajax({
			url: "pegawai_aksi.php",
			method: "POST",
			data: {KodePegawai: KodePegawai, KodeOPD: KodeOPD, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					LoadData();
					$('#ModalHapus').modal("toggle");
				}else{
					alert('request failed');
				}
			}
		});
	});

	$(document).on('click', '#btnMutasi', function () {
		var json = $(this).val();
		window.location = "mutasi_pegawai_detail.php?"+json;
	});

	function LoadData(){
		var action = "LoadData";
		var dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-right'
			},
			{
				targets: 7,
				className: 'text-center'
			}],
			"ajax":{
				url :"mutasi_pegawai_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action
				},
				error: function(){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="8">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");

				}
			}
		} );
	}
</script>
