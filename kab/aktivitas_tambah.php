<?php
include_once 'header.php';
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Tambah Aktivitas Pegawai</h2>
        </div>
    </header>
    <section class="forms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div id="sukses"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../kab/aktivitas_view.php'" class="btn btn-success">
                        Kembali
                    </button>
                </br></br>
                <div class="card">                        
                    <div class="card-body">
                        <form id="form_aktivitas" method="post" action="">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama Aktivitas</label>
                                        <input type="text" placeholder="Nama Aktivitas" class="form-control"
                                        name="txtNamaAktivitas" id="txtNamaAktivitas" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Satuan</label>                     
                                        <input type="number" placeholder="Satuan" class="form-control"
                                        name="txtSatuan" id="txtSatuan" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Tingkat Kesulitan</label>
                                        <input type="number" placeholder="Tingkat Kesulitan" class="form-control" name="txtTingkatKesulitan"
                                        id="txtTingkatKesulitan" autocomplete="off" step="any" required>
                                    </div>                         
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Waktu</label>
                                        <input type="number" placeholder="Waktu" class="form-control" name="txtWaktu"
                                        id="txtWaktu" autocomplete="off" required>
                                    </div>    
                                    <div class="form-group">
                                        <label class="form-control-label">Beban</label>
                                        <input type="number" placeholder="Beban" step="any" class="form-control" name="txtBeban"
                                        id="txtBeban" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" name="submit" value="Simpan">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
    $(document).ready(function () {
    });

    $("#form_aktivitas").submit(function(e) {
        e.preventDefault();
        var NamaAktivitas = $("[name='txtNamaAktivitas']").val();
        var Satuan = $("[name='txtSatuan']").val();
        var TingkatKesulitan = $("[name='txtTingkatKesulitan']").val();
        var Waktu = $("[name='txtWaktu']").val();
        var Beban = $("[name='txtBeban']").val();
        var action = "InsertData";          
        var formData = new FormData();
        formData.append("NamaAktivitas", NamaAktivitas);
        formData.append("Satuan", Satuan);
        formData.append("TingkatKesulitan", TingkatKesulitan);
        formData.append("Waktu", Waktu);
        formData.append("Beban", Beban);
        formData.append("action", action);
        $.ajax({
            url: "aktivitas_aksi.php",
            method: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    $("[name='txtNamaAktivitas']").val(""); 
                    $("[name='txtSatuan']").val(""); 
                    $("[name='txtTingkatKesulitan']").val(""); 
                    $("[name='txtWaktu']").val(""); 
                    $("[name='txtBeban']").val("");
                    swal('Sukses' ,  'Berhasil menambah data aktivitas' ,  'success');
                } else {
                    swal('Error' ,  'Gagal menambah data aktivitas' ,  'error');
                }
            }
        });
    });

</script>