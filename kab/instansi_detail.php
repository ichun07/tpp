<?php
include_once 'header.php';
$KodeOPD = $_GET['KodeOPD'];
$NamaOPD = $_GET['NamaOPD'];
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo $NamaOPD; ?></h2>
		</div>
	</header>
	<section class="forms"> 
		<div class="container-fluid">			
			<div class="row">
				<div class="statistics col-lg-4 col-12" onclick="TampilDetail(1);" style="cursor: pointer;">
					<div class="statistic d-flex align-items-center bg-white has-shadow" id="btnPegawaiMenu">
						<div class="icon bg-red"><i class="fa fa-users" aria-hidden="true"></i></div>
						<div class="text"><strong>Pegawai</strong></div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12" onclick="TampilDetail(2);" style="cursor: pointer;">
					<div class="statistic d-flex align-items-center bg-white has-shadow" id="btnAbsensiMenu">
						<div class="icon bg-orange"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Absensi Bulanan</strong></div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12" onclick="TampilDetail(3);" style="cursor: pointer;">
					<div class="statistic d-flex align-items-center bg-white has-shadow" id="btnProduktifitasMenu">
						<div class="icon bg-green"><i class="fa fa-usd" aria-hidden="true"></i></i></div>
						<div class="text"><strong>Tpp Produktifitas</strong></div>
					</div>
				</div>
			</div>
		</br>
		<div class="row">
			<div class="statistics col-lg-4 col-12" onclick="TampilDetail(4);" style="cursor: pointer;">
				<div class="statistic d-flex align-items-center bg-white has-shadow" id="btnKinerjaMenu">
					<div class="icon bg-red"><i class="fa fa-usd" aria-hidden="true"></i></div>
					<div class="text"><strong>SAKIP OPD</strong></div>
				</div>
			</div>
			<div class="statistics col-lg-4 col-12" onclick="TampilDetail(5);" style="cursor: pointer; display: none;">
				<div class="statistic d-flex align-items-center bg-white has-shadow" id="btnMakanMenu">
					<div class="icon bg-orange"><i class="fa fa-usd" aria-hidden="true"></i></div>
					<div class="text"><strong>Tpp Uang Makan</strong></div>
				</div>
			</div>
			<div class="statistics col-lg-4 col-12" id="btnCetak" style="cursor: pointer;">
				<div class="statistic d-flex align-items-center bg-white has-shadow">
					<div class="icon bg-orange"><i class="fa fa-print" aria-hidden="true"></i></div>
					<div class="text"><strong>Cetak Laporan</strong></div>
				</div>
			</div>			
		</div>
	</br>
	<div class="card">
		<div class="card-header">
			<h4>Detail Data</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12">
					<div id="data_detail" class="table-responsive">							
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	var KodeOPD,Bulan,Tahun,Triwulan;
	var dataTable1;

	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
	});
	var status = 0;

	function TampilDetail(menu){
		document.getElementById("btnPegawaiMenu").className = "statistic d-flex align-items-center bg-white has-shadow";
		document.getElementById("btnAbsensiMenu").className = "statistic d-flex align-items-center bg-white has-shadow";
		document.getElementById("btnProduktifitasMenu").className = "statistic d-flex align-items-center bg-white has-shadow";
		document.getElementById("btnKinerjaMenu").className = "statistic d-flex align-items-center bg-white has-shadow";
		document.getElementById("btnMakanMenu").className = "statistic d-flex align-items-center bg-white has-shadow";
		if(menu == 1){
			document.getElementById("btnPegawaiMenu").className = "statistic d-flex align-items-center bg-blue has-shadow";
			status = 1;
			LoadDataPegawai();
			$('#btnCetak').hide();
		}else if(menu == 2){
			document.getElementById("btnAbsensiMenu").className = "statistic d-flex align-items-center bg-blue has-shadow";
			status = 2;
			LoadData();
			$('#btnCetak').show();
		}else if(menu == 3){
			document.getElementById("btnProduktifitasMenu").className = "statistic d-flex align-items-center bg-blue has-shadow";
			status = 3;
			LoadData();
			$('#btnCetak').show();
		}else if(menu == 4){
			document.getElementById("btnKinerjaMenu").className = "statistic d-flex align-items-center bg-blue has-shadow";
			status = 4;
			// LoadData();						
			$("#data_detail").load("sakip_view.php?KodeOPD="+KodeOPD);
			$('#btnCetak').show();
		}else if(menu == 5){
			document.getElementById("btnMakanMenu").className = "statistic d-flex align-items-center bg-blue has-shadow";
			status = 5;
			LoadData();
			$('#btnCetak').show();
		}
	}

	function LoadDataPegawai(){
		var action = "LoadDataKab";
		$.ajax({
			url: "../opd/pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#data_detail').html(data.Tabel);
					var table = $('#tabeldata').DataTable();					
					table.columns( [8] ).visible( false );
				}else{
					swal('Error','request failed','error');
				}				
			}
		});
	}

	function LoadData(){
		$.ajax({
			url: "instansi_absen.php",
			method: "POST",
			data: {action: status},
			dataType: 'json',
			success: function (data) {
				$('#data_detail').html(data);
				if(status == 4){
					Triwulan = $("[name='cb_triwulan']").val();
					$('#cb_triwulan').change(function () {
						Triwulan = $(this).val();
						console.log("Triwulan : "+Triwulan);
						LoadTabelKinerja();
					});
				}else{
					Bulan = $("[name='cb_bulan']").val();
				}
				Tahun = $("[name='cb_tahun']").val();
				
				if(status == 2){
					LoadTabelAbsen();
				}else if(status == 3){
					LoadTabelTpp();
				}else if(status == 4){
					LoadTabelKinerja();
				}else if(status == 5){
					LoadDataMakan();
				}

				$('#cb_tahun').change(function () {
					Tahun = $(this).val();
					console.log("Tahun : "+Tahun);
					if(status == 2){
						LoadTabelAbsen();
					}else if(status == 3){
						LoadTabelTpp();
					}else if(status == 4){
						LoadTabelKinerja();						
					}else if(status == 5){
						LoadDataMakan();
					}
				});

				$('#cb_bulan').change(function () {
					Bulan = $(this).val();
					console.log("Bulan : "+Bulan);
					if(status == 2){
						LoadTabelAbsen();
					}else if(status == 3){
						LoadTabelTpp();
					}else if(status == 5){
						LoadDataMakan();
					}
				});
			}
		});
	}

	function LoadTabelAbsen(){
		var action = "LoadData";
		$.ajax({
			url: "../opd/absensi_pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					console.log("KodeOPD: "+KodeOPD+" Bulan : "+Bulan+" Tahun : "+Tahun);
					$('#tabel_absensi').html(data.HtmlTabel);
					document.getElementById("txtJmlHariEfektif").innerHTML = data.JmlHariEfektif + ' Hari';
					HKE = data.JmlHariEfektif;
					var table = $('#tabeldata').DataTable();					
					table.columns( [17] ).visible( false );
				}else{
					alert('request failed');
				}				
			}
		});
	}

	function LoadTabelTpp(){
		var action = "TppProduktifitas";
		$.ajax({
			url: "../opd/tpp_bulanan_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					console.log("KodeOPD: "+KodeOPD+" Bulan : "+Bulan+" Tahun : "+Tahun);
					$('#tabel_produktifitas').html(data.DataHtml);
					$('#tabeldata').DataTable();	
				}else{
					alert('request failed');
				}				
			}
		});
	}

	function LoadTabelKinerja(){
		var action = "LoadData";
		$.ajax({
			url: "../opd/capaian_opd_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD,Tahun: Tahun, Triwulan: Triwulan},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#tabel_capaian').html(data.Tabel);
					$('#tabel_pegawai').html(data.Tabel2);
					$('#tabeldata').DataTable();	
				}else{
					alert('request failed');
				}				
			}
		});
	}

	function LoadDataMakan(){
		var action = "TppUangMakan";
		$.ajax({
			url: "../opd/tpp_bulanan_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD, Bulan: Bulan, Tahun: Tahun},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					console.log("KodeOPD: "+KodeOPD+" Bulan : "+Bulan+" Tahun : "+Tahun);
					$('#tabel_makan').html(data.DataHtml);
					$('#tabeldata').DataTable({
						"scrollX": true
					});	
				}else{
					alert('request failed');
				}				
			}
		});
	}

	$(document).on('click', '#btnCetak', function () {
		if(status == 2){
			var Url = "KodeOPD="+KodeOPD+"&Bulan="+Bulan+"&Tahun="+Tahun+"&HKE="+HKE;
			window.open("../opd/absensi_pegawai_report.php?"+Url);
		}else if(status == 3){
			var Url = "KodeOPD="+KodeOPD+"&Bulan="+Bulan+"&Tahun="+Tahun;
			window.open("../opd/tpp_bulanan_report.php?"+Url);
		}else if(status == 4){
			var Url = "KodeOPD="+KodeOPD+"&Triwulan="+Triwulan+"&Tahun="+Tahun;
			window.open("../opd/capaian_opd_report.php?"+Url);
		}else if(status == 5){
			var Url = "KodeOPD="+KodeOPD+"&Bulan="+Bulan+"&Tahun="+Tahun;
			window.open("../opd/tpp_uang_makan_report.php?"+Url);
		}else{
			swal('Perhatian','Anda harus memilih laporan!','warning');
		}
	});

</script>