<?php
include_once 'header.php';
?>
<div class="content-inner">
    <!-- Page Header-->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Tambah Pangkat / Golongan</h2>
        </div>
    </header>
    <section class="forms">
        <div id="sukses"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" onclick="location.href = '../kab/pangkat_view.php'"
                    class="btn btn-success">Kembali
                </button>
                <br></br>
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Detail Pangkat</h3>
                    </div>
                    <div class="card-body">
                       <form id="form_pangkat" method="post" action="">
                        <div class="form-group">
                            <label class="form-control-label">Pangkat</label>
                            <input type="text" placeholder="Pangkat" class="form-control" name="txtPangkat"
                            required data-msg="Pangkat tidak boleh kosong!" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Golongan</label>
                            <input type="text" placeholder="Golongan" class="form-control" name="txtGolongan" id="txtGolongan" required data-msg="Golongan tidak boleh kosong!">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">PPH</label>
                            <input type="number" placeholder="PPH" class="form-control" name="txtPPH" id="txtPPH" required data-msg="PPH tidak boleh kosong!">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">

    $("#form_pangkat").submit(function(e) {
        e.preventDefault();
        var Pangkat = $("[name='txtPangkat']").val();
        var Golongan = $("[name='txtGolongan']").val();
        var PPH = $("[name='txtPPH']").val();
        var action = "InsertData";        
        var formData = new FormData();
        formData.append("Pangkat", Pangkat);
        formData.append("Golongan", Golongan);
        formData.append("PPH", PPH);
        formData.append("action", action);
        $.ajax({
            url: "pangkat_aksi.php",
            method: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            success: function (data) {
                if (data.response == 200) {
                    $("[name='txtPangkat']").val(""); 
                    $("[name='txtGolongan']").val("");
                    $("[name='txtPPH']").val("0");
                    
                    $("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../kab/pangkat_view.php'>lihat semua data</a>.</div>");
                    swal('Sukses' ,  'Berhasil menambah data pangkat' ,  'success');
                } else if(data.response == 404){
                   swal('Peringatan' ,  'Pangkat dan golongan tersebut sudah ada.' ,  'warning');
               }else{
                swal('Error' ,  'Gagal menambah pangkat dan golongan.' ,  'error');
            }
        }
    });
    });

</script>