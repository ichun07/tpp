<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Data Pangkat</h2>
		</div>
	</header>
	<section class="forms">
		<div class="container-fluid">			
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="col-md-12 text-right">
								<button onclick="location.href = '../kab/pangkat_tambah.php'" class="btn btn-primary">Tambah Data</button>
							</div>
						</div>
					</div>
					<br>
					<div class="row">				
						<div class="col-lg-12">
							<div id="tabel_pegawai" class="table-responsive">
								<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
									<thead>
										<tr>
											<th width="30px">No</th>
											<th>Pangkat</th>
											<th width="100px">Golongan</th>
											<th width="100px">PPH 21</th>
											<th width="70px">Aksi</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>No</th>
											<th>Pangkat</th>
											<th>Golongan</th>
											<th>PPH 21</th>
											<th>Aksi</th>
										</tr>
									</tfoot>
									<tbody>
									</tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<h4 class="modal-title" style="text-align:center;">Anda yakin untuk menghapus data ini ?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btnDelete">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<?php
include_once 'footer.php';
?>

<script type="text/javascript">
	$(document).ready(function() {
		LoadData();  
	});

	var Pangkat,Golongan;
	var dataTable1;

	$(document).on('click', '#btnHapus', function () {
		Pangkat = $(this).val();
		Golongan = $(this).attr("data-value");
		console.log("Pangkat : " + Pangkat);
		console.log("Golongan : " + Golongan);
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btnDelete', function () {
		var action = "HapusData";
		$.ajax({
			url: "pangkat_aksi.php",
			method: "POST",
			data: {Pangkat: Pangkat, Golongan: Golongan, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					$('#ModalHapus').modal("toggle");
					swal('Sukses' ,  'Berhasil menghapus data pangkat' ,  'success');
				}else{
					swal('Error' ,  'Gagal menghapus data pangkat' ,  'error');
				}
				dataTable1.ajax.reload(null,false);
			}
		});
	});

	function LoadData(){
		var action = "LoadData";
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 2,
				className: 'text-right'
			},
			{
				targets: 3,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-center'
			}],
			"ajax":{
				url :"pangkat_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action
				},
				error: function(xhr, ajaxOptions, thrownError){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");
					console.log("Error : "+ thrownError);

				}
			}
		} );
	}
</script>
