<?php
include_once 'header.php';
$KodePegawai = '';
$KodeOPD = '';
if(isset($_GET['KodePegawai'])){
	$KodePegawai = $_GET['KodePegawai'];
	$KodeOPD = $_GET['KodeOPD'];
}
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Detail Pegawai</h2>
		</div>
	</header>
	<section class="forms"> 
		<div id="sukses"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="container-fluid">
					<div class="col-md-6 text-left">
					</br>
					<button type="button" onclick="location.href = '../kab/mutasi_pegawai_view.php'" class="btn btn-success">Kembali</button>
				</div>
				<br>					
				<div class="col-lg-12">
					<div class="card">				
						<div class="card-header d-flex align-items-center">
							<h3 class="h4">Detail Pegawai</h3>
						</div>
						<div class="card-body">
							<form method="post">
								<div class="form-group" id="select_opd">
								</div>
								<div class="form-group" id="select_kode_jabatan">
								</div>
								<div class="form-group">
									<label class="form-control-label">Nama Pegawai</label>
									<input type="text" placeholder="Nama Pegawai" class="form-control" name="txtNamaPegawai" id="txtNamaPegawai">
								</div>
								<div class="form-group">       
									<label class="form-control-label">NIP</label>
									<input type="text" placeholder="NIP" class="form-control" name="txtNip"
									id="txtNip">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Pangkat</label>
									<input type="text" placeholder="Pangkat" class="form-control" name="txtPangkat"
									id="txtPangkat">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Golongan</label>
									<input type="number" placeholder="Golongan" class="form-control" name="txtGolongan"
									id="txtGolongan">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Alamat</label>
									<input type="text" placeholder="Alamat" class="form-control" name="txtAlamat"
									id="txtAlamat">
								</div>
								<div class="form-group" style="display: none;">       
									<label class="form-control-label">Pokok TPP</label>
									<input type="number" placeholder="Pokok TPP" class="form-control" name="txtPokokTPP"
									id="txtPokokTPP">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Username</label>
									<input type="text" placeholder="Username" class="form-control" name="txtUsername"
									id="txtUsername">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Password</label>
									<input type="password" placeholder="Password" class="form-control" name="txtPassword"
									id="txtPassword">
								</div>
								<div class="form-group">       
									<button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
								</div>
							</form>							
						</div>					
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	var KodePegawai = '';
	var KodeOPD = '';
	var Status = '';
	var HargaJabatan,NilaiJabatan,PokokTPP = 0;
	var IntStatus = 1;
	$(document).ready(function() {
		LoadDataPegawai();
	});

	function LoadDataPegawai(){
		KodePegawai = "<?php echo $KodePegawai; ?>";
		KodeOPD = "<?php echo $KodeOPD; ?>";
		var action = "AmbilData";
		$.ajax({
			url: "mutasi_pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodePegawai: KodePegawai, KodeOPD: KodeOPD},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					var row = data.Data
					console.log("NamaPegawai " + row.NamaPegawai);
					$('#txtNamaPegawai').val(row.NamaPegawai);
					$('#txtNip').val(row.NIP);
					$('#txtPangkat').val(row.Pangkat);
					$('#txtGolongan').val(row.Golongan);
					$('#txtAlamat').val(row.Alamat);
					$('#txtPokokTPP').val(row.PokokTPP);
					$('#txtUsername').val(row.Username);
					$('#txtPassword').val(row.Password);
					Status = row.Status;

					$('#select_opd').html(row.HtmlOPD);
					$('#select_kode_jabatan').html(row.HtmlJabatan);
					document.getElementById('txtNamaPegawai').disabled = true;
					document.getElementById('txtNip').disabled = true;
					document.getElementById('txtPangkat').disabled = true;
					document.getElementById('txtGolongan').disabled = true;
					document.getElementById('txtAlamat').disabled = true;
					document.getElementById('txtPokokTPP').disabled = true;
					document.getElementById('txtUsername').disabled = true;
					document.getElementById('txtPassword').disabled = true;

					$('#cbKodeOpd').change(function () {
						var selectedItem = $(this).val();
						console.log("KodeOPD : " + selectedItem);
						LoadDataJabatan(selectedItem);
					});
				}else{
					alert('Load data opd failed');
				}
			}
		});
	}

	function LoadDataJabatan(KodeOPD){
		var action = "GetJabatan";
		$.ajax({
			url: "mutasi_pegawai_aksi.php",
			method: "POST",
			data: {action: action, KodeOPD: KodeOPD},
			dataType: 'json',
			success: function (data) {
				if(data.response = 200){
					$('#select_kode_jabatan').html(data.cbJabatan);

					$('#cbJabatan').change(function () {
						NilaiJabatan = $(this).find('option:selected').attr('data-value');
						HargaJabatan = $(this).find('option:selected').attr('data-value2');
					});
				}else{
					alert('Load Data Jabatan Failed');
				}				
			}
		});
	}

	function simpanData() {
		if(Status != "PNS"){
			IntStatus = 0.8;
		}else{
			IntStatus = 1;
		}
		PokokTPP = NilaiJabatan * HargaJabatan * IntStatus;
		var NIP = $("[name='txtNip']").val(); 
		var NamaPegawai = $("[name='txtNamaPegawai']").val(); 
		var Pangkat = $("[name='txtPangkat']").val(); 
		var Golongan = $("[name='txtGolongan']").val(); 
		var Alamat = $("[name='txtAlamat']").val();
		var KodeJabatan = $("[name='cbJabatan']").val(); 
		var KodeOPD = $("[name='cbKodeOpd']").val(); 
		var Username = $("[name='txtUsername']").val(); 
		var Password = $("[name='txtPassword']").val();  
		var action = "UpdateData";
		if(KodeJabatan == 0){
			alert('Data Jabatan Belum Diisi!');
		}else{
			$.ajax({
				url: "mutasi_pegawai_aksi.php",
				method: "POST",
				data: {KodePegawai: KodePegawai,NIP: NIP, NamaPegawai: NamaPegawai, Pangkat: Pangkat, Golongan: Golongan, Alamat: Alamat, PokokTPP: PokokTPP, KodeJabatan: KodeJabatan, KodeOPD: KodeOPD, Username: Username, Password: Password, action: action},
				dataType: 'json',
				success: function (data) {
					if(data.response == 200){
						$("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Ubah Data!</strong> <a href='../kab/mutasi_pegawai_view.php'>lihat semua data</a>.</div>");
						document.getElementById('cbKodeOpd').disabled = true;
						document.getElementById('cbJabatan').disabled = true;
					}else if(data.response == 403){
						swal('Peringatan','Jabatan tersebut telah ditempati.', 'warning');
					}else{
						swal('Error','Terjadi kesalahan.','error');
					}				
				}
			});
		}		
	}
</script>