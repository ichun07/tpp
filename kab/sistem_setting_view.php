<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Sistem Setting</h2>
		</div>
	</header>
	<section class="forms"> 
		<div class="container-fluid">			
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 text-right">
						</br>
						<button style="display: none;" onclick="location.href = '../kab/sistem_setting_tambah.php'" class="btn btn-primary">Tambah Data</button>
					</div>
					<div class="col-lg-12">
						<div id="tabel_setting" class="table-responsive">
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<!-- Modal Popup untuk delete--> 
<div class="modal fade" id="ModalHapus">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top:100px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="text-align:center;">Anda yakin untuk menghapus data ini ?</h4>
			</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Popup untuk Edit--> 
<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Edit Data Setting</h4>
			</div>
			<div class="modal-body">
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="txtSettingName">Nama Setting</label>
					<input disabled type="text" name="txtSettingName" id="txtSettingName" class="form-control" required data-msg="Nama Setting tidak boleh kosong!"/>
				</div>
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="txtSettingValue">Nilai Setting</label>
					<input type="text" name="txtSettingValue" id="txtSettingValue" class="form-control" required data-msg="Nilai Setting tidak boleh kosong!"/>
				</div>
				<div class="form-group" style="padding-bottom: 20px;">
					<label for="txtTipeSetting">Tipe Setting</label>
					<input disabled type="text" name="txtTipeSetting" id="txtTipeSetting" class="form-control" required data-msg="Tipe Setting tidak boleh kosong!"/>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info" id="btSimpan">
						Simpan
					</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">
						Batal
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once 'footer.php';
?>

<script type="text/javascript">
	$(document).ready(function() {
		LoadData();  
	});

	var SettingNameDelete,SettingNameUpdate;

	function LoadData(){
		var action = "LoadData";
		$.ajax({
			url: "sistem_setting_aksi.php",
			method: "POST",
			data: {action: action},
			success: function (data) {
				$('#tabel_setting').html(data);
				$('#tabeldata').DataTable();
			}
		});
	}

	$(document).on('click', '.btn-danger', function () {
		var id = $(this).attr("id");
		SettingNameDelete = id;
		$('#ModalHapus').modal('show');
	});

	$(document).on('click', '#btHapus', function () {
		var action = "HapusData";
		$.ajax({
			url: "sistem_setting_aksi.php",
			method: "POST",
			data: {id: SettingNameDelete, action: action},
			success: function () {
				LoadData();
				$('#ModalHapus').modal("toggle");
			}
		});
	});

	$(document).on('click', '.btn-warning', function () {
		var id = $(this).attr("id");
		SettingNameUpdate = id;
		var action = "AmbilData";
		$.ajax({
			url: "sistem_setting_aksi.php",
			method: "POST",
			data: {id: id, action: action},
			dataType: 'json',
			success: function (data) {
				$('#txtSettingName').val(data.SettingName);
				$('#txtSettingValue').val(data.SettingValue);
				$('#txtTipeSetting').val(data.TipeSetting);
				$('#ModalEdit').modal("show");
			}
		});
	});

	$(document).on('click', '#btSimpan', function () {
		var SettingValue = $("[name='txtSettingValue']").val();
		var TipeSetting = $("[name='txtTipeSetting']").val();
		var action = "UpdateData";
		$.ajax({
			url: "sistem_setting_aksi.php",
			method: 'POST',
			data: {id: SettingNameUpdate, settingValue: SettingValue, tipeSetting: TipeSetting, action: action},
			success: function () {
				$("[name='txtSettingValue']").val("");
				$("[name='txtTipeSetting']").val("");
				LoadData();
				$('#ModalEdit').modal("toggle");
			}
		});
	});
</script>