<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Home</h2>
		</div>
	</header>
	<section class="dashboard-header">
		<div class="container-fluid">
			<div class="row">
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_user_kab">
						<div class="icon bg-red"><i class="icon icon-user"></i></div>
						<div class="text"><strong>User</strong><br>Kabupaten</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_user_opd">
						<div class="icon bg-red"><i class="icon icon-user"></i></div>
						<div class="text"><strong>User</strong><br>OPD</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_pangkat">
						<div class="icon bg-yellow"><i class="fa fa-address-card-o"></i></div>
						<div class="text"><strong>Pangkat</strong><br>Golongan</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_opd">
						<div class="icon bg-green"><i class="fa fa-building-o" aria-hidden="true"></i></div>
						<div class="text"><strong>Intansi</strong><br>OPD</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_mutasi">
						<div class="icon bg-orange"><i class="fa fa-black-tie" aria-hidden="true"></i></div>
						<div class="text"><strong>Mutasi</strong><br>Pegawai</div>
					</div>
				</div>
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_aktivitas">
						<div class="icon bg-blue"><i class="fa fa-users" aria-hidden="true"></i></div>
						<div class="text"><strong>Aktivitas</strong><br>Pegawai</div>
					</div>
				</div>							
			</div>
			<br>
			<div class="row">
				<div class="statistics col-lg-4 col-12">
					<div class="statistic d-flex align-items-center bg-white has-shadow" style="min-height: 120px;cursor: pointer;" id="btn_setting">
						<div class="icon bg-blue"><i class="fa fa-cogs" aria-hidden="true"></i></div>
						<div class="text"><strong>Sistem</strong><br>Setting</div>
					</div>
				</div>							
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	$(document).on('click', '#btn_setting', function () {
		location.href='sistem_setting_view.php';
	});

	$(document).on('click', '#btn_user_kab', function () {
		location.href='daftar_user_kab_view.php';
	});

	$(document).on('click', '#btn_user_opd', function () {
		location.href='daftar_user_view.php';
	});

	$(document).on('click', '#btn_opd', function () {
		location.href='instansi_view.php';
	});

	$(document).on('click', '#btn_mutasi', function () {
		location.href='mutasi_pegawai_view.php';
	});

	$(document).on('click', '#btn_pangkat', function () {
		location.href='pangkat_view.php';
	});

	$(document).on('click', '#btn_aktivitas', function () {
		location.href='aktivitas_view.php';
	});
</script>