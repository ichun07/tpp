<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {
		echo GetDataSetting($conn);
	}
	if ($_POST["action"] == "InsertData") {
		$SettingName = $_POST["settingName"];
		$SettingValue = $_POST["settingValue"];
		$TipeSetting = $_POST["tipeSetting"];
		InsertDataSetting($conn,$SettingName, $SettingValue, $TipeSetting);
	}
	if ($_POST["action"] == "AmbilData") {
		$SettingName = $_POST["id"];
		$output = GetOneDataSetting($conn, $SettingName);
		echo json_encode($output);
	}
	if ($_POST["action"] == "UpdateData") {
		$SettingName = $_POST["id"];
		$SettingValue = $_POST["settingValue"];
		$TipeSetting = $_POST["tipeSetting"];
		UpdateDataSetting($conn,$SettingName, $SettingValue, $TipeSetting);
	}
	if ($_POST["action"] == "HapusData") {
		$SettingName = $_POST["id"];
		DeleteDataSetting($conn,$SettingName);
	}
}


function GetDataSetting($conn){
	$output = '';
	$sql = "SELECT SettingName, SettingValue, TipeSetting FROM sistemsetting ORDER BY SettingName ASC";
	$res = $conn->query($sql);
	$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
	<thead>
	<tr>
	<th width="30px">No</th>
	<th>Nama Setting</th>
	<th>Nilai Setting</th>
	<th>Tipe Setting</th>
	<th>Aksi</th>
	</tr>
	</thead>
	<tfoot>
	<tr>
	<th>No</th>
	<th>Nama Setting</th>
	<th>Nilai Setting</th>
	<th>Tipe Setting</th>
	<th>Aksi</th>
	</tr>
	</tfoot>
	<tbody>';
	if($res){
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$output.= '<tr>
			<td>'.$no++.'</td>
			<td>'.$row['SettingName'].'</td>
			<td>'.$row['SettingValue'].'</td>
			<td>'.$row['TipeSetting'].'</td>
			<td class="text-center">
			<a href="#" name="update" id="' . $row['SettingName'] . '" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></a>
			<a style="display: none;" href="#" name="delete" id="' . $row['SettingName'] . '" class="btn btn-danger"><span class="icon icon-pencil-case" aria-hidden="true"></span></a>
			</td>
			</tr>';
		}
	}
	$output .='</tbody>
	</table>';
	return $output;
}

function GetOneDataSetting($conn, $SettingName){
	$output = '';
	$sql = "SELECT SettingName, SettingValue, TipeSetting FROM sistemsetting WHERE SettingName = '$SettingName' GROUP BY SettingName";
	$res = $conn->query($sql);
	while ($row = $res->fetch_assoc()) {
		$output['SettingName'] = $row['SettingName'];
		$output['SettingValue'] = $row['SettingValue'];
		$output['TipeSetting'] = $row['TipeSetting'];
	}
	return $output;
}

function InsertDataSetting($conn,$SettingName, $SettingValue, $TipeSetting){
	$sql = "INSERT INTO sistemsetting (SettingName, SettingValue, TipeSetting) VALUES ('$SettingName', '$SettingValue', '$TipeSetting')";
	$res = $conn->query($sql);
	return $res;
}

function UpdateDataSetting($conn,$SettingName, $SettingValue, $TipeSetting){
	$sql = "UPDATE sistemsetting SET SettingValue = '$SettingValue', TipeSetting = '$TipeSetting' WHERE SettingName = '$SettingName'";
	session_start();
	InsertLog($conn, 'UPDATE', 'Mengubah sistem setting '.$SettingName, $_SESSION['KodeUserKab']);
	$res = $conn->query($sql);
	return $res;
}

function DeleteDataSetting($conn,$SettingName){
	$sql = "DELETE FROM sistemsetting WHERE SettingName = '$SettingName'";
	$res = $conn->query($sql);
	return $res;
}
?>