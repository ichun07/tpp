<?php
include_once 'header.php';
?>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Tambah Instansi</h2>
		</div>
	</header>
	<section class="forms"> 
		<div id="sukses"></div>
		<div class="container-fluid">
			<div class="row">				
				<div class="col-lg-12">
					<button type="button" onclick="location.href = '../kab/instansi_view.php'" class="btn btn-success">Kembali</button>
					<br></br>
					<div class="card">
						<div class="card-close">
							<div class="dropdown">
								<button type="button" id="closeCard5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
								<div aria-labelledby="closeCard5" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
							</div>
						</div>
						<div class="card-header d-flex align-items-center">
							<h3 class="h4">Add Instansi</h3>
						</div>
						<div class="card-body">
							<form method="post">
								<div class="form-group">
									<label class="form-control-label">Nama OPD</label>
									<input type="text" placeholder="Nama OPD" class="form-control" name="txtNamaOPD">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Alamat OPD</label>
									<input type="text" placeholder="Alamat OPD" class="form-control" name="txtAlamatOPD">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Kode Surat OPD</label>
									<input type="text" placeholder="Kode Surat OPD" class="form-control" name="txtKodeSuratOPD">
								</div>
								<div class="form-group">       
									<label class="form-control-label">Keterangan</label>
									<input type="text" placeholder="Keterangan" class="form-control" name="txtKeterangan">
								</div>
								<div class="form-group">       
									<button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
								</div>
							</form>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
include_once 'footer.php';
?>
<script type="text/javascript">
	function simpanData() {
		var NamaOPD = $("[name='txtNamaOPD']").val();
		var AlamatOPD = $("[name='txtAlamatOPD']").val();
		var KodeSuratOPD = $("[name='txtKodeSuratOPD']").val();
		var Keterangan = $("[name='txtKeterangan']").val();
		var action = "InsertData";
		$.ajax({
			url: "instansi_aksi.php",
			method: "POST",
			data: {namaOPD: NamaOPD, alamatOPD: AlamatOPD, kodeSuratOPD: KodeSuratOPD, keteranganOPD: Keterangan, action: action},
			success: function () {
				$("[name='txtNamaOPD']").val("");
				$("[name='txtAlamatOPD']").val("");
				$("[name='txtKodeSuratOPD']").val("");
				$("[name='txtKeterangan']").val("");
				$("#sukses").html("<div  class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Berhasil Tambah Data!</strong> Tambah lagi atau <a href='../kab/instansi_view.php'>lihat semua data</a>.</div>");
			}
		});
	}	
</script>