<?php
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {

	if($_POST["action"] == "LoadDataSakip"){
		$requestData = $_REQUEST;
		$KodeOPD = $_POST['KodeOPD'];
		$result = DataSakip($conn, $requestData, $KodeOPD);
		echo json_encode($result);
	}

	if($_POST['action'] == 'AmbilData'){
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$Triwulan = $_POST['Triwulan'];
		$result = GetOneDataSakip($conn, $KodeOPD, $Tahun, $Triwulan);
		echo json_encode($result);
	}

	if($_POST['action'] == 'DeleteData'){
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$Triwulan = $_POST['Triwulan'];
		$result = DeleteDataSakip($conn, $KodeOPD, $Tahun, $Triwulan);
		echo json_encode($result);	
	}

	if($_POST['action'] == 'UpdateData'){
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$Triwulan = $_POST['Triwulan'];
		$EvalusasiSAKIP = $_POST['EvaluasiSAKIP'];
		$NilaiKategoriSAKIP = $_POST['NilaiKategoriSAKIP'];
		$result = UpdateDataSakip($conn, $Tahun, $Triwulan, $KodeOPD, $EvalusasiSAKIP, $NilaiKategoriSAKIP);
		echo json_encode($result);
	}

	if($_POST['action'] == 'InsertData'){		
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$Triwulan = $_POST['Triwulan'];
		$EvalusasiSAKIP = $_POST['EvaluasiSAKIP'];
		$NilaiKategoriSAKIP = $_POST['NilaiKategoriSAKIP'];
		$result = InsertDataSakip($conn, $Tahun, $Triwulan, $KodeOPD, $EvalusasiSAKIP, $NilaiKategoriSAKIP);
		echo json_encode($result);
	}
}

function DataSakip($conn, $requestData, $KodeOPD){
	$columns = array( 
		0 => 'Tahun',
		1 => 'Triwulan', 
		2 => 'EvalusasiSAKIP',
		3 => 'NilaiKategoriSAKIP'
	);

	$sql = "SELECT Tahun, Triwulan, KodeOPD, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot ";
	$sql.="FROM capaiankinerjaopd WHERE KodeOPD = '$KodeOPD'";
	$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
	$totalFiltered = $totalData;


	if( !empty($requestData['search']['value']) ) {
		$sql = "SELECT Tahun, Triwulan, KodeOPD, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot ";
		$sql.="FROM capaiankinerjaopd ";
		$sql.="WHERE KodeOPD = '$KodeOPD' AND EvalusasiSAKIP LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR KodeOPD = '$KodeOPD' AND NilaiKategoriSAKIP LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR KodeOPD = '$KodeOPD' AND Triwulan LIKE '%".$requestData['search']['value']."%' ";
		$sql.=" OR KodeOPD = '$KodeOPD' AND Tahun LIKE '%".$requestData['search']['value']."%' ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
		$totalFiltered = mysqli_num_rows($query);
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");	
	} else {
		$sql = "SELECT Tahun, Triwulan, KodeOPD, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot ";
		$sql.="FROM capaiankinerjaopd WHERE KodeOPD = '$KodeOPD'";
		$sql.="ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
		$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");

	}

	$data = array();
	while( $row=mysqli_fetch_array($query) ) {
		$nestedData=array();
		$Aksi = '<button value = "'.$row['Triwulan'].'" data-value="' . $row['Tahun'] . '" name="btnUpdate" id="btnUpdate" class="btn btn-warning"><span class="fa fa-pencil" aria-hidden="true"></span></button>
		<button value="'.$row['Triwulan'].'" data-value="' . $row['Tahun'] . '" name="delete" id="btnDelete" class="btn btn-danger"><span class="fa fa-trash" aria-hidden="true"></span></button>';
		$nestedData[] = $row['Tahun'];
		$nestedData[] = $row['Triwulan'];
		$nestedData[] = $row['EvalusasiSAKIP'];
		$nestedData[] = $row['NilaiKategoriSAKIP'];
		$nestedData[] = $Aksi;		

		$data[] = $nestedData;

	}



	$json_data = array(
		"draw"            => intval( $requestData['draw'] ),
		"recordsTotal"    => intval( $totalData ),
		"recordsFiltered" => intval( $totalFiltered ),
		"data"            => $data   		);

	return $json_data; 
}

function GetOneDataSakip($conn, $KodeOPD, $Tahun, $Triwulan){
	$sql = "SELECT Tahun, Triwulan, KodeOPD, EvalusasiSAKIP, NilaiKategoriSAKIP, SAKIPBobot FROM capaiankinerjaopd WHERE KodeOPD = '$KodeOPD' AND Tahun = '$Tahun' AND Triwulan = '$Triwulan'";
	$res = $conn->query($sql);
	if($res){
		$row = mysqli_fetch_assoc($res);
		$row['response'] = 200;
	}else{
		$row['response'] = 500;
	}
	return $row;
}

function UpdateDataSakip($conn, $Tahun, $Triwulan, $KodeOPD, $EvalusasiSAKIP, $NilaiKategoriSAKIP){
	$sql = "UPDATE capaiankinerjaopd SET EvalusasiSAKIP = '$EvalusasiSAKIP', NilaiKategoriSAKIP = '$NilaiKategoriSAKIP' WHERE Tahun = '$Tahun' AND Triwulan = '$Triwulan' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'UPDATE', 'Mengubah data sakip '.$KodeOPD.' Tahun '.$Tahun.' Triwulan '.$Triwulan, $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function InsertDataSakip($conn, $Tahun, $Triwulan, $KodeOPD, $EvalusasiSAKIP, $NilaiKategoriSAKIP){
	$sql = "INSERT INTO capaiankinerjaopd (Tahun, Triwulan, KodeOPD, EvalusasiSAKIP, NilaiKategoriSAKIP) VALUES ('$Tahun', '$Triwulan', '$KodeOPD', '$EvalusasiSAKIP', '$NilaiKategoriSAKIP')";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'INSERT', 'Menambah data sakip '.$KodeOPD.' Tahun '.$Tahun.' Triwulan '.$Triwulan, $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function DeleteDataSakip($conn, $KodeOPD, $Tahun, $Triwulan){
	$sql = "DELETE FROM capaiankinerjaopd WHERE KodeOPD = '$KodeOPD' AND Tahun = '$Tahun' AND Triwulan = '$Triwulan'";
	$res = $conn->query($sql);
	if($res){
		session_start();
		InsertLog($conn, 'DELETE', 'Menghapus data sakip '.$KodeOPD.' Tahun '.$Tahun.' Triwulan '.$Triwulan, $_SESSION['KodeUserKab']);
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}	
}