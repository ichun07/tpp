<?php 
include_once '../config/koneksi.php';

if (isset($_POST["action"])) {
	if ($_POST["action"] == "LoadData") {		
		$requestData = $_REQUEST;
		$result = GetDataPegawai($conn,$requestData);
		echo json_encode($result);
	}	
	if ($_POST["action"] == "InsertData") {
		$NIP = $_POST['NIP'];
		$NamaPegawai = $_POST['NamaPegawai'];
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$Alamat = $_POST['Alamat'];
		$PokokTPP = $_POST['PokokTPP'];
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$Username = $_POST['Username'];
		$Password = base64_encode($_POST['Password']);
		$result = InsertDataPegawai($conn, $NIP, $NamaPegawai, $Pangkat, $Golongan, $Alamat, $PokokTPP, $KodeJabatan, $KodeOPD, $Username, $Password);
		echo json_encode($result);
	}
	if ($_POST["action"] == "AmbilData") {
		$KodePegawai = $_POST["KodePegawai"];
		$KodeOPD = $_POST["KodeOPD"];
		$result = GetOneDataPegawai($conn, $KodePegawai, $KodeOPD);
		echo json_encode($result);
	}
	if ($_POST["action"] == "UpdateData") {
		$KodePegawai = $_POST['KodePegawai'];
		$NIP = $_POST['NIP'];
		$NamaPegawai = $_POST['NamaPegawai'];
		$Pangkat = $_POST['Pangkat'];
		$Golongan = $_POST['Golongan'];
		$Alamat = $_POST['Alamat'];
		$PokokTPP = $_POST['PokokTPP'];
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$Username = $_POST['Username'];
		$Password = base64_encode($_POST['Password']);
		$result = UpdateDataPegawai($conn, $KodePegawai, $NIP, $NamaPegawai, $Pangkat, $Golongan, $Alamat, $PokokTPP, $KodeJabatan, $KodeOPD, $Username, $Password);
		echo json_encode($result);
	}
	if ($_POST["action"] == "HapusData") {
		$KodePegawai = $_POST["KodePegawai"];
		$KodeOPD = $_POST["KodeOPD"];
		$result = DeleteDataPegawai($conn, $KodePegawai, $KodeOPD);
		echo json_encode($result);
	}

	if ($_POST["action"] == "GetJabatan") {
		$KodeOPD = $_POST['KodeOPD'];
		$result = GetDataJabatan($conn, $KodeOPD);
		echo json_encode($result);
	}
}

function InsertDataPegawai($conn, $NIP, $NamaPegawai, $Pangkat, $Golongan, $Alamat, $PokokTPP, $KodeJabatan, $KodeOPD, $Username, $Password){
	$KodePegawai = AutoKodePegawai($conn,$KodeOPD);
	$sql = "INSERT INTO mstpegawai (KodePegawai, NIP, NamaPegawai, Pangkat, Golongan, Alamat, PokokTPP, KodeJabatan, KodeOPD, Username, Password) VALUES ('$KodePegawai', '$NIP', '$NamaPegawai', '$Pangkat', '$Golongan', '$Alamat', '$PokokTPP', '$KodeJabatan', '$KodeOPD', '$Username', '$Password')";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function UpdateDataPegawai($conn, $KodePegawai, $NIP, $NamaPegawai, $Pangkat, $Golongan, $Alamat, $PokokTPP, $KodeJabatan, $KodeOPD, $Username, $Password){
	$sql = "SELECT COUNT(KodePegawai) AS Jumlah FROM mstpegawai WHERE KodeJabatan = '$KodeJabatan' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$result = mysqli_fetch_array($res);
		if ($result['Jumlah'] > 0) {
			return array('response' => 403, 'error' => 'Jabatan sudah terisi!');
		} else {
			$sql = "UPDATE mstpegawai SET KodeOPD = '$KodeOPD', NIP = '$NIP', NamaPegawai = '$NamaPegawai', Pangkat = '$Pangkat', Golongan = '$Golongan', Alamat = '$Alamat', PokokTPP = '$PokokTPP', KodeJabatan = '$KodeJabatan', Username = '$Username', Password = '$Password' WHERE KodePegawai = '$KodePegawai'";
			$res = $conn->query($sql);
			if($res){
				session_start();
				InsertLog($conn, 'UPDATE', 'Melakukan mutasi pegawai '.$KodePegawai, $_SESSION['KodeUserKab']);
				return array('response' => 200 );
			}else{
				return array('response' => 500);
			}
		}
	}
}

function GetDataPegawai($conn,$requestData){
	$columns = array( 
		0 => 'KodePegawai',
		1 => 'NIP', 
		2 => 'NamaPegawai',
		3 => 'Pangkat',
		4 => 'Golongan',
		5 => 'NamaJabatan',
		6 => 'NamaOPD',
	);

	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password ";
	$sql.="FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
	$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password ";
	$sql.=" FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD ";
	$sql.="WHERE p.NamaPegawai LIKE '%".$requestData['search']['value']."%' ";
	$sql.=" OR p.NIP LIKE '%".$requestData['search']['value']."%' ";
	$sql.=" OR j.NamaJabatan LIKE '%".$requestData['search']['value']."%' ";
	$sql.=" OR o.NamaOPD LIKE '%".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); // again run query with limit
	
} else {	

	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password ";
	$sql.=" FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
$no = 1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array();
	$url = "KodePegawai=".$row['KodePegawai']."&KodeOPD=".$row['KodeOPD'];
	
	$nestedData[] = $no++;
	$nestedData[] = $row['NIP'];
	$nestedData[] = $row['NamaPegawai'];
	$nestedData[] = $row['Pangkat'];
	$nestedData[] = $row['Golongan'];
	$nestedData[] = $row['NamaJabatan'];
	$nestedData[] = $row['NamaOPD'];
	$nestedData[] = '<td class="text-center">
	<button value = '.$url.' name="btnMutasi" id="btnMutasi" class="btn btn-success"><span class="fa fa-arrow-right" aria-hidden="true"></span></button>
	</td>';
	
	$data[] = $nestedData;

}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
		);

return $json_data;  // send data as json format
}

function GetDataPegawai1($conn){
	$arrayOutput = '';
	$output = '';
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password 
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
	ORDER BY p.KodeOPD , p.KodeJabatan ASC";
	$res = $conn->query($sql);
	if($res){
		$arrayOutput['response'] = 200;
		$output = '<table width="100%" class="table table-striped table-bordered dataTable" id="tabeldata">
		<thead>
		<tr>
		<th width="30px">No</th>
		<th>NIP</th>
		<th>Nama Pegawai</th>
		<th>Pangkat</th>
		<th>Golongan</th>
		<th>Pokok TPP</th>
		<th>Jabatan</th>
		<th>Nama OPD</th>
		<th>Aksi</th>
		</tr>
		</thead>
		<tfoot>
		<tr>
		<th>No</th>
		<th>NIP</th>
		<th>Nama Pegawai</th>
		<th>Pangkat</th>
		<th>Golongan</th>
		<th>Pokok TPP</th>
		<th>Jabatan</th>
		<th>Nama OPD</th>
		<th>Aksi</th>
		</tr>
		</tfoot>
		<tbody>';
		$no = 1;
		while ($row = $res->fetch_assoc()) {
			$json = json_encode($row);
			$url = "KodePegawai=".$row['KodePegawai']."&KodeOPD=".$row['KodeOPD'];
			$output.= '<tr>
			<td>'.$no++.'</td>
			<td>'.$row['NIP'].'</td>
			<td>'.$row['NamaPegawai'].'</td>
			<td>'.$row['Pangkat'].'</td>
			<td>'.$row['Golongan'].'</td>
			<td>'.$row['PokokTPP'].'</td>
			<td>'.$row['NamaJabatan'].'</td>
			<td>'.$row['NamaOPD'].'</td>
			<td class="text-center">
			<button value = '.$url.' name="update" id="btnMutasi" class="btn btn-success"><span class="icon icon-pencil-case" aria-hidden="true"></span></button>
			</td>
			</tr>';
		}
		$output .='</tbody>
		</table>';
		$arrayOutput['Tabel'] = $output;
	}else{
		$arrayOutput['response'] = 500;
	}
	return $arrayOutput;							
}

function GetOneDataPegawai($conn, $KodePegawai, $KodeOPD){
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.NamaJabatan, p.KodeOPD, o.NamaOPD, p.Username, p.Password, p.Status, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan 
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
	WHERE p.KodePegawai = '$KodePegawai' AND p.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		while ($row = $res->fetch_assoc()) {
			$row['Password'] = base64_decode($row['Password']);
			$row['HtmlOPD'] = GetDataOPDEdit($conn,$KodeOPD,$row['NamaOPD']);
			$row['HtmlJabatan'] = GetDataJabatanEdit($conn,$row['KodeJabatan'],$KodeOPD,$row['NamaJabatan'],$row['NilaiJabatan'],$row['HargaJabatan']);
			//GetDataJabatanEdit($conn,$KodeJabatan,$KodeOPD,$NamaJabatan,$NilaiJabatan,$HargaJabatan)
			return array('response' => 200, 'Data' => $row);
		}
	}else{
		return array('response' => 500);
	}
}

function GetDataOPDEdit($conn,$KodeOPD,$NamaOPD){
	$output = '';
	$sql = "SELECT KodeOPD,NamaOPD,AlamatOPD,KodeSuratOPD,Keterangan FROM mstopd ORDER BY KodeOPD ASC";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Nama OPD</label>';
		$output .= "<select class='form-control' id='cbKodeOpd' name='cbKodeOpd'>";
		$output .= "<option value='".$KodeOPD."'>".$NamaOPD."</option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option value='" . $row['KodeOPD'] . "'>" . $row['NamaOPD'] . "</option>";
		}
		$output .= '</select>';
	}
	return $output;
}

function GetDataJabatanEdit($conn,$KodeJabatan,$KodeOPD,$NamaJabatan,$NilaiJabatan,$HargaJabatan){
	$output = '';
	$sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD
	FROM mstjabatan j
	WHERE j.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$output .='<label class="form-control-label">Jabatan</label>';
		$output .= "<select class='form-control' id='cbJabatan' name='cbJabatan'>";
		$output .= "<option data-value='" . $NilaiJabatan . "' data-value2='" . $HargaJabatan . "' value='".$KodeJabatan."' selected>".$NamaJabatan."</option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option data-value='" . $row['NilaiJabatan'] . "' data-value2='" . $row['HargaJabatan'] . "' value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
		}
		$output .= '</select>';
	}
	return $output;
}

function DeleteDataPegawai($conn, $KodePegawai, $KodeOPD){
	$sql = "DELETE FROM mstpegawai WHERE KodePegawai = '$KodePegawai' AND KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function GetDataJabatan($conn, $KodeOPD){
	$arrayOutput = '';
	$output = '';
	$sql = "SELECT j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD, o.NamaOPD 
	FROM mstjabatan j
	LEFT JOIN mstopd o
	ON o.KodeOPD = j.KodeOPD
	WHERE j.KodeOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$arrayOutput['response'] = 200;
		$output .='<label class="form-control-label">Jabatan</label>';
		$output .= "<select class='form-control' id='cbJabatan' name='cbJabatan'>";
		$output .= "<option data-value='0' data-value2='0' value='0'> - Pilih Jabatan - </option>";
		while ($row = $res->fetch_assoc()) {
			$output .= "<option data-value='" . $row['NilaiJabatan'] . "' data-value2='" . $row['HargaJabatan'] . "' value='" . $row['KodeJabatan'] . "'>" . $row['NamaJabatan'] . "</option>";
		}
		$output .= '</select>';
		$arrayOutput['cbJabatan'] = $output;
		return $arrayOutput;
	}else{
		return array('response' => 500);
	}
}

function AutoKodePegawai($conn,$KodeOPD){
	date_default_timezone_set('Asia/Jakarta');
	$sql = "SELECT RIGHT(KodePegawai,16) AS kode FROM mstpegawai WHERE KodeOPD = '$KodeOPD' ORDER BY KodePegawai DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
	return 'PGW-' . $bikin_kode;
}
?>