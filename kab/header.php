<?php
session_start();
if (!isset($_SESSION['KodeUserKab'])) {
  echo "<script>location.href='login_kab_view.php'</script>";
}

if (isset($_GET['nama'])) {
  $NamaUser = $_GET['nama'];    
  $_SESSION['Nama'] = $NamaUser;
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bagian Organisasi Kab.Jombang</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">    
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
  <!-- Fontastic Custom icon font-->
  <link rel="stylesheet" href="../komponen/css/fontastic.css">
  <!-- Google fonts - Poppins -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="../komponen/css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="../komponen/css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="../komponen/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <!-- <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css"> -->

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="../komponen/font-awesome/css/font-awesome.min.css">
      </head>
      <body>
        <div class="page">
          <!-- Main Navbar-->
          <header class="header">
            <nav class="navbar">
              <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                  <!-- Navbar Header-->
                  <div class="navbar-header">
                    <!-- Navbar Brand --><a href="index.php" class="navbar-brand d-none d-sm-inline-block">
                      <div class="brand-text d-none d-lg-inline-block"><span>Sistem Informasi </span><strong> TPP</strong></div>
                      <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong>TPP</strong></div></a>
                      <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
                    </div>
                    <!-- Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                      <!-- Logout    -->
                      <li class="nav-item"><a href="logout.php" class="nav-link logout"> <span class="d-none d-sm-inline">Logout</span><i class="fa fa-sign-out"></i></a></li>
                    </ul>
                  </div>
                </div>
              </nav>
            </header>
            <div class="page-content d-flex align-items-stretch"> 
              <!-- Side Navbar -->
              <nav class="side-navbar">
                <!-- Sidebar Header-->
                <div class="sidebar-header d-flex align-items-center">

                  <div onclick="HalamanProfil()" class="title" style="cursor: pointer;">
                    <h1 class="h4"><?php echo $_SESSION['Nama']; ?></h1>
                    <p>Kab. Jombang</p>
                  </div>
                </div>
                <!-- Sidebar Navidation Menus--><span class="heading">Menu</span>
                <ul class="list-unstyled">
                  <li><a href="../kab/index.php"> <i class="icon-home"></i>Home </a></li>

                  <li><a href="#DropdownUser" aria-expanded="false" data-toggle="collapse">
                    <i class="icon icon-user"></i>User </a>
                    <ul id="DropdownUser" class="collapse list-unstyled ">
                      <li><a href="../kab/daftar_user_kab_view.php">User Kabupaten</a></li>
                      <li><a href="../kab/daftar_user_view.php">User OPD</a></li>
                    </ul>
                  </li>
                  <li><a href="../kab/instansi_view.php"><i class="fa fa-building-o" aria-hidden="true"></i>Instansi OPD </a></li>
                  <li><a href="../kab/pangkat_view.php"><i class="fa fa-address-card-o" aria-hidden="true"></i>Pangkat/ Golongan </a></li>
                  <li><a href="../kab/aktivitas_view.php"><i class="fa fa-users" aria-hidden="true"></i>Aktivitas Pegawai </a></li>
                  <li><a href="../kab/mutasi_pegawai_view.php"><i class="fa fa-black-tie" aria-hidden="true"></i>Mutasi Pegawai </a></li>
                  <li><a href="../kab/sistem_setting_view.php"><i class="fa fa-cogs" aria-hidden="true"></i>Sistem Setting </a></li>
                  <li><a href="../kab/log_view.php"><i class="fa fa-server" aria-hidden="true"></i></i>Log Server </a></li>                                
                </nav>