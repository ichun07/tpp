<?php
$KodeOPD = $_GET['KodeOPD'];
?>
<div id="tabel_sakip" class="table-responsive">
	<div class="row">
		<div class="col-lg-12 text-right">
			<button id="btnTambah" class="btn btn-primary">Tambah Data</button>
		</div>		
	</div>
	<br>
	<div class="row">
		<div class="col-lg-12">
			<table width="100%" class="table table-striped table-bordered dataTable" id="lookup">
				<thead>
					<tr>
						<th>Tahun</th>
						<th>Triwulan</th>
						<th>Evaluasi SAKIP</th>
						<th>Nilai Kategori SAKIP</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Tahun</th>
						<th>Triwulan</th>
						<th>Evaluasi SAKIP</th>
						<th>Nilai Kategori SAKIP</th>
						<th>Aksi</th>
					</tr>
				</tfoot>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal Popup untuk delete--> 
	<div class="modal fade" id="ModalHapus">
		<div class="modal-dialog">
			<div class="modal-content" style="margin-top:100px;">
				<div class="modal-header">				
					<h4 class="modal-title">Anda yakin untuk menghapus data ini ?</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
					<a href="#" class="btn btn-info" id="btHapus">Hapus</a>
					<button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Edit Data SAKIP</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="form-control-label">Tahun</label>
								<input type="text" placeholder="Tahun" class="form-control"
								name="txtTahun" id="txtTahun" autocomplete="off" required disabled="true">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="form-control-label">Triwulan</label>
								<input type="text" placeholder="Triwulan" class="form-control"
								name="txtTriwulan" id="txtTriwulan" autocomplete="off" required disabled="true">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">						
							<div class="form-group">
								<label class="form-control-label">Evaluasi SAKIP</label>                     
								<select id="txtEvalusasiSAKIP" name="txtEvalusasiSAKIP" class="form-control">
									<option value="AA">AA</option>
									<option value="A">A</option>
									<option value="BB">BB</option>
									<option value="B">B</option>
									<option value="CC">CC</option>
									<option value="C">C</option>
									<option value="DD">DD</option>
									<option value="D">D</option>
								</select>
							</div>
							<div class="form-group">
								<label class="form-control-label">Nilai Kategori SAKIP</label>
								<input type="number" placeholder="Nilai Kategori SAKIP" class="form-control" name="txtNilaiKategoriSAKIP"
								id="txtNilaiKategoriSAKIP" autocomplete="off" step="any" required disabled="true">
							</div>
						</div>						
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-info" id="btnSimpan">
						Simpan
					</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">
						Batal
					</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Popup untuk Tambah--> 
	<div id="ModalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Tambah Data SAKIP</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<?php 
							date_default_timezone_set('Asia/Jakarta');
							$year = date('Y');
							$month = date('m');
							?>   
							<div class="form-group">
								<label class="form-control-label">Tahun</label>
								<input disabled type="text" placeholder="Tahun" class="form-control" name="txtTahunAdd" value="<?php echo $year; ?>">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="form-control-label">Triwulan</label>
							</br>
							<select id="txtTriwulanAdd" name="txtTriwulanAdd" class="form-control">             
								<option value="1" <?php if (3 >= (int)$month and (int)$month >= 1) echo ' selected="selected"'; ?>>Triwulan 1</option>
								<option value="2"<?php if (6 >= (int)$month and (int)$month >= 4) echo ' selected="selected"'; ?>>Triwulan 2</option>
								<option value="3"<?php if (9 >= (int)$month and (int)$month >= 7) echo ' selected="selected"'; ?>>Triwulan 3</option>
								<option value="4"<?php if (12 >= (int)$month and (int)$month >= 10) echo ' selected="selected"'; ?>>Triwulan 4</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">						
						<div class="form-group">
							<label class="form-control-label">Evaluasi SAKIP</label>                     
							<select id="txtEvalusasiSAKIPAdd" name="txtEvalusasiSAKIPAdd" class="form-control">
								<option selected="selected" value="AA">AA</option>
								<option value="A">A</option>
								<option value="BB">BB</option>
								<option value="B">B</option>
								<option value="CC">CC</option>
								<option value="C">C</option>
								<option value="DD">DD</option>
								<option value="D">D</option>
							</select>
						</div>
						<div class="form-group">
							<label class="form-control-label">Nilai Kategori SAKIP</label>
							<input type="text" placeholder="Nilai Kategori SAKIP" class="form-control" name="txtNilaiKategoriSAKIPAdd"
							id="txtNilaiKategoriSAKIPAdd" autocomplete="off" step="any" required disabled="true">
						</div>
					</div>						
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-info" id="btnSimpanAdd">
					Simpan
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal">
					Batal
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var dataTable1, EvaluasiSAKIP, NilaiKategoriSAKIP, KodeOPD;

	$(document).ready(function() {
		KodeOPD = "<?php echo $KodeOPD; ?>";
		LoadDataSakip();  
	});

	function LoadDataSakip(){
		console.log("Load Sakip");
		var action = "LoadDataSakip";
		dataTable1 = $('#lookup').DataTable( {
			"processing": true,
			"serverSide": true,
			"columnDefs": [
			{
				targets: 0,
				className: 'text-center'
			},
			{
				targets: 1,
				className: 'text-center'
			},
			{
				targets: 2,
				className: 'text-center'
			},
			{
				targets: 3,
				className: 'text-center'
			},
			{
				targets: 4,
				className: 'text-center'
			}],
			"ajax":{
				url :"sakip_aksi.php",
				type: "post",						
				data: function ( d ) {
					d.action = action,
					d.KodeOPD = KodeOPD;
				},
				error: function(){
					$(".lookup-error").html("");
					$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
					$("#lookup_processing").css("display","none");
				}
			}
		});	
	}

	$(document).on('click', '#btnTambah', function () {
		$('#ModalTambah').modal('show');
		SelectElement('txtEvalusasiSAKIPAdd', 'AA');
		$('#txtNilaiKategoriSAKIPAdd').val(1.10);
		$('#txtEvalusasiSAKIPAdd').change(function () {
			EvaluasiSAKIP = $(this).find('option:selected').attr('value');
			if(EvaluasiSAKIP == 'AA'){
				NilaiKategoriSAKIP = 1.10;
			}else if (EvaluasiSAKIP == 'A') {
				NilaiKategoriSAKIP = 1.00;
			}else if (EvaluasiSAKIP == 'BB') {
				NilaiKategoriSAKIP = 0.90;
			}else if (EvaluasiSAKIP == 'B') {
				NilaiKategoriSAKIP = 0.85;
			}else if (EvaluasiSAKIP == 'CC') {
				NilaiKategoriSAKIP = 0.60;
			}else if (EvaluasiSAKIP == 'C') {
				NilaiKategoriSAKIP = 0.55;
			}else {
				NilaiKategoriSAKIP = 0;
			}
			$('#txtNilaiKategoriSAKIPAdd').val(NilaiKategoriSAKIP);
		});
	});

	$(document).on('click', '#btnSimpanAdd', function () {
		var action = "InsertData";
		var Tahun = $("[name='txtTahunAdd']").val();
		var Triwulan = $("[name='txtTriwulanAdd']").val();
		var EvaluasiSAKIP = $("[name='txtEvalusasiSAKIPAdd']").val();
		var NilaiKategoriSAKIP = $("[name='txtNilaiKategoriSAKIPAdd']").val();
		$.ajax({
			url: "sakip_aksi.php",
			method: "POST",
			data: {KodeOPD: KodeOPD, Tahun: Tahun, Triwulan: Triwulan, EvaluasiSAKIP: EvaluasiSAKIP, NilaiKategoriSAKIP: NilaiKategoriSAKIP, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){				
					dataTable1.ajax.reload();
					swal('Sukses', 'Berhasil menambah data', 'success');
					$('#ModalTambah').modal("toggle");
				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});
	});

	$(document).on('click', '#btnUpdate', function () {
		var Triwulan = $(this).val();
		var Tahun = $(this).attr("data-value");
		console.log("Tahun : " + Tahun + " Triwulan : " + Triwulan + " KodeOPD : " + KodeOPD);
		var action = "AmbilData";
		$.ajax({
			url: "sakip_aksi.php",
			method: "POST",
			data: {KodeOPD: KodeOPD, Tahun: Tahun, Triwulan: Triwulan, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					console.log("EvaluasiSAKIP : " + data.EvalusasiSAKIP);
					$('#txtTahun').val(data.Tahun);
					$('#txtTriwulan').val(data.Triwulan);
					SelectElement('txtEvalusasiSAKIP', data.EvalusasiSAKIP);
					$('#txtNilaiKategoriSAKIP').val(data.NilaiKategoriSAKIP);
					$('#ModalEdit').modal("show");

					$('#txtEvalusasiSAKIP').change(function () {
						EvaluasiSAKIP = $(this).find('option:selected').attr('value');
						if(EvaluasiSAKIP == 'AA'){
							NilaiKategoriSAKIP = 1.10;
						}else if (EvaluasiSAKIP == 'A') {
							NilaiKategoriSAKIP = 1.00;
						}else if (EvaluasiSAKIP == 'BB') {
							NilaiKategoriSAKIP = 0.90;
						}else if (EvaluasiSAKIP == 'B') {
							NilaiKategoriSAKIP = 0.85;
						}else if (EvaluasiSAKIP == 'CC') {
							NilaiKategoriSAKIP = 0.60;
						}else if (EvaluasiSAKIP == 'C') {
							NilaiKategoriSAKIP = 0.55;
						}else {
							NilaiKategoriSAKIP = 0;
						}
						$('#txtNilaiKategoriSAKIP').val(NilaiKategoriSAKIP);
					});

				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});
	});

	$(document).on('click', '#btnSimpan', function () {
		var action = "UpdateData";
		var Tahun = $("[name='txtTahun']").val();
		var Triwulan = $("[name='txtTriwulan']").val();
		var EvaluasiSAKIP = $("[name='txtEvalusasiSAKIP']").val();
		var NilaiKategoriSAKIP = $("[name='txtNilaiKategoriSAKIP']").val();
		$.ajax({
			url: "sakip_aksi.php",
			method: "POST",
			data: {KodeOPD: KodeOPD, Tahun: Tahun, Triwulan: Triwulan, EvaluasiSAKIP: EvaluasiSAKIP, NilaiKategoriSAKIP: NilaiKategoriSAKIP, action: action},
			dataType: 'json',
			success: function (data) {
				if(data.response == 200){
					dataTable1.ajax.reload();
					swal('Sukses', 'Berhasil mengubah data', 'success');
					$('#ModalEdit').modal("toggle");
				}else{
					swal('Error', 'Terjadi Kesalahan', 'error');
				}
			}
		});
	});

	function SelectElement(id, valueToSelect){    
		var element = document.getElementById(id);
		element.value = valueToSelect;
	}

	$(document).on('click', '#btnDelete', function () {
		var Triwulan = $(this).val();
		var Tahun = $(this).attr("data-value");
		$('#ModalHapus').modal('show');

		$(document).on('click', '#btHapus', function () {
			var action = "DeleteData";
			$.ajax({
				url: "sakip_aksi.php",
				method: "POST",
				data: {KodeOPD: KodeOPD,Tahun: Tahun, Triwulan: Triwulan, action: action},
				dataType: 'json',
				success: function (data) {
					if(data.response == 200){
						swal('Sukses', 'Berhasil menghapus data', 'success');
						dataTable1.ajax.reload();
						$('#ModalHapus').modal("toggle");
					}else{
						swal('Error', 'Terjadi Kesalahan', 'error');
					}
				}
			});
		});
	});

</script>