<?php
include_once '../config/koneksi.php';

if($_POST){
	if($_POST['action'] == "GetDataJurnal"){
		$KodePegawai = $_POST['KodePegawai'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$Value = $_POST['Value'];
		$Index = $_POST['Index'];
		$result = GetJurnalBulanan($conn, $KodePegawai, $Bulan, $Tahun, $Index, $Value);
		echo json_encode($result);
	}

	if($_POST['action'] == "AddJurnal"){
		$KodePegawai = $_POST['KodePegawai'];
		$NamaAktivitas = $_POST['NamaAktivitas'];
		$Deskripsi = $_POST['Deskripsi'];
		$DurasiKerja = $_POST['DurasiKerja'];
		$KodePegawaiAtasan = $_POST['KodePegawaiAtasan'];
		if(isset($_FILES['image'])){
			$DokumenPendukung = $_FILES['image'];
			$validextensions = array("jpeg", "jpg", "png");
			$temporary = explode(".", $_FILES["image"]["name"]);
			$file_extension = end($temporary);
		}else{
			$DokumenPendukung = null;
			$validextensions = "";
			$temporary = "";
			$file_extension = "";
		}
		$result = AddJurnalBulanan($conn, $KodePegawai, $NamaAktivitas, $Deskripsi, $DurasiKerja, $KodePegawaiAtasan, $DokumenPendukung, $file_extension, $validextensions);
		echo json_encode($result);
	}

	if($_POST['action'] == "MenitBulanan"){
		$KodePegawai = $_POST['KodePegawai'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetMenitBulanan($conn, $KodePegawai, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == "PegawaiBawahan"){
		$KodeJabatan = $_POST['KodeJabatan'];
		$KodeOPD = $_POST['KodeOPD'];
		$Value = $_POST['Value'];
		$Index = $_POST['Index'];
		$result = GetPegawaiBawahan($conn,$KodeJabatan, $KodeOPD, $Index, $Value);
		echo json_encode($result);
	}

	if($_POST['action'] == 'UpdateAcc'){
		$KodeAktivitas = $_POST['KodeAktivitas'];
		$KodePegawaiAtasan = $_POST['KodePegawaiAtasan'];
		$ACCAtasan = $_POST['ACCAtasan'];
		$KeteranganACC = $_POST['KeteranganACC'];
		$DurasiKerjaACC = $_POST['DurasiKerjaACC'];
		$result = UpdateStatusAcc($conn, $KodeAktivitas, $KodePegawaiAtasan, $ACCAtasan, $DurasiKerjaACC, $KeteranganACC);
		echo json_encode($result);
	}

	if($_POST['action'] == 'GetAktivitas'){
		$Index = $_POST['Index'];
		$Value = $_POST['Value'];
		$result = GetDataAktivitas($conn, $Value, $Index);
		echo json_encode($result);
	}
}

if($_GET){
	if($_GET['action'] == "TampilFoto"){
		$KodeAktivitas = $_GET['KodeAktivitas'];
		$image = GetDokumenPendukung($conn,$KodeAktivitas);
		echo $image;
	}
}

// [{"KodeAktivitasPegawai":"AKT-000000000000000000001","NamaAktivitas":"Mengikuti Apel pagi\/sore","Satuan":"1","TingkatKesulitan":"0.5","Waktu":"15.7","Beban":"7.5"}]

function GetDataAktivitas($conn, $Value, $Index){
	if($Value == ''){
		$sql = "SELECT KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban 
		FROM mstaktivitas 
		ORDER BY KodeAktivitasPegawai ASC	
		LIMIT $Index,10";
	}else{
		$sql = "SELECT KodeAktivitasPegawai, NamaAktivitas, Satuan, TingkatKesulitan, Waktu, Beban 
		FROM mstaktivitas ";
		$sql .= "WHERE NamaAktivitas LIKE '%".$Value."%' ";
		$sql .= "ORDER BY KodeAktivitasPegawai ASC	
		LIMIT $Index,10";
	}

	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function SearchPegawaiBawahan($conn,$KodeJabatan, $KodeOPD, $Value){
	$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai , p.Pangkat, p.Golongan, p.Status, p.Alamat, p.PokokTPP, p.PokokUangMakan, j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD , j.AtasanLangsungOPD, j.IsKepala, o.NamaOPD 
	FROM mstpegawai p 
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD ";
	$sql .= "WHERE j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
	AND p.NamaPegawai LIKE '%".$Value."%' ";
	$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
	AND p.NIP LIKE '%".$Value."%' ";
	$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
	AND p.Pangkat LIKE '%".$Value."%' ";
	$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
	AND p.Golongan LIKE '%".$Value."%' ";
	$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
	AND j.NamaJabatan LIKE '%".$Value."%' ";
	$sql .= "GROUP BY p.KodePegawai";

	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function GetJurnalBulanan($conn, $KodePegawai, $Bulan, $Tahun, $Index, $Value){
	if($Value == ""){
		$sql = "SELECT ap.KodeAktivitas, ap.Tanggal, ap.NamaAktivitas, ap.Deskripsi, ap.DurasiKerja, ap.ACCAtasan, ap.KodePegawaiAtasan, ap.KeteranganACC, ap.DurasiKerjaACC, ap.KodePegawai, '' AS DokumenPendukung, p.NamaPegawai AS NamaAtasan
		FROM aktivitaspegawai ap
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawaiAtasan
		WHERE ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' 
		GROUP BY ap.KodeAktivitas 
		ORDER BY ap.Tanggal ,ap.KodeAktivitas DESC
		LIMIT $Index,10";	
	}else{
		$sql = "SELECT ap.KodeAktivitas, ap.Tanggal, ap.NamaAktivitas, ap.Deskripsi, ap.DurasiKerja, ap.ACCAtasan, ap.KodePegawaiAtasan, ap.KeteranganACC, ap.DurasiKerjaACC, ap.KodePegawai, '' AS DokumenPendukung, p.NamaPegawai AS NamaAtasan
		FROM aktivitaspegawai ap
		LEFT JOIN mstpegawai p ON p.KodePegawai = ap.KodePegawaiAtasan ";
		$sql .= "WHERE ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.Tanggal LIKE '%".$Value."%' ";
		$sql .= "OR ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.NamaAktivitas LIKE '%".$Value."%' ";
		$sql .= "OR ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.DurasiKerja LIKE '%".$Value."%' ";
		$sql .= "OR ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND p.NamaPegawai LIKE '%".$Value."%' ";
		$sql .= "OR ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.ACCAtasan LIKE '%".$Value."%' ";
		$sql .= "OR ap.KodePegawai = '$KodePegawai' AND MONTH(ap.Tanggal) = '$Bulan' AND YEAR(ap.Tanggal) = '$Tahun' AND ap.KeteranganACC LIKE '%".$Value."%' ";
		$sql .= "GROUP BY ap.KodeAktivitas 
		ORDER BY ap.Tanggal ,ap.KodeAktivitas DESC
		LIMIT $Index,10";
	}
	
	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
			$row['ACCAtasan'] = ucwords(strtolower($row['ACCAtasan']));
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function AddJurnalBulanan($conn, $KodePegawai, $NamaAktivitas, $Deskripsi, $DurasiKerja, $KodePegawaiAtasan, $Dokumen, $file_extension, $validextensions){	
	date_default_timezone_set('Asia/Jakarta');
	$Tanggal = date('Y-m-d');
	$KodeAktivitas = AutoKodeAktivitas($conn);
	if($Dokumen != null){
		if (($Dokumen["size"] < 500000000) ){
			if ($Dokumen["error"] > 0){
				$error_message = "Return Code: " . $Dokumen["error"]."";
				return array('response' => 500, 'error_response' => 'File Error');
			}else{
				if (file_exists($Dokumen["name"])) {
					$error_message = $Dokumen["name"] . " already exists";
					return array('response' => 500, 'error_response' => 'File Exists');
				}else{
					$sourcePath = $Dokumen['tmp_name'];
					$targetPath = $Dokumen['name'];
					move_uploaded_file($sourcePath,$targetPath);
					$DokumenPendukung = mysqli_real_escape_string($conn, file_get_contents($targetPath));
					if($KodePegawaiAtasan != null && $KodePegawaiAtasan !=''){
						$sql = "INSERT INTO aktivitaspegawai (KodeAktivitas, Tanggal, NamaAktivitas, Deskripsi, DurasiKerja, KodePegawai, DokumenPendukung, KodePegawaiAtasan) VALUES ('$KodeAktivitas', '$Tanggal', '$NamaAktivitas', '$Deskripsi', '$DurasiKerja',  '$KodePegawai', '$DokumenPendukung', '$KodePegawaiAtasan')";
					}else{
						$sql = "INSERT INTO aktivitaspegawai (KodeAktivitas, Tanggal, NamaAktivitas, Deskripsi, DurasiKerja, KodePegawai, DokumenPendukung) VALUES ('$KodeAktivitas', '$Tanggal', '$NamaAktivitas', '$Deskripsi', '$DurasiKerja',  '$KodePegawai', '$DokumenPendukung')";
					}				
					$res = $conn->query($sql);
					if($res){
						unlink($targetPath);
						return array('response' => 200);
					}else{
						unlink($targetPath);
						return array('response' => 500, 'error_response' => 'Upload Failed');
					}				
				}
			}
		}else{
			return array('response' => 500, 'type' => $Dokumen["type"],'size' => $Dokumen["size"]);
		}
	}else{
		if($KodePegawaiAtasan != null && $KodePegawaiAtasan !=''){
			$sql = "INSERT INTO aktivitaspegawai (KodeAktivitas, Tanggal, NamaAktivitas, Deskripsi, DurasiKerja, KodePegawai, KodePegawaiAtasan) VALUES ('$KodeAktivitas', '$Tanggal', '$NamaAktivitas', '$Deskripsi', '$DurasiKerja',  '$KodePegawai', '$KodePegawaiAtasan')";
		}else{
			$sql = "INSERT INTO aktivitaspegawai (KodeAktivitas, Tanggal, NamaAktivitas, Deskripsi, DurasiKerja, KodePegawai) VALUES ('$KodeAktivitas', '$Tanggal', '$NamaAktivitas', '$Deskripsi', '$DurasiKerja',  '$KodePegawai')";
		}				
		$res = $conn->query($sql);
		if($res){
			return array('response' => 200);
		}else{
			return array('response' => 500, 'error_response' => 'Upload Failed');
		}
	}
}

function AutoKodeAktivitas($conn){
	//ATV-0000000000000003
	$sql = "SELECT RIGHT(KodeAktivitas,16) AS kode FROM aktivitaspegawai ORDER BY KodeAktivitas DESC LIMIT 1";
	$res = mysqli_query($conn, $sql);
	$result = mysqli_fetch_array($res);
	if ($result['kode'] == null) {
		$kode = 1;
	} else {
		$kode = ++$result['kode'];
	}
	$bikin_kode = str_pad($kode, 16, "0", STR_PAD_LEFT);
	return 'ATV-' . $bikin_kode;
}

function GetDokumenPendukung($conn,$KodeAktivitas){
	$sql = "SELECT DokumenPendukung AS Foto FROM aktivitaspegawai WHERE KodeAktivitas LIKE '$KodeAktivitas' LIMIT 0, 1;";
	$query = $conn->query($sql);
	$result=mysqli_fetch_array($query);
	return $result['Foto'];
}

function GetMenitBulanan($conn, $KodePegawai, $Bulan, $Tahun){
	$sql = "SELECT IF(SUM(DurasiKerja) IS NULL,'0',SUM(DurasiKerja)) AS DurasiKerja, IF(SUM(DurasiKerjaACC) IS NULL,'0',SUM(DurasiKerjaACC)) AS DurasiKerjaACC FROM aktivitaspegawai WHERE KodePegawai = '$KodePegawai' AND MONTH(Tanggal) = '$Bulan' AND YEAR(Tanggal) = '$Tahun'";
	$res = $conn->query($sql);
	if($res){
		$rowArray;
		while ($row = $res->fetch_assoc()) {
			$rowArray = $row;
		}
		return $rowArray;	
	}
	
}

function GetPegawaiBawahan($conn, $KodeJabatan, $KodeOPD, $Index, $Value){
	if($Value == ""){
		$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai , p.Pangkat, p.Golongan, p.Status, p.Alamat, p.PokokTPP, p.PokokUangMakan, j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD , j.AtasanLangsungOPD, j.IsKepala, o.NamaOPD 
		FROM mstpegawai p 
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
		WHERE j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
		GROUP BY p.KodePegawai
		LIMIT $Index,10";
	}else{
		$sql = "SELECT p.KodePegawai, p.NIP, p.NamaPegawai , p.Pangkat, p.Golongan, p.Status, p.Alamat, p.PokokTPP, p.PokokUangMakan, j.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD , j.AtasanLangsungOPD, j.IsKepala, o.NamaOPD 
		FROM mstpegawai p 
		LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
		LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD ";
		$sql .= "WHERE j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
		AND p.NamaPegawai LIKE '%".$Value."%' ";
		$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
		AND p.NIP LIKE '%".$Value."%' ";
		$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
		AND p.Pangkat LIKE '%".$Value."%' ";
		$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
		AND p.Golongan LIKE '%".$Value."%' ";
		$sql .= "OR j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'
		AND j.NamaJabatan LIKE '%".$Value."%' ";
		$sql .= "GROUP BY p.KodePegawai LIMIT $Index,10";
	}	
	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function UpdateStatusAcc($conn, $KodeAktivitas, $KodePegawaiAtasan, $ACCAtasan, $DurasiKerjaACC, $KeteranganACC){
	$sql = "UPDATE aktivitaspegawai SET ACCAtasan = '$ACCAtasan',KodePegawaiAtasan = '$KodePegawaiAtasan',KeteranganACC = '$KeteranganACC', DurasiKerjaACC = '$DurasiKerjaACC' WHERE KodeAktivitas = '$KodeAktivitas'";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

