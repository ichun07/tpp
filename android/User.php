<?php
include_once '../config/koneksi.php';

if($_POST['action'] == "UserLogin"){
	$Username = $_POST['Username'];
	$Password = base64_encode($_POST['Password']);
	$result = UserLogin($conn, $Username, $Password);
	echo json_encode($result);
}

if($_POST['action'] == "History"){
	$KodePegawai = $_POST['KodePegawai'];
	$index = $_POST['index'];
	$result = GetHistoryJabatan($conn, $KodePegawai,$index);
	echo json_encode($result);
}

if($_POST['action'] == "UpdateProfil"){
	$KodePegawai = $_POST['KodePegawai'];
	$NamaPegawai = $_POST['NamaPegawai'];
	$Username	 = $_POST['Username'];
	$Password = $_POST['Password'];
	$result = EditDataPegawai($conn, $KodePegawai, $NamaPegawai, $Username, $Password);
	echo json_encode($result);
}

function UserLogin($conn, $Username, $Password){
	$sql = "SELECT p.KodePegawai, p.NoAkun, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.KodeOPD, j.AtasanLangsungOPD, j.IsKepala, p.KodeOPD, o.NamaOPD, p.Username, p.Password, peg.KodePegawai AS KodeAtasan, peg.NamaPegawai AS NamaAtasan
	FROM mstpegawai p
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	LEFT JOIN mstpegawai peg ON peg.KodeJabatan = j.AtasanLangsungKodeJab AND peg.KodeOPD = j.AtasanLangsungOPD
	LEFT JOIN mstopd o ON o.KodeOPD = p.KodeOPD
	WHERE p.Username = '$Username' AND p.Password = '$Password'
	LIMIT 0,1";
	$res = $conn->query($sql);
	if($res){
		$count = mysqli_num_rows($res);
		if($count > 0){
			$rowArray = '';
			while ($row = $res->fetch_assoc()) {
				$row['Password'] = base64_decode($row['Password']);
				$row['IsAtasan'] = HitungPegawaiBawahan($conn, $row['KodeJabatan'], $row['KodeOPD']);
				$rowArray = $row;
			}
			return array('response' => 200, 'DataUser' => $rowArray);
		}else{
			return array('response' => 500);
		}
	}else{
		return array('response' => 501);
	}
}

function GetHistoryJabatan($conn, $KodePegawai, $Index){
	$sql = "SELECT h.KodePegawai, h.KodeHistory, h.TanggalPengangkatan, h.NoSKPengangkatan, h.TanggalSampai, h.Keterangan, h.KodeJabatan, h.KodeOPD, j.KodeManual, j.NamaJabatan, j.Keterangan, j.Deskripsi, j.TipeJabatan, j.KelasJabatan, j.NilaiJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala
	FROM historyjabatan h
	LEFT JOIN mstjabatan j ON j.KodeJabatan = h.KodeJabatan AND j.KodeOPD = h.KodeOPD
	WHERE h.KodePegawai = '$KodePegawai'
	ORDER BY h.TanggalPengangkatan ASC
	LIMIT $Index,10";
	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function EditDataPegawai($conn, $KodePegawai, $NamaPegawai, $Username, $Password){
	$Password = base64_encode($Password);
	$sql = "UPDATE mstpegawai 
	SET NamaPegawai = '$NamaPegawai', Username = '$Username', Password = '$Password' 
	WHERE KodePegawai = '$KodePegawai'";
	$res = $conn->query($sql);
	if($res){
		return array('response' => 200);
	}else{
		return array('response' => 500);
	}
}

function HitungPegawaiBawahan($conn, $KodeJabatan, $KodeOPD){
	$sql = "SELECT COUNT(p.KodePegawai) AS Jumlah 
	FROM mstjabatan j
	INNER JOIN mstpegawai p ON p.KodeJabatan = j.KodeJabatan AND p.KodeOPD = j.KodeOPD
	WHERE j.AtasanLangsungKodeJab = '$KodeJabatan' AND j.AtasanLangsungOPD = '$KodeOPD'";
	$res = $conn->query($sql);
	if($res){
		$num = mysqli_fetch_array($res);
		return $num['Jumlah'];
	}else{
		return 0;
	}
}