<?php
include_once '../config/koneksi.php';
include_once '../opd/rumus.php';

if($_POST){
	if($_POST['action'] == 'TppProduktivitas'){
		$KodePegawai = $_POST['KodePegawai'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetTppProduktivitas($conn, $KodePegawai, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'TppUangMakan'){
		$KodePegawai = $_POST['KodePegawai'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetTppUangMakan($conn, $KodePegawai, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'TppKinerja'){
		$KodePegawai = $_POST['KodePegawai'];
		$Triwulan = $_POST['Triwulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetTppKinerja($conn, $KodePegawai, $Triwulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'Dashboard'){
		$KodePegawai = $_POST['KodePegawai'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = GetInfoDashboard($conn, $KodePegawai, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'ProdukTahun'){
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$result = TppSatuTahun($conn, $KodePegawai, $KodeOPD, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'MakanTahun'){
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$result = TppMakanSatuTahun($conn, $KodePegawai, $KodeOPD, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'KinerjaTahun'){
		$KodePegawai = $_POST['KodePegawai'];
		$KodeOPD = $_POST['KodeOPD'];
		$Tahun = $_POST['Tahun'];
		$result = TppKinerjaSatuTahun($conn, $KodePegawai, $KodeOPD, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'DataTpp'){
		$KodePegawai = $_POST['KodePegawai'];
		$Bulan = $_POST['Bulan'];
		$Tahun = $_POST['Tahun'];
		$result = TppBerdasarKinerjaNew($conn, $KodePegawai, $Bulan, $Tahun);
		echo json_encode($result);
	}

	if($_POST['action'] == 'DataTppTahun'){
		$KodePegawai = $_POST['KodePegawai'];
		$Tahun = $_POST['Tahun'];
		$result = HitungTppSatuTahun($conn, $KodePegawai, $Tahun);
		echo json_encode($result);
	}
}

function GetTppProduktivitas($conn, $KodePegawai, $Bulan, $Tahun){
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IF(SUM(atv.DurasiKerjaACC) IS NULL,0,SUM(atv.DurasiKerjaACC)) AS DurasiKerjaACC FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	WHERE tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun' AND tb.KodePegawai = '$KodePegawai'
	ORDER BY tb.Bulan, tb.Tahun
	LIMIT 0,1";
	$res = $conn->query($sql);
	if ($res) {
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
            # code...
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = number_format((($JmlHadir / $row['JmlHariEfektif']) * 100),2);
            //NilaiTingkatKehadiran adalah ProsenKehadiran - ProsenPelanggaran
			$NilaiTingkatKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $row['KodeOPD'], $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);
			$MenitKerjaEfektif = 300 * $row['JmlHariEfektif'];
            //Jika Staff atau Fungsional
			if($row['TipeJabatan'] == 'FUNGSIONAL'){
				$NilaiAktivitasKerjaPegawai = NilaiAktivitasKerjaPegawaiStaff($row['MenitAktivitas'], $MenitKerjaEfektif);
			}else{
            // Jika Pimpinan
				$NilaiAktivitasKerjaPegawai = HitungNilaiAktivitasPegawaiTerpimpin($conn, $row['MenitAktivitas'], $MenitKerjaEfektif,$row['KodeOPD'],$row['KodeJabatan'],$row['KodeOPD'],$Bulan,$Tahun);
			}
			$RpTPP = TTPProduktivitas($row['PokokTPPPegawai'], $NilaiTingkatKehadiran, $NilaiAktivitasKerjaPegawai);
			$PersenTPP = number_format(($RpTPP / $row['PokokTPPPegawai'] * 100),2);
			$PPNTPP = $RpTPP * 0.21;
			$TotalTPPBersih = $RpTPP - $PPNTPP;

			$row['JmlHadir'] = $JmlHadir;
			$row['PersenHadir'] = $PersenHadir;
			$row['NilaiTingkatKehadiran'] = $NilaiTingkatKehadiran;
			$row['MenitKerjaEfektif'] = $MenitKerjaEfektif;
			$row['NilaiAktivitasKerjaPegawai'] = $NilaiAktivitasKerjaPegawai;
			$row['TppProduktivitas'] = $RpTPP;
			$row['PersenTPP'] = $PersenTPP;
			$row['PPNTPP'] = $PPNTPP;
			$row['TotalTPPBersih'] = $TotalTPPBersih;
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function GetTppUangMakan($conn, $KodePegawai, $Bulan, $Tahun){
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'UANG_MAKAN' AND IsAnjab = 0) AS PokokUangMakan, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IF(SUM(atv.DurasiKerjaACC) IS NULL,0,SUM(atv.DurasiKerjaACC)) AS DurasiKerjaACC FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, (tb.JmlHadir * (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'UANG_MAKAN' AND IsAnjab = 0)) AS JmlUangMakan
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	WHERE tb.KodePegawai = '$KodePegawai' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun
	LIMIT 0,1";
	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
            # code...
			$PotonganPPH = $row['JmlUangMakan'] * 21/100;
			$TppDiterima = $row['JmlUangMakan'] - $PotonganPPH;
			$row['TppDiterima'] = $TppDiterima;
			$row['PotonganPPH'] = $PotonganPPH;
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function GetTppKinerja($conn, $KodePegawai, $Triwulan, $Tahun){
	$sql = "SELECT c.Tahun, c.Triwulan, c.KodeOPD, c.RealisasiAnggaran, c.NilaiKategoriNRA, c.NRABobot, c.NilaiOutputKegiatan, c.NilaiKategoriNOK, c.NOKBobot, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP, c.SAKIPBobot, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan
	FROM mstpegawai p
	LEFT JOIN capaiankinerjaopd c ON p.KodeOPD = c.KodeOPD
	WHERE p.KodePegawai = '$KodePegawai' AND c.Tahun = '$Tahun' AND c.Triwulan = '$Triwulan'
	LIMIT 0,1";
	$res = $conn->query($sql);
	if($res){
		$rowArray = [];
		while ($row = $res->fetch_assoc()) {
			$TPPKinerja = TPPKinerja($row['PokokTPP'], $row['NilaiKategoriNRA'],$row['NilaiKategoriNOK'],$row['NilaiKategoriSAKIP']);			
			$PotonganPajak = $TPPKinerja * 21/100;
			$Diterima = ($TPPKinerja - $PotonganPajak);
			$row['TPPKinerja'] = $TPPKinerja;
			$row['PotonganPPH'] = $PotonganPajak;
			$row['TppDiterima'] = $Diterima;
			$rowArray[] = $row;
		}
		return $rowArray;
	}else{
		return array();
	}
}

function GetInfoDashboard($conn, $KodePegawai, $Bulan, $Tahun){
	$HasilA = array();
	$HasilA['TppUangMakan'] = 0;
	$HasilA['TppProduktivitas'] = 0;
	$HasilA['TppKinerja'] = 0;
	$HasilA['DurasiKerja'] = 0;
	$HasilA['DurasiKerjaACC'] = 0;
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, p.PPH, j.KodeManual, j.NilaiJabatan, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT SUM(atv.DurasiKerjaACC) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan != 'DITOLAK') AS MenitAktivitas, s.PersenSKP, IFNULL(s.NilaiSKP, 0) AS NilaiSKP, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	LEFT JOIN skppegawai s ON s.KodePegawai = tb.KodePegawai AND s.Tahun = (tb.Tahun - 1)
	LEFT JOIN capaiankinerjaopd c ON c.KodeOPD = p.KodeOPD AND c.Tahun = tb.Tahun AND c.Triwulan = IF(3 >= tb.Bulan AND tb.Bulan >= 1, 1, IF(6 >= tb.Bulan AND tb.Bulan >= 4, 2, IF(9 >= tb.Bulan AND tb.Bulan >= 7, 3, IF(12 >= tb.Bulan and tb.Bulan >= 10, 4, TRUE))))
	WHERE tb.KodePegawai = '$KodePegawai' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun";
	
	$res = $conn->query($sql);
	if ($res) {
		while ($row = $res->fetch_assoc()) {
            # code...
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
			$ProsenKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $row['KodeOPD'], $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);

			$JmlPoinAktivitas = $row['MenitAktivitas'] > $row['NilaiJabatan'] ? $row['NilaiJabatan'] : $row['MenitAktivitas'];
			$NilaiJabatan = $row['NilaiJabatan'];
			$PokokTPP = $row['PokokTPPPegawai'];
			$PersenSKP = $row['PersenSKP'];
			$NilaiSKP = $row['NilaiSKP'];
			$NilaiSAKIP = $row['NilaiKategoriSAKIP'];

			$NilaiAktivitas = $JmlPoinAktivitas == 0 ? 0 : ($JmlPoinAktivitas / $NilaiJabatan);
			$TppDisiplin = Disiplin($ProsenKehadiran, $PokokTPP);
			$TppAktivitas = Aktivitas($JmlPoinAktivitas, $NilaiJabatan, $PokokTPP);
			$TppSKP = SKP($PersenSKP, $PokokTPP);
			$TppSakip = Sakip($NilaiSAKIP, $PokokTPP);

			$BrutoTPP = TPP($TppDisiplin, $TppAktivitas, $TppSKP, $TppSakip);
			$Pph21 = $row['PPH'] == 0 ? 0 : ($BrutoTPP * ($row['PPH'] / 100));
			$TppDiterima = $BrutoTPP - $Pph21;
			$HasilA['TppProduktivitas'] = $TppDiterima;
		}
	}

	$sql3 = "SELECT 
	IF(SUM(a.DurasiKerja) IS NULL,'0',SUM(a.DurasiKerja)) AS DurasiKerja, 
	(SELECT IF(SUM(ap.DurasiKerjaACC) IS NULL,'0',SUM(ap.DurasiKerjaACC)) FROM aktivitaspegawai ap WHERE ap.KodePegawai = a.KodePegawai AND MONTH(ap.Tanggal) = MONTH(a.Tanggal) AND YEAR(ap.Tanggal) = YEAR(a.Tanggal) AND ap.ACCAtasan = 'DISETUJUI') AS DurasiKerjaACC,
	(SELECT IF(SUM(ap.DurasiKerja) IS NULL,'0',SUM(ap.DurasiKerja)) FROM aktivitaspegawai ap WHERE ap.KodePegawai = a.KodePegawai AND MONTH(ap.Tanggal) = MONTH(a.Tanggal) AND YEAR(ap.Tanggal) = YEAR(a.Tanggal) AND ap.ACCAtasan = 'DITOLAK') AS DurasiKerjaDitolak,
	(SELECT IF(SUM(ap.DurasiKerjaACC) IS NULL,'0',SUM(ap.DurasiKerjaACC)) FROM aktivitaspegawai ap WHERE ap.KodePegawai = a.KodePegawai AND MONTH(ap.Tanggal) = MONTH(a.Tanggal) AND YEAR(ap.Tanggal) = YEAR(a.Tanggal) AND ap.ACCAtasan = 'PENYESUAIAN') AS DurasiKerjaSesuai,
	(SELECT IF(SUM(ap.DurasiKerja) IS NULL,'0',SUM(ap.DurasiKerja)) FROM aktivitaspegawai ap WHERE ap.KodePegawai = a.KodePegawai AND MONTH(ap.Tanggal) = MONTH(a.Tanggal) AND YEAR(ap.Tanggal) = YEAR(a.Tanggal) AND ap.ACCAtasan IS NULL) AS DurasiKerjaMenunggu
	FROM aktivitaspegawai a WHERE a.KodePegawai = '$KodePegawai' AND MONTH(a.Tanggal) = '$Bulan' AND YEAR(a.Tanggal) = '$Tahun'";
	$res3 = $conn->query($sql3);
	if($res3){
		while ($row = $res3->fetch_assoc()) {
			$HasilA['DurasiKerja'] = $row['DurasiKerja'];
			$HasilA['DurasiKerjaDitolak'] = $row['DurasiKerjaDitolak'];
			$HasilA['DurasiKerjaSesuai'] = $row['DurasiKerjaSesuai'];
			$HasilA['DurasiKerjaMenunggu'] = $row['DurasiKerjaMenunggu'];
			$HasilA['DurasiKerjaACC'] = $row['DurasiKerjaACC'];
		}
	}

	return $HasilA;
}

function TppSatuTahun($conn, $KodePegawai, $KodeOPD, $Tahun){
	$output = '';
	for ($i=1; $i < 13; $i++) { 
		$row = HitungTppProdukTahunan($conn, $KodePegawai, $i, $Tahun);
		$output[$i] = $row;
	}
	return $output;
}

function HitungTppProdukTahunan($conn, $KodePegawai, $Bulan, $Tahun){
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IF(SUM(atv.DurasiKerjaACC) IS NULL,0,SUM(atv.DurasiKerjaACC)) AS DurasiKerjaACC FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	WHERE tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun' AND tb.KodePegawai = '$KodePegawai'
	ORDER BY tb.Bulan, tb.Tahun
	LIMIT 0,1";
	$res = $conn->query($sql);
	if ($res) {
		$rowArray = '';
		while ($row = $res->fetch_assoc()) {
            # code...
			$TotalTPPBersih = 0;
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = number_format((($JmlHadir / $row['JmlHariEfektif']) * 100),2);
            //NilaiTingkatKehadiran adalah ProsenKehadiran - ProsenPelanggaran
			$NilaiTingkatKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $row['KodeOPD'], $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);
			$MenitKerjaEfektif = 300 * $row['JmlHariEfektif'];
            //Jika Staff atau Fungsional
			if($row['TipeJabatan'] == 'FUNGSIONAL'){
				$NilaiAktivitasKerjaPegawai = NilaiAktivitasKerjaPegawaiStaff($row['MenitAktivitas'], $MenitKerjaEfektif);
			}else{
            // Jika Pimpinan
				$NilaiAktivitasKerjaPegawai = HitungNilaiAktivitasPegawaiTerpimpin($conn, $row['MenitAktivitas'], $MenitKerjaEfektif,$row['KodeOPD'],$row['KodeJabatan'],$row['KodeOPD'],$Bulan,$Tahun);
			}
			$RpTPP = TTPProduktivitas($row['PokokTPPPegawai'], $NilaiTingkatKehadiran, $NilaiAktivitasKerjaPegawai);
			$PersenTPP = number_format(($RpTPP / $row['PokokTPPPegawai'] * 100),2);
			$PPNTPP = $RpTPP * 0.21;
			$TotalTPPBersih = $RpTPP - $PPNTPP;
			$rowArray = $TotalTPPBersih+0;
		}
		return $rowArray+0;
	}else{
		return 0;
	}
}

function TppMakanSatuTahun($conn, $KodePegawai, $KodeOPD, $Tahun){
	$output = '';
	for ($i=1; $i < 13; $i++) { 
		$row = HitungTppMakanTahunan($conn, $KodePegawai, $i, $Tahun);
		$output[$i] = $row;
	}
	return $output;
}

function HitungTppMakanTahunan($conn, $KodePegawai, $Bulan, $Tahun){
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokUangMakan, p.KodeJabatan, p.KodeOPD, j.KodeManual, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, (SELECT SettingValue FROM sistemsetting WHERE SettingName = 'HARGA_JABATAN' AND IsAnjab = 0) AS HargaJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IF(SUM(atv.DurasiKerjaACC) IS NULL,0,SUM(atv.DurasiKerjaACC)) AS DurasiKerjaACC FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, (tb.JmlHadir * p.PokokUangMakan) AS JmlUangMakan
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	WHERE tb.KodePegawai = '$KodePegawai' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun
	LIMIT 0,1";
	$res = $conn->query($sql);
	if($res){
		$rowArray = '';
		while ($row = $res->fetch_assoc()) {
            # code...
			$PotonganPPH = $row['JmlUangMakan'] * 21/100;
			$TppDiterima = $row['JmlUangMakan'] - $PotonganPPH;
			$rowArray = $TppDiterima;
		}
		return $rowArray+0;
	}else{
		return 0;
	}
}

function TppKinerjaSatuTahun($conn, $KodePegawai, $KodeOPD, $Tahun){
	$output = '';
	for ($i=1; $i < 5; $i++) { 
		$row = HitungTppKinerjaTahunan($conn, $KodePegawai, $i, $Tahun);
		$output[$i] = $row;
	}
	return $output;
}

function HitungTppKinerjaTahunan($conn, $KodePegawai, $Triwulan, $Tahun){
	$sql = "SELECT c.Tahun, c.Triwulan, c.KodeOPD, c.RealisasiAnggaran, c.NilaiKategoriNRA, c.NRABobot, c.NilaiOutputKegiatan, c.NilaiKategoriNOK, c.NOKBobot, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP, c.SAKIPBobot, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP, p.KodeJabatan
	FROM mstpegawai p
	LEFT JOIN capaiankinerjaopd c ON p.KodeOPD = c.KodeOPD
	WHERE p.KodePegawai = '$KodePegawai' AND c.Tahun = '$Tahun' AND c.Triwulan = '$Triwulan'
	LIMIT 0,1";
	$res = $conn->query($sql);
	if($res){
		$rowArray = '';
		while ($row = $res->fetch_assoc()) {
			$TPPKinerja = TPPKinerja($row['PokokTPP'], $row['NilaiKategoriNRA'],$row['NilaiKategoriNOK'],$row['NilaiKategoriSAKIP']);			
			$PotonganPajak = $TPPKinerja * 21/100;
			$Diterima = ($TPPKinerja - $PotonganPajak);
			$rowArray = $Diterima;
		}
		return $rowArray+0;
	}else{
		return 0;
	}
}

function TppBerdasarKinerjaNew($conn, $KodePegawai, $Bulan, $Tahun){
	$rowArray = [];
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, p.PPH, j.KodeManual, j.NilaiJabatan, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IFNULL(SUM(atv.DurasiKerjaACC),0) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, s.PersenSKP, IFNULL(s.NilaiSKP, 0) AS NilaiSKP, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	LEFT JOIN skppegawai s ON s.KodePegawai = tb.KodePegawai AND s.Tahun = (tb.Tahun - 1)
	LEFT JOIN capaiankinerjaopd c ON c.KodeOPD = p.KodeOPD AND c.Tahun = tb.Tahun AND c.Triwulan = IF(3 >= tb.Bulan AND tb.Bulan >= 1, 1, IF(6 >= tb.Bulan AND tb.Bulan >= 4, 2, IF(9 >= tb.Bulan AND tb.Bulan >= 7, 3, IF(12 >= tb.Bulan and tb.Bulan >= 10, 4, TRUE))))
	WHERE tb.KodePegawai = '$KodePegawai' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun";
	$res = $conn->query($sql);
	if ($res) {
		$total = mysqli_num_rows($res);
		if($total < 1){
			$rowArray = array('response' => 404);
		}else{
			while ($row = $res->fetch_assoc()) {
            # code...
				$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
				$PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
				$ProsenKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $row['KodeOPD'], $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);

				$NilaiJabatan = $row['NilaiJabatan'];
				$PokokTPP = $row['PokokTPPPegawai'];
				$PersenSKP = $row['PersenSKP'];
				$NilaiSKP = $row['NilaiSKP'];
				$NilaiSAKIP = $row['NilaiKategoriSAKIP'];

				$BebanAktivitas = $row['MenitAktivitas'];

			//jika pegawai mempunyai bawahan maka mendapat 1% dari beban tiap aktivitas bawahan
				$NilaiTambah = HitungNilaiAktivitasBawahan($conn, $row['KodeOPD'], $row['KodeJabatan'], $Bulan, $Tahun);
			//cek apakah beban pegawai melebihi nilai jabatan
			//jika beban melebihi nilai jabatan maka yang dipakai adalah nilai jabatan 
				$JmlPoinAktivitas = ($BebanAktivitas + $NilaiTambah) > $row['NilaiJabatan'] ? $row['NilaiJabatan'] : ($BebanAktivitas + $NilaiTambah);
			//menghitung nilai aktivitas dengan membagi jml poin aktivitas dengan nilai jabatan
				$NilaiAktivitas = $JmlPoinAktivitas == 0 ? 0 : ($JmlPoinAktivitas / $NilaiJabatan);

				$TppDisiplin = Disiplin($ProsenKehadiran, $PokokTPP);
				$TppAktivitas = Aktivitas($JmlPoinAktivitas, $NilaiJabatan, $PokokTPP);
				$TppSKP = SKP($PersenSKP, $PokokTPP);
				$TppSakip = Sakip($NilaiSAKIP, $PokokTPP);

				$BrutoTPP = TPP($TppDisiplin, $TppAktivitas, $TppSKP, $TppSakip);
				$Pph21 = $row['PPH'] == 0 ? 0 : ($BrutoTPP * ($row['PPH'] / 100));
				$TppDiterima = $BrutoTPP - $Pph21;

				$row['ProsenKehadiran'] = $ProsenKehadiran;
				$row['TppDisiplin'] = $TppDisiplin;
				$row['NilaiAktivitas'] = $NilaiAktivitas;
				$row['TppAktivitas'] = $TppAktivitas;
				$row['NilaiSKP'] = $NilaiSKP;
				$row['TppSKP'] = $TppSKP;
				$row['NilaiSAKIP'] = $NilaiSAKIP;
				$row['TppSakip'] = $TppSakip;
				$row['BrutoTPP'] = $BrutoTPP;
				$row['Pph21'] = $Pph21;
				$row['TppDiterima'] = $TppDiterima;
				$row['response'] = 200;
				$rowArray = $row;
			}
		}
		return $rowArray;
	} else {
		$rowArray = array('response' => 500);
		return $rowArray;
	}
}

function HitungTppSatuTahun($conn, $KodePegawai, $Tahun){
	$output = '';
	for ($i=1; $i < 13; $i++) { 
		$row = TppBerdasarKinerjaTahun($conn, $KodePegawai, $i, $Tahun);
		$output[$i] = $row;
	}
	return $output;
}

function TppBerdasarKinerjaTahun($conn, $KodePegawai, $Bulan, $Tahun){
	$sql = "SELECT tb.Tahun, tb.Bulan, tb.KodePegawai, tb.PokokTPP, tb.JmlHariEfektif, tb.JmlSakit, tb.JmlCuti, tb.JmlAlpha, tb.JmlHadir, tb.JmlSenam, tb.JmlApel, tb.JmlTL1, tb.JmlTL2, tb.JmlTL3, tb.JmlPA1, tb.JmlPA2, tb.JmlPA3, tb.JmlLF1, tb.JmlLF2, p.KodePegawai, p.NIP, p.NamaPegawai, p.Pangkat, p.Golongan, p.Alamat, p.PokokTPP AS PokokTPPPegawai, p.KodeJabatan, p.KodeOPD, p.PPH, j.KodeManual, j.NilaiJabatan, j.NamaJabatan, j.TipeJabatan, j.KelasJabatan, j.AtasanLangsungKodeJab, j.AtasanLangsungOPD, j.IsKepala, (SELECT IFNULL(SUM(atv.DurasiKerjaACC),0) FROM aktivitaspegawai atv WHERE atv.KodePegawai = tb.KodePegawai AND MONTH(atv.Tanggal) = tb.Bulan AND YEAR(atv.Tanggal) = tb.Tahun AND atv.ACCAtasan = 'DISETUJUI') AS MenitAktivitas, s.PersenSKP, IFNULL(s.NilaiSKP, 0) AS NilaiSKP, c.EvalusasiSAKIP, c.NilaiKategoriSAKIP
	FROM tppbulanan tb
	LEFT JOIN mstpegawai p ON p.KodePegawai = tb.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeOPD = p.KodeOPD AND j.KodeJabatan = p.KodeJabatan
	LEFT JOIN skppegawai s ON s.KodePegawai = tb.KodePegawai AND s.Tahun = (tb.Tahun - 1)
	LEFT JOIN capaiankinerjaopd c ON c.KodeOPD = p.KodeOPD AND c.Tahun = tb.Tahun AND c.Triwulan = IF(3 >= tb.Bulan AND tb.Bulan >= 1, 1, IF(6 >= tb.Bulan AND tb.Bulan >= 4, 2, IF(9 >= tb.Bulan AND tb.Bulan >= 7, 3, IF(12 >= tb.Bulan and tb.Bulan >= 10, 4, TRUE))))
	WHERE tb.KodePegawai = '$KodePegawai' AND tb.Bulan = '$Bulan' AND tb.Tahun = '$Tahun'
	ORDER BY tb.Bulan, tb.Tahun";
	$res = $conn->query($sql);
	if ($res) {
		$rowArray = '';
		while ($row = $res->fetch_assoc()) {
            # code...
			$JmlHadir = $row['JmlHariEfektif'] - $row['JmlAlpha'];
			$PersenHadir = (($JmlHadir / $row['JmlHariEfektif']) * 100);
			$ProsenKehadiran = $PersenHadir - GetNilaiPelanggaran($conn, $row['KodeOPD'], $row['JmlSenam'], $row['JmlApel'], $row['JmlTL1'], $row['JmlTL2'], $row['JmlTL3'], $row['JmlPA1'], $row['JmlPA2'], $row['JmlPA3'], $row['JmlLF1'], $row['JmlLF2']);

			$NilaiJabatan = $row['NilaiJabatan'];
			$PokokTPP = $row['PokokTPPPegawai'];
			$PersenSKP = $row['PersenSKP'];
			$NilaiSKP = $row['NilaiSKP'];
			$NilaiSAKIP = $row['NilaiKategoriSAKIP'];

			$BebanAktivitas = $row['MenitAktivitas'];

			//jika pegawai mempunyai bawahan maka mendapat 1% dari beban tiap aktivitas bawahan
			$NilaiTambah = HitungNilaiAktivitasBawahan($conn, $row['KodeOPD'], $row['KodeJabatan'], $Bulan, $Tahun);
			//cek apakah beban pegawai melebihi nilai jabatan
			//jika beban melebihi nilai jabatan maka yang dipakai adalah nilai jabatan 
			$JmlPoinAktivitas = ($BebanAktivitas + $NilaiTambah) > $row['NilaiJabatan'] ? $row['NilaiJabatan'] : ($BebanAktivitas + $NilaiTambah);
			//menghitung nilai aktivitas dengan membagi jml poin aktivitas dengan nilai jabatan
			$NilaiAktivitas = $JmlPoinAktivitas == 0 ? 0 : ($JmlPoinAktivitas / $NilaiJabatan);

			$TppDisiplin = Disiplin($ProsenKehadiran, $PokokTPP);
			$TppAktivitas = Aktivitas($JmlPoinAktivitas, $NilaiJabatan, $PokokTPP);
			$TppSKP = SKP($PersenSKP, $PokokTPP);
			$TppSakip = Sakip($NilaiSAKIP, $PokokTPP);

			$BrutoTPP = TPP($TppDisiplin, $TppAktivitas, $TppSKP, $TppSakip);
			$Pph21 = $row['PPH'] == 0 ? 0 : ($BrutoTPP * ($row['PPH'] / 100));
			$TppDiterima = $BrutoTPP - $Pph21;

			$row['ProsenKehadiran'] = $ProsenKehadiran;
			$row['TppDisiplin'] = $TppDisiplin;
			$row['NilaiAktivitas'] = $NilaiAktivitas;
			$row['TppAktivitas'] = $TppAktivitas;
			$row['NilaiSKP'] = $NilaiSKP;
			$row['TppSKP'] = $TppSKP;
			$row['NilaiSAKIP'] = $NilaiSAKIP;
			$row['TppSakip'] = $TppSakip;
			$row['BrutoTPP'] = $BrutoTPP;
			$row['Pph21'] = $Pph21;
			$row['TppDiterima'] = $TppDiterima;
			$rowArray = $TppDiterima;
		}
		return $rowArray+0;
	}else{
		return 0;
	}
}

function HitungNilaiAktivitasBawahan($conn, $AtasanKodeOPD, $KodeJabatanAtasan, $Bulan, $Tahun){
	$NilaiTambahan = 0;
	$sql = "SELECT IFNULL(atv.DurasiKerjaACC, 0) AS DurasiKerjaACC
	FROM aktivitaspegawai atv
	LEFT JOIN mstpegawai p ON p.KodePegawai = atv.KodePegawai
	LEFT JOIN mstjabatan j ON j.KodeJabatan = p.KodeJabatan AND j.KodeOPD = p.KodeOPD
	WHERE j.AtasanLangsungKodeJab = '$KodeJabatanAtasan' AND j.AtasanLangsungOPD = '$AtasanKodeOPD' AND atv.ACCAtasan = 'DISETUJUI' AND MONTH(atv.Tanggal) = '$Bulan' AND YEAR(atv.Tanggal) = '$Tahun'
	GROUP BY atv.KodeAktivitas";
	$res = $conn->query($sql);
	if ($res) {
		while ($row = $res->fetch_assoc()) {
			$NilaiTambahan += (0.01 * $row['DurasiKerjaACC']);
		}
		return $NilaiTambahan;
	}else{
		return $NilaiTambahan;
	}
}